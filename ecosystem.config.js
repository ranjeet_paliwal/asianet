module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    // First application
    {
      name: "np",
      script: "app.js",
      env: {
        COMMON_VARIABLE: "true"
      },
      env_production: {
        NODE_ENV: "production"
      }
    },

    // Second application
    {
      name: "WEB",
      script: "web.js"
    }
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy: {
    production: {
      user: "anuj.kumar",
      host: "192.168.24.51",
      host: ["192.168.25.144", "192.168.25.165"],
      ref: "origin/master",
      repo: "git@bitbucket.org:times_internet/np.git",
      "pre-setup":
        "sudo yum install -y git; sudo yum install -y nodejs; sudo npm install yarn -g; sudo mkdir /opt/deployment; sudo chown -R anuj.kumar /opt/deployment",
      path: "/opt/deployment/production",
      "post-setup":
        "yarn install && yarn prod:build; pm2 start -i 0 --name np ./server/index.js -- -r ./server/environment.js",
      "post-deploy":
        "cp ./server/_env ./server/.env; yarn prod:build; pm2 reload np",
      "pre-deploy": "git fetch --all"
    },
    dev: {
      user: "deploy",
      host: "192.169.38.172",
      ref: "origin/master",
      repo: "git@bitbucket.org:times_internet/np.git",
      "pre-setup":
        "yum install -y git; yum install -y nodejs;npm install -g yarn",
      path: "/var/www/development",
      "pre-setup": "npm install yarn",
      "post-deploy": "yarn install && yarn prod:build && pm2 reload yarn",
      env: {
        NODE_ENV: "dev"
      }
    },
    psetup: {
      user: "anuj.kumar",
      host: "192.168.25.165",
      ref: "origin/master",
      repo: "git@bitbucket.org:times_internet/np.git",
      "pre-setup":
        "sudo yum install -y git; sudo yum install -y nodejs; sudo npm install yarn -g; sudo npm install newrelic -g; sudo mkdir /opt/deployment; sudo chown -R anuj.kumar /opt/deployment; sudo mkdir /var/log/node;  sudo chown -R anuj.kumar /var/log/node",
      path: "/opt/deployment/production",
      "post-setup":
        "cp ./server/_env ./server/.env; yarn install && yarn prod:build; pm2 start -i 0 --name np ./server/index.js -- -r ./server/environment.js",
      "post-deploy":
        "cp ./server/_env ./server/.env; yarn install && yarn prod:build; pm2 reload np",
      "pre-deploy": "git fetch --all"
    }
  }
};

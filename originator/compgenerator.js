// var readline = require('readline');
var path = require("path");
var fs = require("fs");

// var prompts = readline.createInterface(process.stdin, process.stdout);

String.prototype.capitalizeFirstLetter = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

const rootDirname = process.env.DIR || "src/widgets/";
const defaultFileName = "index.js";

// prompts.question("Type Component Name: ", function(userinput){
var componentName = process.argv[2] ? process.argv[2] : "default";
var captalizeFCharName = String(componentName).capitalizeFirstLetter();
var captalizeName = String(componentName).toUpperCase();

var basepath = rootDirname + componentName;
var componentpath = basepath + "/component/";
var actionpath = basepath + "/action/";
var reducerpath = basepath + "/reducer/";

const fileCreator = pathToCreate => {
  console.log(pathToCreate);
  pathToCreate.split(path.sep).reduce((currentPath, folder) => {
    currentPath += folder + path.sep;
    if (!fs.existsSync(currentPath)) {
      fs.mkdirSync(currentPath);
    }
    return currentPath;
  }, "");
};

let Structure = {
  component: `import React, { Component } from 'react'
	import { Link } from 'react-router'
	import PropTypes from 'prop-types'

	class ${captalizeFCharName} extends Component {
		constructor(props) {
			super(props);
		}

		render() {
			let {} = this.props;
		    return (
		      <div>               
		        

		      // write html jxs here...........


		      </div>
		    )
		}
	}					

	export default ${captalizeFCharName};
	`,

  action: `import fetch from 'utils/fetch/fetch';
	export const FETCH_${captalizeName}_REQUEST = 'FETCH_${captalizeName}_REQUEST';
	export const FETCH_${captalizeName}_SUCCESS = 'FETCH_${captalizeName}_SUCCESS';
	export const FETCH_${captalizeName}_FAILURE = 'FETCH_${captalizeName}_FAILURE';

	function fetch${captalizeName}_DataFailure(error) {
		return {
			type: FETCH_${captalizeName}_FAILURE,
			payload: error.message
		};
	}

	function fetch${captalizeName}_DataSuccess(data) {  
		return {
			type: FETCH_${captalizeName}_SUCCESS,
			payload: data
		};
	}

	function fetch${captalizeName}_Data(state, params, query) {							  
	}

	function shouldFetch${captalizeName}_Data(state,params, query) {
		return true;
	}

	export function fetch${captalizeName}_IfNeeded(params, query) {
		return (dispatch, getState) => {
			if (shouldFetch${captalizeName}_Data(getState(),params, query)) {
				return dispatch(fetch${captalizeName}_Data(getState(), params, query));
			}
		};
	}
	`,

  reducer: `import {
		FETCH_${captalizeName}_REQUEST,
		FETCH_${captalizeName}_SUCCESS,
		FETCH_${captalizeName}_FAILURE
	} from './../action/index';

	function ${captalizeName}(state = {},action) {
		switch (action.type) {
			case FETCH_${captalizeName}_REQUEST:return {};

			case FETCH_${captalizeName}_SUCCESS:return {};

			case FETCH_${captalizeName}_FAILURE:return {};

			default: return state;
		}
	}

	export default ${captalizeName};
	`,
};

try {
  fileCreator(basepath);
  fileCreator(componentpath); //Create component Directory
  fileCreator(actionpath); //Create action Directory
  fileCreator(reducerpath); //Create reducer Directory

  fs.writeFileSync(componentpath + defaultFileName, Structure.component, "UTF-8"); //(Filename, data, formate, callback)
  fs.writeFileSync(actionpath + defaultFileName, Structure.action, "UTF-8"); //(Filename, data, formate, callback)
  fs.writeFileSync(reducerpath + defaultFileName, Structure.reducer, "UTF-8"); //(Filename, data, formate, callback)

  console.log("Component created");
} catch (err) {
  if (err.code == "EEXIST") {
    console.log("Component already exist");
  } else {
    console.log(err);
  }
}
process.exit();
// })

import { FETCH_TOPLIST_REQUEST, FETCH_TOPLIST_SUCCESS, FETCH_TOPLIST_FAILURE } from "../../actions/home/home";

const initialState = {
  data: "",
};

function home(state = initialState, action) {
  switch (action.type) {
    case FETCH_TOPLIST_REQUEST:
      return {
        ...state,
      };
    case FETCH_TOPLIST_SUCCESS: {
      return {
        ...state,
        data: action.payload,
      };
    }

    case FETCH_TOPLIST_FAILURE:
      return {
        ...state,
      };

    default:
      return state;
  }
}

export default home;

const relatedVideoUrl1 =
  document &&
  document.getElementById("relatedVideos") &&
  document.getElementById("relatedVideos").attributes &&
  document.getElementById("relatedVideos").attributes.length > 0 &&
  document.getElementById("relatedVideos").attributes[1] &&
  document.getElementById("relatedVideos").attributes[1].value;

const relatedVideoUrl0 =
  document &&
  document.getElementById("relatedVideos") &&
  document.getElementById("relatedVideos").attributes &&
  document.getElementById("relatedVideos").attributes.length > 0 &&
  document.getElementById("relatedVideos").attributes[0] &&
  document.getElementById("relatedVideos").attributes[0].value;

const pagetype =
  document &&
  document.getElementById("pagetype") &&
  document.getElementById("pagetype").attributes &&
  document.getElementById("pagetype").attributes.length > 0 &&
  document.getElementById("pagetype").attributes[1] &&
  document.getElementById("pagetype").attributes[1].value;

const relatedVideoUrl = relatedVideoUrl1.includes("https://") ? relatedVideoUrl1 : relatedVideoUrl0;

if (relatedVideoUrl && relatedVideoUrl != "" && relatedVideoUrl != undefined) {
  fetch(relatedVideoUrl)
    .then(response => response.json())
    .then(data => {
      let relatedVideosData = data && data.items;
      let regionalVideoName = (data && data.secname) || "Popular Videos";
      let relatedVideosContent = "";
      relatedVideosData &&
        Array.isArray(relatedVideosData) &&
        relatedVideosData.forEach(element => {
          let imageSrc = element && element.imgsrc;
          if (pagetype && pagetype === "home") {
            imageSrc = `https://static.langimg.com/thumb/msid-${element.imageid},imgsize-65252,width-540,height-405,resizemode-75/navbharat-times.jpg`;
          }
          relatedVideosContent += `<li data-attr="as" title="${element &&
            element.hl}" itemtype="http://schema.org/ListItem" 
    class=" news-card vertical col video">
       <span class="table_col img_wrap" >
         <a href="${element && element.wu}"> 
           <amp-img alt="${element && element.hl}" src="${imageSrc}"
           layout= "responsive" width="540" height="300">
             <amp-img fallback layout="fill" src="https://static.langimg.com/thumb/msid-67144443,width-540,height-405,resizemode-4/xyz.jpg" width="540" height="405" alt="" />
           </amp-img>
         </a>
       </span>
       <span class="table_col con_wrap">
         <a href="${element && element.wu}">
           <span class="text_ellipsis">${element && element.hl}</span>
         </a>
       </span>
     </li>`;
        });

      if (relatedVideosContent != "") {
        let relatedvideos = `<div class="row ${pagetype && pagetype === "home" ? "box-item" : "wdt_popular_videos"}">
      <div class="section">
        <div class="top_section">
          <h3>
            <span>${regionalVideoName}</span>
          </h3>
        </div>
      </div>
      <div class="list-slider">
        <ul class="col12 view-horizontal">
          ${relatedVideosContent}
        </ul>
      </div>
    </div>`;

        //document.getElementsByClassName("row wdt_superhit").innerHTML = relatedvideos;
        document.body.innerHTML = relatedvideos;
      }
    });
}

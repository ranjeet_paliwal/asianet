const superHitWidgetURL1 =
  document &&
  document.getElementById("superHitWidget") &&
  document.getElementById("superHitWidget").attributes &&
  document.getElementById("superHitWidget").attributes.length > 0 &&
  document.getElementById("superHitWidget").attributes[1] &&
  document.getElementById("superHitWidget").attributes[1].value;

const superHitWidgetURL0 =
  document &&
  document.getElementById("superHitWidget") &&
  document.getElementById("superHitWidget").attributes &&
  document.getElementById("superHitWidget").attributes.length > 0 &&
  document.getElementById("superHitWidget").attributes[0] &&
  document.getElementById("superHitWidget").attributes[0].value;

const superHitWidgetURL = superHitWidgetURL1.includes("https://") ? superHitWidgetURL1 : superHitWidgetURL0;

if (superHitWidgetURL && superHitWidgetURL != "" && superHitWidgetURL != undefined) {
  fetch(superHitWidgetURL)
    .then(response => response.json())
    .then(data => {
      const superHitWidgetData = data && data.items;
      const regionalName = (data && data.secname) || "Superhit";
      let superHitWidgetContent = "";
      superHitWidgetData &&
        Array.isArray(superHitWidgetData) &&
        superHitWidgetData.forEach((element, index) => {
          // Handling href link convert "/articleshow/" --> "/amp_articleshow/"
          let hrefUrl = element && element.wu;
          hrefUrl =
            hrefUrl && hrefUrl.indexOf("/articleshow/")
              ? hrefUrl.replace("/articleshow/", "/amp_articleshow/")
              : hrefUrl;
          hrefUrl = `${hrefUrl}?utm_source=mostreadwidget&utm_medium=referral&utm_campaign=article${index + 1}`;

          superHitWidgetContent += `<li data-attr="as" title="${element &&
            element.hl}" itemtype="http://schema.org/ListItem" 
    class="news-card horizontal col news">
       <span class="table_col img_wrap" >
         <a href="${hrefUrl}"> 
           <amp-img alt= "${element && element.hl}" src="${element && element.imgsrc}"
           layout= "responsive" width="135" height="102">
             <amp-img fallback layout="fill" src="https://static.langimg.com/thumb/msid-67144443,width-135,height-102,resizemode-75/xyz.jpg" width="135" height="102" alt="" />
           </amp-img>
         </a>
       </span>
       <span class="table_col con_wrap">
         <a href="${hrefUrl}">
           <span class="text_ellipsis">${element && element.hl}</span>
         </a>
       </span>
     </li>`;
        });

      if (superHitWidgetContent != "") {
        const superHitWidget = `<div class="row wdt_superhit">
      <div class="section">
        <div class="top_section">
          <h3>
            <span>${regionalName}</span>
          </h3>
        </div>
      </div>
      <ul class="col12 rest-topics">
        ${superHitWidgetContent}
      </ul>
    </div>`;
        document.body.innerHTML = superHitWidget;
      }
    });
}

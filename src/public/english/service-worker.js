var izCacheVer = "1";
// importScripts("https://cdn.izooto.com/scripts/workers/8b6fb35825c21cb0989eff9eb3e02e24aa1dea4e.js");
importScripts("https://static.growthrx.in/js/v2/push-sw.js");

const DOMAIN_NAME = self.location.origin;
//  Trying fix for service worker nbt
const API_BASEPOINT = "https://navbharattimesfeeds.indiatimes.com/pwafeeds";
const API_BASEPOINT_TECH = "https://navbharattimes.indiatimes.com/pwafeeds";
const ARTICLE_API_CACHE = "articles-api-cache";
const PHOTO_API_CACHE = "photos-api-cache";
const VIDEO_API_CACHE = "videos-api-cache";
const HOME_HTML_CACHE = "homepage-html-cache";
const LangNetStorageBasePath = "https://langnetstorage.indiatimes.com/langappfeeds";

const SITE_ID = "696089404";
// const SITE_ID_GN = "66130905";
const SITE_ID_GN = "19615041"; //test id should be replace

if (workbox) {
  console.log(`Yay! Workbox is loaded 🎉 at`, DOMAIN_NAME);
  workbox.clientsClaim();
  workbox.precaching.suppressWarnings();

  /*   workbox.routing.registerRoute(
    "/",
    workbox.strategies.staleWhileRevalidate({
      cacheName: HOME_HTML_CACHE,
      plugins: [
        new workbox.expiration.Plugin({
          maxEntries: 1,
          maxAgeSeconds: 7 * 24 * 60 * 60, // 7 days
        }),
      ],
    }),
  ); */

   
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}

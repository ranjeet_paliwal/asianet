if (workbox) {
  console.log(`Yay! Workbox is loaded 🎉 at`, DOMAIN_NAME);
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}

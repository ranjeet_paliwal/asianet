import fetch from "utils/fetch/fetch";
import { getCountryCode, isInternationalUrl, _filterALJSON, isMobilePlatform } from "../../utils/util";

import { _getStaticConfig, _getCookie } from "../../utils/util";
import { internationalconfig } from "../../utils/internationalpageConfig";
const siteName = process.env.SITE;

// import { fetchLOKSABHAELECTIONRESULT_DataSuccess } from "../../campaign/election/loksabhaelectionresult/action";
// import { electionConfig } from "../../campaign/election/utils/config";

const siteConfig = _getStaticConfig();
// const _electionConfig = electionConfig[process.env.SITE];

export const FETCH_TOPLIST_REQUEST = "FETCH_TOPLIST_REQUEST";
export const FETCH_TOPLIST_SUCCESS = "FETCH_TOPLIST_SUCCESS";
export const FETCH_TOPLIST_FAILURE = "FETCH_TOPLIST_FAILURE";

function fetchTopListDataFailure(error) {
  return {
    type: FETCH_TOPLIST_FAILURE,
    payload: error.message,
  };
}

function fetchTopListDataSuccess(data) {
  return {
    type: FETCH_TOPLIST_SUCCESS,
    payload: data,
  };
}

function fetchTopListData(state, params, query, router, isFromRefreshFeed) {
  let apiUrl =
    "https://timesofindia.indiatimes.com/mobile_xml_feed_news_list.cms?msid=7137896&tag=articlelistroot&perpage=10&feedtype=sjson";
  if (process.env.SITE == "hindi") {
    apiUrl = "https://navbharattimesfeeds.indiatimes.com/pwafeeds/sc_articlelist/2354729.cms?feedtype=sjson";
  }

  return dispatch => {
    dispatch({
      type: FETCH_TOPLIST_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchTopListDataSuccess(data)),
        error => dispatch(fetchTopListDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchTopListDataFailure(error));
      });
  };
}

function shouldFetchTopListData(state, params, query, isFromRefreshFeed, region) {
  return true;
}

export function fetchTopListDataIfNeeded(params, query, router, isFromRefreshFeed, region) {
  return (dispatch, getState) => {
    if (shouldFetchTopListData(getState(), params, query, isFromRefreshFeed, region)) {
      return dispatch(fetchTopListData(getState(), params, query, router, isFromRefreshFeed));
    }
    return Promise.resolve([]);
  };
}

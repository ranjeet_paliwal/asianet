import React from "react";
import { Route, IndexRoute, Redirect } from "react-router";
import NotFound from "../../containers/notfound/NotFound";
import HomeLoadableD from "../../containers/desktop/home/HomeLoadable";
// import ArticleshowLoadableD from "../../containers/desktop/articleshow/ArticleshowLoadable";
import AppD from "../../containers/app/AppD";

export default (
  <Route path="/" component={AppD}>
    <IndexRoute component={HomeLoadableD} />
    <Route path="/articleshow/:msid" component={() => "Articleshow Page Desktop"} />
    <Route path="*" component={NotFound} />
  </Route>
);
export { NotFound as NotFoundComponent };

import React from "react";
import { Route, IndexRoute, Redirect } from "react-router";
import NotFound from "../../containers/notfound/NotFound";
import HomeLoadable from "../../containers/mobile/home/HomeLoadable";
// import ArticleshowLoadable from "../../containers/mobile/articleshow/ArticleshowLoadable";
import App from "../../containers/app/App";

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomeLoadable} />
    <Route path="/articleshow/:msid" component={() => "Articleshow Page Mobile "} />
    <Route path="*" component={NotFound} />
  </Route>
);
export { NotFound as NotFoundComponent };

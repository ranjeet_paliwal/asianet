/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/iframe-has-title */
/* eslint-disable react/button-has-type */
/* eslint-disable indent */
/* eslint-disable camelcase */
/* eslint-disable react/no-danger */
/*
This component is responsible for creating header section & analytics code injection
 of the FBIA markup. 
*/
import React from "react";
import PropTypes from "prop-types";
import serialize from "serialize-javascript";
import { _getStaticConfig, getPWAmetaByPagetype } from "../utils/util";
import fbiaConfig from "./FbIAConfig";

const siteConfig = _getStaticConfig();

function FbIA({ html, initialState, pagetype, location }) {
  const eventRefreshRate = location.query.eventRefreshRate ? location.query.eventRefreshRate : 8;

  let stateKey = pagetype;
  if (pagetype === "photoshow") {
    stateKey = "photomazzashow";
  }
  const pwa_meta = getPWAmetaByPagetype(initialState, pagetype);
  const pageData = initialState[stateKey];
  let item;
  let pubTime;
  let modTime;
  let title;
  let msid;
  const photoArr = [];
  let storyCount = 0;
  if (pagetype === "articleshow") {
    item = pageData && pageData.items && pageData.items[0];
    title = item.hl;
    pubTime = item.dlseo;
    modTime = item.luseo;
    msid = item.id;
    storyCount = item.substory ? item.substory.length : 0;
  } else if (pagetype === "photoshow") {
    item = pageData && pageData.value && pageData.value[0] && pageData.value[0][0];
    title = item && item.it && item.it.hl;
    pubTime = pwa_meta && (pwa_meta.dlseo || pwa_meta.publishtime);
    modTime = pwa_meta && (pwa_meta.luseo || pwa_meta.modified);
    msid = item.it.id;
    if (item.items) {
      item.items.map(photo => {
        photoArr.push({ title: photo.hl, url: getPsUrl(photo.wu) });
      });
    }
    storyCount = photoArr.length;
  } else if (pagetype === "videoshow") {
    item = pageData && pageData.value && pageData.value.items && pageData.value.items[0];
    title = item.hl;
    pubTime = item.dlseo;
    modTime = item.luseo;
    msid = item.id;
  }

  const app_tn = pwa_meta && pwa_meta.app_tn;

  const seolocation = pwa_meta && pwa_meta.canonical;
  // seolocation = seolocation && seolocation
  //   .replace(`/${msid}.cms`, "")
  //   .replace(/http:\/\/|https:\/\//gi, "");

  const byline = getByline(item, pwa_meta);
  const authorName =
    pwa_meta && typeof pwa_meta.editorname !== "undefined" ? pwa_meta.editorname.toLowerCase().replace(/ /g, "-") : "";
  const authorCDName = fbiaConfig.authorCDName[process.env.SITE];
  return (
    <html
      prefix="op: http://media.facebook.com/op#"
      lang={siteConfig.languagemeta ? `${siteConfig.languagemeta}` : "en"}
    >
      <head>
        <meta charSet="utf-8" />
        <title>{title}</title>
        <meta property="fb:use_automatic_ad_placement" content="enable=true" />
        <meta
          content={`placement_id=${fbiaConfig.fbia_recirculation_placement_id[process.env.SITE]}`}
          property="fb:op-recirculation-ads"
        />
        <link rel="canonical" href={pwa_meta && pwa_meta.canonical} />
        <meta content="v1.0" property="op:markup_version" />
      </head>
      <body>
        <article>
          <header>
            {byline && (
              <address>
                <a>{byline}</a>
              </address>
            )}
            {pubTime && (
              <time className="op-published" dateTime={pubTime}>
                {pubTime}
              </time>
            )}
            {modTime && (
              <time className="op-modified" dateTime={modTime}>
                {modTime}
              </time>
            )}
            {getFacebookAdIframe()}
          </header>
          <script
            id="initialState"
            dangerouslySetInnerHTML={{
              __html: `window.__INITIAL_STATE__ = ${serialize(initialState)}`,
            }}
          />
          {getAnalyticsIframe({ pagetype, storyCount, title, photoArr, eventRefreshRate, authorName, authorCDName })}
          {getComscoreIframe(pagetype, storyCount, title, photoArr, pwa_meta.canonical, eventRefreshRate)}

          <figure className="op-tracker">
            <iframe hidden>
              <script
                dangerouslySetInnerHTML={{
                  __html: `
               (function() { var vl = (document.location.protocol == "https:" ? "https://" : "http://") + "ase.clmbtech.com/message?cid=${siteConfig.ads.dmpAmpCode}&val_101=${siteConfig.ads.dmpAmpCode}&val_101=int:${seolocation}&val_120=10&val_102=${siteConfig.webdomain}"; (new Image()).src = vl; })();
            `,
                }}
              ></script>
            </iframe>
          </figure>
          <figure className="op-tracker">
            <iframe>
              <script
                dangerouslySetInnerHTML={{
                  __html: `(function(){ var script = document.createElement("script"), elm = document.getElementsByTagName("script")[0]; script.async = true; script.src = 'https://secure-gl.imrworldwide.com/cgi-bin/m?ci=ent1236951067&am=3&mr=1&ty=js&ep=1&at=view&rt=banner&st=image&ca=cmp935695&cr=crv2601637&pc=plc43886890_fbia&r='; elm.parentNode.insertBefore(script, elm); })();`,
                }}
              ></script>
              {/* Commenting noscript comscore img loading , as throwing error of small size in IA
              <noscript>
                <img src="https://secure-gl.imrworldwide.com/cgi-bin/m?ci=ent1236951067&amp;am=3&amp;ep=1&amp;at=view&amp;rt=banner&amp;st=image&amp;ca=cmp935695&amp;cr=crv2601637&amp;pc=plc43886890&amp;r=" />
              </noscript> */}
            </iframe>
          </figure>

          <figure className="op-tracker">
            <iframe hidden>
              <script
                dangerouslySetInnerHTML={{
                  __html: `
                      // growthrx.in snippet
                      (function (g, r, o, w, t, h, rx) {
                          g[t] = g[t] || function () {(g[t].q = g[t].q || []).push(arguments)
                          }, g[t].l = 1 * new Date();
                          g[t] = g[t] || {}, h = r.createElement(o), rx = r.getElementsByTagName(o)[0];
                          h.async = 1;h.src = w;rx.parentNode.insertBefore(h, rx)
                      })(window, document, 'script', 'https://static.growthrx.in/js/v2/web-sdk.js', 'grx');
                      grx("init", '${siteConfig.growthrx.prod.id}');
                      grx('track', 'page_view', {url: location.pathname, source: "IA"});
                  `,
                }}
              />
            </iframe>
          </figure>

          <div
            id="root"
            className="overflowHidden"
            dangerouslySetInnerHTML={{
              __html: html,
            }}
          />
        </article>
      </body>
    </html>
  );
}

function getByline(item, pwa_meta) {
  let byline = "";
  if (item && item.bl && item.aufullname && item.auid && item.bl !== "" && item.aufullname !== "" && item.auid !== "") {
    byline = item.bl;
  } else if (
    pwa_meta &&
    pwa_meta.editorname &&
    pwa_meta.editornameseo &&
    pwa_meta.editornameseo !== "" &&
    pwa_meta.editornameseo !== "No-Author"
  ) {
    byline = pwa_meta.editorname.replace(/-/g, " ");
  }
  if (item && item.ag && item.ag !== "" && !item.bl) {
    byline = byline ? `${byline} | ${item.ag}` : `${item.ag}`;
  }
  return byline;
}

function getFacebookAdIframe() {
  return (
    <figure className="op-ad">
      <iframe
        src={`https://www.facebook.com/adnw_request?placement=${
          fbiaConfig.fbia_ad_placement_id[process.env.SITE]
        }&adtype=banner300x250`}
        style={{ border: 0, margin: 0 }}
        height="250"
        width="300"
      />
    </figure>
  );
}

function getAnalyticsIframe({ pagetype, storyCount, title, photoArr, eventRefreshRate, authorName, authorCDName }) {
  return (
    <figure className="op-tracker">
      <iframe>
        <script
          dangerouslySetInnerHTML={{
            __html: `var pagetitle = ia_document.title; // Referrer is always set to 'ia.facebook.com' 
              var referrer = ia_document.referrer; 
              var urlParams = new URL(ia_document.shareURL).searchParams; 
              var source = urlParams.get('utm_source'); 
              var medium = urlParams.get('utm_medium');
              if(!source) { source = 'Facebook'; } 
              if(!medium) { medium = 'Instant Article'; } 
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); 
              ga ( 'create' , "${siteConfig.ga.gatrackid}" , 'auto' ); 
              ga ( 'require' , 'displayfeatures' ); 
              ga('set', 'campaignSource', source); 
              ga('set', 'campaignMedium', medium); 
              ga('set', 'campaignName', 'IA'); 
              ga('set', 'referrer', referrer); 
              ga('set', "${authorCDName}", "${authorName}"); 
              ga ( 'send' , 'pageview', {title : "${title}"} ); 
              let count = 0;
              let storyCount = ${storyCount || null};
              let breakloop = 1;
              let photoArr = JSON.parse('${JSON.stringify(photoArr)}');
              while(breakloop <= 60){
                if(storyCount)count++;
                (function(storyNumber,timer){
                  setTimeout(
                    ${getTimeoutFunction(pagetype, title, "ga")}, timer * 1000 * ${eventRefreshRate});
                })(count,breakloop);
                breakloop++;
                if(storyCount && count >= storyCount){
                  count = 0;
                };
              }`,
          }}
        ></script>
      </iframe>
    </figure>
  );
}

function getTimeoutFunction(pagetype, title, analyticsType) {
  if (pagetype === "photoshow") {
    if (analyticsType === "ga") {
      return `function(){
        ga('send', 'pageview',photoArr[storyNumber].url,{title : photoArr[storyNumber].title});
        grx('track', 'page_view', {url: photoArr[storyNumber].url , source: "IA"});
      }`;
    }
    if (analyticsType === "comscore") {
      return `function(){
          var storyAppendStr = '&c4=' + photoArr[storyNumber].url;
          var img = document.createElement("img");
          img.src = "https://sb.scorecardresearch.com/p?c1=${siteConfig.comscore.c1}&c2=${siteConfig.comscore.c2}&cv=3.1&cj=1&comscorekw=fbia"+storyAppendStr;
          var el = document.getElementsByTagName("body")[0];
          el.parentNode.insertBefore(img, el);
        }`;
    }
  } else {
    if (analyticsType === "ga") {
      return `function(){
        ga('send','pageview',location.pathname+(storyNumber==0 ? "" : ("?story=" + storyNumber)),{title : "${title}"});
        grx('track', 'page_view', {url: location.pathname+(storyNumber==0 ? "" : ("?story=" + storyNumber)) , source: "IA"});
      }`;
    }
    if (analyticsType === "comscore") {
      return `function(){
          var storyAppendStr = (storyNumber==0 ? '' : ('&story=' + storyNumber));
          var img = document.createElement("img");
          img.src = "https://sb.scorecardresearch.com/p?c1=${siteConfig.comscore.c1}&c2=${siteConfig.comscore.c2}&cv=3.1&cj=1&comscorekw=fbia"+storyAppendStr;
          var el = document.getElementsByTagName("body")[0];
          el.parentNode.insertBefore(img, el);
        }`;
    }
  }
}

function getComscoreIframe(pagetype, storyCount, title, photoArr, url, eventRefreshRate) {
  return (
    <figure className="op-tracker">
      <iframe>
        <script
          dangerouslySetInnerHTML={{
            __html: `
                var _comscore = _comscore || []; _comscore.push(${JSON.stringify(
                  Object.assign(siteConfig.comscore, { c4: url }, { options: { url_append: "comscorekw=fbia" } }),
                )}); 
                (function() { var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true; s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js"; el.parentNode.insertBefore(s, el); })();
                /*
                count = 0;
                storyCount = ${storyCount || null};
                breakloop = 1;
                photoArr = JSON.parse('${JSON.stringify(photoArr)}');
                while(breakloop <= 60){
                  if(storyCount)count++;
                  (function(storyNumber,timer){
                    setTimeout(
                      ${getTimeoutFunction(pagetype, title, "comscore")}, timer * 1000 * ${eventRefreshRate});
                  })(count,breakloop);
                  breakloop++;
                  if(storyCount && count >= storyCount){
                    count = 0;
                  };
                }*/
              `,
          }}
        ></script>
      </iframe>
    </figure>
  );
}

const getPsUrl = url => url.replace(siteConfig.weburl, "");

FbIA.propTypes = {
  js: PropTypes.array.isRequired,
  css: PropTypes.array.isRequired,
  html: PropTypes.string,
  head: PropTypes.object.isRequired,
  initialState: PropTypes.object.isRequired,
};

export default FbIA;

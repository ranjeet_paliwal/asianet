/* eslint-disable no-underscore-dangle */
/* eslint-disable camelcase */
/* eslint-disable indent */
/* eslint-disable eqeqeq */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
/* eslint-disable func-names */
import "babel-polyfill";
import cheerio from "cheerio";
import { _getStaticConfig, getPWAmetaByPagetype } from "../utils/util";
import fetch from "../utils/fetch/fetch";
import fbiaConfig from "./FbIAConfig";
const siteConfig = _getStaticConfig();
const addedArticles = [];
let _msid;

function fbIAConverter({ html, pagetype, rhs, renderProps }) {
  if (!html) return "";

  // check if we need to render dfp ads or not basis of ?shouldRenderDFP=true or false
  const shouldRenderDFP =
    renderProps && renderProps.location && renderProps.location.query
      ? renderProps.location.query.shouldRenderDFP
      : "true";

  const $ = cheerio.load(html);

  let initialState = $("#initialState")
    .html()
    .split("window.__INITIAL_STATE__ = ")[1];
  initialState = JSON.parse(initialState);
  let stateKey = pagetype;
  if (pagetype === "photoshow") {
    stateKey = "photomazzashow";
  }
  const pwa_meta = getPWAmetaByPagetype(initialState, pagetype);
  const pageData = initialState[stateKey];
  const seolocation = pwa_meta && pwa_meta.canonical;
  const isTechPage = !!(seolocation && seolocation.indexOf("/tech/") > -1);

  let isPhotoFeature = false; // check for photoFeature articles

  let item;
  if (pagetype === "articleshow") {
    item = pageData && pageData.items && pageData.items[0];
    _msid = item.id;
  } else if (pagetype === "photoshow") {
    item = pageData && pageData.value && pageData.value[0];
    _msid = item[0].it.id;
  } else if (pagetype === "videoshow") {
    item = pageData && pageData.value && pageData.value.items && pageData.value.items[0];
    _msid = item.id;
  }

  // check whether article is news or nonnews
  const isNews = !(pwa_meta && pwa_meta.sectiontype);
  // For top related articles
  let relatedList = [];
  // For bottom related articles
  let relatedSlider = [];

  if (rhs && rhs.items && rhs.items.length) {
    if (isNews) {
      relatedSlider = getRelatedData(rhs.items, "news", 3);
    } else {
      relatedSlider = getRelatedData(rhs.items, "nonnews", 3);
    }
    if (rhs.apnabazaar && rhs.apnabazaar[0]) {
      relatedList = getRelatedData(rhs.apnabazaar, "apnabazar", 1);
    }
    relatedList = [...relatedList, ...getRelatedData(rhs.items, "adv", relatedList.length ? 2 : 3)];
  }

  const read_more_title = fbiaConfig.read_more_title[process.env.SITE];
  const related_title = fbiaConfig.related_title[process.env.SITE];

  //   remove all style and script
  $('style , script, [type="esi"],ad,amazonwidget').each(function(i, elem) {
    if ($(elem).attr("data-type") != "ampElement") {
      $(this).replaceWith("");
    }
  });
  $(".start-quote, .end-quote,svg").remove();
  $("a.ads-center").remove();
  // Handline Twitter Widget
  if ($(".twitter_widget")) {
    $(".twitter_widget").each((i, elem) => {
      $(elem).replaceWith(twitter(elem, $));
    });
  }

  // wrap all image tags with figure tag
  $("img").each(function(i, elem) {
    $(this).replaceWith(
      ampImgConverter(
        {
          alt: elem.attribs.alt || elem.attribs.title,
          src: elem.attribs.src,
          datasrc: elem.attribs["data-src"],
          class: elem.attribs.class,
          id: elem.attribs.id,
        },
        $(elem).html(),
        pagetype,
      ),
    );
  });

  // Append the main image of the article to the top of hte header of the markup
  if ($(".img_wrap.leadImage img").length > 0) {
    $("header").prepend(`<figure>${$(".img_wrap.leadImage img")}</figure>`);
  }
  $(".img_wrap.leadImage").remove();
  $("h1").removeAttr("itemprop");

  // Append the h1 tag to the header of the markup
  $("header").append($("h1"));

  // Append the short description of the article to the header of the markup
  if ($(".news_card h2").text()) {
    $("header").append(`<h2>${$(".news_card h2").text()}</h2>`);
  }

  $(".news_card div").each((i, e) => {
    if (e.attribs && e.attribs["data-attr-slk"]) {
      $("body > article").append(videoSource(e.attribs["data-attr-slk"]));
    }
  });

  // Start parsing the body of the article
  if (pagetype === "articleshow") {
    const sections = $("section");
    // check if article is a PF by checking if it has sections
    if (sections && sections.length > 0) {
      // Parse body for PF articleshow
      const lengthCheck = 0;
      $("article.story-content").each((i, e) => {
        if (e.childNodes && e.childNodes.length) {
          // loop through childnodes to get the main description of the PF and exit once it is found
          for (i = 0; i < e.childNodes.length; i++) {
            if (e.childNodes[i] && e.childNodes[i].type === "section") {
              break;
            } else if (e.childNodes[i] && e.childNodes[i].type === "text") {
              $("body > article").append(`<p>${e.childNodes[i].data}</p>`);
            }
          }
        }
      });
      // Loop through each section
      $("section").each((index, elem) => {
        const children = elem.childNodes;
        let bodyHtml = "";
        isPhotoFeature = true;
        if (index == 1) {
          bodyHtml += getAppBannerMarkup({ type: "photoFeature", msid: _msid });
        }
        // Loop through each child of section and get title, image and description
        for (let i = children.length - 1; i >= 0; i--) {
          bodyHtml = $(traverseList(children[i], $)) + bodyHtml;
        }
        $("body > article").append(bodyHtml);
        // lengthCheck += $(bodyHtml).text().length;
        // Check if length of text added so far is greater than 220 chars(as strictly asked by FB for distance between ads )
        // if (lengthCheck > 220) {
        //   lengthCheck = 0;
        //   // Append ad iframe after 220 chars
        //   $("body > article").append(
        //     `<figure class="op-ad"><iframe src="https://www.facebook.com/adnw_request?placement=${
        //       fbiaConfig.fbia_ad_placement_id[process.env.SITE]
        //     }&amp;adtype=banner300x250" height="250" width="300" ></iframe></figure>`,
        //   );
        // }
      });
    } else {
      // Parse body for normal articleshow
      // let lengthCheck = 0;
      $(".story-article article").each((i, elem) => {
        const children = elem.childNodes;
        let bodyHtml = "";
        // Loop through each child and get title, image and description, and other embeds
        // We have revered the loop becuase there was the DOM issue we were facing.
        for (let i = children.length - 1; i >= 0; i--) {
          bodyHtml = $(traverseList(children[i], $)) + bodyHtml;
          // for inserting ads
          // lengthCheck += $(traverseList(children[i], $)).text().length;

          // if (lengthCheck > 220) {
          //   lengthCheck = 0;
          //   bodyHtml =
          //     '<figure class="op-ad"><iframe src="https://www.facebook.com/adnw_request?placement=592217817647550_592225644313434&amp;adtype=banner300x250" height="250" width="300" ></iframe></figure>' +
          //     bodyHtml;
          // }
        }
        $("body > article").append(bodyHtml);
      });
    }

    // hack for handling breaking topic link issue. It removes some extra p tags that gets created
    for (let i = 0; i <= 2; i++) {
      $(".bottom").each((k, elem) => {
        if (
          $(elem)
            .next()
            .hasClass("1")
        ) {
          $(elem).remove();
        }
      });
    }

    $(".done.2").each((i, elem) => {
      if (
        $(elem)
          .children("a")
          .hasClass("remove")
      ) {
        $(elem).remove();
      }
      if (
        ($(elem)
          .next()
          .hasClass("done") ||
          $(elem)
            .next()
            .hasClass("bottom")) &&
        !$(elem)
          .next()
          .hasClass("1")
      ) {
      } else {
        $(elem).remove();
      }
    });
  } else if (pagetype === "photoshow") {
    getPhotoshowMarkup(html, $, item[0].it.wu);
  } else if (pagetype === "videoshow") {
    getVideoShowMarkup(item, $);
  }
  // End parsing the body of the article

  $("a").each((i, elem) => {
    $(elem).removeAttr("istrending");
  });

  // hack for handling breaking topic link issue. It removes some extra p tags that gets created
  $(".done").each((k, elem) => {
    if (
      !$(elem)
        .next()
        .hasClass("done")
    ) {
      $(elem)
        .next()
        .remove();
    }
  });

  // Remove all the other tags from page
  $("#root,.ad1").remove();

  let read_more_html = "";
  let dfp_html = "";
  // For top related articles
  if (relatedList.length > 0) {
    read_more_html = getRelatedListMarkup(relatedList, related_title);
  }
  // Get DFP ad markup
  dfp_html = getDFPAdsMarkup(shouldRenderDFP);
  let isDFPAdded = false;
  let isTopListAdded = false;
  let lengthCheck = 0;
  if ($("body > article") && $("body > article").children().length > 0) {
    if (read_more_html || dfp_html) {
      $("body > article")
        .children()
        .each((i, elem) => {
          if (i > 5) {
            let newhtml = "";
            lengthCheck += $(elem).text().length;
            // Append Ad and Related articles markup after 220 chars to avoid FB warning
            if (lengthCheck > 250 && !isDFPAdded) {
              isDFPAdded = true;
              newhtml = dfp_html;
              if (pagetype === "articleshow" && !isPhotoFeature) {
                newhtml += getAppBannerMarkup({ msid: _msid });
              }
              // $(newhtml).insertAfter($(elem));
            }
            if (lengthCheck > 500) {
              isTopListAdded = true;
              newhtml += read_more_html;
            }
            if (newhtml) {
              $(newhtml).insertAfter($(elem));
            }
            if (isTopListAdded && isDFPAdded) {
              return false;
            }
          }
        });
    }
  }
  // In case article doesn't have 220 chars then simply append ad and Related articles markup at the end
  // commented becuase FB is throwing error
  // if (isRelatedAdded === 0) {
  //   $("body > article").append(dfp_html + read_more_html);
  // }
  // For Amazon widget
  let amazonWidgetHtml = "";
  if (isTechPage) {
    amazonWidgetHtml = getAmazonWidgetMarkup();
  }
  // For bottom related articles
  if (relatedSlider.length > 0) {
    read_more_html = getRelatedListMarkup(relatedSlider, read_more_title);
    $("body > article").append(read_more_html + amazonWidgetHtml);
  } else {
    $("body > article").append(amazonWidgetHtml);
  }

  // replace h3 and h4 articles with h2 tag as h3 h4 are not supported by FB
  $("h3,h4").each((_, elem) => {
    $(elem).replaceWith(`<h2>${$(elem).html()}</h2>`);
  });

  // Remove empty tags
  $("li,p,ul,cite,h2").each((_, elem) => {
    if (
      $(elem)
        .text()
        .trim() === ""
    ) {
      $(elem).remove();
    }
  });
  return $.html();
}

// Recursive function to handle paragraphs where multiple topic link are there.
function getHTML(list, $) {
  if (list && list.prev && (list.prev.type === "text" || list.prev.name === "a")) {
    $(list)
      .prev()
      .addClass("remove");
    return getHTML(list.prev, $) + $(list);
  }
  // Br tag suggests that paragraph has ended and so we return from here breaking the recusrion chain.
  if (list && list.name === "br") {
    $(list.prev).remove();
    return "";
  }
  return $(list);
}

// Function to parse and return new markup as required by facebook
function traverseList(list, $) {
  if (list && list.type === "tag") {
    // incase of IA supported tags, return the original tags
    if (["ul", "ol"].indexOf(list.name) > -1) {
      return getListHtml(list, $);
    }
    if (["p", "h2", "figure", "em"].indexOf(list.name) > -1) {
      return list;
    }
    // Handle a tags that are in between paragraphs
    if (
      ["a"].indexOf(list.name) > -1 &&
      ((list.attribs && list.attribs.class && list.attribs.class.indexOf("topic") > -1) ||
        (list.attribs && list.attribs.href && list.attribs.href.indexOf("topics") > -1))
    ) {
      if (list.next && list.next.type === "text" && list.prev && list.prev.type === "text") {
        if (list.attribs && list.attribs.class && list.attribs.class.indexOf("remove") > -1) {
          $(list).remove();
          return "";
        }
        let html = getHTML(list, $);
        html = `<p class="done 1">${html}${$(list.next)}</p>`;
        html = html.replace(/remove/gi, "");
        return html;
      }
      if (list.next && list.next.type === "text") {
        const html = `<p class="done 2">${$(list)}${$(list.next)}</p>`;
        $(list).remove();

        return html;
      }
      if (list.prev && list.prev.type === "text") {
        const html = `<p class="done 3">${$(list.prev)}${$(list)}</p>`;
        $(list.prev).remove();
        $(list).remove();

        return html;
      }
      return `<p>${$(list)}</p>`;
    }
    if (["a"].indexOf(list.name) > -1) {
      return `<p>${$(list)}</p>`;
    }
    if (["strong"].indexOf(list.name) > -1) {
      // if (
      //   list.childNodes &&
      //   list.childNodes[0] &&
      //   list.childNodes[0].attribs &&
      //   list.childNodes[0].attribs["data-attr-slk"]
      // ) {
      //   return videoSource(list.childNodes[0].attribs["data-attr-slk"]);
      // }
      let html = "";
      list.childNodes.forEach(elem => {
        if (elem.type === "text") {
          html += `<h2>${$(elem)}</h2>`;
        } else {
          html += $(traverseList(elem, $));
        }
      });
      return html;
    }
    // For FB embed
    if (list.name === "div" && list.attribs && list.attribs.class && list.attribs.class.indexOf("fb-post") > -1) {
      return `<figure class="op-interactive"><iframe><script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>  
      <div class="fb-post" 
          data-href="${decodeURIComponent(list.attribs["data-href"])}"
          data-width="500"></div></iframe></figure>`;
    }
    // For Video embed
    if (list.name === "div" && list.attribs && list.attribs["data-videourl"]) {
      if (
        list.childNodes &&
        list.childNodes[0] &&
        list.childNodes[0].attribs &&
        list.childNodes[0].attribs["data-attr-slk"]
      )
        return videoSource(list.childNodes[0].attribs["data-attr-slk"]);
    }
    if (
      list.name === "div" &&
      list.attribs &&
      list.attribs.class &&
      list.attribs.class.indexOf("tableContainer") > -1
    ) {
      return `<figure class="op-interactive"><iframe width="600" height="450" />
      ${$(list)}</iframe></figure>`;
    }
    if (list.name === "div" && list.attribs && list.attribs.class && list.attribs.class.indexOf("img_wrap") > -1) {
      return list.childNodes[0];
    }
    if (list.name === "div" && list.attribs && list.attribs["data-attr-slk"]) {
      return videoSource(list.attribs["data-attr-slk"]);
    }
    // For handling bullet and highlight content
    if (
      list.attribs &&
      list.attribs.class &&
      (list.attribs.class.indexOf("bulletContent") > -1 || list.attribs.class.indexOf("tophighlight") > -1)
    ) {
      return $(list.childNodes);
    }
    // Handling quotation
    if (list.attribs && list.attribs.class && list.attribs.class.indexOf("quote") > -1) {
      return `<blockquote>${$(list.childNodes[0]).text()} - ${$(list.childNodes[1]).text()}</blockquote>`;
    }
    if (list.attribs && list.attribs["data-slideshow"]) {
      // As of now slideshow if not available in SSR so returning blank
      return ``;
    }
    if (list.name === "div") {
      // if inside div there is a figure tag then return the figure tag
      // if (list.childNodes && list.childNodes[0] && list.childNodes[0].name === "figure") {
      //   return list.childNodes[0];
      // }
      let html = "";
      list.childNodes.forEach(elem => {
        // if (elem.type === "text") {
        //   html += `<h2>${$(elem)}</h2>`;
        // } else {
        html += $(traverseList(elem, $));
        // }
      });
      return html;
      // console.log(list);
      // if (list.childNodes && list.childNodes[0] && list.childNodes[0].name === "twitter_widget") {
      //   return list.childNodes[0];
      // }
      // if (
      //   list.childNodes &&
      //   list.childNodes[0] &&
      //   list.childNodes[0].attribs &&
      //   list.childNodes[0].attribs.class &&
      //   list.childNodes[0].attribs.class.indexOf("twitter_widget") > -1
      // ) {
      //   return list.childNodes[0];
      // }
    }
    if (list.attribs && list.attribs.class && list.attribs.class.indexOf("inlineshare") > -1) {
      return `<p>${$(list.childNodes).text()}</p>`;
    }
    if (list.name === "instagram") {
      // /Instagram embed
      return `<figure class="op-interactive"><iframe>${$(list).children(
        "blockquote",
      )}<script async="" src="//www.instagram.com/embed.js"></script></iframe></figure>`;
    }
    if (list.name === "iframe") {
      // Iframe embeds
      let height = 315;
      if (list.attribs && list.attribs.height) {
        height = parseInt(list.attribs.height) + 20;
      }
      return `<figure class="op-interactive"><iframe width="560" height="${height}" src="${list.attribs.src}"></iframe></figure>`;
    }
  } else if (list.type === "text" && list.data !== "\n" && list.data.trim() !== "") {
    if (list.prev && list.prev.attribs && list.prev.attribs.class && list.prev.attribs.class.indexOf("done") > -1) {
      return ``;
    }
    if (list.next && list.next.attribs && list.next.attribs.class && list.next.attribs.class.indexOf("done") > -1) {
      return ``;
    }
    return `<p class="bottom">${$(list)}</p>`;
  }
}

function getListHtml(list, $) {
  const children = list.childNodes;
  let bodyHtml = `<${list.name}>`;
  // Loop through each child of section and get title, image and description
  for (let i = 0; i < children.length; i++) {
    const subChild = children[i].childNodes;
    bodyHtml += "<li>";
    for (let j = 0; j < subChild.length; j++) {
      if (subChild[j].name === "ul") {
        const subsubChild = subChild[j].childNodes;
        for (let k = 0; k < subsubChild.length; k++) {
          bodyHtml += `- ${$(subsubChild[k]).text()}<br>`;
        }
      } else {
        bodyHtml += `${$(subChild[j])}`;
      }
    }
    bodyHtml += "</li>";
  }
  bodyHtml += `</${list.name}>`;
  return bodyHtml;
}

// Get video url
function videoSource(slike) {
  const domain =
    siteConfig.channelCode == "mt"
      ? "https://navbharattimes.indiatimes.com/off-url-forward"
      : "https://maharashtratimes.com/off-url-forward";

  return `<figure class="op-interactive"><iframe width="600" height="450" src="${domain}/videoplayer.cms?wapCode=${siteConfig.wapCode}&msid=${slike}&label=fb_ia_videoshow"/></iframe></figure>`;
}

function ampImgConverter(attributes, html, pagetype) {
  attributes.src = attributes.datasrc != undefined && attributes.datasrc != "" ? attributes.datasrc : attributes.src;

  attributes.class = attributes.class != undefined ? attributes.class : "amp-image";

  attributes.width = attributes.src.indexOf("width") > -1 ? attributes.src.split("width-")[1].split(",")[0] : "540";
  attributes.height = attributes.src.indexOf("height") > -1 ? attributes.src.split("height-")[1].split(",")[0] : "405";

  return `<figure><img
			alt= "${attributes.alt}" 
			src= "${attributes.src}"
        />
      	</figure>`;
}

function twitter(obj, $) {
  const twitterId = obj.attribs.id ? obj.attribs.id.split("container_")[1] : null;
  return `<figure class="op-interactive"><iframe>
  ${$(obj)}
  <script>window.twttr = (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
      t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);
  
    t._e = [];
    t.ready = function(f) {
      t._e.push(f);
    };
  
    return t;
  }(document, "script", "twitter-wjs"));</script>
  <script>
  const twitterId = "${twitterId}";
      let eleContainer = document.getElementById("container_" + twitterId);
      setTimeout(function() {
        if (
        typeof twttr != "undefined" &&
        eleContainer
      ) {
        twttr.widgets.createTweet(twitterId, eleContainer);
      }
    }, 1000);
      </script>
    </iframe>
    </figure>`;
}

function shuffleArray(arr) {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * i);
    const temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
  }
  return arr;
}

const getRHS = async () => {
  try {
    const response = await fetch(
      `http://langnetstorage.indiatimes.com/langappfeeds/${siteConfig.channelCode}headline.htm`,
      {},
      true,
    );
    return response;
  } catch (e) {
    console.log("error", e);
  }
};

const getPhotoshowMarkup = (html, $, url) => {
  $("ul.photo_story li").each((index, elem) => {
    if (elem.attribs.class && elem.attribs.class.indexOf("photo_card") > -1) {
      const children = elem.childNodes;
      let bodyHtml = "";
      bodyHtml += "<figure>";

      for (let i = 0; i < children.length; i++) {
        if (
          children[i] &&
          children[i].attribs &&
          children[i].attribs.class &&
          children[i].attribs.class.indexOf("photo_des") > -1
        ) {
          bodyHtml += `<figcaption class="op-extra-large">`;
          bodyHtml += `<h2>${$(children[i])
            .children(".photo_con")
            .children("h2")
            .text()}</h2>`;
          bodyHtml += `<cite>`;
          bodyHtml += $(children[i])
            .children(".photo_con")
            .children(".table")
            .text();
          bodyHtml += `</cite>`;
          bodyHtml += `</figcaption>`;
        }
        if (children[i].attribs.class && children[i].attribs.class.indexOf("place_holder") > -1) {
          bodyHtml += $(children[i])
            .children("figure")
            .children("img");
        }
      }
      bodyHtml += "</figure>";

      if (index == 1) {
        bodyHtml += getAppBannerMarkup({ type: "photo", msid: _msid });
      }

      $("body > article").append(bodyHtml);
    }
  });
  // Append more button to see full photoshow
  if ($(".more-btn")) {
    const fullShow = `<h2>${$(".more-btn")}</h2>`;
    $("body > article").append(fullShow);
  }
};

const getVideoShowMarkup = (item, $) => {
  let bodyHtml = "";
  if ($("#parent_video_show_container")) {
    bodyHtml += videoSource($("#parent_video_show_container").attr("data-attr-slk"));
  } else if (item && item.pu) {
    bodyHtml = `<figure>
      <video>
        <source src="${item.pu}" type="video/mp4" />  
      </video>
    </figure>`;
  }
  if (item && item.hl && item.Story) {
    bodyHtml += `<h1>${item.hl}</h1>
        <p>${item.Story}</p>`;
  }
  bodyHtml += getAppBannerMarkup({ type: "video", msid: _msid });
  $("body > article").append(bodyHtml);
};

// Function to create DFP ad markup : POC running from iframe
const getDFPAdsMarkup = shouldRenderDFP =>
  /* `
    <figure class="op-interactive">
      <iframe width="400" height="250" src="https://navbharattimes.indiatimes.com/ia_dfp.cms?channelcode=${siteConfig.channelCode}"></iframe>
    </figure>
  `; */
  shouldRenderDFP == "false"
    ? ""
    : `
    <figure class="op-ad">
      <iframe style="border:0;margin:0;padding:0;">
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
          window.googletag = window.googletag || {cmd: []};
          var adSlot = {};
          var shouldRefreshAd = 0;
          googletag.cmd.push(function() {
            adSlot = adSlot = googletag.defineSlot('${fbiaConfig.ads[process.env.SITE]}', ${
        fbiaConfig.size
      }, 'banner-ad')
                .addService(googletag.pubads());
            googletag.enableServices();
          });
        </script>
        <div id="banner-ad">
          <script>
            googletag.cmd.push(function() {
              googletag.display('banner-ad');
            });
          </script>
        </div>
        <script>
          window.addEventListener('message', receiveMessage);
          function receiveMessage(event) {
            if (event.data === 'center_enters_viewport') {
              // ad center entered viewport
              if(shouldRefreshAd){
                googletag.cmd.push(() => {
                  googletag.pubads().refresh([adSlot]);
                });
              }  
              shouldRefreshAd = 0;
            }
            if (event.data === "exits_viewport") {
              // ad exits viewport
              shouldRefreshAd = 1;
            }
          }
        </script>
      </iframe>
    </figure>
  `;

const getRelatedListMarkup = (related_list, related_title) => {
  if (related_list.length == 0) {
    return "";
  }

  let read_more_html = `<ul class="op-related-articles" title="${related_title}">`;
  related_list.map((link, i) => {
    let _anchorlink = getLink(link);
    // check for wu , it should be from same domain
    if (typeof link.hl == "string" && link.hl != "" && _anchorlink && _anchorlink.indexOf(siteConfig.webdomain) > -1) {
      const sponsored = "";
      if (_anchorlink.includes("?")) {
        _anchorlink += "&";
      } else {
        _anchorlink += "?";
      }
      if (link.ctype === "adv" || link.isApnaBazar) {
        // sponsored = `data-sponsored="true"`;
        _anchorlink += `utm_source=Nativeadv${i + 1}&medium=Instant Article&utm_campaign=IA`;
      } else {
        _anchorlink += "utm_source=Facebook Related&medium=Instant Article&utm_campaign=IA";
      }
      read_more_html += `<li ${sponsored}><a href="${_anchorlink}">${link.hl}</a></li>`;
    }
  });
  read_more_html += `</ul>`;

  return read_more_html;
};

const getAmazonWidgetMarkup = () => `<figure class="op-interactive">
  <iframe class="column-width" src="https://navbharattimes.indiatimes.com/pwafeeds/amazon_wdt_tas.cms?host=${process.env.SITE}&platform=desktop&tag=IA" width="600" height="960"></iframe>
</figure>`;

// Function to get related data based on type and count
const getRelatedData = (data, type, count) => {
  const relatedData = [];
  if (!data) {
    return relatedData;
  }
  data.forEach(element => {
    const _anchorlink = getLink(element);
    if (
      element.ctype &&
      typeof element.hl == "string" &&
      element.hl != "" &&
      _anchorlink &&
      _anchorlink.indexOf(siteConfig.webdomain) > -1
    ) {
      if (element.ctype === type || type === "apnabazar") {
        addedArticles.push(element.id);
        relatedData.push({ ...element, isApnaBazar: type === "apnabazar" });
      }
    }
  });
  // If still count is less for advertorial article then normal articles are appended
  if (type === "adv" && relatedData.length < count) {
    for (let i = 0; i < data.length; i++) {
      const element = data[i];
      const _anchorlink = getLink(element);
      if (
        element.ctype &&
        typeof element.hl == "string" &&
        element.hl != "" &&
        _anchorlink &&
        _anchorlink.indexOf(siteConfig.webdomain) > -1
      ) {
        if (element.ctype === "news" || element.ctype === "nonnews") {
          relatedData.push(element);
        }
      }
    }
  }
  return shuffleArray(relatedData).slice(0, count);
};

/**
 * Function for creating iframe inbetween article
 * @param {string} type : it will define which template(video,photo,PF,article)
 */
const getAppBannerMarkup = ({ type = "news", msid }) => `<figure class="op-interactive"> <iframe class="no-margin" 
    src="https://navbharattimes.indiatimes.com/ia_app_banner/${msid}.cms?site=${process.env.SITE}&type=${type}" 
    height="100" width="320"></iframe> 
</figure>`;

// Function to get link for related articles
function getLink(obj) {
  let url = "";
  if (!obj) {
    return url;
  }
  if (obj.wu && obj.wu !== "") {
    url = obj.wu;
  } else if (obj.override && obj.override !== "") {
    url = obj.override;
  } else if (obj.seolocation && obj.id) {
    url = `${siteConfig.weburl}/${obj.seolocation}/articleshow/${obj.id}.cms`;
  }
  url = removeParam("utm_source", url);
  url = removeParam("utm_medium", url);
  url = removeParam("utm_campaign", url);
  return url;
}

function removeParam(key, sourceURL) {
  let rtn = sourceURL.split("?")[0];

  let param;

  let params_arr = [];

  const queryString = sourceURL.indexOf("?") !== -1 ? sourceURL.split("?")[1] : "";
  if (queryString !== "") {
    params_arr = queryString.split("&");
    for (let i = params_arr.length - 1; i >= 0; i -= 1) {
      param = params_arr[i].split("=")[0];
      if (param === key) {
        params_arr.splice(i, 1);
      }
    }
    if (params_arr.length) {
      rtn = `${rtn}?${params_arr.join("&")}`;
    }
  }
  return rtn;
}

export { fbIAConverter, getRHS };

/* eslint-disable radix */
/* eslint-disable no-console */
import axios from "axios";
import { _isCSR, checkIsFbPage, logError } from "../util";
import cachemodule from "../CacheProvider/cacheService";
// import isomorphicFetch from "isomorphic-fetch";
import globalconfig from "../../globalconfig";

let cacheInstance;

// Set it for only SSR , as we don't need caching in CSR
if (!_isCSR()) {
  cacheInstance = cachemodule;
}

const onSuccess = response => {
  // console.log("API Request Successful!", response.data);
  // console.log("API SUCCESS - data -", response);
  if (response && response.data && response.data.error) {
    return Promise.reject({ message: response.data.error });
  }
  return response.data;
};

const onError = error => {
  console.log("API Request Failed:", error.config);
  // console.log("API- FAILURE-data-", error);

  // Code for logging API level errors
  if (!_isCSR()) {
    logError(error);
  }
  if (error.response) {
    // console.log("API- FAILURE-data-", error.response);
    // Request was made but server responded with something
    // other than 2xx
    // console.log("API Status:", error.response.status);
    // console.log("TestttData:", error.response.data);
    //  console.log("TestttHeaders:", error.response.headers);
  } else {
    // Something else happened while setting up the request
    // triggered the error
    // console.log("API- FAILURE-data-", error.response);
    console.log("API Error Message:", error.message);
  }

  return Promise.reject(error.rnse || error.message);
};

/**
 * Wrapper around `window.fetch` to handle json parsing and offer a
 * consistent error interface.
 *
 * fetch('/Users')
 *     .then(json => {
 *         console.log('success', json);
 *     }, networkError => {
 *         console.log('error', networkError);
 *     }).catch(error => {
 *         throw error; // Ensure runtime errors bubble up to `window.onerror`
 *     });
 *
 * @param  {String} url     The url.
 * @param  {Object} options https://developer.mozilla.org/en-US/docs/Web/API/GlobalFetch
 * @return {Promise}
 */
function fetch(url, options = {}, isCacheable) {
  //console.log("APIs", url);
  Object.assign(options, {
    url,
    headers: {
      Accept: "application/json",
    },
  });

  // Conditions only for SSR
  if (!_isCSR()) {
    // Set timeout for servers only for 9sec
    // console.log("=================Timeout:", 9 * 1000);
    const reqUrl = globalconfig && globalconfig.reqUrl ? globalconfig.reqUrl : "";
    // adding a check for iag to set timeout of 30 seconds
    const timeout = checkIsFbPage(reqUrl) ? 30 : process.env.SITE === "iag" ? 30 : 9;
    Object.assign(options, {
      timeout: parseInt(timeout * 1000),
    });
    Object.assign(options.headers, {
      "content-encoding": "gzip",
    });
  }
  // Conditions only for SSR and for NODE_ENV : production,development
  if (
    !_isCSR() &&
    isCacheable &&
    cacheInstance &&
    (process.env.NODE_ENV === "production" || process.env.NODE_ENV === "development")
  ) {
    // console.log("APIs", url);
    return cacheInstance
      .get(url, () =>
        // console.log("**************fresh Call**************", url);
        axios(options)
          .then(onSuccess)
          .catch(onError),
      )
      .then(
        result =>
          // console.log("#################from cache##################", url);
          result,
      );
  }

  return axios(options)
    .then(onSuccess)
    .catch(onError);
}

export default fetch;

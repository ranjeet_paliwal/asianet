/* eslint-disable no-console */
/**
* @Description:
    This file takes care of GET & POST API calls for app.
    method : triggerServiceRequest - Requires 'opt' parameter, which is an object containing API uri, method/type & params
    Sample Usage:
        triggerServiceRequest({
          url: url,
          method: 'GET',
          data: {
                firstName: 'Fred',
                lastName: 'Flintstone'
            }
        });
*
*/

import axios from "axios";
import _merge from "lodash/merge";
/**
 * Request Wrapper with default success/error actions
 */
export const triggerServiceRequest = options => {
  const onSuccess = response => {
    console.log("API Request Successful!");
    return response.data;
  };

  const onError = error => {
    // console.log("API Request Failed:", error.config);

    if (error.response) {
      // Request was made but server responded with something
      // other than 2xx
      console.log("API Status:", error.response.status);
      //   console.log("TestttData:", error.response.data);
      //   console.log("TestttHeaders:", error.response.headers);
    } else {
      // Something else happened while setting up the request
      // triggered the error
      console.log("API Error Message:", error.message);
    }

    return Promise.reject(error.rnse || error.message);
  };
  if (typeof window === "undefined") {
    console.log("Timeout:", process.env.API_TIMEOUT);
    _merge(options, {
      timeout: parseInt(process.env.API_TIMEOUT),
    });
  }
  return axios(options)
    .then(onSuccess)
    .catch(onError);
};

export const MediaWireServiceRequest = (options) => {
  let apiUrl = "";
  if (["development", "stg1", "stg2", "stg3"].includes(process.env.DEV_ENV)) {
    apiUrl = `https://stg.mediawire.in/mediawire-api/mediawireHomePage/getHPStory?hostid=${options.hostid}&platform=${options.platform}&app_platform=${options.app_platform}&secid=${options.secid}&sltno=${options.sltno}&geocl=${options.geocl}&geostate=${options.geostate}&geolocation=${options.geolocation}&geocontinent=${options.geocontinent}`;

  } else {
    apiUrl = `https://api.mediawire.in/mediawire-api/mediawireHomePage/getHPStory?hostid=${options.hostid}&platform=${options.platform}&app_platform=${options.app_platform}&secid=${options.secid}&sltno=${options.sltno}&geocl=${options.geocl}&geostate=${options.geostate}&geolocation=${options.geolocation}&geocontinent=${options.geocontinent}`;
  }

  let axiosOption = {
    url: apiUrl,
    method: "GET",
  };

  const onSuccess = response => {

    let mediaWireCard;
    if (response && response.data && response.data.wu != null && response.data.hl != null){
      mediaWireCard = response.data;
      mediaWireCard.override = mediaWireCard.wu;
      return mediaWireCard;
    } else {
      mediaWireCard = options.defaultData;
      return mediaWireCard;
    }
  };

  const onError = error => {
    
    if (error.response) {
      // Request was made but server responded with something
      // other than 2xx
      console.log("API Status:", error.response.status);
    } else {
      // Something else happened while setting up the request
      // triggered the error
      console.log("API Error Message:", error.message);
    }
    
    return Promise.reject(error.rnse || error.message);
  };

  return axios(axiosOption)
    .then(onSuccess)
    .catch(onError);
};

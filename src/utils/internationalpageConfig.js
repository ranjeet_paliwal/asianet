import { isMobilePlatform } from "./util";

export const countryList = {
  bangladesh: {
    countryName: "BAN",
    iconName: "flag-bangladesh",
    urlPath: "/bangladesh",
  },
  us: {
    countryName: "US",
    iconName: "flag-usa",
    urlPath: "/us",
  },
  gulf: {
    countryName: "UAE",
    iconName: "flag-uae",
    urlPath: "/gulf",
  },
  india: {
    countryName: "IND",
    iconName: "flag-india",
    urlPath: "/",
  },
};

export const internationalconfig = {
  routes: ["/", "/us", "/bangladesh", "/gulf"],
  eisamay: {
    internationalPages: {
      US: "82047661",
      BD: "62281110",
    },
    internationalPagesUrl: {
      US: "/us",
      BD: "/bangladesh",
    },
    urlMappingMsid: {
      "/us": "82047661",
      "/bangladesh": "62281110",
    },
    headlineConfig: {
      label: "headlines",
      sec_id: "2147477985",
      _actuallabel: "headlines",
      _count: "10",
      _ctnadtype: "homehorizontalsmall",
      _layouttype: "hpui6",
      _sec_id: "2147477985",
      _type: "headline",
    },
    dropdownArray: [countryList.india, countryList.us, countryList.bangladesh],
    dropdownObject: {
      US: countryList.us,
      BD: countryList.bangladesh,
    },
    urldropdowm: {
      "/us": countryList.us,
      "/bangladesh": countryList.bangladesh,
      "/": countryList.india,
    },
  },
  mly: {
    internationalPages: {
      AE: "79036149",
    },
    internationalPagesUrl: {
      AE: "/gulf",
    },
    urlMappingMsid: {
      "/gulf": "79036149",
    },
    headlineConfig: {
      label: "headlines",
      sec_id: "2147478019",
      _actuallabel: "headlines",
      _count: "10",
      _ctnadtype: "homehorizontalsmall",
      _layouttype: "hpui6",
      _sec_id: "2147478019",
      _type: "headline",
    },
    dropdownArray: [countryList.india, countryList.gulf],
    dropdownObject: {
      AE: countryList.gulf,
    },
    urldropdowm: {
      "/gulf": countryList.gulf,
      "/": countryList.india,
    },
  },
  nbt: {
    internationalPages: {
      AE: "82062404",
    },
    internationalPagesUrl: {
      AE: "/gulf",
    },
    urlMappingMsid: {
      "/gulf": "82062404",
    },
    headlineConfig: {
      label: "headlines",
      sec_id: "696089404",
      _actuallabel: "headlines",
      _count: "10",
      _ctnadtype: "homehorizontalsmall",
      _layouttype: "hpui6",
      _sec_id: "696089404",
      _type: "headline",
    },
    dropdownArray: [countryList.india, countryList.gulf],
    dropdownObject: {
      AE: countryList.gulf,
    },
    urldropdowm: {
      "/gulf": countryList.gulf,
      "/": countryList.india,
    },
  },
  mt: {
    internationalPages: {
      US: "82062779",
    },
    internationalPagesUrl: {
      US: "/us",
    },
    urlMappingMsid: {
      "/us": "82062779",
    },
    headlineConfig: {
      label: "headlines",
      sec_id: "2147477892",
      _actuallabel: "headlines",
      _count: "10",
      _ctnadtype: "homehorizontalsmall",
      _layouttype: "hpui6",
      _sec_id: "2147477892",
      _type: "headline",
    },
    dropdownObject: {
      US: countryList.us,
    },
    dropdownArray: [countryList.india, countryList.us],
    urldropdowm: {
      "/us": countryList.us,
      "/": countryList.india,
    },
  },
  tlg: {
    internationalPages: {
      US: "",
    },
    internationalPagesUrl: {
      US: "",
    },
    urlMappingMsid: {
      "/us": "",
    },
    headlineConfig: {
      label: "headlines",
      sec_id: "",
      _actuallabel: "headlines",
      _count: "10",
      _ctnadtype: "homehorizontalsmall",
      _layouttype: "hpui6",
      _sec_id: "",
      _type: "headline",
    },
    dropdownObject: {
      US: countryList.us,
    },
    dropdownArray: [countryList.india, countryList.us],
    urldropdowm: {
      "/us": countryList.us,
      "/": countryList.india,
    },
  },
  tml: {
    internationalPages: {
      US: "",
    },
    internationalPagesUrl: {
      US: "",
    },
    urlMappingMsid: {
      "/us": "",
    },
    headlineConfig: {
      label: "headlines",
      sec_id: "",
      _actuallabel: "headlines",
      _count: "10",
      _ctnadtype: "homehorizontalsmall",
      _layouttype: "hpui6",
      _sec_id: "",
      _type: "headline",
    },
    dropdownObject: {
      US: countryList.us,
    },
    dropdownArray: [countryList.india, countryList.us],
    urldropdowm: {
      "/us": countryList.us,
      "/": countryList.india,
    },
  },
  iag: {
    internationalPages: {
      US: "",
    },
    internationalPagesUrl: {
      US: "",
    },
    urlMappingMsid: {
      "/us": "",
    },
    headlineConfig: {
      label: "headlines",
      sec_id: "",
      _actuallabel: "headlines",
      _count: "10",
      _ctnadtype: "homehorizontalsmall",
      _layouttype: "hpui6",
      _sec_id: "",
      _type: "headline",
    },
    dropdownObject: {
      US: countryList.us,
    },
    dropdownArray: [countryList.india, countryList.us],
    urldropdowm: {
      "/us": countryList.us,
      "/": countryList.india,
    },
  },
  vk: {
    internationalPages: {
      US: "",
    },
    internationalPagesUrl: {
      US: "",
    },
    urlMappingMsid: {
      "/us": "",
    },
    headlineConfig: {
      label: "headlines",
      sec_id: "",
      _actuallabel: "headlines",
      _count: "10",
      _ctnadtype: "homehorizontalsmall",
      _layouttype: "hpui6",
      _sec_id: "",
      _type: "headline",
    },
    dropdownObject: {
      US: countryList.us,
    },
    dropdownArray: [countryList.india, countryList.us],
    urldropdowm: {
      "/us": countryList.us,
      "/": countryList.india,
    },
  },
};

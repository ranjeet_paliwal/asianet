/* eslint-disable no-plusplus */
const events = {};

/**
 * Method is used to subscribe the custom events.
 * @param ename is custom event to be subscribed
 * @param eventCallback function executed when the above event(ename) is published
 */
export const subscribe = (ename, eventCallback) => {
  events[ename] = events[ename] || [];
  events[ename].push(eventCallback);
};

/**
 * Method is used to publish the custom events.
 * @param ename is custom event to be published
 * @param data to be passed to subscribe event.
 */
export const publish = (ename, data) => {
  if (events[ename]) {
    events[ename].forEach(fn => {
      fn(data);
    });
  }
};

/**
 * Method is used to unsubscribe the custom events when not in need.
 * @param {*} ename name of the custom event for which unsubscribe will run.
 * @param {*} eventCallback the particular function which needs to be unsubscribe from the function list.
 */
export const unsubscribe = (ename, eventCallback) => {
  if (events[ename]) {
    for (let i = 0; i < events[ename].length; i++) {
      if (events[ename][i] === eventCallback) {
        events[ename].splice(i, 1);
        break;
      }
    }
  }
};

export default { publish, subscribe, unsubscribe };

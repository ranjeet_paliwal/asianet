// @flow weak

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { mod } from "react-swipeable-views-core";

export function virtualize(MyComponent) {
  class Virtualize extends PureComponent {
    static propTypes = {
      /**
       * @ignore
       */
      children: (props, propName) => {
        if (props[propName] !== undefined) {
          return new Error("The children property isn't supported.");
        }

        return null;
      },
      /**
       * @ignore
       */
      index: PropTypes.number,
      activeIndex: PropTypes.number,
      /**
       * @ignore
       */
      onChangeIndex: PropTypes.func,
      /**
       * @ignore
       */
      onTransitionEnd: PropTypes.func,
      /**
       * Number of slide to render after the visible slide.
       */
      overscanSlideAfter: PropTypes.number,
      /**
       * Number of slide to render before the visible slide.
       */
      overscanSlideBefore: PropTypes.number,
      /**
       * When set, it's adding a limit to the number of slide: [0, slideCount].
       */
      slideCount: PropTypes.number,
      /**
       * Responsible for rendering a slide given an index.
       * ({ index: number }): node.
       */
      slideRenderer: PropTypes.func.isRequired,
    };

    static defaultProps = {
      overscanSlideAfter: 2,
      // Render one more slide for going backward as it's more difficult to
      // keep the window up to date.
      overscanSlideBefore: 3,
    };

    /**
     *
     *           index          indexStop
     *             |              |
     * indexStart  |       indexContainer
     *   |         |         |    |
     * ------------|-------------------------->
     *  -2    -1   0    1    2    3    4    5
     */
    state = {};

    componentWillMount() {
      this.setState({
        index: this.props.index || 0,
      });

      this.setWindow(this.props.index || 0);
    }

    /* shouldComponentUpdate(nextProps, nextState) {
      console.log("virtualize should component update", this.props,nextProps);
      if(this.props.index!=nextProps.index){
        return true;
      }
      
      return false;
    } */

    componentWillReceiveProps(nextProps) {
      // console.log("virtualize component will receive props ");
      const { index } = nextProps;

      if (typeof index === "number" && index !== this.props.index) {
        const indexDiff = index - this.props.index;
        // commented by sandeep
        // this.setIndex(index, this.state.indexContainer + indexDiff, indexDiff);
        this.setIndex(index, index, indexDiff);
      }
    }

    componentWillUnmount() {
      clearInterval(this.timer);
    }

    timer = null;

    handleChangeIndex = (indexContainer, indexLatest) => {
      const { slideCount, onChangeIndex } = this.props;

      const indexDiff = indexContainer - indexLatest;
      let index = this.state.index + indexDiff;

      if (slideCount) {
        index = mod(index, slideCount);
      }

      // Is uncontrolled
      // if (this.props.index === undefined) {
      this.setIndex(index, indexContainer, indexDiff);
      // }

      if (onChangeIndex) {
        onChangeIndex(index, this.state.index);
      }
    };

    handleTransitionEnd = () => {
      // Delay the update of the window to fix an issue with react-motion.
      this.timer = setTimeout(() => {
        this.setWindow();
      }, 0);

      if (this.props.onTransitionEnd) {
        this.props.onTransitionEnd();
      }
    };

    setIndex(index, indexContainer, indexDiff) {
      const nextState = {
        index,
        indexContainer,
        indexStart: this.state.indexStart,
        indexStop: this.state.indexStop,
      };

      // We are going forward, let's render one more slide ahead.
      if (indexDiff > 0 && (!this.props.slideCount || nextState.indexStop < this.props.slideCount - 1)) {
        nextState.indexStop += 1;
      }

      // Extend the bounds if needed.
      if (index > nextState.indexStop) {
        nextState.indexStop = index;
      }

      const beforeAhead = nextState.indexStart - index;

      // Extend the bounds if needed.
      if (beforeAhead > 0) {
        nextState.indexContainer += beforeAhead;
        nextState.indexStart -= beforeAhead;
      }

      this.setState(nextState);
    }

    setWindow(index = this.state.index) {
      const { slideCount } = this.props;

      let beforeAhead = this.props.overscanSlideBefore;
      let afterAhead = this.props.overscanSlideAfter;

      if (slideCount) {
        if (beforeAhead > index) {
          beforeAhead = index;
        }

        if (afterAhead + index > slideCount - 1) {
          afterAhead = slideCount - index - 1;
        }
      }

      this.setState({
        indexContainer: beforeAhead,
        indexStart: index - beforeAhead,
        indexStop: index + afterAhead,
      });
    }

    render() {
      const {
        children, // eslint-disable-line no-unused-vars
        index: indexProp, // eslint-disable-line no-unused-vars
        onChangeIndex, // eslint-disable-line no-unused-vars
        overscanSlideAfter, // eslint-disable-line no-unused-vars
        overscanSlideBefore, // eslint-disable-line no-unused-vars
        slideCount, // eslint-disable-line no-unused-vars
        slideRenderer,
        ...other
      } = this.props;

      const { indexContainer, indexStart, indexStop } = this.state;
      const slides = [];
      /* if(indexStart<slideCount-1){
        indexStop+=1;
        indexStop=Math.max(indexContainer+1, indexStop);
      }
      console.log("INSIDE Virtualize", this.props.index); */
      for (let slideIndex = indexStart; slideIndex <= indexStop; slideIndex += 1) {
        /* if(typeof(window)=='undefined' && slideIndex!=this.props.index){
          continue;
        }
        if(slideIndex<this.props.index-1 || slideIndex>this.props.index+1){
          slides.push(<div key={slideIndex}></div>);
        }  else { */
        slides.push(
          slideRenderer({
            index: slideIndex,
            key: slideIndex,
          }),
        );
        // }
      }

      return (
        <MyComponent
          index={indexContainer}
          onChangeIndex={this.handleChangeIndex}
          onTransitionEnd={this.handleTransitionEnd}
          {...other}
        >
          {slides}
        </MyComponent>
      );
    }
  }

  return Virtualize;
}

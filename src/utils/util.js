/* eslint-disable no-nested-ternary */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-underscore-dangle */
import React from "react";
import fs from "fs";
import { recreateAds, refreshAds } from "../components/lib/ads";
import globalconfig from "../globalconfig";
// import tpConfig from "../components/common/TimesPoints/TimesPointConfig";
import Loader from "../components/lib/loader/Loader";
import versionObject from "../../tasks/build/createConfig/version";
import { getSplatsVars } from "../components/lib/ads/lib/utils";

const siteConfig = require(`./../../common_prod/${process.env.SITE}`);
const footerSiteConfig = require(`./../../common_prod/footer/${process.env.SITE}`);
const cssConfig = require(`./../../common_prod/css/${process.env.SITE}`);
import gadgetsConfig from "../utils/gadgetsConfig";

const tpConfig = siteConfig.TimesPoint;

export function getCssVersion() {
  return cssConfig.cssVersion;
}

export function getCssPath(portalName, spriteType) {
  let spritePath;
  // need to discuss this as it is causing problem whilr deplying on the stg servers
  // node env is dev_env
  if (cssConfig.nodeEnv === "production") {
    // spritePath = `/img/${spriteType}.svg?v=${getCssVersion()}`;
    spritePath = `https://static.${portalName}.indiatimes.com/img/${spriteType}.svg?v=${getCssVersion()}`;
  } else if (cssConfig.nodeEnv === "development") {
    spritePath = `/${process.env.SITE}/img/${spriteType}.svg?v=${getCssVersion()}`;
  } else {
    spritePath = `/img/${spriteType}.svg?v=${getCssVersion()}`;
  }
  return spritePath;
}

export function newsUrl(newsData) {
  let url = `/news/${newsData.seopath}`;
  if (typeof newsData.overridelink !== "undefined" && newsData.overridelink != "") {
    url = `${url}/${newsData.msid}/photostory/${newsData.overridelink}`;
  } else {
    url = `${url}/articleshow/${newsData.msid}`;
  }
  return url;
}

export function scrollTo(element, to, duration) {
  if (duration <= 0) return;
  const difference = to - element.scrollTop;
  const perTick = (difference / duration) * 10;

  setTimeout(() => {
    element.scrollTop += perTick;
    if (element.scrollTop === to) return;
    scrollTo(element, to, duration - 10);
  }, 10);
}

export function scrollLeft(element, to, duration) {
  if (duration <= 0) return;
  const difference = to - element.scrollTop;
  const perTick = (difference / duration) * 10;

  setTimeout(() => {
    element.scrollLeft += perTick;
    if (element.scrollLeft === to) return;
    scrollLeft(element, to, duration - 10);
  }, 10);
}

// https://remysharp.com/2010/07/21/throttling-function-calls
export function throttle(fn, threshhold, scope) {
  threshhold || (threshhold = 250);
  let last;
  let deferTimer;
  return function() {
    const context = scope || this;

    const now = +new Date();

    const args = arguments;
    if (last && now < last + threshhold) {
      // hold on to it
      clearTimeout(deferTimer);
      deferTimer = setTimeout(() => {
        last = now;
        fn.apply(context, args);
      }, threshhold);
    } else {
      last = now;
      fn.apply(context, args);
    }
  };
}

export function debounce(func, interval) {
  let timeout;
  return function() {
    const context = this;

    const args = arguments;
    const later = function() {
      timeout = null;
      func.apply(context, args);
    };
    clearTimeout(timeout);
    timeout = setTimeout(later, interval || 200);
  };
}

/**
 * Checks if the provided element is in viewport.
 *
 * //todo create event 'inview' with callback which fires when element comes in view
 *
 * @param {selector/instance} el element to be checked
 * @param {boolean} partial  when true,returns true when element is partially in view.
 * @param {int} skew Pixels to skew/move view from the top
 * @returns {boolean} true if element is in view
 */
export function elementInView(el, partial, skew, skipParent) {
  skew = skew || 0;

  // Check instanceof ele
  if (!el || typeof el !== "object") return false;

  let top = el.offsetTop; // elem top
  let left = el.offsetLeft; // elem left
  const width = el.offsetWidth;
  const height = el.offsetHeight;
  let bottom = top + height; // elem bottom
  let right = left + width; // elem right

  const docViewTop = window.pageYOffset;
  const docViewBottom = docViewTop + window.innerHeight - skew;

  /**
   *
   *    |               |
   *    ---------------------   ViewStart
   *    |               |
   *    |               |
   *    |               |
   *    |               |
   *    |...............|.....  +SKEW
   *    |               |
   *    ----------------------  ViewEnd
   *    |               |
   *    |...............|.....  -SKEW
   *    |               |
   *    |               |
   *
   */

  while (el.offsetParent) {
    el = el.offsetParent;
    top += el.offsetTop;
    bottom += el.offsetTop;
    left += el.offsetLeft;
    right += el.offsetLeft;
  }

  let in_view = false;

  if (partial === true) {
    in_view = top < docViewBottom && bottom > docViewTop; // todo also handle horizontal movements as elementInViewport
  } else {
    in_view = top >= docViewTop && bottom <= docViewBottom; // todo also handle horizontal movements as elementInViewport
  }

  return in_view;
}

// export function elementInViewport(el) {
//   var top = el.offsetTop;
//   var left = el.offsetLeft;
//   var width = el.offsetWidth;
//   var height = el.offsetHeight;

//   while (el.offsetParent) {
//     el = el.offsetParent;
//     top += el.offsetTop;
//     left += el.offsetLeft;
//   }

//   return (
//     top < (window.pageYOffset + window.innerHeight) &&
//     left < (window.pageXOffset + window.innerWidth) &&
//     (top + height) > window.pageYOffset &&
//     (left + width) > window.pageXOffset
//   );
// }

export function parseUrl(str) {
  return str
    .toLowerCase()
    .split(" ")
    .join("-");
}

export function removeClass(elements, className) {
  for (const elem of elements) {
    elem.classList.remove(className);
  }
}

export function addClass(elements, className) {
  for (const elem of elements) {
    elem.classList.add(className);
  }
}

export function capitalizeFirstLetter(string) {
  if (typeof string === "object") {
    if (string.hasOwnProperty("name")) return string.name;
    return "";
  }
  if (typeof string === "undefined" || string.split(" ").join("") == "") return string;

  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function camelize(str) {
  if (typeof str !== "undefined") {
    return str
      .replace(/(?:^\w|[A-Z]|\b\w)/g, (letter, index) => (index == 0 ? letter.toLowerCase() : letter.toUpperCase()))
      .replace(/\s+/g, "");
  }
  return str;
}

export function calculateTime(time, page) {
  try {
    const st = new Date();
    const isPM = time.indexOf("PM") != -1;
    if (time && time.split(" ").join("") != "") {
      time = time
        .toLowerCase()
        .replace("am", "")
        .replace("pm", "")
        .replace("ist", "")
        .trim();
      if (page == "pubhome") {
        const t = time.split(",")[2].trim();
        if (isPM) {
          if (parseFloat(t, 10) < 12) {
            time = time.replace(t, (parseFloat(t, 10) + 12).toFixed(2)).replace(".", ":");
          } else {
            time = time.replace(".", ":");
          }
        } else if (parseInt(t, 10) == 12) {
          time = time.replace(t, (parseFloat(t, 10) - 12).toFixed(2)).replace(".", ":");
        } else {
          time = time.replace(".", ":");
        }
      }
      const tmpDate = new Date(time).getTime();
      if (tmpDate < st) {
        let diff = Math.abs((tmpDate - st) / 60000);
        diff = typeof diff === "number" ? Math.floor(diff) : diff;
        if (diff <= 1440) {
          if (diff < 60) {
            if (diff < 1) {
              return "Just Now";
            }
            return diff + (diff > 1 ? " mins ago" : " min ago");
          }
          var tmp = Math.floor(diff / 60);
          if (tmp <= 1) {
            return `${tmp} hr ago`;
          }
          if (tmp < 24) {
            return `${tmp} hrs ago`;
          }
        } else if (diff > 1440) {
          var tmp = Math.floor(Math.floor(diff / 60) / 24);
          if (tmp == 1) {
            return `${tmp} day ago`;
          }
          return `${tmp} days ago`;
        } else {
          /* if (format == 'full'){
              return (time + " IST");
          } */
        }
      }
    }
  } catch (ex) {
    return time;
  }
}

export function splitUrl(url) {
  if (typeof url !== "undefined" && url != "") {
    return url.replace("-", " ").toLowerCase();
  }
  return url;
}

export function checkIsAmpPage(url) {
  if (
    url &&
    (url.indexOf("amp_") >= 0 ||
      url.indexOf("ampdefault") >= 0 ||
      url.indexOf("ampnews") >= 0 ||
      url.indexOf("ampphotos") >= 0 ||
      url.indexOf("ampvideos") >= 0)
  ) {
    return true;
  }
  return false;
}

export function checkIsATPListing(url) {
  if ((url && url.indexOf("articlelist") >= 0) || url == "/") {
    return true;
  }
  return false;
}

export function getMobileOperatingSystem() {
  if (typeof navigator === "undefined") {
    return "android";
  }
  const userAgent = navigator.userAgent || navigator.vendor || window.opera;

  // Windows Phone must come first because its UA also contains "Android"
  if (/windows phone/i.test(userAgent)) {
    return "windows";
  }

  if (/android/i.test(userAgent)) {
    return "android";
  }

  // iOS detection from: http://stackoverflow.com/a/9039885/177710
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    return "ios";
  }

  return "unknown";
}
export function getMobileOs() {
  // if (typeof navigator === "undefined") {
  //   return "unknown";
  // }
  if (_isCSR()) {
    const userAgent = typeof navigator !== "undefined" && (navigator.userAgent || navigator.vendor || window.opera);
    if (/android/i.test(userAgent)) {
      return "android";
    }
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      return "ios";
    }
    return "unknown";
  }
}

export function gtmEvent(event) {
  if (typeof window !== "undefined" && typeof window.dataLayer !== "undefined") {
    window.dataLayer.push({ event });
  }
}

export function gaEvent(category, action, label) {
  const gtmObject = {};
  let categoryForEvent = category.replace(/ /g, "_");
  categoryForEvent = categoryForEvent.toLowerCase();
  gtmObject.event = `tvc_${categoryForEvent}`;
  gtmObject.eventCategory = category;
  gtmObject.eventAction = action;
  if (typeof label !== "undefined") {
    gtmObject.eventLabel = label;
  }
  if (typeof ga !== undefined && typeof window !== "undefined" && action != undefined && label != undefined) {
    // ReactGA.event({category: category, action: action, label: label});
    // ga("send", "event", category, action, label);
    gtmEvent(gtmObject);
  } else if (typeof ga !== undefined && typeof window !== "undefined") {
    // ReactGA.event({category: category, action: action});
    // ga("send", "event", category, action);
    gtmEvent(gtmObject);
  }
}

/**
 * @mohit.rathi
 * Load javascript file on a page
 * @function js
 * @param {String} url URL of the javascript file
 * @param {Function} [callback] function to be called when js is loaded
 * @param {String} [id] id to be given to the javascript tag
 * @param {Boolean} [async] true by default, set false to load synchronously
 * @returns {HTMLElement} generated script tag element
 *
 */
export function loadJS(url, callback, id, async, attributes) {
  const head = document.getElementsByTagName("head") ? document.getElementsByTagName("head")[0] : null;
  if (head) {
    const script = document.createElement("script");
    const done = false; // Handle Script loading
    if (id) {
      script.id = id;
    }
    if (async) {
      script.async = async;
    }
    if (attributes && Array.isArray(attributes)) {
      attributes.forEach(item => {
        script.setAttribute(item.name, item.value);
      });
    }
    if (!url) {
      throw new Error("Param 'url' not defined.");
    }
    script.src = url;
    script.onload = script.onreadystatechange = function() {
      // Attach handlers for all browsers
      if (!script.loaded && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
        script.loaded = true;
        const endTime = new Date().getTime();
        const timeSpent = endTime - script.startTime;
        if (callback) {
          try {
            callback();
          } catch (e) {
            // to handle
            throw new Error("logger.error", e.stack);
          }
        }
        script.onload = script.onreadystatechange = null; // Handle memory leak in IE
      }
    };
    script.startTime = new Date().getTime();
    head.appendChild(script);
    return script;
  }
  throw new Error(`Head Element not found. JS '${url}' not loaded. `);
  return null;
}

/**
 * @kankan.das
 * Load style file on a page
 * @function style
 * @param {String} url URL of the style file
 * @param {String} [id] id to be given to the style tag
 * @param {Boolean} [preload] true by default, set false to preload
 * @returns {HTMLElement} generated script tag element
 *
 */
export function loadCss(url, rel, preload) {
  const head = document.getElementsByTagName("head") ? document.getElementsByTagName("head")[0] : null;
  if (head) {
    const styletag = document.createElement("link");
    styletag.rel = rel;
    if (id) {
      styletag.id = id;
    }
    if (preload) {
      styletag.preload = preload;
    }
    if (!url) {
      throw new Error("Param 'url' not defined.");
    }
    styletag.href = url;
    head.appendChild(styletag);
    return styletag;
  }
  throw new Error(`Head Element not found. Style '${url}' not loaded. `);
  return null;
}

export function isDefined(obj) {
  return typeof obj !== "undefined";
}

export function silentRedirect(url) {
  if (typeof window !== "undefined") {
    try {
      window.history.replaceState({}, "", url);
    } catch (ex) {}
  }
}

export function silentRedirectPush(url) {
  if (typeof window !== "undefined") {
    try {
      window.history.pushState({}, "", url);
    } catch (ex) {}
  }
}

export function makeUrl(url) {
  if (typeof url !== "undefined" && url != "") {
    return url
      .split(" ")
      .join("-")
      .toLowerCase();
  }
  return url;
}
export function parseFeedSectionTitle(str) {
  if (str == "" || typeof str === "undefined") {
    str = "top-news";
  }
  if (typeof str !== "undefined" && str != "") {
    str = str.replace(" ", "");
    str = str.replace("-", "");
    return str.toLowerCase();
  }
  return str;
}

export function createArticleUrl(item) {
  let title = makeUrl(item.hl);
  title = title.replace(" ", "-");
  title = title.replace(/[^a-zA-Z0-9\-]/g, "");
  return `/${title}/articleshow/${item.id}`;
}

export function isEmpty(obj) {
  for (const prop in obj) {
    if (obj.hasOwnProperty(prop)) return false;
  }
  return true;
}

export function calculateIndex(index, total) {
  let newIndex = index % total;
  if (newIndex < 0) {
    newIndex = total + newIndex;
  }
  return newIndex;
}

export function isObject(ele) {
  return ele !== null && typeof ele === "object"; // && !(ele instanceof Array);
}

export function mapObj(mapping, obj) {
  if (!isObject(mapping) && !isObject(obj)) {
    throw new Error("Invalid Parameters"); // todo give proper message
  }
  const newObj = {};
  each(mapping, (k, v) => {
    if (is.funct(v)) {
      newObj[k] = v(obj);
    } else {
      newObj[k] = obj[v];
    }
  });
  return newObj;
}

export function each(obj, callback) {
  let i;
  let o;
  if (!callback) {
    return null;
  }
  if (obj instanceof Array) {
    for (i = 0; i < obj.length; i++) {
      if (callback(i, obj[i]) === false) {
        break;
      }
    }
  } else if (obj instanceof Object) {
    for (o in obj) {
      if (obj.hasOwnProperty(o)) {
        if (callback(o, obj[o]) === false) {
          break;
        }
      }
    }
  } else {
    // callback(0, obj);
  }
}

export function processLink(str) {
  str = str.replace(/<a/g, '<a target="_blank" ');
  return str;
}

export function convertTime(m) {
  const hour = parseInt(m / 60);
  const minut = m % 60;
  return `${hour}h ${minut}m`;
}

export function parseQueryString(url) {
  const qSIndex = url.indexOf("?");
  if (qSIndex >= 0) {
    const qs = url.substring(qSIndex + 1);
    const qsArr = qs.split("&");
    const retVal = {};
    for (const q of qsArr) {
      // console.log(qsArr);
      const temp = q.split("=");
      // console.log(temp,temp[0], temp[1]);
      retVal[temp[0]] = temp[1];
    }
    return retVal;
  }
  return null;
}

export function isUcBrowser() {
  let isUcBrows = false;
  if (typeof navigator.userAgent !== "undefined") {
    isUcBrows = navigator.userAgent.toLowerCase().indexOf("ucbrowser") > -1;
  }
  return isUcBrows;
}

export function isOperaMini() {
  let isOperaMiniBrowser = false;
  if (typeof navigator.userAgent !== "undefined") {
    isOperaMiniBrowser = navigator.userAgent.indexOf("Opera Mini") > -1;
  }
  return isOperaMiniBrowser;
}

export function isOperaMiniAtServer(req) {
  let isOperaMiniBrowser = false;
  if (typeof req.headers["user-agent"] !== "undefined") {
    isOperaMiniBrowser = req.headers["user-agent"].indexOf("Opera Mini") > -1;
  }
  return isOperaMiniBrowser;
}

export function getSectionName(sections, id) {
  const value = sections.find(obj => {
    if (obj.uid == id) {
      return obj;
    }
  });
  if (value) return value.name;
}

export function sortObject(o) {
  return Object.keys(o)
    .sort()
    .reduce((r, k) => ((r[k] = o[k]), r), {});
}

export function getAbsoluteBoundingRect(el) {
  const doc = document;

  const win = window;

  const body = doc.body;

  // pageXOffset and pageYOffset work everywhere except IE <9.

  let offsetX =
    win.pageXOffset !== undefined ? win.pageXOffset : (doc.documentElement || body.parentNode || body).scrollLeft;

  let offsetY =
    win.pageYOffset !== undefined ? win.pageYOffset : (doc.documentElement || body.parentNode || body).scrollTop;

  const rect = el.getBoundingClientRect();

  if (el !== body) {
    let parent = el.parentNode;

    // The element's rect will be affected by the scroll positions of
    // *all* of its scrollable parents, not just the window, so we have
    // to walk up the tree and collect every scroll offset. Good times.
    while (parent !== body) {
      offsetX += parent.scrollLeft;
      offsetY += parent.scrollTop;
      parent = parent.parentNode;
    }
  }

  return {
    bottom: rect.bottom + offsetY,
    height: rect.height,
    left: rect.left + offsetX,
    right: rect.right + offsetX,
    top: rect.top + offsetY,
    width: rect.width,
  };
}

export function _assign(target, ...args) {
  if (target === null) {
    throw new TypeError("Cannot convert undefined or null to object");
  }

  const newTarget = target;
  for (let index = 0; index < args.length; index++) {
    const source = args[index];
    if (source !== null) {
      for (const key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          newTarget[key] = source[key];
        }
      }
    }
  }
  return newTarget;
}

// export function _nameAnchor() {
//   if (typeof (window) != 'undefined') {
//     document.querySelectorAll("a[href^='#']").forEach(node => {
//       node.addEventListener('click', e => {
//         e.preventDefault();
//         console.log(e.target.href);
//         //this.props.history.push(e.target.href);
//       });
//     })
//   }
// }

// _nameAnchor();

export const _isCSR = () => {
  if (typeof window !== "undefined") {
    return true;
  }
  return false;
};

export const _dateFormatter = date => {
  // if(_isCSR()){
  const date_obj = new Date(date);

  return {
    datestring: date_obj.toDateString(),
    date: date_obj.getDate(),
    month: date_obj.getMonth(),
    day: date_obj.getDay(),
    year: date_obj.getFullYear(),
    hour: date_obj.getHours(),
    min: date_obj.getMinutes(),
  };
  // }
};

export const _loadJS = options => {
  if (options && options.scriptpath) {
    (function(i, s, o, g, r, a, m) {
      (a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
      a.async = r || 1;
      a.src = g;
      m.parentNode.insertBefore(a, m);
    })(window, document, "script", options.scriptpath, options.async);
  }
};

/**
 * Get value of a cookie
 *
 * @param name {String} name of the cookie for which value is required,
 *                        if name is not provided an object with all cookies is returned
 * @returns value {String | Array} value of the requested cookie / Array of all cookies
 */

export const _getCookie = name => {
  let result = name ? undefined : {};
  const cookies = document.cookie ? document.cookie.split("; ") : [];
  for (let i = 0, l = cookies.length; i < l; i++) {
    const parts = cookies[i].split("=");
    const nameK = decodeURIComponent(parts.shift());
    let cookie = parts.join("=");
    cookie = _parseCookieValue(cookie);
    if (name && name === nameK) {
      result = cookie;
      break;
    }
    if (!name && cookie !== undefined) {
      result[nameK] = cookie;
    }
  }
  return result;
};

const _parseCookieValue = s => {
  if (s.indexOf('"') === 0) {
    // This is a quoted cookie as according to RFC2068, unescape...
    s = s
      .slice(1, -1)
      .replace(/\\"/g, '"')
      .replace(/\\\\/g, "\\");
  }
  try {
    // If we can't decode the cookie, ignore it, it's unusable.
    // Replace server-side written pluses with spaces.
    return decodeURIComponent(s.replace(/\+/g, " "));
  } catch (e) {}
};

export const _setCookie = (cname, cvalue, exdays, domain) => {
  const d = new Date();
  const domainVal = domain ? `domain=${domain}; ` : "";
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  const expires = `expires=${d.toUTCString()}`;
  document.cookie = `${cname}=${cvalue}; ${expires}; ${domainVal} path=/`;
};

export const _getOS = () => {
  const ua = navigator.userAgent.toLowerCase();

  let os = "";

  let iosVersion;
  (os =
    ua.indexOf("lumia") != -1
      ? "windows"
      : ua.indexOf("iphone") != -1
      ? "iphone"
      : ua.indexOf("nexus") != -1
      ? "nexus"
      : ua.indexOf("android") != -1
      ? "android"
      : ua.indexOf("blackberry") != -1
      ? "blackberry"
      : ua.indexOf("windows phone") != -1
      ? "windows"
      : "other"),
    (iosVersion =
      parseFloat(
        `${(/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0, ""])[1]}`
          .replace("undefined", "3_2")
          .replace("_", ".")
          .replace("_", ""),
      ) || !1);
  try {
    if (
      (iosVersion && iosVersion < 8) ||
      os == "nexus" ||
      os == "windows" ||
      os == "blackberry" ||
      os == "" ||
      os == "other"
    ) {
      const element = document.querySelectorAll(".social-icons .sms_li");
      Array.prototype.forEach.call(element, e => {
        const o = e.parentNode.children;
        e.parentNode.removeChild(e),
          Array.prototype.forEach.call(o, e => {
            e.style.width = "20%";
          });
      });
    }
  } catch (e) {
    alert(`Ex:${e}`);
  }

  return os;
};

export const closeIframe = iframeid => {
  const iframe = document.querySelector(`#${iframeid}`);
  iframe.parentNode.removeChild(iframe);
};

export const _isMobile = () =>
  (a =>
    /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
      a,
    ) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
      a.substr(0, 4),
    ))(navigator.userAgent || navigator.vendor || window.opera);

export const getPageType = (pathname, forAds) => {
  if (typeof pathname === "undefined") {
    return "";
  }
  let pagetype =
    pathname.indexOf("/newsbrief") > -1
      ? "newsbrief"
      : pathname.indexOf("articleshow/") > -1
      ? "articleshow"
      : pathname.indexOf("moviereview/") > -1
      ? "moviereview"
      : pathname.indexOf("photoarticlelist/") > -1
      ? "photolist"
      : pathname.indexOf("articlelist/") > -1 ||
        pathname.indexOf("electionlist/") > -1 ||
        pathname.indexOf("/reviews") > -1 ||
        pathname.indexOf("/iplt20.cms") > -1
      ? "articlelist"
      : pathname.indexOf("liveblog/") > -1
      ? "liveblog"
      : pathname.indexOf("photomazaashow/") > -1
      ? "photomazaashow"
      : pathname.indexOf("photoshow/") > -1
      ? "photoshow"
      : pathname.indexOf("videoshow/") > -1
      ? "videoshow"
      : pathname.length == 1 || pathname.indexOf("/default.cms") > -1 || pathname.indexOf("/amp_default.cms") > -1
      ? "home"
      : pathname.indexOf("videolist/") > -1
      ? "videolist"
      : pathname.indexOf("photolist/") > -1 ||
        pathname.indexOf("photomazza/") > -1 ||
        pathname.indexOf("photogallery/") > -1
      ? "photolist"
      : pathname.indexOf("/elections/") > -1
      ? "elections"
      : pathname.indexOf("/recommended.cms") > -1
      ? "recommended"
      : pathname.indexOf("/topics/") > -1 ||
        pathname.indexOf("/profile.cms") > -1 ||
        pathname.indexOf("/glossary/") > -1 ||
        pathname.indexOf("/landing/") > -1 ||
        pathname.indexOf("/glossary.cms") > -1 ||
        pathname.indexOf("/landing.cms") > -1 ||
        pathname.indexOf("/cricketer/") > -1 ||
        pathname.indexOf("/politician/") > -1 ||
        pathname.indexOf("/cricketer.cms") > -1 ||
        pathname.indexOf("/politician.cms") > -1
      ? "topics"
      : pathname.indexOf("movieshow/") > -1
      ? "movieshow"
      : pathname.indexOf("/gulf") > -1 || pathname.indexOf("/bangladesh") > -1 || pathname.indexOf("/us") > -1
      ? "home"
      : "others";
  // check pathname in router and handle ads data
  if (forAds) {
    pagetype =
      pagetype == "videolist" || pagetype == "photolist"
        ? "articlelist"
        : pagetype === "moviereview"
        ? "articleshow"
        : pagetype === "topics"
        ? "others"
        : pagetype === "movieshow"
        ? "articleshow"
        : pagetype;
  }

  if (
    pathname &&
    pagetype == "others" &&
    pathname.indexOf("/tech/reviews.cms") < 0 &&
    pathname != "/tech" &&
    pathname != "/tech/" &&
    pathname != "/tech/default.cms"
  ) {
    // amp is removed from path while calling ampConverter in server.js
    // thats why compared /tech/compareshow instead of /tech/amp_compareshow and so on..

    // console.log("pathname", pathname);

    if (pathname.includes("compareshow.cms")) {
      pagetype = "compareshow";
    } else if (pathname.includes("gadgetshow.cms")) {
      pagetype = "gadgetshow";
    } else if (pathname.includes("gadgetslist.cms")) {
      pagetype = "gadgetslist";
    } else if (pathname.includes("/gadgetshome/")) {
      pagetype = forAds ? "tech" : "gadgethome";
    } else if (pathname.indexOf("/tech/") > -1) {
      pagetype = "tech";
    }
  }

  return pagetype;
};

/**
 * Get value of a pwa meta from initialstate as per pagetype
 * @param initialState {Object} It is state from store , which has different reducers mapped carrying meta
 * @param pagetype {String} pagetype based on above function "getPageType"
 * @returns pwa_meta {Object} value of the requested pagetype respective pwa_meta
 */
export const getPWAmetaByPagetype = (initialState, pagetype) => {
  let pwa_meta = {};
  let audetails = "";
  if (pagetype === "articleshow" || pagetype === "moviereview" || pagetype === "movieshow") {
    pagetype = "articleshow";
    const temp_items = initialState[pagetype].items;
    const temp_audetails =
      initialState[pagetype].items && initialState[pagetype].items[0] && initialState[pagetype].items[0].audetails;
    pwa_meta =
      temp_items && temp_items.length > 0 && temp_items[0] && temp_items[0].pwa_meta
        ? temp_items[0].pwa_meta
        : undefined;
    if (temp_audetails) {
      audetails = temp_audetails;
    }
  } else if (pagetype === "photomazaashow" || pagetype === "photoshow") {
    pagetype = "photomazzashow"; // tempfix for photoshow
    const temp_items = initialState[pagetype].value[0];
    const temp_audetails =
      initialState[pagetype].value && initialState[pagetype].value[0] && initialState[pagetype].value[0].audetails;
    pagetype = "photoshow"; // tempfix for photoshow
    pwa_meta =
      temp_items && temp_items.length > 0 && temp_items[0] && temp_items[0].pwa_meta
        ? temp_items[0].pwa_meta
        : undefined;
    if (temp_audetails) {
      audetails = temp_audetails;
    }
  } else if (pagetype === "videoshow") {
    const temp_items = initialState[pagetype].value;
    const temp_audetails = initialState[pagetype].value && initialState[pagetype].value.audetails;
    pwa_meta = temp_items && temp_items.pwa_meta ? temp_items.pwa_meta : undefined;
    if (temp_audetails) {
      audetails = temp_audetails;
    }
  } else if (pagetype === "liveblog") {
    const temp_items = initialState[pagetype].meta;
    pwa_meta = temp_items && temp_items.pwa_meta ? temp_items.pwa_meta : undefined;
  } else if (pagetype === "articlelist" || pagetype === "videolist" || pagetype === "photolist") {
    pagetype = "articlelist";
    const temp_items = pagetype === "videolist" ? initialState[pagetype].value : initialState[pagetype].value[0];
    pwa_meta = temp_items && temp_items.pwa_meta ? temp_items.pwa_meta : undefined;
  } else if (pagetype === "topics") {
    const temp_items = initialState[pagetype].value[0];
    pwa_meta = temp_items && temp_items.pwa_meta ? temp_items.pwa_meta : undefined;
  } else if (pagetype == "home") {
    const temp_items = initialState[pagetype].value;
    pwa_meta =
      temp_items && temp_items.length > 0 && temp_items[0] && temp_items[0].pwa_meta
        ? temp_items[0].pwa_meta
        : undefined;
  }
  if (audetails) {
    pwa_meta = Object.assign(pwa_meta, audetails);
    //pwa_meta["audetails"] = audetails
  }
  return pwa_meta;
};

export const detach = (node, next, async, fn) => {
  const parent = node ? node.parentNode : document.body;
  // No parent node? Abort!
  if (!parent) {
    return;
  }
  // Detach node from DOM.
  parent.removeChild(node);
  // Handle case where optional `async` argument is omitted.
  if (typeof async !== "boolean") {
    fn = async;
    async = false;
  }
  // Note that if a function wasn't passed, the node won't be re-attached!
  if (fn && async) {
    // If async == true, reattach must be called manually.
    fn.call(node, reattach);
  } else if (fn) {
    // If async != true, reattach will happen automatically.
    fn.call(node);
    reattach();
  }
  // Re-attach node to dom.
  function reattach() {
    next.appendChild(node);
  }
};

export const get_Scroll_dir = () => {
  const up_event = new Event("scroll_up"); // Create custom event 'scroll_up'
  const down_event = new Event("scroll_down"); // Create custom event 'scroll_down'

  let scrollPos = 0; // Set initial scroll position

  window.addEventListener(
    "scroll",
    throttle(() => {
      // detects new state and compares it with the new one
      if (document.body.getBoundingClientRect().top > scrollPos) {
        window.dispatchEvent(down_event);
      } else {
        window.dispatchEvent(up_event);
      }
      // saves the new position for iteration.
      scrollPos = document.body.getBoundingClientRect().top;
    }, 500),
  );
  return true;
};

/**
 * Set a cookie
 * @param {String} name name of the cookie to be set
 * @param {String} value value of the cookie to be set
 * @param {Number} days number of days for which the cookie is to be set
 * @param {String} path path of the cookie to be set
 * @param {String} domain domain of the cookie to be set
 * @param {Boolean} secure true if the cookie is to be set on https only
 */
export const _set_cookie = (name, value, days, path, domain, secure) => {
  // This is done to not set any cookie in case of Europe or GDPR compliant continent
  // if(window.geoinfo && !window.geoinfo.notEU){
  //     return false;
  // }

  let expires = "";
  days = days !== undefined ? days : 30;
  const date = new Date();
  date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
  expires = `; expires=${date.toGMTString()}`;
  domain = (domain || document.location.host).split(":")[0]; // removing port
  path = path || document.location.pathname;
  // Removing file name, fix for IE11
  if (/\/.*\..*/.test(path)) {
    // if path contains file name
    path = path.split("/");
    path.pop();
    path = path.join("/");
  }
  document.cookie = `${name}=${value}${expires}${path ? `;path=${path}` : ""}${
    domain && domain != "localhost" ? `;domain=${domain}` : ""
  }${secure ? ";secure" : ""}`;
};

export const _checkUserStatus = callback => {
  if (_isCSR()) {
    if (window._user) {
      callback({ detail: window._user });
    } else {
      document.addEventListener("user.status", callback, false);
    }
  }
};

// Initialise setting hyp1 module
export const setHyp1Data = pwameta => {
  if (!_isCSR()) return false;
  // set hyp1 variable
  window.hyp1 = pwameta ? pwameta.hyp1 : "";
};

// Run handleReadMore module
export const handleReadMore = () => {
  if (!_isCSR()) return false;
  setTimeout(() => {
    const btn_more = Array.prototype.slice.call(document.querySelectorAll(".caption.text_ellipsis:not([read-more])"));
    btn_more.length > 0
      ? btn_more.forEach((element, index) => {
          const curheight = element.clientHeight;
          const clone_ele = element.cloneNode(true);

          clone_ele.style.height = "auto";
          clone_ele.style.display = "block";
          clone_ele.style.visibility = "hidden";
          clone_ele.classList.add("clone-caption");
          clone_ele.style.width = `${element.clientWidth}px`;
          document.querySelector("body").appendChild(clone_ele);

          try {
            if (
              (clone_ele.clientHeight > curheight + 5 || clone_ele.scrollHeight > curheight + 5) &&
              element.parentElement.nextElementSibling &&
              element.parentElement.nextElementSibling.childNodes[0]
            ) {
              element.parentElement.nextElementSibling.childNodes[0].style.display = "block";
              element.parentElement.nextElementSibling.childNodes[0].addEventListener("click", e => {
                if (
                  e.target.parentElement.previousElementSibling.querySelector(".caption").classList.contains("more")
                ) {
                  e.target.parentElement.previousElementSibling.querySelector(".caption").classList.remove("more");
                  e.target.classList.add("expand");
                  e.target.classList.remove("collapse");
                } else {
                  e.target.parentElement.previousElementSibling.querySelector(".caption").classList.add("more");
                  e.target.classList.remove("expand");
                  e.target.classList.add("collapse");
                }
              });
            } else {
              element.parentElement.nextElementSibling.childNodes[0].style.display = "none";
            }
          } catch (error) {}
          element.setAttribute("read-more", "handled");
          clone_ele.remove();
        })
      : null;
  }, 500);
};

/**
 *
 * @param {*} dfpAdConfig alaska ad config required to check topatf needs to enable or not.
 * @param {*} reqUrl location url to compare with.
 * @returns
 */
export const checkTopAtf = (dfpAdConfig, reqUrl) => {
  let isTopAtfShown = false;
  // let topAtfArr = [];
  const topAtfVal =
    dfpAdConfig && dfpAdConfig.topatf && dfpAdConfig.topatf.indexOf("empty") === -1 ? dfpAdConfig.topatf : undefined;
  if (topAtfVal) {
    const splatVars = getSplatsVars(reqUrl);
    const sectionData = splatVars.splats;
    if (sectionData && sectionData[0]) {
      if (topAtfVal.indexOf(sectionData[0]) > -1) {
        isTopAtfShown = true;
      }
    }
    // if (topAtfVal.indexOf("*:") > -1) {
    //   isTopAtfShown = true;
    // } else if (reqUrl === "/" && topAtfVal.indexOf("home") > -1) {
    //   isTopAtfShown = true;
    // } else {
    //   topAtfArr = topAtfVal.split(":");
    //   topAtfArr.forEach(ele => {
    //     if (reqUrl.indexOf(`/${ele}/`) > -1) {
    //       isTopAtfShown = true;
    //     }
    //   });
    // }
  }
  return isTopAtfShown;
};

// Filtered feeds from Alasaka JSON.
// _filterALJSON(object,[L1Seq,L2Seq]);
export const _filterALJSON = (header, tree) => {
  const ALSequqnce = [];
  const ALPWAConfig = header && header.alaskaData ? header.alaskaData : {};

  Object.keys(ALPWAConfig).map((key, index) => {
    if (
      ALPWAConfig[key] &&
      ALPWAConfig[key].label != undefined &&
      ALPWAConfig[key].label != null &&
      ALPWAConfig[key].label.toLowerCase() == tree[0].toLowerCase() &&
      tree.length > 0
    ) {
      const L1Seq = ALPWAConfig[key];
      if (tree.length > 1) {
        Object.keys(L1Seq).map((key, index) => {
          if (
            L1Seq[key].label != undefined &&
            L1Seq[key].label != null &&
            L1Seq[key].label.toLowerCase() == tree[1].toLowerCase()
          ) {
            const L2Seq = L1Seq[key];
            Object.keys(L2Seq).map((key, index) => {
              if (L2Seq[key].label != undefined && L2Seq[key].label != null) {
                ALSequqnce.push(L2Seq[key]);
              }
            });
          }
        });
      } else {
        ALSequqnce.push(ALPWAConfig[key]);
      }
    }
  });
  return ALSequqnce;
};

// _filterALJSON(object,[L1Seq,L2Seq]);
/**
 * A recursive function to get exact key node from ALASKA PANEL FEED
 * @param{Boolean} shouldExtractObj , form gettig excat node , removing '_' fields
 * @param{Array} tree , array of all key strings top to bottom
 * return Obj or Array based on shouldExtractObj
 */

let objAlaska = {};

const helperfunction = (ALPWAConfig, tree, i = 0) => {
  Object.keys(ALPWAConfig).map(key => {
    if (
      tree instanceof Array &&
      tree.length > i &&
      tree[i] !== undefined &&
      ALPWAConfig[key].label !== undefined &&
      ALPWAConfig[key].label != null &&
      ALPWAConfig[key].label.toLowerCase() === tree[i].toLowerCase()
    ) {
      // tree 1st key respective object returned
      objAlaska = ALPWAConfig[key];
      i++;
      if (tree.length > i) {
        helperfunction(objAlaska, tree, i);
      }
    }
  });

  return objAlaska;
};

export const _filterRecursiveALJSON = (header, tree, shouldExtractObj) => {
  const ALSequqnce = [];
  let tempObj = {};
  const ALObj = {};
  const ALPWAConfig = header && header.alaskaData ? header.alaskaData : {};
  try {
    tempObj = helperfunction(ALPWAConfig, tree);
    Object.keys(tempObj).map((key, index) => {
      if (tempObj[key].label != undefined && tempObj[key].label != null) {
        ALSequqnce.push(tempObj[key]);
      } else if (shouldExtractObj && key.charAt(0) == "_") {
        ALObj[key.substring(1)] = tempObj[key];
      }
    });
  } catch (e) {
    return undefined;
  }

  return shouldExtractObj ? ALObj : ALSequqnce;
};

// tech functions
export const _getUrlOfCategory = category => {
  if (category == "smartwatch" || category == "fitnessband") {
    return category;
  }
  if (category == "mobile") {
    category = "mobile-phones";
  } else if (category != null && category != undefined) {
    category += "s";
  }
  return category;
};

/**
 * Below method is used to manipulate URL for utm param
 * @param {*} url
 */
export const getURL = url => {
  let utmSrc = "";

  const pageMap = {
    articlelist: "readwidget",
    articleshow: "readwidget",
    videolist: "videowidget",
    photolist: "photowidget",
    home: "home",
    tech: "readwidget",
  };

  if (globalconfig.router.location.pathname.indexOf("amp_") > -1) {
    utmSrc = `utm_source=amp_most${pageMap[globalconfig.pagetype]}`;
  } else if (globalconfig.pagetype) {
    utmSrc = `utm_source=most${pageMap[globalconfig.pagetype]}`;
  }

  // utm_source=mostreadwidget&utm_medium=referral&utm_campaign=article1

  const queryStr = url && url.indexOf("?") > -1 ? url.split("?")[1] : "";
  let exctURL;
  if (queryStr && queryStr.length > 1) {
    exctURL = `${url}?${queryStr}&${utmSrc}`;
  } else {
    exctURL = `${url}?${utmSrc}`;
  }
  return exctURL;
};

export const _getBranchfeedCategory = category => {
  if (category == "smartwatch" || category == "fitnessband") {
    return category;
  }
  if (category == "mobile-phones") {
    category = "mobile";
  } else if (category != null && category != undefined) {
    category = category.substr(0, category.length);
  }
  return category;
};

export const _getfeedCategory = category => {
  if (category == "smartwatch" || category == "fitnessband") {
    return category;
  }
  if (category == "mobile-phones") {
    category = "mobile";
  } else if (category != null && category != undefined) {
    category = category.substr(0, category.length - 1);
  }
  return category;
};
export const _getCategory = (routepath, params) => {
  if (params && params.category && params.category != "") {
    return params.category;
  }
  const pathArray = routepath.split("/");
  let category;
  if (pathArray[2] && pathArray[2].indexOf("compare-") > -1)
    category = pathArray[2].substring(pathArray[2].indexOf("compare-") + 8);
  else category = pathArray[2];
  return category;
};
// set feed url to get content from diff domain as like tech
export const _apiBasepointUpdate = (router, condition2) => {
  const api_base = process.env.API_BASEPOINT;
  /* if (
    router &&
    router.location &&
    router.location.pathname &&
    process.env.SITE == "nbt" &&
    router.location.pathname.indexOf("/tech") > -1
  ) {
    if (api_base.indexOf("/pwafeeds") > -1) api_base = api_base.replace("/pwafeeds", "/gnfeeds"); // reset feed pattern for tech
    if (api_base.indexOf("/langdev") > -1 && api_base.indexOf("/langdev9338") < 0)
      api_base = api_base.replace("langdev", "langdev9338"); // reset local feed host id for tech
  } */
  // return if tech section exist otherwise nothing happen in base API
  return api_base;
};
export const _deferredDeeplink = (type, appdeeplink, msid, url, sectioninfo, campaignname) => {
  if (appdeeplink != null && appdeeplink != undefined) {
    let deeplink;
    if (type == "hrnav" || type == "serviceDrawer" || type == "article_end" || type == "topStrip") {
      deeplink = appdeeplink.openapp;
    } else if (type == "openapp" || type == "toparticle" || type == "news") {
      deeplink = appdeeplink.opennews;
    } else if (type && type.toLowerCase() == "photofeature") {
      deeplink = appdeeplink.openphotofeature;
    } else if (type && type.toLowerCase() == "recipe") {
      deeplink = appdeeplink.openrecipe;
    } else if (type == "pg" || type == "photoshow" || type == "photo" || type == "photopreview") {
      deeplink = appdeeplink.openphoto;
    } else if (type == "campaign") {
      deeplink = appdeeplink.openhtmlview;
    } else if (type == "campaignHome") {
      deeplink = appdeeplink.openhtmlview;
    } else if (type == "htmlview") {
      deeplink = appdeeplink.openhtmlview;
    } else if (type == "extphoto") {
      deeplink = appdeeplink.openextphoto;
    } else if (type == "section") {
      deeplink = appdeeplink.opensection;
    } else if (type == "videoshow") {
      deeplink = appdeeplink.openvideoshow;
    } else {
      deeplink = appdeeplink.openapp;
    }
    msid && deeplink ? (deeplink = deeplink.replace("%7BMSID%7D", msid)) : null;
    url && deeplink ? (deeplink = deeplink.replace("%7BURL%7D", encodeURIComponent(encodeURIComponent(url)))) : null;

    // let defDeeplink = `${appdeeplink.domain}/${appdeeplink.shortcode}?pid=${getPID}&c=${appdeeplink.c}&af_dp=${deeplink}`;

    let getPID = appdeeplink.pid[campaignname != undefined ? campaignname : type];

    // replace section inforamtion deffered deeplink from feed
    if (sectioninfo && deeplink) {
      // sectioninfo.sectionType ? deeplink = deeplink.replace("%7BsectionType%7D", sectioninfo.sectionType) : null
      sectioninfo.sectionName ? (deeplink = deeplink.replace("%7BsectionName%7D", sectioninfo.sectionName)) : null;
      sectioninfo.sectionID ? (deeplink = deeplink.replace("%7BsectionID%7D", sectioninfo.sectionID)) : null;
      sectioninfo.pid ? (getPID == null || getPID == undefined ? (getPID = sectioninfo.pid) : null) : null;
    }
    const defDeeplink = `${appdeeplink.domain}/${appdeeplink.shortcode}?pid=${getPID}&c=${appdeeplink.c}&af_dp=${deeplink}`;

    return defDeeplink;
  }
};

export const _deferredLinkAtListPage = (appdeeplink, msid, secName) => {
  if (appdeeplink != null && appdeeplink != undefined) {
    let deeplink = appdeeplink.openarticlelist;
    let defaultUrl = appdeeplink.articlelistdefaultUrl;
    let getPID = appdeeplink.pid && appdeeplink.pid["articlelist"];

    msid && deeplink ? (deeplink = deeplink.replace("%7BMSID%7D", msid)) : null;
    secName ? (deeplink = deeplink.replace("%7BSECTION_NAME%7D", secName)) : null;
    defaultUrl ? (defaultUrl = defaultUrl.replace("<MSID>", msid)) : null;

    defaultUrl && deeplink
      ? (deeplink = deeplink.replace("%7BFeed_Path%7D", encodeURIComponent(encodeURIComponent(defaultUrl))))
      : null;

    const defDeeplink = `${appdeeplink.domain}/${appdeeplink.shortcode}?pid=${getPID}&c=${appdeeplink.c}&af_dp=${deeplink}`;
    return defDeeplink;
  }
};

export const animate_btn_App = () => {
  // Check is button visible or not
  let ele = document.querySelector('[data-attr="btn_openinapp"]');
  if (ele) {
    ele.style.left = "-175px";
    ele.style.display = "none";
    window.addEventListener("scroll_down", function() {});

    window.addEventListener("scroll_up", function() {
      ele.style.display = "block";
      ele.style.left = "0";
    });
  }
};

/**
 * get value of frmapp
 * @param {object} props props from routes for routing
 * */
export const _isFrmApp = router => {
  let isFrmapp = false;
  const queryparameter = router && router.location ? router.location.search : "";
  if (queryparameter && queryparameter.toLowerCase().indexOf("frmapp=yes") > -1) {
    isFrmapp = true;
  }
  return isFrmapp;
};

export const _isOPPO = router => {
  let isoppUrl = false;
  const queryparameter = router && router.location ? router.location.search : "";
  if (
    queryparameter &&
    queryparameter.toLowerCase().indexOf("utm_source=oppo") > -1 &&
    queryparameter.toLowerCase().indexOf("frmapp=yes") > -1
  ) {
    isoppUrl = true;
  }
  return isoppUrl;
};
export const _isVivo = router => {
  let isvivoUrl = false;
  const queryparameter = router && router.location ? router.location.search : "";
  if (
    queryparameter &&
    queryparameter.toLowerCase().indexOf("utm_source=vivo") > -1 &&
    queryparameter.toLowerCase().indexOf("frmapp=yes") > -1
  ) {
    isvivoUrl = true;
  }
  return isvivoUrl;
};
export const _isSamsung = router => {
  let isSamsungUrl = false;
  const queryparameter = router && router.location ? router.location.search : "";
  if (
    queryparameter &&
    queryparameter.toLowerCase().indexOf("utm_source=samsung") > -1 &&
    queryparameter.toLowerCase().indexOf("frmapp=yes") > -1
  ) {
    isSamsungUrl = true;
  }
  return isSamsungUrl;
};
/**
 * get config file data as per SITE
 * @param {String} additionalPath for additional folder in common/
 * */
export const _getStaticConfig = additionalPath => {
  // console.log(siteConfig.wapsitename + '-------------------------');
  if (additionalPath) {
    if (additionalPath == "cssConfig") {
      return cssConfig;
    }
    return footerSiteConfig;
  }
  return siteConfig;
};

/**  Bad escape webmaster issue * */
export const filterDescription = description => {
  const regex = /[\\]/g;
  const desc = description && description.replace(regex, "");
  return desc;
};
/**
 * get config file data as per SITE
 * @param {Object} options for additional folder in common/
 * */
export const _setStaticConfig = options => {
  if (typeof options === "object") {
    siteConfig.imageconfig.thumbid = options.imageconfig.thumbid;
  }
};

export const LoadingComponent = ({ isLoading, error, pastDelay }) => {
  try {
    if (isLoading && pastDelay) {
      return <Loader />;
    }
  } catch (ex) {
    console.log(ex);
  }

  return null;
};

export function isMobilePlatform(param) {
  if (param) {
    return process.env.PLATFORM;
  }
  return process.env.PLATFORM == "mobile";
}

export function isTechSite() {
  if (
    process.env.SITE == "nbt" ||
    process.env.SITE == "vk" ||
    process.env.SITE == "mt" ||
    process.env.SITE == "tlg" ||
    process.env.SITE == "tml" ||
    process.env.SITE == "mly" ||
    process.env.SITE == "eisamay"
  ) {
    return true;
  }
  return false;
}

// to check if it is an international site from geo location cookie
export function getCountryCode() {
  if (typeof window !== "undefined") {
    let geoData = "";
    if (_getCookie("geo_data")) {
      geoData = JSON.parse(_getCookie("geo_data"));
    }
    if (process.env.SITE == "nbt" || process.env.SITE == "mly") {
      if (geoData.CountryCode == "AE") {
        return geoData.CountryCode;
      } else {
        return false;
      }
    } else if (process.env.SITE == "mt") {
      if (geoData.CountryCode == "US") {
        return geoData.CountryCode;
      } else {
        return false;
      }
    } else if (process.env.SITE == "eisamay") {
      if (geoData.CountryCode == "BD" || geoData.CountryCode == "US") {
        return geoData.CountryCode;
      } else {
        return false;
      }
    }
  }
}
// to check if it is an international site from router location
export function isInternationalUrl(router) {
  const pathName = router && router.location && router.location.pathname;
  if (process.env.SITE == "nbt" || process.env.SITE == "mly") {
    if (pathName == "/gulf") {
      return true;
    } else {
      return false;
    }
  } else if (process.env.SITE == "mt") {
    if (pathName == "/us") {
      return true;
    } else {
      return false;
    }
  } else if (process.env.SITE == "eisamay") {
    if (pathName == "/us" || pathName == "/bangladesh") {
      return true;
    } else {
      return false;
    }
  }
}

/**
 * browser _sessionStorage implementation
 * @param {String} key session object key
 * @param {String} value session object value
 * */
export const _sessionStorage = () => {
  if (!_isCSR()) return false;

  const _set = (key, value) => window.sessionStorage.setItem(key, value);

  const _get = key => window.sessionStorage.getItem(key);

  const _remove = key => window.sessionStorage.removeItem(key);

  return {
    set: _set,
    get: _get,
    remove: _remove,
  };
};

/**
 * Personalize feed parser
 * @param {Array} feedData contains data which need to be filtered
 * */

export const personalizedFeedParser = (feedData, indexStarting) => {
  if (typeof feedData === "undefined" || !feedData || feedData.length == 0) {
    return [];
  }
  const tempArr = [];
  for (let i = 0; i < feedData.length; i++) {
    let itemObj = {};
    // Item object other than ad
    if (feedData[i].oItems) {
      // Item containing single story type defined
      if (feedData[i].oItems.length == 1 && feedData[i].oItems[0].tn == "html") {
        feedData[i].oItems[0].tn == "news";
        itemObj = feedData[i].oItems[0];
        // tempArr.push(feedData[i].oItems[0]);
      } else if (feedData[i].oItems.length == 1 && feedData[i].oItems[0].tn) {
        itemObj = feedData[i].oItems[0];
        // tempArr.push(feedData[i].oItems[0]);
      } else if (feedData[i].oItems.length > 1 && feedData[i].oItems[0].tn && feedData[i].oItems[0].tn == "video") {
        itemObj.tn = "videolist";
        itemObj.items = feedData[i].oItems;
        // tempArr.push(videoObj);
      } else if (feedData[i].oItems.length > 1 && feedData[i].oItems[0].tn && feedData[i].oItems[0].tn == "photo") {
        itemObj.tn = "photolist";
        itemObj.items = feedData[i].oItems;
        // tempArr.push(photoObj);
      } else if (feedData[i].oItems.length > 1 && feedData[i].oItems[0].tn) {
        itemObj = feedData[i].oItems;
        // tempArr.push(feedData[i].oItems);
      }

      // Keeping nmeta key in case of organic items, which needs to be passed to CTN via method
      // itemObj.nmeta = itemObj.tn != "ad" ? feedData[i].nmeta : undefined;
      itemObj.nmeta = feedData[i].nmeta;
    }
    // Item object containing ad
    else if (feedData[i].items) {
      // itemObj.tn = "ctnad";
      itemObj = { tn: "ctnad", ...feedData[i] };
      // tempArr.push(feedData[i])
    }
    itemObj.index = indexStarting ? indexStarting + i : i;
    tempArr.push(itemObj);
  }
  return tempArr;
};

/**
 * modifyAdsOnScrollView implementation is a implementation
 * of ad refresh when ads are in view
 * */
export const modifyAdsOnScrollView = reset => {
  if (!_isCSR()) return false;
  // let lastScrolledPostion = 0;
  const adObj = {
    atf: `/7176/${siteConfig.ads.dfpads.adSiteName}_Mweb/${siteConfig.ads.dfpads.adSiteShortName}_Mweb_AcrossSite/${siteConfig.ads.dfpads.adSiteShortName}_Mweb_AcrossSite_ATF`,
    mrec: `/7176/${siteConfig.ads.dfpads.adSiteName}_Mweb/${siteConfig.ads.dfpads.adSiteShortName}_Mweb_AcrossSite/${siteConfig.ads.dfpads.adSiteShortName}_Mweb_AcrossSite_Mrec1`,
    mrec2: `/7176/${siteConfig.ads.dfpads.adSiteName}_Mweb/${siteConfig.ads.dfpads.adSiteShortName}_Mweb_AcrossSite/${siteConfig.ads.dfpads.adSiteShortName}_Mweb_AcrossSite_Mrec2`,
    fbn: `/7176/${siteConfig.ads.dfpads.adSiteName}_Mweb/${siteConfig.ads.dfpads.adSiteShortName}_Mweb_AcrossSite/${siteConfig.ads.dfpads.adSiteShortName}_Mweb_AcrossSite_FBN`,
    default: `/7176/${siteConfig.ads.dfpads.adSiteName}_Mweb/${siteConfig.ads.dfpads.adSiteShortName}_Mweb_AcrossSite/${siteConfig.ads.dfpads.adSiteShortName}_Mweb_AcrossSite_ATF`,
  };

  if (!reset) {
    // Attach a scroll event with throttle
    window.addEventListener("scroll", e => {
      try {
        throttle(
          (() => {
            // Pause CTN
            // let adsContainers = document.querySelectorAll('.ad1, [data-plugin=ctn]');
            const adsContainers = document.querySelectorAll(".ad1");
            for (let i = 0; i < adsContainers.length; i++) {
              if (
                // skip empty adContainers
                adsContainers[i] &&
                adsContainers[i].innerHTML != "" &&
                // skip fbn
                adsContainers[i].classList &&
                adsContainers[i].classList.value.indexOf("fbn") == -1 &&
                // skip if attribute 'ad-direct' present
                !adsContainers[i].getAttribute("ad-direct")
              ) {
                // then check element in view
                if (elementInView(adsContainers[i], false)) {
                  // then check conditions for ad Refresh
                  if (
                    // skip refresh if no data-id (means no ad-slot)
                    (adsContainers[i].getAttribute("data-id") != "" || adsContainers[i].getAttribute("id") != "") &&
                    // skip if already refreshed
                    adsContainers[i].getAttribute("ad-refresh") == undefined &&
                    // skip if adInViewCount == 0 , used to check if rendered ad doesn't get refreshed for first time
                    adsContainers[i].getAttribute("adInViewCount") != 0
                  ) {
                    // If element has attribute id, we assumimg it is as ctn ad
                    if (
                      adsContainers[i].getAttribute("data-plugin") === "ctn" &&
                      adsContainers[i].getAttribute("id") != "" &&
                      colombia.refresh
                    ) {
                      colombia.refresh(adsContainers[i].getAttribute("id"));
                    }
                    // Call recreate dfp ads
                    else if (
                      adsContainers[i].getAttribute("ad-recreate") == undefined &&
                      adsContainers[i].classList[1] != ""
                    ) {
                      adsContainers[i].setAttribute("ad-recreate", true);
                      recreateAds([adsContainers[i].classList[1]], null, adObj);
                    }
                    // Call refresh dfp ads
                    else {
                      refreshAds([adsContainers[i].getAttribute("data-id")], true);
                    }
                  }

                  // set ad-refresh attribute true
                  adsContainers[i].setAttribute("ad-refresh", true);
                  // set adInViewCount attribute 1
                  adsContainers[i].setAttribute("adInViewCount", 1);
                }
                // if not inview remove ad-refresh
                else if (adsContainers[i]) {
                  adsContainers[i].removeAttribute("ad-refresh");
                }
              }
            }

            // if(window && window.pageYOffset > window.innerHeight
            //     && (Math.floor(window.pageYOffset/window.innerHeight) % 2) == 0 ){
            //   if(Math.abs(lastScrolledPostion - window.pageYOffset) > window.innerHeight){
            //     //Call refresh dfp ads
            //     Ads_module.refreshAds(['fbn'],true);
            //   }
            //   lastScrolledPostion = window.pageYOffset;
            // }
          })(),
        );
      } catch (e) {}
    });
  }

  // Special handling for fbn , refresh after 30 sec
  if (reset) clearInterval(window.fixedAdInterval);
  window.fixedAdInterval = setInterval(() => {
    const fbnAdContainer = document.querySelector(".ad1.fbn");
    if (fbnAdContainer && !fbnAdContainer.getAttribute("ad-direct")) {
      if (fbnAdContainer.getAttribute("ad-recreate") == undefined) {
        fbnAdContainer.setAttribute("ad-recreate", true);
        recreateAds(["fbn"], null, adObj);
      }
      // Call refresh dfp ads
      else {
        refreshAds(["fbn"], true);
      }
    }
  }, 30000);
};

export const truncateStr = (passedString, length) => {
  if (typeof passedString === "string" && parseInt(length)) {
    return passedString.slice(0, length);
  }
  // Return empty string as default value
  return "";
};

export const isLoggedIn = () => {
  if (typeof window !== "undefined") {
    return Boolean(_getCookie("ssoid") && window._user != null);
  }
  return false;
};

// const findObjectBySectionId = function(data, sectionId) {
//   const subsections = [];

//   for (const key in data) {
//     if (data.hasOwnProperty(key)) {
//       if (data[key] && typeof data[key] === "object") {
//         const sectionData = data[key];
//         if (sectionData.msid != sectionId) {
//           return findObjectBySectionId(sectionData, sectionId);

//           // for (const key2 in sectionData) {
//           //   if (sectionData.hasOwnProperty(key2)) {
//           //     const objectVal = sectionData[key2];
//           //     if (
//           //       objectVal &&
//           //       typeof objectVal === "object" &&
//           //       objectVal.msid &&
//           //       objectVal.weblink
//           //     ) {
//           //       subsections.push(objectVal);
//           //     }
//           //   }
//           // }
//           // break;
//         }
//         return sectionData;
//       }
//     }

//     // if (sectionExist) {
//     //   return foundLabel;
//     // }
//   }
// };

/**
 * parse object and returns first level info
 * @data Object
 * Object format is Alaska data
 */

export const getAlaskaSections = (data, includeParentSection) => {
  try {
    // console.log("datadata", data);
    const arrayNavData = [];
    const parentnavObj = {};
    if (data && typeof data === "object" && data.constructor === Object) {
      for (const key in data) {
        if (data.hasOwnProperty(key)) {
          const sectionData = data[key];
          if (sectionData && sectionData.weblink && sectionData.label) {
            arrayNavData.push(sectionData);
          } else {
            parentnavObj[key] = sectionData;
          }
        }
      }

      // console.log("datadata", arrayNavData);

      if (includeParentSection && arrayNavData.length > 0) {
        // This code only include parentsection for Mobile NavBar
        if (data.class) {
          parentnavObj.class = `parent ${data.class}`;
        }
        arrayNavData.unshift(parentnavObj);
      }
    }
    return arrayNavData;
  } catch (ex) {
    console.log(ex);
  }
};

const processData = (data, searchItems, filterBy) => {
  try {
    // console.log("filterBy", filterBy, searchItems);
    if (searchItems.length > 0) {
      const searchItemArr = searchItems.splice(0, 1);
      let searchItem = searchItemArr[0];

      if (filterBy === "label") {
        searchItem = `"label":"${searchItem}"`; // this was changed to avoid any other occurence of same id in other places like link.
      } else if (
        /* As Ids are different in msid and overidelink , searching in alaskadata not working .
        So for now we have made a slight improvement of searchAll in SectionHeader , for considering urls & msid */
        filterBy === "searchAll"
      ) {
        searchItem = searchItem;
      } else {
        searchItem = `"msid":"${searchItem}"`;
      }

      let clonedCopy = JSON.parse(JSON.stringify(data)); // deep copying object to varaible clonedCopy
      const objString = JSON.stringify(clonedCopy).toLowerCase(); // return string of object, used for searching.
      const position = objString.indexOf(searchItem.toLowerCase()); // returns position of search, for eg : 238
      if (position !== -1) {
        const subStr = objString.substring(0, position); // returns substring before the search position
        const arrSubstr = subStr.split(',"c'); // converts string to array based on some generic keyword like ,"C
        const arrayLastVal = arrSubstr.pop(); // return last value from array
        const finalArr = arrayLastVal.split('":{'); // return array, like:  ["7-1-2", ""label":"உலக கோப்பை கிரிக்கெட்","msid":""]
        const finalIndexesARR = finalArr[0].split("-"); // return array like :  ["7", "1", "2"]
        const arrWithActualKeys = []; // stores actual keys like ["C7", "C7-1", "C7-1-2"]
        let keys = "";
        finalIndexesARR.forEach((item, index) => {
          keys += index == 0 ? `C${item}` : `-${item}`;
          arrWithActualKeys.push(keys);
        });

        arrWithActualKeys.forEach(item => (clonedCopy = clonedCopy[item]));

        if (searchItems.length > 0) {
          return processData(data, searchItems, filterBy);
        }
        // console.log("clonedCopy", clonedCopy);
        // const sectionList = getAlaskaSections(clonedCopy);
        return clonedCopy; // returns the object of searched string/id
      }
      return "";
    }
  } catch (ex) {
    console.log("Custom error:", ex);
  }
};

/**
 * @searchItems Items to be searched, for more than one provide array
 * @data Object of Alaska
 * @filterBy whether filter is by msid or label
 */

export const filterAlaskaData = (data, searchItems, filterBy) => {
  const navData = {};
  // searchItems = "711517871111";
  if (data && typeof data === "object" && data.constructor === Object && searchItems) {
    if (typeof searchItems === "string") {
      // if accidently passed string instead of array
      searchItems = [searchItems];
    }
    return processData(data, [...searchItems], filterBy);
  }
  // console.log("arrayNavData", arrayNavData);
  return navData;
};

export const generateUrl = item => {
  if (!item) {
    return "";
  }
  const templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
  const seolocation = item.seolocation ? `${item.seolocation}/` : "";
  const url = `/${seolocation}${templateName}/${item.id}.cms`;

  return item.override ? item.override : url;
};

/*
 * @pwaMeta : Object Meta Data
 * @dispatch: Redux method
 * setParentId: Action creator Method
 * */

export const updateConfig = (pwaMeta, dispatch, setParentId) => {
  // const parentId = isMobilePlatform() ? pwaMeta && pwaMeta.parentidnew : pwaMeta && pwaMeta.navsecid; // need to cross-check value of parentid for mobile

  const parentId = pwaMeta && pwaMeta.parentidnew;
  const subsec1 = (pwaMeta && pwaMeta.subsec1) || "";
  const secId = (pwaMeta && pwaMeta.sectionid) || "";
  const hierarchylevel = (pwaMeta && pwaMeta.hierarchylevel) || "";

  dispatch(setParentId(parentId, subsec1, secId, hierarchylevel));
};

export const extractMsidFromUrl = longurl => {
  const result = longurl.match(/.*\/([^.]+)/);
  if (result !== null && result[1]) {
    return result[1];
  }
  return "";
};
/*
 * Helper function to disable scroll
 * */
export function disableBodyScroll() {
  const $body = document.getElementsByTagName("body")[0];
  let classList = $body.className;
  const classListArr = classList ? classList.split(" ") : [];
  if (classListArr.indexOf("disable-scroll") !== -1) {
    return;
  }
  classListArr.push("disable-scroll");
  classList = classListArr.join(" ");
  $body.className = classList;
}

/*
 * Helper function to enable scroll
 * */
export function enableBodyScroll() {
  const $body = document.getElementsByTagName("body")[0];
  let classList = $body.className;
  const classListArr = classList ? classList.split(" ") : [];
  const disableArrayIndex = classListArr.indexOf("disable-scroll");
  if (disableArrayIndex === -1) {
    return;
  }
  classListArr.splice(disableArrayIndex, 1);
  classList = classListArr.join(" ");
  $body.className = classList;
}

export const customEventPolyfill = () => {
  /* istanbul igonore if */
  if (typeof window.CustomEvent === "function") {
    return false;
  }

  const CustomEvent = (event, params) => {
    const paramsObj = params || {
      bubbles: false,
      cancelable: false,
      detail: undefined,
    };
    const evt = document.createEvent("CustomEvent");
    evt.initCustomEvent(event, paramsObj.bubbles, paramsObj.cancelable, paramsObj.detail);
    return evt;
  };

  CustomEvent.prototype = window.Event.prototype;
  window.CustomEvent = CustomEvent;
};

export const isIe11 = () => {
  if (typeof window === "undefined") {
    return false;
  }

  const ua = window.navigator.userAgent;
  const trident = ua.indexOf("Trident/"); // IE 11

  return trident > 0;
};

export function checkIsFbPage(url) {
  if (
    url.indexOf("/fb_") >= 0 ||
    url.indexOf("/fbdefault") >= 0 ||
    url.indexOf("/fbnews") >= 0 ||
    url.indexOf("/fbphotos") >= 0 ||
    url.indexOf("/fbvideos") >= 0
  ) {
    return true;
  }
  return false;
}

export function isTechHomePage(pathName) {
  // gadget-news/articlelist/2499221.cms is the tech home page path for MT
  if (
    pathName == "/tech" ||
    pathName == `/gadget-news/articlelist/${siteConfig.pages.tech}.cms` ||
    pathName == `/gadget-news/amp_articlelist/${siteConfig.pages.tech}.cms` ||
    pathName == `/tech/articlelist/${siteConfig.pages.tech}.cms` ||
    pathName == `/tech/amp_articlelist/${siteConfig.pages.tech}.cms`
  ) {
    return true;
  }
  return false;
}
// for business section return
export function isBusinessSection(pathName) {
  const siteName = process.env.SITE;
  if (siteName === "nbt" && pathName && (pathName.includes("/business/") || pathName.includes("/business-news/"))) {
    return true;
  }
  return false;
}

export function isTechPage(path) {
  let pathName = "";
  if (typeof document !== "undefined") {
    if (document.location && document.location.pathname) {
      pathName = document.location.pathname;
    }
  } else if (path) {
    // SSR case
    pathName = path;
  }

  if (pathName) {
    const pathNameArr = pathName.split("/");
    if (pathNameArr && pathNameArr[1] == "tech") {
      return true;
    }
    if (pathName.includes("/tech/") || pathName.includes("/gadget-news/")) {
      // made this to pass video/tech/videolist/20104392.cms where the pathname not starts with /tech
      // /gadget-news/ pattern used in MT
      return true;
    }
  }

  return false;
}

/**
 * Below function is used to check nested properties exists on object
 * @param {*} obj
 * @param {*} prop
 * e.g. (parentSection, "recommended.trendingtopics.0.items") for arguments-
 * If we need to check items property exists on parentsection.
 */
export function hasValue(obj, prop, defaultVal = null) {
  prop = prop.replace(/\[(\w+)\]/g, ".$1"); // convert indexes to properties
  prop = prop.replace(/^\./, ""); // strip a leading dot
  const value = prop.split(".").reduce((o, key) => (typeof o === "undefined" || o === null ? o : o[key]), obj);
  if (value) return value;
  return defaultVal;
}

// Original in scbmaster
export function deleteNode(obj, labelToKeep, labelToDelete) {
  if (obj) {
    Object.keys(obj).map(item => {
      if (item !== "name" && item !== "weblink" && obj[item] && obj[item].label && obj[item].label !== labelToKeep) {
        delete obj[item];
      }
      if (labelToDelete && obj[item] && obj[item].label && obj[item].label === labelToKeep) {
        Object.keys(obj[item]).map(it => {
          if (obj[item][it] && obj[item][it].label && obj[item][it].label === labelToDelete) {
            delete obj[item][it];
          }
        });
      }
    });
  }
}

export function _loadsvgsprite(header) {
  let spriteversion = "01122020";
  if (header && header.alaskaData) {
    const vesrionlabel = filterAlaskaData(header.alaskaData, ["pwaconfig", "sprite"], "label");
    spriteversion = vesrionlabel && vesrionlabel._svgversion ? vesrionlabel._svgversion : "01122020";
  }

  const basePath = process.env.API_BASEPOINT;

  const spritepath = `${basePath}/svgsprite/${spriteversion}.cms`;

  if (typeof window !== "undefined") {
    fetch(spritepath)
      .then(res => res.text())
      .then(data => {
        document.getElementById("svgsprite").innerHTML = data;
      });
  }
}

export function filterInitialState(initialState) {
  const appState = initialState && initialState.app;
  // Deleting Rhs data from initial state because we are already fetching in CSR too.
  if (appState && appState.rhsData) {
    delete appState.rhsData;
  }
  if (appState && appState.election && appState.election.cube && appState.election.cube.show !== "true") {
    delete appState.election.cube;
  }
  if (
    appState &&
    appState.election &&
    appState.election.exitpoll &&
    appState.election.exitpoll.showwidget &&
    appState.election.exitpoll.showwidget !== "true"
  ) {
    delete appState.election.exitpoll;
  }
  if (
    appState &&
    appState.election &&
    appState.election.result &&
    appState.election.result.showwidget &&
    appState.election.result.showwidget !== "true"
  ) {
    delete appState.election.result;
  }
  if (appState && appState.prebid) {
    delete appState.prebid;
  }
  // if (appState && appState.budget) {
  //   delete appState.budget;
  // }
  if (appState && appState.cubead) {
    delete appState.cubead;
  }
  if (appState && appState.adconfig && appState.adconfig.fan) {
    delete appState.adconfig.fan;
  }
  if (appState && appState.adconfig && appState.adconfig.fanads) {
    delete appState.adconfig.fanads;
  }
  if (appState && appState.adconfig && appState.adconfig.fbnsize) {
    Object.keys(appState.adconfig.fbnsize).forEach(key => {
      if (key !== process.env.SITE) delete appState.adconfig.fbnsize[key];
    });
  }
  if (appState && appState.adconfig && appState.adconfig.campaign && appState.adconfig.campaign.mobile) {
    Object.keys(appState.adconfig.campaign.mobile).forEach(key => {
      if (key !== process.env.SITE) delete appState.adconfig.campaign.mobile[key];
    });
  }

  if (initialState.pointstablecard) {
    delete initialState.pointstablecard;
  }
  // Commenting as it is causing page jump after hydration
  // TODO: ASk why newsbrief deleted from initialState
  // if (initialState.newsbrief) {
  //   delete initialState.newsbrief;
  // }
}

export function isProdEnv(getEnv) {
  if (getEnv) {
    return process.env.DEV_ENV;
  }
  return process.env.DEV_ENV == "production";
}
/**
 *  @description Helper function which parses location and returns page type
 * @param {String} location pathname in string to be parsed
 * @param {Boolean} forAds if need page mappings for ads
 * @param {Boolean} unparsed if need unparsed location
 * @returns {String} Page type in string
 */
export function getPageTypeUpdated(location, forAds, unparsed) {
  const pageTypeMappings = {
    photoarticlelist: "photolist",
    photomazza: "photolist",
    photogallery: "photolist",
    electionlist: "articlelist",
    reviews: "articlelist",
    default: "home",
    amp_default: "home",
  };
  const adsPageType = {
    videolist: "articlelist",
    photolist: "articlelist",
    moviereview: "articleshow",
    movieshow: "articleshow",
    topics: "others",
  };

  // Normal urls eg articleshow/1234.cms
  const regexIfId = /\/(\w+)\/\d+.cms/;
  // Old urls which were in xslt for list pages, eg https://telugu.samayam.com/photo-gallery/cinema/photolist/msid-47783654,curpg-5.cms
  const regexMsidWithCurpg = /\/(\w+)\/msid-\d+,curpg-\d+.cms/;
  // for eg: election.cms
  const regex = /\/(\w+)\.cms/;
  let pageType = "home";
  let result = "";
  if (regexIfId.exec(location)) {
    // location contains {id}.cms, for eg: articleshow/1234.cms
    // exec returns the array of strings if matches the pattern, null otherwise
    result = regexIfId.exec(location);
  } else if (regex.exec(location)) {
    // location dont have {id}.cms, for eg: election.cms
    result = regex.exec(location);
  } else if (regexMsidWithCurpg.exec(location)) {
    // Old urls which were in xslt for list pages, eg https://telugu.samayam.com/photo-gallery/cinema/photolist/msid-47783654,curpg-5.cms
    result = regexMsidWithCurpg.exec(location);
  }
  if (result && result[1]) {
    pageType = result[1];
  }

  if (location && location.includes(",picid-")) {
    if (location.includes("/photoshow/")) {
      pageType = "photoshow";
    } else if (location.includes("/amp_photoshow/")) {
      pageType = "amp_photoshow";
    }
  } else if (
    !result &&
    location &&
    (location.includes("/topics/") ||
      location.includes("/landing/") ||
      location.includes("/glossary/") ||
      location.includes("/cricketer/") ||
      location.includes("/politician/"))
  ) {
    pageType = "topics";
  }
  if (location.includes("/newsbrief/") || location.includes("/newsbrief.cms")) {
    pageType = "newsbrief";
  }

  if (
    location.includes("/landing.cms") ||
    location.includes("/glossary.cms") ||
    location.includes("/cricketer.cms") ||
    location.includes("/politician.cms")
  ) {
    pageType = "topics";
  }
  if (location.includes("/newsbrief/") || location.includes("/newsbrief.cms")) {
    pageType = "newsbrief";
  }

  // Growthrx scenario where photolist / videolist  other levels of specificity is required
  if (unparsed) {
    return pageType;
  }

  if (pageType && pageTypeMappings[pageType]) {
    pageType = pageTypeMappings[pageType];
  }
  if (pageType && forAds && adsPageType[pageType]) {
    pageType = adsPageType[pageType];
  }

  // Need to validate if below case is required, else can remove this.
  // As I did'nt find its relvance anywhere.

  if (
    pageType == "others" &&
    location.indexOf("/tech/") > -1 &&
    location.indexOf("/tech/reviews.cms") < 0 &&
    location != "/tech" &&
    location != "/tech/" &&
    location != "/tech/default.cms"
  ) {
    pageType = "tech";
  }

  return pageType;
}

export function getAlaskaLinks(data) {
  const arrayLinks = [];
  if (data && typeof data === "object" && data.constructor === Object) {
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        const sectionData = data[key];
        if (sectionData && sectionData._icontext && sectionData._destionationurl) {
          arrayLinks.push({
            link: sectionData._destionationurl,
            label: sectionData._icontext,
            class: sectionData.class,
          });
        }
      }
    }
  }
  return arrayLinks;
}
export function shouldTPRender() {
  const siteName = process.env.SITE;
  if (_isCSR() && window.geoinfo && window.geoinfo.isGDPRRegion) {
    return false;
  }
  return typeof tpConfig && siteName && tpConfig.channels && tpConfig.channels.includes(siteName);
}

export function loadInstagramJS() {
  const scripts = document.createElement("script");
  scripts.setAttribute("src", "https://www.instagram.com/embed.js");
  document.head.appendChild(scripts);
}

export function loadTwitterJS() {
  const scripts = document.createElement("script");
  scripts.setAttribute("src", "https://platform.twitter.com/widgets.js");
  document.head.appendChild(scripts);
}

export function handleFBembed() {
  // fbscript if present collect all fb-post and render

  if (document.getElementById("fbScript") && typeof FB === "object" && FB) {
    const emframe = document.querySelectorAll(".fb-post");
    emframe.forEach((item, index) => {
      item.setAttribute("data-href", decodeURIComponent(item.getAttribute("data-href")));
    });

    window.fbAsyncInit();
    return false;
  }

  const fbScriptStr = `var emframe = document.querySelectorAll('.fb-post');
    emframe.forEach(function(item,index){
        item.setAttribute('data-href', decodeURIComponent(item.getAttribute('data-href')))
  });
  
    window.fbAsyncInit = function() {
          FB.init({
            xfbml      : true,
            version    : 'v2.12'
          });
        };
        (function(d, s, id){
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {return;}
          js = d.createElement(s); js.id = id;
          js.src = "https://connect.facebook.net/en_US/sdk.js";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
      `;

  const fbScript = document.createElement("script");
  fbScript.setAttribute("id", "fbScript");
  fbScript.innerHTML = fbScriptStr;

  document.body.appendChild(fbScript);
}

const isElectionPage = pathname => {
  const electionPageArray = ["/exitpoll/", "/results/", "/electoralmap/", "/candidates/"];
  electionPageArray.forEach(page => {
    if (pathname.indexOf(page) > -1) {
      return true;
    }
  });
  return false;
};

export const getElectionConfigFromData = ({ electionConfig, pathname, pagetype, params }) => {
  // not a good way but still need to think about this as of now
  if (!pathname) {
    pathname = "";
  }
  const msid = params && params.msid ? params.msid : undefined;
  const config = { showWidget: false };
  // handle indexof array
  if (
    electionConfig &&
    electionConfig.exitpoll &&
    electionConfig.exitpoll.showwidget === "true" &&
    (electionConfig.exitpoll.pages.indexOf(pagetype) > -1 || electionConfig.exitpoll.msids.indexOf(msid) > -1)
  ) {
    config.showWidget = true;
    // use this to decide when to set config - election
    // for all of the election related pages, don't set any config as it is called there itself
    config.shouldSetConfig = !isElectionPage(pathname);
    config.widgetType = "exitpoll";
    // config.widgetType = "result";
    config.widgetUrl = electionConfig.exitpoll.feedurl;
    // config.widgetUrl = "https://toibnews.timesofindia.indiatimes.com/electionfeed/dlfeb2020/dl_al_pr_2020_mob.htm";
    config.stateName = electionConfig.exitpoll.state || "bihar";
  } else if (
    electionConfig &&
    electionConfig.result &&
    electionConfig.result.showwidget === "true" &&
    (electionConfig.result.pages.indexOf(pagetype) > -1 || electionConfig.result.msids.indexOf(msid) > -1)
  ) {
    config.showWidget = true;
    config.widgetType = "result";
    config.shouldSetConfig = !isElectionPage(pathname);
    config.widgetUrl = electionConfig.result.feedurl;
    config.stateName = electionConfig.result.state || "bihar";
  }
  return config;
};

export const checkWidgetToRender = props => {
  const { election, pagetype, msid } = props;
  let type;
  if (
    typeof election != "undefined" &&
    election.result &&
    election.result.showwidget &&
    election.result.showwidget === "true" &&
    ((election.result.pages && election.result.pages.indexOf(pagetype) > -1) ||
      (election.result.msids && election.result.msids.indexOf(msid) > -1) ||
      (_isCSR() &&
        election.result.csrpages &&
        election.result.csrpages.indexOf(pagetype) > -1 &&
        election.result.csrsites.indexOf(process.env.SITE) > -1))
  ) {
    type = "results";
    return type;
  }

  if (
    typeof election != "undefined" &&
    election.exitpoll &&
    election.exitpoll.showwidget &&
    election.exitpoll.showwidget == "true" &&
    ((election.exitpoll.pages && election.exitpoll.pages.indexOf(pagetype) > -1) ||
      (election.exitpoll.msids && election.exitpoll.msids.indexOf(msid) > -1) ||
      (_isCSR() &&
        election.exitpoll.csrpages &&
        election.exitpoll.csrpages.indexOf(pagetype) > -1 &&
        election.exitpoll.csrsites.indexOf(process.env.SITE) > -1))
  ) {
    type = "exitpoll";
    return type;
  }

  return type;
};

export function getBuyLink({ data, type, url, tag, price, title }) {
  let buyURL = "";
  // if (!data) {
  //   return "";
  // }

  if (!tag) {
    tag = siteConfig && siteConfig.locale.tech.affiliateTags.homePage;
  }

  if (!price) {
    price = data && data.price;
  }
  if (!title) {
    title = data && data.uname;
  }

  const amzga = `${title}_${price}`;

  const affiliateData = data && data.affiliate;
  let affiliateDataForBuy = "";
  const linkCategory = !type ? "amazon" : type;
  if (url) {
    // in case we have direct amazon URL
    buyURL = url;
  } else {
    if (affiliateData && affiliateData.exact) {
      affiliateDataForBuy = affiliateData.exact;
    } else if (affiliateData && affiliateData.related) {
      affiliateDataForBuy = affiliateData.related;
    }
    if (affiliateDataForBuy && affiliateDataForBuy[0]) {
      const amazonData = affiliateDataForBuy.filter(amazonItem => amazonItem.Identifier === linkCategory);
      if (amazonData && amazonData[0]) {
        buyURL = amazonData[0].url;
      }
    } else {
      buyURL = affiliateDataForBuy && affiliateDataForBuy.items.url;
    }
  }

  /* if (url) {
    // in case we have direct amazon URL
    buyURL = url;
  } else if (affiliateData && affiliateData.exact && affiliateData.exact[0]) {
    if (affiliateData.exact[0]) {
      const amazonData = affiliateData.exact.filter(amazonItem => amazonItem.Identifier === linkCategory);
      if (amazonData && amazonData[0]) {
        buyURL = amazonData[0].url;
      }
    } else {
      buyURL = affiliateData.exact.items.url;
    }
  } else if (affiliateData && affiliateData.related) {
    if (affiliateData.related[0]) {
      const amazonData = affiliateData.related.filter(amazonItem => amazonItem.Identifier === linkCategory);
      if (amazonData && amazonData[0]) {
        buyURL = amazonData[0].url;
      }
    } else {
      buyURL = affiliateData.related.items.url;
    }
  } */

  let affiliateUrl = "";
  let finalURL = "";

  if (buyURL && buyURL.indexOf("timofind") !== -1) {
    finalURL = buyURL.replace("timofind", tag);
  } else {
    finalURL = buyURL.indexOf("?") === -1 ? `${buyURL}?tag=${tag}` : `${buyURL}&tag=${tag}`;
  }

  if (linkCategory === "amazon") {
    affiliateUrl = `${siteConfig.mweburl}/pwafeeds/affiliate_amazon.cms?url=${encodeURIComponent(
      finalURL,
    )}&price=${price}&title=${title}&amz_ga=${amzga}`;
  }
  return affiliateUrl;
}

export function getAffiliateUrl(card, tag) {
  // const tag = siteConfig && siteConfig.locale.tech.affiliateTags[page];
  const amztitle = card.name.replace(/\s+/g, "-");
  const amzga = `${amztitle}_${card.sort_price}`;
  const price = card.sort_price;
  const title = amztitle;
  let affiliateUrl = "";
  let finalURL = "";
  const feedUrl = card.url;
  if (feedUrl) {
    if (feedUrl.indexOf("timofind") !== -1) {
      finalURL = feedUrl.replace("timofind", tag);
    } else {
      finalURL = feedUrl.indexOf("?") === -1 ? `${feedUrl}?tag=${tag}` : `${feedUrl}&tag=${tag}`;
    }
  }
  affiliateUrl = `${siteConfig.mweburl}/pwafeeds/affiliate_amazon.cms?url=${encodeURIComponent(
    finalURL,
  )}&price=${price}&title=${title}&amz_ga=${amzga}`;

  return affiliateUrl;
}

export function getKeyByValue(dataObject, value) {
  for (const key in dataObject) {
    if (Object.prototype.hasOwnProperty.call(dataObject, key)) {
      if (dataObject[key] === value) return key;
    }
  }
  return "";
}

export function getAffiliateTags(affiliateTags) {
  const siteName = process.env.SITE;
  const platform = isMobilePlatform() ? "wap" : "web";
  if (affiliateTags) {
    return `${siteName}_${platform}_${affiliateTags}`;
  }
  return "";
}

export function formatMoney(number, decPlaces, decSep, thouSep) {
  decPlaces = isNaN((decPlaces = Math.abs(decPlaces))) ? 2 : decPlaces;
  decSep = typeof decSep === "undefined" ? "." : decSep;
  thouSep = typeof thouSep === "undefined" ? "," : thouSep;

  const sign = number < 0 ? "-" : "";
  const i = String(parseInt((number = Math.abs(Number(number) || 0).toFixed(decPlaces))));
  var j = (j = i.length) > 3 ? j % 3 : 0;

  return (
    sign +
    (j ? i.substr(0, j) + thouSep : "") +
    i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, `₹ 1${thouSep}`) +
    (decPlaces
      ? decSep +
        Math.abs(number - i)
          .toFixed(decPlaces)
          .slice(2)
      : "")
  );
}

export function adsPlaceholder(className) {
  return <div className={`${className}`} />;
}

export function initEvent(eventName, fn, options = null) {
  window.addEventListener(eventName, fn);
}

export function removeEvent(eventName, fn, options = null) {
  window.removeEventListener(eventName, fn);
}

export function objToQueryStr(obj) {
  if (obj && typeof obj === "object" && obj.constructor === Object) {
    return Object.keys(obj)
      .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`)
      .join("&");
  }
  return "";
}

/**
 * This Method Use to Log the error Day wise in server log files
 * As of now this is called from onError method which is in fetch.js file
 * and Server.js when exception occured
 * @param {object} ex it contains the error data
 */
export function logError(ex) {
  try {
    const today = new Date();
    const separator = "\n";
    const comma = ",";
    const todayString = `${today.getDate()}-${today.getMonth() + 1}-${today.getFullYear()}`;
    const timeString = `${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`;
    // Add time to log
    let errorMsg = `ERROR AT : ${todayString} ${timeString}${comma}${separator}`;
    // Add platform to log eg mobile / desktop
    errorMsg += `PLATFORM:${process.env.PLATFORM}${comma}${separator}`;

    // User agent hit at server
    if (globalconfig && globalconfig.userAgent) {
      errorMsg += `USER-AGENT:${globalconfig.userAgent}${comma}${separator}`;
    }

    // Referer of request at server
    if (globalconfig && globalconfig.referer) {
      errorMsg += `REFERER:${globalconfig.referer}${comma}${separator}`;
    }

    // Requested url hit on server
    if (globalconfig && globalconfig.reqUrl) {
      errorMsg += `Requested URL :${globalconfig.reqUrl}${comma}${separator}`;
    }

    // Api url ( passed from fetch , if api fails)
    if (ex.config && ex.config.url) {
      errorMsg += `Requested Feed :${ex.config.url}${comma}${separator}`;
    }

    // Exception stack

    if (ex.ampDuplicateError) {
      errorMsg += ex.ampDuplicateError + comma + separator + separator;
      fs.appendFileSync(`/var/log/node/amp-error-${todayString}.log`, errorMsg);
    } else {
      errorMsg += ex + comma + separator + separator;
      fs.appendFileSync(`/var/log/node/lang-error-${todayString}.log`, errorMsg);
    }
  } catch (e) {
    // Might be visible in pm2 out.log
    console.log("ERROR WHILE APPENDING LOG FILE");
  }
}

export function getURLParams(url, param) {
  const urlObj = new URL(url);
  return urlObj.searchParams.get(param);
}

/**
 *  @description async function which returns network speed
 *               by either calculating it or returns the already calculated speed
 *  @param {Function} callback callback function returned with the network speed
 * */

export function getNetworkSpeed(callback) {
  if (!callback || typeof callback !== "function") {
    return;
  }

  if (_isCSR()) {
    if (window._network_speed) {
      callback(window._network_speed);
    }
    try {
      const imageAddr = "https://static.langimg.com/thumb/msid-76874587,width-2000,height-2000.cms";
      const downloadSize = 11700; // size in bytes of above image

      function MeasureConnectionSpeed(cb) {
        let startTime;
        let endTime;
        const download = new Image();
        download.onload = function() {
          endTime = new Date().getTime();
          setResults(cb);
        };

        download.onerror = function(err, msg) {
          console.log("Error while calculating network speed, image load", err);
          cb("NA");
        };

        startTime = new Date().getTime();
        // So path is not fetched from cache
        const cacheBuster = `?nnn=${startTime}`;
        download.src = imageAddr; // + cacheBuster;

        function setResults(cb) {
          const duration = (endTime - startTime) / 1000;
          const bitsLoaded = downloadSize * 8;
          const speedBps = (bitsLoaded / duration).toFixed(2);
          const speedKbps = (speedBps / 1024).toFixed(2);
          const speedMbps = (speedKbps / 1024).toFixed(2);

          window._network_speed = speedMbps;
          cb(window._network_speed);
        }
      }
      window.setTimeout(() => MeasureConnectionSpeed(callback), 1);
    } catch (e) {
      console.log("Error calculating network speed");
    }
  }
}

/**
 * @description Creates a new intersection observer and returns it's reference (if possible,else returns null)
 * @param {Function} callback callback to execute with respect to observer
 * @param {Object} optional parameter to pass to the observer ( root, threshold and margin)
 */

export const createInterSectionObserver = (callback, options = {}) => {
  try {
    if ("IntersectionObserver" in window && typeof callback === "function") {
      return new IntersectionObserver(callback, options);
    }
    return null;
  } catch (e) {
    console.log("Failed to create intersection observer", e);
  }
};

/**
 * @description Creates and assigns observer reference to passed 'this' object
 * @param {ThisType} _this 'this 'parameter to attach property IObserver
 * @param {Object} config config to pass to create observer
 * @param {Function} callback callback function to execute when entries are observed
 * @param {Object} options Options object to turn this function on and off (through feed)
 */
export function createAndCacheObserver(
  _this,
  config = {
    root: null,
    rootMargin: "0px",
    threshold: 0.2, // when 20% in view
  },
  callback = (entries, self) => {
    entries.forEach(entry => {
      if (entry && entry.isIntersecting) {
        console.log("observed");
      }
    });
  },
  options = { shouldCreate: false },
) {
  if (options && !options.shouldCreate) {
    return;
  }
  try {
    const observer = createInterSectionObserver(callback, config);
    _this.IObserver = observer;
  } catch (e) {
    console.log("Error attaching observer to this", e);
  }
}

/**
 *
 * @param {ThisParameterType} _this context to add observer to ( this reference)
 * @param {Object} rowsSelector Object to iterate over with keys as the row selectors to observe
 * @param {Object} options object to turn scroll depth on / off through feed
 */
export function addObserverToWidgets(_this, rowsSelector, options = { shouldCreate: false }) {
  if (options && !options.shouldCreate) {
    return;
  }
  if (_isCSR()) {
    if (!_this.memoizedObserver) {
      _this.memoizedObserver = observeAndCacheElements(_this.IObserver, {});
    }

    _this.memoizedObserver(rowsSelector);
  }
}

/**
 *
 * @param {IntersectionObserver} obs Observer reference which caches elements ( hits them only once )*
 * @param {Object} elementCache cache of elements passed to observer ( in the form of an object with keys as selector)
 */
export function observeAndCacheElements(obs, elementCache) {
  return elementsToObserve => {
    for (const elSelector in elementsToObserve) {
      if (!(elSelector in elementCache)) {
        const elem = document.querySelector(elSelector);
        if (obs && elem) {
          obs.observe(elem);
          elementCache[elSelector] = true;
        }
      }
    }
  };
}

export function fetchHomeCityWidgetData(alaskaData, searchItem, secId, params, dispatch) {
  let data = filterAlaskaData(alaskaData, secId);
  let item = "";
  let clonedCopy = JSON.parse(JSON.stringify(alaskaData)); // deep copying object to varaible clonedCopy
  const objString = JSON.stringify(data).toLowerCase(); // return string of object, used for searching.
  const position = objString.indexOf(searchItem.toLowerCase()); // returns position of search, for eg : 238
  if (position !== -1) {
    const subStr = objString.substring(0, position); // returns substring before the search position
    const arrSubstr = subStr.split(',"c'); // converts string to array based on some generic keyword like ,"C
    const arrayLastVal = arrSubstr.pop(); // return last value from array
    const finalArr = arrayLastVal.split('":{'); // return array, like:  ["7-1-2", ""label":"உலக கோப்பை கிரிக்கெட்","msid":""]
    const finalIndexesARR = finalArr[0].split("-"); // return array like :  ["7", "1", "2"]
    const arrWithActualKeys = []; // stores actual keys like ["C7", "C7-1", "C7-1-2"]
    let keys = "";
    finalIndexesARR.forEach((item, index) => {
      keys += index == 0 ? `C${item}` : `-${item}`;
      arrWithActualKeys.push(keys);
    });
    arrWithActualKeys.forEach(item => (clonedCopy = clonedCopy[item]));

    item = clonedCopy;
  }

  // let widgetInfo =
  //   process.env.PLATFORM === "desktop"
  //     ? homeWidgetInfo.filter(widgetInfo => widgetInfo._sec_id == secId)
  //     : homeWidgetInfo.filter(widgetInfo => widgetInfo.id == secId);
  // let params =
  //   process.env.PLATFORM === "desktop"
  //     ? widgetInfo && widgetInfo[0]
  //     : widgetInfo && widgetInfo[0] && widgetInfo[0].params;

  if (params && params != undefined) {
    if (item != "" && item != undefined) {
      let cityId =
        item &&
        item.weblink &&
        item.weblink
          .split("/")
          .pop()
          .split(".cms")[0];
      params.citySecId = cityId ? cityId : item && item.msid;
      return params;
    } else {
      return params;
    }
  }
}

/*******
 * Critical Css spliting
 */

export function addCriticalCss() {
  let version;
  let sitename = process.env.SITE === "eisamay" ? "eis" : process.env.SITE;
  if (isMobilePlatform()) {
    version = versionObject.criticalCssVersion;
  } else {
    version = versionObject.criticalCssVersionD;
  }
  const filename = "csrcritical";
  const critstyle = document.createElement("link");
  critstyle.rel = "stylesheet";
  critstyle.type = "text/css";
  critstyle.href = ["development"].includes(process.env.DEV_ENV)
    ? `/${filename}.css`
    : ["stg1", "stg2", "stg3"].includes(process.env.DEV_ENV)
    ? `/css/${filename}.${version}.css`
    : `https://static.${sitename}.indiatimes.com/css/${filename}.${version}.css`;
  let referenceNode = document.getElementById("criticalCss");
  if (referenceNode) {
    referenceNode.parentNode.insertBefore(critstyle, referenceNode.nextSibling);
  }
}

export function isGadgetPage(config, path) {
  //  const gadgetPages = ["gadgethome", "gadgetlist", "gadgetshow", "comparelist", "compareshow"];
  const gadgetPages = ["gadgethome"];
  if (config && config.pagetype && gadgetPages.includes(config.pagetype)) {
    return true;
  }
  return false;
}

export function getLogoLink(pagetype, pathname, countryCode, siteName) {
  let url = "/";
  if (isGadgetPage({ pagetype: pagetype }) || siteName == gadgetsConfig.gnMetaVal) {
    url = siteConfig.gadgetdomain;
  } else if (countryCode) {
    url = internationalconfig[siteName].internationalPagesUrl[countryCode];
  } else if (isBusinessSection(pathname)) {
    url = siteConfig.businessLink;
  }
  return url;
}

export const getLogoPath = () => process.env.ASSET_BASEPATH + siteConfig.logoPath;

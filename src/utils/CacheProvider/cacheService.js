/* eslint-disable no-underscore-dangle */
/* eslint-disable no-plusplus */

class InMemoryCache {
  // "minutesToLive" default is 1 day , expiry time
  // "maxSpace" must be a number greater than 0
  constructor(options = { minutesToLive: 24 * 60, maxSpace: 10 }) {
    this.millisecondsToLive = options.minutesToLive * 60 * 1000;
    this.maxSpace = options.maxSpace;
    this.usedSpace = 0;
    this.cacheMap = new Map();
    this.oldcacheMap = new Map();
    this.fetchDate = new Date(0);

    console.log(`InMemoryCache initialized with caching for : ${options.minutesToLive} mins`);

    this.get = this.get.bind(this);
    this._set = this._set.bind(this);
    this.flush = this.flush.bind(this);
    this.isCacheExpired = this.isCacheExpired.bind(this);
    this.isSpaceExpired = this.isSpaceExpired.bind(this);
    this._delete = this._delete.bind(this);
  }

  isCacheExpired(key) {
    if (this.cacheMap.has(key)) {
      const keyvalue = this.cacheMap.get(key);
      return keyvalue.expiry.getTime() + this.millisecondsToLive < new Date().getTime();
    }
    return true;
  }

  isSpaceExpired() {
    if (this.usedSpace == this.maxSpace) {
      this.oldcacheMap = this.cacheMap;
      this.flush();
    }
  }

  /**
   * _set function for setting value in cache
   * here we check for space availability
   * @param {string} key , key of the map , mostly URLs
   * @param {*} value , data to be set
   */
  _set(key, value) {
    this.isSpaceExpired();

    this.cacheMap.set(key, value);
    this.fetchDate = new Date();
    this.usedSpace++;
    /* 
      FIX FOR  LAN-5599
      (accessing cache memory reference directly)
      Always clone value while getting and re-setting it, to avoid object directreference and cache data getting dirty issues.
    */
    const valueClone = JSON.parse(JSON.stringify(value));

    return Promise.resolve(valueClone.data);
  }

  /**
   * get
   * @param {string} key , to get data
   * @param {function} fetchFunction , used for
   */
  get(key, fetchFunction) {
    if (this.isCacheExpired(key)) {
      // console.log(`-------expired - fetching new data---${key}`);
      if (fetchFunction instanceof Function) {
        return fetchFunction().then(data => this._set(key, { data, expiry: new Date() }));
      }
    }

    if (this.cacheMap.has(key)) {
      // console.log(`@@@@---cache hit --->>>${key}`);
      const currentValue = this.cacheMap.get(key);
      /* 
      FIX FOR  LAN-5599
      (accessing cache memory reference directly)
      Always clone value while getting and re-setting it, to avoid object direct reference and cache data getting dirty issues.
    */
      const valueClone = JSON.parse(JSON.stringify(currentValue));
      return Promise.resolve(valueClone.data);
    }

    if (this.oldcacheMap.has(key)) {
      // console.log("old cache hit");
      return this._set(key, this.oldcacheMap.get(key));
    }

    return Promise.reject(new Error("No Data Found"));
  }

  /**
   * @memberof InMemoryCache
   * Delete an item.
   * @param {string} key to be deleted
   * Returns `true` if the item is removed or `false` if the item doesn't exist.
   */
  _delete(key) {
    const isDeleted = this.cacheMap.delete(key);
    if (isDeleted) {
      this.usedSpace--;
      this.oldcacheMap.delete(key);
    }
    return isDeleted;
  }

  /**
   * Flush all the cache
   */
  flush() {
    this.usedSpace = 0;
    this.cacheMap.clear();
    this.fetchDate = new Date(0);
  }
}

// Create a new cache service instance , for expiry of for 30 mins
const cachemodule = new InMemoryCache({ minutesToLive: 30 });

export default cachemodule;

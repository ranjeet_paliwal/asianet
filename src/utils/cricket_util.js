//cricket Module start
export const _getTeamLogo = (teamid, type) => {
  let teamidint = parseInt(teamid);
  if (type == "square") {
    switch (teamidint) {
      case 1:
        return "69754952"; //Australia
        break;
      case 2:
        return "69754953"; //Bangladesh
        break;
      case 3:
        return "69754957"; //England
        break;
      case 4:
        return "69754958"; //India
        break;
      case 5:
        return "69758913"; //New Zealand
        break;
      case 6:
        return "69754962"; //Pakistan
        break;
      case 7:
        return "69754966"; //South Africa
        break;
      case 8:
        return "69754967"; //Sri Lanka
        break;
      case 9:
        return "69754968"; //West Indies
        break;
      case 10:
        return "69754969"; //Zimbabwe
        break;
      case 11:
        return "69754954"; //Bermuda
        break;
      case 12:
        return "69754955"; //Canada
        break;
      case 13:
        return "69758912"; //Ireland
        break;
      case 14:
        return "69754959"; //Kenya
        break;
      case 15:
        return "69754961"; //Netherland
        break;
      case 16:
        return "69754963"; //Scotland
        break;
      case 1188:
        return "69754951"; //AFG
        break;
      case 19:
        return "69758916"; //Hong Kong
        break;

      case 1105:
        return "68704746"; //RCB
        break;
      case 1108:
        return "68704747"; //CSK
        break;
      case 1379:
        return "68704749"; //SH
        break;
      case 1109:
        return "68704748"; //DC
        break;
      case 1107:
        return "68704752"; //KXIP
        break;
      case 1106:
        return "68704750"; //KKR
        break;
      case 1111:
        return "68704751"; //MI
        break;
      case 1110:
        return "68704754"; //RR
        break;
      default:
        return "70294619";
    }
  } else {
    switch (teamidint) {
      case 1:
        return "20291721"; //Australia
        break;
      case 2:
        return "20291723"; //Bangladesh
        break;
      case 3:
        return "20291728"; //England
        break;
      case 4:
        return "20291740"; //India
        break;
      case 5:
        return "20291760"; //New Zealand
        break;
      case 6:
        return "20291765"; //Pakistan
        break;
      case 7:
        return "20291771"; //South Africa
        break;
      case 8:
        return "20291772"; //Sri Lanka
        break;
      case 9:
        return "21953215"; //West Indies
        break;
      case 10:
        return "20291775"; //Zimbabwe
        break;
      case 11:
        return "20292237"; //Bermuda
        break;
      case 12:
        return "20291725"; //Canada
        break;
      case 13:
        return "20291746"; //Ireland
        break;
      case 14:
        return "20291751"; //Kenya
        break;
      case 15:
        return "20291755"; //Netherland
        break;
      case 16:
        return "20291768"; //Scotland
        break;
      case 1188:
        return "20291715"; //AFG
        break;
      case 19:
        return "32242546"; //Hong Kong
        break;

      case 1105:
        return "20292284"; //RCB
        break;
      case 1108:
        return "20292243"; //CSK
        break;
      case 1379:
        return "20292664"; //SH
        break;
      case 1109:
        return "68422578"; //DC
        break;
      case 1107:
        return "20292264"; //KXIP
        break;
      case 1106:
        return "20292260"; //KKR
        break;
      case 1111:
        return "20292270"; //MI
        break;
      case 1110:
        return "20292287"; //RR
        break;
      default:
        return "70294619";
    }
  }
};

export const getTeamColor = teamid => {
  let teamno = parseInt(teamid);
  switch (teamno) {
    case 1105:
      return "rcb"; //RCB
    case 1108:
      return "csk"; //CSK
    case 1379:
      return "sh"; //SH
    case 1109:
      return "dc"; //DC
    case 1107:
      return "kxip"; //KXIP
    case 1106:
      return "kkr"; //KKR
    case 1111:
      return "mi"; //MI
    case 1110:
      return "rr"; //RR
    default:
      return "default-color";
  }
};

export const _getShortMonthName = monthnumber => {
  let month = parseInt(monthnumber) + 1;
  switch (month) {
    case 1:
      return "JAN";
      break;
    case 2:
      return "FEB";
      break;
    case 3:
      return "MAR";
      break;
    case 4:
      return "APR";
      break;
    case 5:
      return "MAY";
      break;
    case 6:
      return "JUN";
      break;
    case 7:
      return "JUL";
      break;
    case 8:
      return "AUG";
      break;
    case 9:
      return "SEP";
      break;
    case 10:
      return "OCT";
      break;
    case 11:
      return "NOV";
      break;
    case 12:
      return "DEC";
      break;
    default:
      return monthnumber;
  }
};
//cricket Module end

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import DesktopHeader from "../../components/desktop/DesktopHeader";
import Footer from "../../components/common/Footer";
require("es6-promise").polyfill();
import ErrorBoundary from "../../components/lib/errorboundery/ErrorBoundary";

class AppD extends Component {
  render() {
    return (
      <div>
        <DesktopHeader />
        <div>
          <ErrorBoundary>
            <div id="parentContainer" className="animated desktop_body">
              {this.props.children}
            </div>
          </ErrorBoundary>
        </div>
        <Footer />
      </div>
    );
  }
}

AppD.fetchData = function({ dispatch, params, query, history, router }) {
  return Promise.resolve([]);
};

AppD.propTypes = {
  children: PropTypes.node,
};

function mapStateToProps(state) {
  return {
    ...state.app,
  };
}

export default connect(mapStateToProps)(AppD);

import React from "react";
import { connect } from "react-redux";
import AnchorLink from "../../../components/common/AnchorLink";

import { fetchTopListDataIfNeeded } from "../../../actions/home/home";

export class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const _this = this;

    const { home } = _this.props;
    const { data } = home;
    // console.log("sss", data);

    return data && data.items && Array.isArray(data.items) ? (
      <ul style={{ textAlign: "center" }}>
        {data.items.map(item => {
          return (
            <li>
              <AnchorLink href={`/articleshow/${item.id}`}>{item.hl}</AnchorLink>
            </li>
          );
        })}
      </ul>
    ) : (
      "Loading.."
    );
  }
}

Home.fetchData = function({ dispatch, query, params, router }) {
  return dispatch(fetchTopListDataIfNeeded(params, query, router)).then(data => {
    // fetchDesktopHeaderPromise(dispatch);
    return data;
  });
};

Home.propTypes = {};

function mapStateToProps(state) {
  return {
    home: state.home,
  };
}

export default connect(mapStateToProps)(Home);

/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/no-danger */
/* eslint-disable indent */
import React from "react";
import PropTypes from "prop-types";
import serialize from "serialize-javascript";

import { _getDeviceRelatedJS, _getMainCss, _getMainJS, _getPreRenderAdsJS } from "./utils";

import { _getStaticConfig, isMobilePlatform } from "../../utils/util";

const criticalCss = require("./criticalCss"); // for mobile critical css
const criticalCssD = require("./criticalCssD");
const ASSET_PATH = process.env.ASSET_PATH || "/";
const SITE_PATH = process.env.SITE_PATH || "";
const siteConfig = _getStaticConfig();
const fs = require("fs");

function Html({ js, css, html, head, initialState, version, pagetype, reqURL }) {
  const themeStyle = "";

  const criticalStyle =
    process.env.PLATFORM == "desktop"
      ? criticalCssD.common(process.env.SITE, themeStyle, pagetype, reqURL)
      : criticalCss.common(process.env.SITE, themeStyle, pagetype, reqURL);

  function csrCriticalDev() {
    if (["development"].includes(process.env.DEV_ENV)) {
      const fileContent = process.env.PLATFORM == "desktop" ? criticalCssD.csrCritical : criticalCss.csrCritical;
      // Path of the new file with its name
      const filepath = "src/public/csrcritical.css";

      fs.writeFile(filepath, fileContent, err => {
        if (err) throw err;

        console.log("CSR Critical Css file was succesfully saved!");
      });
    }
  }
  csrCriticalDev();

  return (
    <html lang={siteConfig.languagemeta ? `${siteConfig.languagemeta}` : "en"}>
      <head>
        {/* Common schema organization and website */}
        <noscript>
          <style>{`.nojsicon {display:none;}`}</style>
          Please enable javascript.
        </noscript>
        <meta value="summary_large_image" name="twitter:card" />
        <meta content="IE=edge" httpEquiv="X-UA-Compatible" />
        {/* <meta content="NOINDEX, NOFOLLOW" name="ROBOTS" /> */}
        <link rel="shortcut icon" href={siteConfig.icon} />
        <meta property="fb:admins" content="556964827" />
        <meta property="fb:app_id" content="972612469457383" />
        <meta content="SSM4580016100404216113TIL" name="tpngage:name" />
        <meta content={siteConfig.google_site_verification} name="google-site-verification" />
        <meta content="text/html; charset=UTF-8" httpEquiv="Content-Type" />
        <meta httpEquiv="content-language" content={siteConfig.languagemeta ? `${siteConfig.languagemeta}` : "en"} />
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge,chrome=1" />
        {head.title.toComponent()}
        {head.meta.toComponent()}
        <meta
          name="viewport"
          content="width=device-width, height=device-height,initial-scale=1.0,user-scalable=yes,maximum-scale=5"
        />
        {/* {process.env.SITE == "nbt" && pagetype && (pagetype == "photolist" || pagetype == "recommended") ? (
          <meta content="NOINDEX,NOFOLLOW" name="robots" />
        ) : null} */}
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta content="yes" name="apple-touch-fullscreen" />
        <meta name="msapplication-tap-highlight" content="no" />
        {/* Instant article activation meta */}
        <meta property="fb:pages" content={`${siteConfig.fbIAcontentId}`} />
        {/* Need to preload or DNS-prefetch a manifest.json file for performance */}
        {/* preconnect a font file to eliminate render-blocking resources */}
        <link rel="preload" href={`${siteConfig.fontUrl}`} as="font" crossOrigin="anonymous" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="manifest" href={`/${SITE_PATH}manifest.json`} />
        {/* prefetching */}
        <link rel="dns-prefetch" href={`https://${siteConfig.staticSiteName}`} />
        <link rel="dns-prefetch" href={`https://${siteConfig.siteName}`} />
        <link rel="dns-prefetch" href="https://static.langimg.com" />
        <link rel="dns-prefetch" href="https://ade.clmbtech.com" />
        <link rel="dns-prefetch" href="https://static.clmbtech.com" />
        <link rel="dns-prefetch" href="https://image2.pubmatic.com" />
        <link rel="dns-prefetch" href="https://image6.pubmatic.com" />
        <link rel="dns-prefetch" href="https://www.google-analytics.com" />
        {/* <link rel="dns-prefetch" href="https://www.googletagservices.com" /> */}
        <link rel="dns-prefetch" href="https://securepubads.g.doubleclick.net" />
        <link rel="dns-prefetch" href="https://adservice.google.co.in" />
        <link rel="dns-prefetch" href="https://connect.facebook.net" />

        {/* prefetching */}
        {/* <link rel="apple-touch-icon" sizes="72x72" href={`${ASSET_PATH}${SITE_PATH}icons/icon-72x72.png`} />
        <link rel="apple-touch-icon" sizes="96x96" href={`${ASSET_PATH}${SITE_PATH}icons/icon-96x96.png`} />
        <link rel="apple-touch-icon" sizes="128x128" href={`${ASSET_PATH}${SITE_PATH}icons/icon-128x128.png`} />
        <link rel="apple-touch-icon" sizes="144x144" href={`${ASSET_PATH}${SITE_PATH}icons/icon-144x144.png`} />
        <link rel="apple-touch-icon" sizes="152x152" href={`${ASSET_PATH}${SITE_PATH}icons/icon-152x152.png`} />
        <link rel="apple-touch-icon" sizes="192x192" href={`${ASSET_PATH}${SITE_PATH}icons/icon-192x192.png`} />
        <link rel="apple-touch-icon" sizes="192x192" href={`${ASSET_PATH}${SITE_PATH}icons/icon-192x192.png`} />
        <link rel="apple-touch-icon" sizes="384x384" href={`${ASSET_PATH}${SITE_PATH}icons/icon-384x384.png`} /> */}
        <link rel="apple-touch-icon" sizes="192x192" href={`${ASSET_PATH}${SITE_PATH}icons/icon-192x192.png`} />
        <link rel="apple-touch-icon" sizes="512x512" href={`${ASSET_PATH}${SITE_PATH}icons/icon-512x512.png`} />
        {/* <link href="https://fonts.googleapis.com/css?family=Mukta" rel="stylesheet" /> */}
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="msapplication-TileImage" content={`${ASSET_PATH}${SITE_PATH}icons/icon-144x144.png`} />
        <meta name="theme-color" content="#ffffff" />
        <meta name="apple-mobile-web-app-title" content={siteConfig.siteName} />
        <meta name="application-name" content={siteConfig.siteName} />
        <script defer src="https://securepubads.g.doubleclick.net/tag/js/gpt.js" type="text/javascript" />
        {/* <script defer src={ccaudUrl} type="text/javascript" /> */}
        {head.link.toComponent()}

        {/* <style id="criticalCss">{criticalStyle.commonStyle}</style> */}

        <div id="comscoreContainer"></div>
        <script
          dangerouslySetInnerHTML={{
            __html: `window.landing_page = "${siteConfig.weburl + reqURL}";`,
          }}
        />
      </head>
      <body
        data-platform={isMobilePlatform() ? "mobile" : "desktop"}
        /* Condition for handling election widget */
        className={reqURL && reqURL.indexOf("isElectionWidget=result") > -1 ? "election-widget-result" : ""}
      >
        <div
          id="root"
          className={`container ${process.env.SITE}`}
          dangerouslySetInnerHTML={{
            __html: html,
          }}
        />

        {process.env.NODE_ENV == "production" ? (
          <script
            dangerouslySetInnerHTML={{
              __html: `window.process = ${serialize({
                env: {
                  BROWSER: "true",
                  API_BASEPOINT: process.env.API_BASEPOINT,
                  NODE_API_BASEPOINT: process.env.NODE_API_BASEPOINT,
                  API_TIMEOUT:
                    process.env.PLATFORM === "desktop" ? process.env.API_TIMEOUT : process.env.API_TIMEOUT_MOBILE,
                  WEBSITE_URL: process.env.WEBSITE_URL,
                  ASSET_BASEPATH: process.env.ASSET_BASEPATH,
                  SITE: process.env.SITE,
                  NODE_ENV: process.env.NODE_ENV,
                  DEV_ENV: process.env.DEV_ENV,
                  PLATFORM: process.env.PLATFORM == "desktop" ? "desktop" : "mobile",
                },
              })}`,
            }}
          />
        ) : (
          <script
            dangerouslySetInnerHTML={{
              __html: `window.process = ${serialize({
                env: {
                  BROWSER: "true",
                  REDUX_LOGGER: process.env.REDUX_LOGGER,
                  API_BASEPOINT: process.env.API_BASEPOINT,
                  ASSET_BASEPATH: process.env.ASSET_BASEPATH,
                  API_TIMEOUT:
                    process.env.PLATFORM === "desktop" ? process.env.API_TIMEOUT : process.env.API_TIMEOUT_MOBILE,
                  WEBSITE_URL: process.env.WEBSITE_URL,
                  SITE: process.env.SITE,
                  NODE_ENV: process.env.NODE_ENV,
                  DEV_ENV: process.env.DEV_ENV,
                  PLATFORM: process.env.PLATFORM == "desktop" ? "desktop" : "mobile",
                },
              })}`,
            }}
          />
        )}
        <script
          dangerouslySetInnerHTML={{
            __html: `window.__INITIAL_STATE__ = ${serialize(initialState)}`,
          }}
        />

        {/* {js.map(js => <script data-val={js} key={js} defer={true} src={`${ASSET_PATH}${js}`} />)} */}

        <script
          dangerouslySetInnerHTML={{
            __html: `${_getMainJS(js, ASSET_PATH, SITE_PATH, version, reqURL)}`,
          }}
        />
        {/* DMP JS
        <script src="https://static.clmbtech.com/ase/16153/790/aa.js" type="text/javascript"></script>
        <script defer={true} type="text/javascript" src="https://www.googletagservices.com/tag/js/gpt.js"></script>
        */}
        {css.map(css => (
          <script
            dangerouslySetInnerHTML={{
              __html: `${_getMainCss(css, ASSET_PATH, SITE_PATH)}`,
            }}
          />
        ))}
        {/* css.map(css => <link key={css} rel="stylesheet" href={`${ASSET_PATH}${css}`} />) */}
      </body>
    </html>
  );
}

Html.propTypes = {
  js: PropTypes.array.isRequired,
  css: PropTypes.array.isRequired,
  html: PropTypes.string,
  head: PropTypes.object.isRequired,
  initialState: PropTypes.object.isRequired,
};

export default Html;

/* eslint-disable camelcase */

const criticalCss = (portalName, themeStyle, pagetype, reqURL) => {
  // const mobilesprite = spritepath && spritepath !== "" ? spritepath : getCssPath(portalName, "pwa_sprite");
  // const sprite = `https://static.${portalName}.indiatimes.com/img/pwa_desktop_sprite.svg?v=${getCssVersion}`; // sprite path
  // const sprite = getCssPath(portalName, "pwa_desktop_sprite");
  // export const criticalCss = (portalName, themeStyle, spritepath) => {
  // let mobilesprite = (spritepath && spritepath != '') ? spritepath : `https://static.${portalName}.indiatimes.com/img/pwa_sprite.svg?v=16072020`;
  // const sprite = `https://static.${portalName}.indiatimes.com/img/pwa_desktop_sprite.svg?v=16072020`; // sprite path
  // const spritesize = "383px 186px"; // sprite size
  const commonCss = `
            html {-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%}

            @media (max-width: 319px) {
            html {font-size: 56.25%;}
            }

            @media (min-width: 320px) and (max-width: 359px) {
            html {font-size: 62.5%;}
            }

            @media (min-width: 360px) and (-webkit-min-device-pixel-ratio : 3 ), (min--moz-device-pixel-ratio : 3 ), (-o-min-device-pixel-ratio : 3/1 ), (min-device-pixel-ratio : 3 ) {
            html {font-size: 62.5%;}
            }

            @media (min-width: 360px) and (-webkit-max-device-pixel-ratio : 2.9 ), (max--moz-device-pixel-ratio : 2.9 ), (-o-max-device-pixel-ratio : 2.9/1 ), (max-device-pixel-ratio : 2.9 ) {
            html {font-size: 68.75%;}
            }

            #__SVG_SPRITE_NODE__, .notification_popup {display:none;}
            .svgiconcount {display:inline-flex; width:100%; height:100%;}
            .svgiconcount svg {margin:auto;}

            body {margin: 0; padding: 0; font-size: 1.6rem; background: #fff; color: #000; margin: auto; font-family: Noto Sans, Arial,Helvetica,sans-serif;}
            body.disable-scroll {overflow: hidden !important; margin-right: 17px;}
            .clear{clear:both}
            .clr-overflow{overflow:hidden}
            .posrelation {position:relative;}

            img {border: 0; vertical-align: bottom;}
            a{color:#000; text-decoration: none;}
            a:focus, a:hover, a:active {outline:none; color:#000; cursor: pointer; text-decoration: none;}
            ul, li{list-style: none; margin: 0; padding: 0;}

            .desktop_body .breadcrumb {padding: 0 0 10px;}
            .desktop_body .breadcrumb ul {line-height: 1.8rem;}
            .desktop_body .breadcrumb ul li{list-style-type: none;display: inline;}
            .desktop_body .breadcrumb ul li a, .breadcrumb ul li a span{text-decoration:none; font-size: 1.1rem; vertical-align:top; line-height: 1.6rem; font-weight: bold;}
            .desktop_body .breadcrumb ul li span{font-size: 1.1rem; vertical-align:top; line-height: 1.6rem;}

            .row{display:flex;flex-wrap:wrap;align-content:flex-start;margin-right:-15px;margin-left:-15px;}
            .row .col{margin:0;}
            .row .col1, .row .col2, .row .col3, .row .col4, .row .col5, .row .col6, .row .col7, .row .col8, .row .col9, .row .col10, .row .col11, .row .col12{display:flex;flex-wrap:wrap;align-content:flex-start;padding-right:15px;padding-left:15px;box-sizing:border-box;}
            .row .col12{flex:0 0 100%;max-width:100%;}
            .row .col11{flex:0 0 91.66%;max-width:91.66%;}
            .row .col10{flex:0 0 83.33%;max-width:83.33%;}
            .row .col9{flex:0 0 75%;max-width:75%;}
            .row .col8{flex:0 0 66.66%;max-width:66.66%;}
            .row .col7{flex:0 0 58.33%;max-width:58.33%;}
            .row .col6{flex:0 0 50%;max-width:50%;}
            .row .col5{flex:0 0 41.66%;max-width:41.66%;}
            .row .col4{flex:0 0 33.33%;max-width:33.33%;}
            .row .col3{flex:0 0 25%;max-width:25%;}
            .row .col2{flex:0 0 16.66%;max-width:16.66%;}
            .row .col1{flex:0 0 8.33%;max-width:8.33%;}
            .row .mr0, .row.mr0{margin:0;}
            .row .pd0, .row.pd0{padding:0;}
            
            .news-card.lead{margin-bottom:20px;}
            .news-card.lead .img_wrap{display:block;position:relative;}
            .news-card.lead .img_wrap img{width:100%;max-width:320px;}
            .news-card.lead .con_wrap{display:block;margin:10px 0 0 0;}
            .news-card.lead .con_wrap .section_name{font-size:1.1rem;color:#1f81db;display: block;}
            .news-card.lead .con_wrap .text_ellipsis{font-size:1.5rem;line-height:2.3rem;max-height:6.9rem;-webkit-line-clamp:3;font-weight:bold; word-break: break-word;}
            
            .news-card.horizontal{display:flex;border-bottom:1px solid #dfdfdf;margin-bottom:11px;padding-bottom:11px;width:100%;}
            .news-card.horizontal .img_wrap{margin:0 10px 0 0;position:relative;}
            .news-card.horizontal .img_wrap img{width:80px;height:60px;}
            
            .news-card.horizontal .con_wrap .section_name{display:none;}
            .news-card.horizontal .con_wrap .text_ellipsis{font-size:1.2rem;line-height:1.8rem;max-height:5.4rem;-webkit-line-clamp:3; word-break: break-word;display: -webkit-box;overflow:hidden;}
            
            .news-card.horizontal-lead{display:flex;flex-wrap:nowrap;border-bottom:1px solid #dfdfdf;margin-bottom:11px;padding-bottom:11px;}
            .news-card.horizontal-lead .img_wrap{margin:0 10px 10px 0;position:relative;}
            .news-card.horizontal-lead .img_wrap img{width:140px;}
            .news-card.horizontal-lead .con_wrap .section_name{font-size:1.1rem;color:#1f81db;display: block;}
            .news-card.horizontal-lead .con_wrap .text_ellipsis{font-size:1.2rem;line-height:1.8rem;max-height:5.4rem;-webkit-line-clamp:3; word-break: break-word;}
            
            .news-card.vertical{margin-bottom:20px;}
            .news-card.vertical .img_wrap{margin:0 0 10px 0;position:relative;}
            .news-card.vertical .img_wrap img{width:100%;}
            .news-card.vertical .con_wrap .section_name{font-size:1.1rem;color:#1f81db;display: block;}
            .news-card.vertical .con_wrap .text_ellipsis{font-size:1.2rem;line-height:1.8rem;max-height:5.4rem;-webkit-line-clamp:3; word-break: break-word;}
            
            .news-card.only-info{border-bottom:1px solid #dfdfdf;margin-bottom:10px;padding-bottom:10px;display:flex;width:100%;}
            .news-card.only-info img{display:none;}
            .news-card.only-info .con_wrap .section_name{font-size:1.1rem;color:#1f81db;display: block;}
            .news-card.only-info .con_wrap .text_ellipsis{font-size:1.2rem;line-height:1.8rem;max-height:5.4rem;-webkit-line-clamp:3; word-break: break-word;}

            .newtoplayout .news-card.lead .img_wrap img{max-width:385px;}
            .newtoplayout .news-card.only-info .con_wrap .text_ellipsis{max-height:3.7rem;-webkit-line-clamp:2;}

            #comscoreContainer{height:0px}
            
            .table {display: table; width: 100%;}
            .table .table_row {display: table-row; vertical-align: middle;}
            .table .table_col {display: table-cell; vertical-align: middle;}
            
            .ad1 {text-align: center; position: relative; z-index: 0;padding:2%;}
            .ad1.emptyAdBox{padding: 0 ; margin: 0 ; min-height: 0px;}
            .ad1.andbeyond{padding: 0 ; margin: 0 ; min-height: 0px; height: 0px;}
            .ad1.emptyAdBox:after{content: ''; display: none;}
            .ad1.topatf{min-height:60px;}
            .desktop_body .ad1 {padding: 0;}
            .desktop_body .slug {min-height: 100px;}
            .colombiaFail:after{content: ''; display: none;}
            .desktop_body .wdt_skin_lhsrhs{width:1000px; position:relative}
            .bgImg .desktop_body .wdt_skin_lhsrhs{display:none;}
            .desktop_body .wdt_skin_lhsrhs .rhs{position:absolute; right:-10px}
            .desktop_body .wdt_skin_lhsrhs .rhs .ad1.skinrhs {position: fixed;top: 5px;min-width: auto;}
            .desktop_body .wdt_skin_lhsrhs .rhs .ad1.skinrhs:after{display:none}            
            .ad1.innove {position: fixed;left: 0;}
            .ad1:empty{padding:0}
            .ad1:empty:after{content: ''; display: none;}
            /* [data-plugin=ctn]:empty{min-height:1px} */
            #tileyeDiv {position: relative;max-width: 1000px;margin: 0 auto; z-index: 103;}
            [ctn-style="ctnppdad"] {min-height: 0px;}
            [ctn-style="ctnppdad"]:after {display: none;}
            .adblockdiv {width: 100%; min-height:100px;}
            [data-slotname=ctnbccl] { width:100%;}
            body[data-platform=desktop]{display:block !important}
            .desktop_body .hideSecName li.news-card .con_wrap .section_name{display:none}
            .desktop_body .ad1.ad-top{position:relative; z-index:1002}
            .wdt_data_drawer{min-height:160px;}  
            .wdt_photo_video{min-height:240px;}
            
            .wdt_select_city{display:none}
            .text_ellipsis {overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 2;}
            .transform{transition: all 0.25s ease-in}
            .desktop_body{width:1000px; margin:0 auto;}
            .desktop_body .body-content{margin: 10px 0 0 0;}
            .desktop_body .lft_content{display: inline-block; vertical-align: top; width:670px; margin: 0 30px 0 0;}
            .desktop_body .rgt_content{display: inline-block; vertical-align: top; width:300px;}
            
            .section h3, .section h2, .section h1{font-size: 1.8rem; font-weight: bold; display: block; height: 40px; position: relative; border-bottom: 2px solid #dfdfdf; margin:0}
            .section h3 span, .section h2 span,  .section h1 span {height: 40px; line-height: 40px; padding: 0 10px 0 0; display: inline-block; position: relative; font-weight: bold;}
            .section {width: 100%;}
            .section .top_section {margin: 10px 0; position:relative}
            .section .top_section .read_more {position: absolute; right: 0; bottom: 10px; font-size: 13px; margin: 0 15px 0 0; color: #000; font-weight: normal;}
            .desktop_body .headerContainer .ad1 {margin: 10px 0; padding: 0; min-height:90px}
            .section-wrapper .top_headlines ul.more_headlines{margin:10px 0 0;padding:0;}
            .section-wrapper .top_headlines ul.more_headlines li{max-width:205px;margin-right:20px;padding-left:0;padding-right:0;}
            .section-wrapper .top_headlines ul.more_headlines li:nth-child(3n+3){margin-right:0;}
            .section-wrapper .top_headlines .news-card.lead .con_wrap {position: relative;margin-top: 20px;}
            /* .desktop_body .wdt_video.slider-horizontalView .slide.news-card.video.col .img_wrap{height:9vw;} */
            .trending-bullet{border:1px solid #dddddd; width: 100%; display: flex; box-sizing: border-box; flex-wrap: wrap; text-align:center; padding: 10px 5px 5px; margin-bottom: 10px;}
            .trending-bullet:empty {display:none;}
            .trending-bullet h3{display:none}
            .trending-bullet .scroll_content{overflow: hidden;}
            .trending-bullet a{font-size: 1.1rem; line-height: 1.9rem; color: #000; margin: 0 0 5px 0; display: inline; vertical-align: top; font-weight: bold;}
            .desktop_body .section-wrapper .rhs-widget .ad1.mrec1{min-height:250px; min-width:300px; margin-bottom:30px}
            .desktop_body .articlelist .ad1.mrec1{min-height:250px; min-width:300px; margin-bottom:15px}
            // .wdt-trending-minitv-breaking{min-height:37px; margin-top:10px; margin-bottom:10px; padding: 2px 0 0 0;}
            .banner-image img{max-height:100px}
            .articleshow_body .banner-image{margin:20px 0}
            .articleshow_body .banner-image img{max-height:190px}

            .yContainer{text-align: right; margin: -10px 3% 5px 0; display:block; overflow:hidden;} 
            .yContainer span{display: inline-block; vertical-align:top; text-transform: uppercase; font-size: 12px; line-height: 28px; color: #716f6f; margin: 0 10px 0 0; height: 24px;}
            .story-article .news_card .yContainer, .videolist-container .top-news-content .yContainer, .videoshow-container.box-content .yContainer{padding:8px 3% 2px 0; background: #eaeaea; border-bottom: 1px solid #c6c6c6; margin:0}
            .videoshow-container.box-content .yContainer{margin:10px 0 0 0}
            .box-content .yContainer{background:transparent}
            .master-player-container .yContainer{display:none}

            #timespoint-popup { background:#000; box-shadow: 0 -2px 22px 5px rgba(177, 164, 164, 0.5); padding:10px 15px 10px 0; position: fixed; z-index: 1000; width: auto; bottom: 0; display: none; border-radius: 8px 8px 0 0; left:12%;}
            #timespoint-popup .close-btn { background:none; border:0; position:absolute; right:0; top: 15px; margin-top:-20px; transform: rotate(45deg); font-size:30px; line-height:40px; font-weight:400; color:#fff; cursor:pointer;}
            #timespoint-popup .wrapper .top{ display:flex; align-items:center; margin: 5px 0 3px;}
            #timespoint-popup .wrapper .top .tp-btn{font-size: 11px; height: 30px; line-height: 30px; padding: 0 5px;white-space: nowrap; background:#e52727; text-transform: uppercase;}
            #timespoint-popup h2 { font-size:14px; color:#fff; padding-left:35px; position: relative; }
            #timespoint-popup h2 .icon { position:absolute; width:30px; height:30px; left:0; top: 50%; transform: translateY(-50%); }
            #timespoint-popup h2 .icon:before{background-position: -146px -110px;}
            #timespoint-popup h2 span { display:block; font-size:12px; color:#888; font-weight:400; margin-top: 4px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;}
            #timespoint-popup .column { padding:0 10px; font-family: Arial, Helvetica, sans-serif;}
            #timespoint-popup .column.heading{max-width:270px; padding-right:0}
            #timespoint-popup .column.heading b{font-size: 12px; line-height: 20px; max-height: 20px; text-overflow: ellipsis; white-space: nowrap; -webkit-line-clamp: 1;}
            #timespoint-popup .daily-points { color:#fff; display:flex; align-items:center; position: relative; padding-left:44px;}
            #timespoint-popup .daily-points li { display:flex; text-align:center; justify-content:center; flex-direction:column; position: relative; width:50px; height: 40px;}
            #timespoint-popup .daily-points li + li { margin-left:10px; }
            #timespoint-popup .daily-points li span.daily-status{width:13px; height:13px; border-radius:50%; background:#acacac; margin: auto;}
            #timespoint-popup .daily-points li.bonus-earned span.daily-status{ background:#aede69; }
            #timespoint-popup .daily-points li.bonus-lost span.daily-status{ background:#ef5d5d; }
            #timespoint-popup .daily-points li.active span.daily-status {box-shadow:0px 0px 0px 4px rgba(174, 222, 105, 0.58); background:#aede69;}
            #timespoint-popup .daily-points p { font-size:10px; text-transform:uppercase; color: #666666;}
            #timespoint-popup .daily-points li.active p {color:#999999;}

            body[pagetype=photolist] iframe.adgsplash, body[pagetype=home] iframe.adgsplash{display:none !important}

            .videoshow-container .top_wdt_video .con_social .wdt_social_share{display:none}
            .ieWrap{background:#dc4242; height:25px; line-height:25px; text-align:center; display:none}
            .ieWrap a, .ieWrap a:hover, .ieWrap a:visited{font-size:12px; color:#fff;}
            .ie-eleven .ieWrap {display:block}
            .alaskamasthead {text-align:center;}
            .alaskamasthead img {max-width:1000px; height: 50px;}

            #wdt_oneLaststep{background:rgba(0, 0, 0, 0.6);position:fixed;width:100%;height:100%;left:0;top:0;overflow:hidden;cursor:default;opacity:0;z-index:-1;transition:all 0.5s;box-sizing:border-box; font-family:arial}
            #wdt_oneLaststep.active{opacity:1;z-index:1050;}
            #wdt_oneLaststep a{text-decoration:none; color:#5b9ae4}
            #wdt_oneLaststep input[type=button], #wdt_oneLaststep input[type=submit]{background:#be281a;box-shadow:0 2px 2px 0 rgba(0, 0, 0, 0.24), 0 0 2px 0 rgba(0, 0, 0, 0.12);height:40px;color:#fff;text-transform:uppercase;font-weight:500;-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;-webkit-appearance:none;-moz-appearance:none;cursor:pointer;}
            #wdt_oneLaststep input:disabled{opacity:0.4;}
            #wdt_oneLaststep .box_oneLaststep{width:50%;background:#fff;overflow:hidden;overflow-y:auto;box-shadow:0 1px 12px 0 rgba(0, 0, 0, 0.3);position:absolute;right:0;top:10%;left:0;margin:auto;border-radius:3px;}
            #wdt_oneLaststep .box_oneLaststep .head{background:#f5f5f5;padding:20px 20px 10px;font-size:18px; line-height:28px; position:relative;}
            #wdt_oneLaststep .box_oneLaststep .head b{text-transform:uppercase;font-weight:bold;}
            #wdt_oneLaststep .box_oneLaststep .txt_content{padding:20px;}
            #wdt_oneLaststep .box_oneLaststep .txt_content .txt-info{font-size:16px;line-height:24px;color:#000;}
            #wdt_oneLaststep .box_oneLaststep .txt_content .txt-info strong{margin:0 0 10px;display:block;font-weight:bold;}
            #wdt_oneLaststep .box_oneLaststep .txt_content .txt-info label{margin:0 0 20px;font-size:16px;line-height:24px;color:#222;}
            #wdt_oneLaststep .box_oneLaststep .txt_content input[type=button]{width:55%;margin:20px auto 10px;display:block;line-height:normal;border:0;font-size:14px; outline:none}
            #wdt_oneLaststep .box_oneLaststep .txt_content ul li p{font-size:14px;line-height:22px;color:#333; display:flex;}
            #wdt_oneLaststep .box_oneLaststep .txt_content ul li p:last-child{margin-bottom:0}
            #wdt_oneLaststep .close-btn{ background:none;border:0;font-size:40px; font-weight:normal; line-height:1;color:#7b7b7b;transform:rotate(-45deg);position:absolute;top:-8px;z-index:1; font-family:initial; outline:none;cursor:pointer;}
            #wdt_oneLaststep.active .close-btn{top:-4px; right:0;}
            #wdt_oneLaststep .box_oneLaststep .checkbox{display:block;margin:20px 0 0;}
            #wdt_oneLaststep .box_oneLaststep .checkbox p{margin-bottom:20px;}
            #wdt_oneLaststep .box_oneLaststep .checkbox label{cursor:pointer;vertical-align:middle;font-weight:normal;line-height:20px;color: #333;position:relative;}
            #wdt_oneLaststep .box_oneLaststep .checkbox input[type=checkbox]{margin:5px 10px 0 0}
            #wdt_oneLaststep .box_oneLaststep .checkbox label a{color:#5b9ae4;}

            .toastmessage{background: #3b4045; color:#fff; position: fixed; bottom: 0; left: 0; right: 0; z-index: 1005; opacity: .95; padding: 20px; text-align:center; font-size:15px; line-height:25px; font-family:arial; width: 1000px; margin: auto; box-sizing: border-box;}
            .toastmessage a, .toastmessage a:hover, .toastmessage a:focus{text-decoration:underline; color: #fff;}
            .toastmessage button.okBtn{border: 0; outline: none; height: 25px; width: 50px; text-align: center; font-size: 12px; font-family: arial; font-weight: bold; cursor: pointer; line-height: 25px; margin-left: 20px; color:#000; background:#fff}
            .toastmessage button.okBtn:hover{color:#fff; background:#000}
            .experiencepage .rhspdtop {padding-top:80px;}
            .experiencepage .rhspdtop .colombiatracked {margin: 10px auto 0;}
            .wdt_timespoints {font-size:80%; font-family:arial}
            .news-card.only-info.col4.con_ads {min-height:86px;padding: 0 0 3%;}
            .col12.pd0.top_headlines .news-card.lead .img_wrap{height:240px;}
            .youtube-iframe{position:relative;width:180px;height:25px;display:flex;align-items:center;z-index:10;}
            .youtube-iframe .txt{font-family:arial;font-size:12px;color:#000;text-transform:capitalize;}
            .youtube-iframe iframe{position:absolute;width:118px;height:25px;top:0;left:65px;z-index:100;}
            .youtube-iframe iframe:hover{height:150px;width:250px;}

            .slider .hidden_layer {overflow: hidden;white-space: nowrap;}
            .slider ul li .con_wrap {margin: 10px 0 0; padding: 0; display: block; white-space: normal;}
            .slider .slider_content li {display: inline-block; vertical-align: top;}
            .header-desktop .logo .logo-gadget img{width:151px}
            .default-outer-player{height:367px; width:100%; position:relative;margin:auto; background-size: contain; background-repeat: no-repeat; background-position: center center;}
            .liveblink_newscard {display: inline-block; position: relative; font-size: 12px; text-transform: uppercase; padding-left: 13px; color: red;line-height:24px; vertical-align:text-top; font-family:sans-serif;}
            .news-card.lead .liveblink_newscard {vertical-align:text-bottom;font-size:16px;}
  `;

  const headerCss = `
            .header-desktop {width: 1000px; margin: 0 auto;}
            .header-desktop .logo{width: 180px; float: left; text-align: center; border: 1px solid #e9e9e9; border-top: 0; box-sizing: border-box; height: 76px; position:relative; z-index:12}
            .header-desktop .logo h1, .header-desktop .logo h2{margin:0}
            .header-desktop .logo h1 a, .header-desktop .logo h2 a {height: 60px; line-height: 60px; display: block;}
            .header-desktop .logo h1 a img, .header-desktop .logo h2 a img {vertical-align: middle;}
            .header-desktop .logo .other-sites {display:none;}
            .header-desktop .logo .txt-seo{font-size: 1px; display: block; text-indent: -100000px; height: 1px; color:#fff;}
            .header-desktop .logo .datetime{display:none}
            .header-desktop .top_nav{display: inline-block; vertical-align: top; width: calc(1000px - 180px); height:42px; background: #f1f1f1;}
            .header-desktop .top_nav ul.extranav{display:none}
            .header-desktop .nav_wrap .second-level-menu {height: 34px; background:#4d4d4d;}
            .header-desktop .nav_wrap .first-level-menu{display: inline-block; vertical-align: top; width: calc(1000px - 180px); background: #4d4d4d; position: relative; height: 34px;}
            .header-desktop .nav_wrap .first-level-menu .items {margin: 0 40px 0 40px; display: flex; flex-wrap: nowrap; overflow: hidden;}
            .header-desktop .nav_wrap ul {display: flex; flex-wrap: wrap; margin:0; padding:0;}
            .header-desktop .nav_wrap ul li {height: 34px; line-height: 34px; white-space: nowrap; padding: 0 5px;}
            .header-desktop .nav_wrap ul li a {font-size: 1.1rem; font-weight:bold; color: #fff; display: block;}
            .header-desktop .nav_wrap ul li.highlights{background:#e29494}
            .header-desktop .nav_wrap ul li.desktophide, .desktophide{display:none}
            .header-desktop .top_nav .globalnav, .header-trending, .header-desktop .nav_wrap .nav-more, .header-desktop .nav_wrap .first-level-menu h3.home, .header-desktop .nav_wrap .first-level-menu .search_in_header{display:none}
            
            .top_banner{width:1000px; margin:auto; position:relative; height:50px; background:#1c1c1e}
            .top_banner a{display:block}
            .top_banner .corona-numbers.desktop{position:absolute; top:0; bottom:0; left:70px; right:0; margin:auto; color:#fff; font-family:arial}
            .top_banner .corona-numbers.desktop .inline-values{display:flex; height: 50px; align-items: center;}
            .top_banner .corona-numbers.desktop .name{font-size:12px; text-transform: uppercase; display:block; margin-bottom:5px}
            .top_banner .corona-numbers.desktop .odometer{min-height:20px; height:20px; font-weight:bold}
            .top_banner .corona-numbers.desktop .odometer, .top_banner .corona-numbers.desktop .odometer .digit{font-size:20px; line-height:20px; box-shadow: none; font-weight:normal}
            .top_banner .corona-numbers.desktop .odometer{color:#6c6c6c}
            .top_banner .corona-numbers.desktop .heading{width:34%; text-align:left; text-transform: uppercase;}
            .top_banner .corona-numbers.desktop .heading h4{font-size:18px; margin:8px 0 0 0; display:inline-block; vertical-align:top}
            .top_banner .corona-numbers.desktop .heading h4 b{font-size:18px; font-weight:normal; color:#b4b4b4}
            .top_banner .corona-numbers.desktop .heading span{font-size: 14px; display: inline-block; vertical-align: top; margin: 4px 0 0 25px; padding: 5px 8px; border: 1px solid #4c4b4b; background: #000; border-radius: 4px; position:relative;}
            .top_banner .corona-numbers.desktop .confirmed{width:22%; text-align:center; border-right:2px solid #3b3b3b}
            .top_banner .corona-numbers.desktop .confirmed .name{color:#66a6f3}
            .top_banner .corona-numbers.desktop .recovered{width:22%; text-align:center; border-right:2px solid #3b3b3b}
            .top_banner .corona-numbers.desktop .recovered .name{color:#42ef82}
            .top_banner .corona-numbers.desktop .deaths .name{color:#fb2b2b}
            .top_banner .corona-numbers.desktop .deaths{width:22%; text-align:center}
            
            .desktop_body .headerContainer .ad1 {margin: 10px 0; padding: 0; min-height:90px}
            .section-wrapper .top_headlines ul.more_headlines{margin:10px 0 0;padding:0;}
            .section-wrapper .top_headlines ul.more_headlines li{max-width:205px;margin-right:20px;padding-left:0;padding-right:0;}
            .section-wrapper .top_headlines ul.more_headlines li:nth-child(3n+3){margin-right:0;}
            .section-wrapper .briefContainer {min-height:480px;}
            .section-wrapper .cityContainer {min-height:480px;}
            .section-wrapper .video-section {min-height:700px;}
            .trending-bullet{border:1px solid #dddddd; width: 100%; display: flex; box-sizing: border-box; flex-wrap: wrap; text-align:center; padding: 10px 5px 5px; margin-bottom: 10px;}
            .trending-bullet h3{display:none}
            .trending-bullet .scroll_content{overflow: hidden;}
            .trending-bullet a{font-size:1.1rem; line-height:1.9rem; color:#000; margin: 0 0 5px 0; display: inline; vertical-align: top; font-weight: bold;}
            .desktop_body .section-wrapper .rhs-widget .ad1.mrec1{min-height:250px; min-width:300px}
            // .wdt-trending-minitv-breaking{min-height:37px; margin-top:10px; margin-bottom:10px}

            .banner-image img{max-height:100px}
            .articleshow_body .banner-image{margin:20px 0}
            .articleshow_body .banner-image img{max-height:190px}

            .yContainer{text-align: right; margin: -10px 3% 5px 0; display:block; overflow:hidden;} 
            .yContainer span{display: inline-block; vertical-align:top; text-transform: uppercase; font-size: 12px; line-height: 28px; color: #716f6f; margin: 0 10px 0 0; height: 24px;}
            .story-article .news_card .yContainer, .videolist-container .top-news-content .yContainer, .videoshow-container.box-content .yContainer{padding:8px 3% 2px 0; background: #eaeaea; border-bottom: 1px solid #c6c6c6; margin:0}
            .videoshow-container.box-content .yContainer{margin:10px 0 0 0}
            .box-content .yContainer{background:transparent}
            .master-player-container .yContainer, .social_drop{display:none}
            .desktop_body .headerContainer .atf-wrapper {margin: 10px 0; padding: 0; height: 50px;}
            .header-desktop .dropDown_city svg{width: 20px; height: 23px;}
  `;

  const articleshowCss = `
            .story-article {color: #000;}
            .article-section {background: #fff;}
            .news_card{margin:0 0 4%;}
            .news_card h1{font-size:2.0rem;line-height:3rem;padding:15px 3% 5px;color:#000;}
            .news_card .news_card_source{margin:0 3% 10px;display:block;font-size:1.2rem;color:#a0a0a0;}
            .story-article img {max-width: 100%; height: auto;}
            .story-article .story-content{padding:0 4% 20px 4%; font-size: 1.8rem; line-height:3.2rem; word-wrap:break-word;}
            .story-article .story-content section{margin: 5px 0px 20px; box-shadow: 0px 5px 3px -2px rgba(143, 143, 143, 0.29); padding-bottom: 15px;}
            .story-article .story-content section:last-child{margin-bottom: 0;}
            .story-article .story-content section h2{margin: 15px 0 5px;}
            .story-article .story-content section p{display: block; margin:0 0 15px;}
            .story-article .story-content section p:last-child{margin:0}
            
            .story-article .source_sharing{min-height:74px;}
            .share_icon, .news_card .source_sharing .btn-subscribe{display:none}

            .news_card .enable-read-more {padding: 0 3% 10px;}
            .enable-read-more .first_col {width: 90%; display: inline-block; vertical-align: top;}
            .enable-read-more .second_col{width: 10%; vertical-align:bottom; margin: 0 0 5px; display: inline-block;}
            .enable-read-more .caption, .clone-caption {font-size: 1.4rem; line-height: 2.2rem; max-height: 4.4rem; transition: max-height .4s ease-out; font-weight: normal; overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 2; margin:0}
            // .articleshow_body .wdt_social_share svg{display:none; width:48px; height:48px}
            .articleshow_body .story-article .story-content{overflow:hidden}
            .story-article .story-content [data-plugin=ctn] {min-height: 250px;}
            .story-article .embedarticle {border-left: 5px solid #000; padding: 0 0 0 10px; position: relative; clear: both; display: block; margin: 10px 0;}  
            .story-article .embedarticle a {display: flex;}
            .story-article .embedarticle a img {width: 68px; height: 51px; margin-right: 15px;}
            .story-article .embedarticle a strong {font-size: 1.4rem; line-height: 2.2rem; max-height: 4.4rem; overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 2;}

            .articleshow_body .story-article .story-content .midArtContainer .ad1 {width: 300px; min-height: 250px; float: left; margin: 20px 20px 20px 0;}
            .articleshow_body .story-article .story-content .midArtContainer .ad1:empty{width: auto; min-width:auto; float: none; margin:0; min-height: 250px;}

            .recipeArticle .con_review_rate{display:none}
            .recipeArticle .time_calor{font-size: 12px; background-color: #f2f2f2; padding: 10px 0; overflow:hidden; margin:0 0 20px}
            .recipeArticle .time_calor ul{margin:0; padding:0; white-space:nowrap;}
            .recipeArticle .time_calor ul li{display:inline-block; vertical-align:top; width:33%; text-align:center;}
            .recipeArticle .time_calor .txt_innner {display: inline-block; vertical-align: middle; text-align: left; color:#333;}
            .recipeArticle .time_calor ul li i{display:inline-block; vertical-align:top;}
            .recipeArticle .time_calor ul li span {font-size:14px; line-height:12px; font-weight:bold; font-family:arial; display:block; margin-top: 3px;}
            .recipeArticle .time_calor ul li svg{ width:28px; height:28px; display: inline-block; vertical-align: middle; margin-right: 8px; color:#333;}
            .recipe_ingriedient{border-bottom:1px solid #d8d8d8; padding: 0 0 20px 0; margin-bottom: 15px}
            .recipe_ingriedient h4{color: #525252; font-size: 2.0rem; margin: 5px 0 20px; text-transform: uppercase;}
            
            .desktop_body .box-item {margin: 0 0 30px;}
            .articleshow_body .news_card h1 {padding: 0; margin: 0 0 15px;}
            .section-wrapper .data-drawer {padding-bottom: 20px;}
            .articleshow_body .news_card .enable-read-more {padding: 0 0 10px;}
            .articleshow_body .news_card .source_sharing {padding: 0 0 10px 0; margin: 0 0 10px 0; border-bottom: 1px solid #c3c3c3;}
            .articleshow_body .wdt_social_share {text-align: right; float: right; width: 36%;}
            .articleshow_body .news_card .news_card_source {margin: 2px 0 5px; display: inline-block; vertical-align: top;}
            .articleshow_body .news_card .news_card_source span[itemProp=author] {font-weight: bold;display: inline-block;margin: 0 0 2px;}
            .articleshow_body .news_card .news_card_source span.time {display: inline-block;margin-top: 3px; font-size: 1.0rem; line-height: 1.4rem;}
            .articleshow_body .news_card .news_card_source .txt{font-size:1rem; margin-right:3px}
            .articleshow_body .news_card .news_card_source .txt span{color:#a0a0a0}
            .desktop_body .wdt_social_share span {width: 45px; height: 35px; display: inline-block; vertical-align: top; margin: 0 0 0 2px;}
            .articleshow_body .story-article .img_wrap .img_caption {font-size: 1.2rem; line-height: 2rem; margin: 5px 0 0 0;}
            .articleshow_body .story-article .story-content {padding: 0 0 20px;}
            .articleshow_body .story-article .img_wrap {margin: 0 0 10px;}
            .articleshow_body .pwa-deals {min-height: 400px; margin-bottom:20px;}
            .story-article .asleadimg{min-height: 492px;}
            .rhs-mrec-wrapper {min-height: 280px;}
            .desktop_body .rhs-mrec-wrapper .box-item {margin-bottom:0; padding-bottom: 30px;}
            .desktop_body .rhs-other-container .briefParent{min-height: 420px;}
            .desktop_body .rhs-other-container .dataDrawerContainer{margin: 5px 0; min-height: 119px;}
            .desktop_body .wdt_social_share span.sms {display: none;}
            .top-article { padding: 2% 3%; margin: 0 0 4%;}
            .top-article h3 { font-size: 16px; margin: 0 0 5px;}
            .top-article li { padding: 0 0 0px 15px; font-size: 1.4rem; line-height: 2.2rem; margin: 0 0 10px;}
            .shareModalContainer .iconsquare svg, .wdt_social_share .iconsquare svg {width: 24px; height: 24px;}
            .weather-data-drawer {width: 315px; margin: 5px 0; min-height: 119px;}
            .news-card.horizontal.con_ads {padding: 0 0 3%;}
            div[ctn-style=ctnppdad] {min-height: 0px}
            .articleshow_body .story-article iframe.amzproduct{height:190px}
  `;

  const moviereviewCss = `
            .moviereview-summaryCard .img_wrap {width: 237px;}
            .moviereview-summaryCard .img_wrap img {max-width: 237px; max-height: 313px; width: auto;}
            .moviereview-summaryCard .con_wrap h1.movie-name {color:#000; font-size: 1.6rem; line-height: 2.4rem; max-height: 4.8rem; white-space: normal; display: -webkit-box; -webkit-line-clamp: 2; padding: 0;}
            .moviereview-summaryCard .table .con_wrap {text-align: left; word-break: break-word; padding: 0 0 0 10px; vertical-align: top; position: relative; width: 100%;}
  `;

  const gadgetnowCss = `
            .wdt-inline-gadgets{padding:0 0 30px; position:relative; width:100%}
            .wdt-inline-gadgets ul.scroll{display:flex; align-items: flex-end; width:100%; overflow: visible; padding:0}
            .wdt-inline-gadgets ul.scroll li {display: inline-block; vertical-align: top; position:relative}
            .wdt-inline-gadgets ul.scroll li a{border-radius:4px;box-shadow:0 1px 4px 0 rgba(0, 0, 0, 0.15), 0 1px 4px 3px rgba(0, 0, 0, 0.1);border:solid 1px #b6b6b6;background:#ffffff;height:80px;line-height:50px; width: 73px; text-align: center; padding: 20px 2px 5px; position: relative; margin:5px; display:block}
            .wdt-inline-gadgets ul.scroll li label{font-size:1.2rem; line-height:1.6rem; display:block;margin-top:5px;cursor:pointer;position:absolute;bottom:5px;left:0;right:0; white-space: normal;}
            .wdt-inline-gadgets.enable-slider{max-width:655px; padding-bottom: 0;}
            .wdt-inline-gadgets.enable-slider .slider-hidden{overflow:hidden; padding-bottom: 30px;}
            .btn-blue {background: #135394; height: 40px; padding: 0 15px; color: #fff; font-size: 18px; cursor: pointer; line-height: 40px; outline: none; border: 0; display: block; width: 60%; margin: 20px auto; border-radius: 5px; text-align: center;}
            .container-comparewidget.web .wdt-inline-gadgets ul.scroll li a{width:73px}
            .container-comparewidget.web .wdt_compare-gadgets{background:#f0f0f0;padding:20px 20px 5px;margin:0 0 20px 0;}
            .container-comparewidget.web .wdt_compare-gadgets h1, .container-comparewidget.web .wdt_compare-gadgets h2{font-size:1.6rem;margin:0 0 10px;font-weight:bold;}
            .container-comparewidget.web .wdt_compare-gadgets .search-suggest{font-size: 1.1rem; font-weight: normal; color: #888585;}
            .container-comparewidget.web .wdt_compare-gadgets .input_field:nth-child(4){margin-right:0;}
            .container-comparewidget.web .wdt_compare-gadgets .input_field{display:inline-block;vertical-align:top;margin-right:4%;width:22%;position:relative;}
            .container-comparewidget.web .wdt_compare-gadgets .input_field input[type=text]{padding:8px 25px 6px 10px;font-size:14px;box-sizing:border-box;width:100%;color:#000;border-radius:5px;outline:none;border:1px solid #d5d5d5;}
            .container-comparewidget.web .wdt_compare-gadgets .btn-captions{display: flex; align-items: center; justify-content: center; margin: 20px 0 10px;}
            .container-comparewidget.web .wdt_compare-gadgets .info-camparsion{display:block;text-align:center;color:#888;font-size: 1.1rem; line-height: 1.7rem;}
            .container-comparewidget.web .wdt_compare-gadgets .info-camparsion.red{color:#ff0000;}
            .container-comparewidget.web .wdt_compare-gadgets .btn-blue{margin:0 20px 0 0;width:25%;}

            .container-compare-show h1 {font-size: 1.8rem; line-height: 2.6rem; margin-bottom: 15px;}
            .wdt_compare-gadgets.details {background: #f0f0f0; padding: 20px; margin: 0 0 20px 0;}
            .wdt_compare-gadgets.details ul li{position:relative;background:#fff;display:inline-block;vertical-align:top;width:195px;margin:0 20px 0 0;min-height:240px;padding:15px;border-radius:6px;white-space:normal;}
            .wdt_compare-gadgets.details ul li:last-child{margin-right:0;}
            .wdt_compare-gadgets.details ul li span {display: block; text-align: center; margin: 0 0 5px;}
            .wdt_compare-gadgets.details ul li .img_wrap{height:130px;margin-top:10px;}
            .wdt_compare-gadgets.details ul li .img_wrap img{max-height:100px;width:auto;height:auto;max-width:100%;}
            .wdt_compare-gadgets.details ul li .con_wrap h4{height:44px;font-weight:bold; font-size: 14px; line-height: 22px; color: #000; margin:0}
            .wdt_compare-gadgets.details ul li .con_wrap .price_tag{font-size:1.2rem; color:#d70000; line-height: 20px; font-weight: bold; font-family: arial;}
            .wdt_compare-gadgets.details ul li .btn_wrap{margin:5px 0 0 0;}
            .wdt_compare-gadgets.details ul li .btn_wrap .btn-buy{width: 100%; font-size: 12px; height: 25px; line-height: 28px;}
            .wdt_compare-gadgets.details .input_field{width: 100%; margin-top: 90px; position: relative;}
            .wdt_compare-gadgets.details .compare_criteria{display:none}

            ul.gn-tabs{display:flex;-ms-flex-wrap:nowrap;flex-wrap:nowrap;overflow-x:auto;overflow-y:hidden;width:80%;margin:22px auto 20px;}
            ul.gn-tabs li{display:inline-block;text-align:center;white-space:nowrap;flex-basis:100%;cursor:pointer;}
            ul.gn-tabs li a{display: block; height: 3rem; line-height: 3rem;}
            ul.gn-tabs li:first-child{border-radius:5px 0 0 5px;}
            ul.gn-tabs li:last-child{margin-right:0;border-radius:0 5px 5px 0;}
            ul.gn-tabs li{display:block;color:#5a5a5a;font-size:1.4rem;line-height:3.4rem;height:3.0rem;border:solid 1px #acacac;}
            ul.gn-tabs li.active{color:#fff;background:#000;border-color:#000;}
            ul.gn-tabs li.active a{color:#fff;}

            .wdt-inline-gadgets .slider .slider_content li .slide{display:inline-block;vertical-align:top;box-shadow:0 0 5px #b8b8b8;white-space:normal;text-align:center;margin:5px 0 5px 5px;padding:5px 5px 40px;min-height:350px;position:relative; box-sizing: border-box;}
            .wdt-inline-gadgets .slider .slider_content li .slide:nth-child(3n + 3){margin-right:0;}
            .wdt-inline-gadgets .slider .slider_content li .slide .txt_container{display:block;font-size:16px;line-height:24px;font-weight:bold;white-space:normal;margin:10px 0 0 0;}
            .wdt-inline-gadgets .slider .slider_content li:last-child .slide:last-child{margin:5px 0 5px 5px}
            .wdt-inline-gadgets .slider.trendingslider .prod_img{width:auto;height:120px;padding-top:20px;}
            .wdt-inline-gadgets .slider.trendingslider .slide span{display:block; cursor: pointer;}
            .wdt-inline-gadgets .slider.trendingslider ul.ram-storage{display: flex; flex-wrap: wrap; justify-content: center;}
            .wdt-inline-gadgets .slider.trendingslider ul.ram-storage li{font-size:1.1rem;line-height:1.7rem;color:#666666;padding:0;margin:0 0 5px 10px;position:relative; border: 0;}
            .wdt-inline-gadgets .slider.trendingslider img{max-width:100%; max-height:100px; width:auto}
            .wdt-inline-gadgets .slider.trendingslider .title{display:inline-block;}
            .wdt-inline-gadgets .slider.trendingslider .title .text_ellipsis{font-size:1.4rem;line-height:2.2rem; height:4.4rem;font-weight:bold; display: -webkit-box;}
            .wdt-inline-gadgets .slider.trendingslider .rating span{display:inline-block;vertical-align:top;position:relative;font-size:1.1rem; margin-right: 10px; color:#777}
            .wdt-inline-gadgets .slider.trendingslider .rating span:last-child{margin-right: 0;}
            .wdt-inline-gadgets .slider.trendingslider .rating span a{color:#1a75ff}
            .wdt-inline-gadgets .slider.trendingslider .rating b{font-family:arial;}
            .wdt-inline-gadgets .slider.trendingslider .rating b small{color:#f5a623;font-size:1rem;}
            .wdt-inline-gadgets .slider.trendingslider .rating span:first-child{margin:0 10px 0 0;}
            .wdt-inline-gadgets .slider.trendingslider .price{margin-top:5px;min-height:21px;}
            .wdt-inline-gadgets .slider.trendingslider .price b{font-size:1.2rem; font-family:arial; font-weight:bold;}
            .wdt-inline-gadgets .slider.trendingslider .btn_wrap{position: absolute; left: 0; right: 0; margin: auto; bottom: 10px;}
            .wdt-inline-gadgets .slider.trendingslider .btn_wrap button{text-align: center; color: #000; padding: 0 8px; font-size: 1.0rem; border: 0; height: 28px; line-height: 32px; cursor: pointer; background: #fed20a; border-radius: 3px; outline: none; font-weight: bold;}
            .wdt-inline-gadgets .seo-gadget-tags{display:none}

            .gl_top_bar ul.gl_suggested_keywords li{display:inline-block;vertical-align:top;background:#e6e6e6;border:1px solid #dadada;white-space:nowrap;border-radius:18px;margin:0 15px 15px 0;}
            .gl_top_bar ul.gl_suggested_keywords li:last-child{margin-right:0;}
            .gl_top_bar ul.gl_suggested_keywords li a{display:block;font-size:1.1rem;color:#000;height:34px;line-height:34px;padding:0 20px;}
            .gl_top_bar ul.gl_suggested_keywords li.active a{color:#fff;font-weight:bold;}
            .gl_top_bar ul.gl_suggested_keywords li.active{background:#135394;border-color:#135394;}
  `;

  const gadgetnow_nbt_css = `
      .m-scene.tech .gn_trending .list-trending-topics a{font-size:1rem; line-height:1.6rem}
      .m-scene.tech .wdt-inline-gadgets .slider.trendingslider .title .text_ellipsis{font-size: 1.2rem; line-height: 2.0rem; height: 4.0rem;}
      .m-scene.tech .wdt-inline-gadgets .slider.trendingslider .price b{font-size:1.4rem}
 `;

  const sportsCss = `
      .sports-section .rhs-widget .ad1{margin-top: 20px;}      
      .desktop_body .mini_schedule{width:100%}
      .desktop_body .mini_schedule .slider{width:980px !important}
      .desktop_body .mini_schedule .wdt_schedule{width:280px; padding: 5px 10px 5px; margin: 10px 10px 10px 5px; min-height: 120px; display:inline-block; vertical-align:top; position:relative; overflow:hidden} 
      .desktop_body .mini_schedule .wdt_schedule .con_wrap{margin:0; display:inline-block}
      .mini_schedule .slider .hidden_layer {overflow: hidden; white-space: nowrap;}
      .mini_schedule .wdt_schedule .match_date_venue{height: 1.5rem;}  
      .mini_schedule .wdt_schedule .score{font-size: 1.2rem}
      .mini_schedule .wdt_schedule .over, .mini_schedule .wdt_schedule .country{font-size:1.0rem}
      .mini_schedule .wdt_schedule .teama .teaminfo img, .mini_schedule .wdt_schedule .teamb .teaminfo img{width:35px}
      .mini_schedule .wdt_schedule .match_final_status{margin-top:5px; max-height:32px; overflow:hidden}
      .mini_schedule .wdt_schedule .match_date_venue, .mini_schedule .wdt_schedule .match_final_status .match_venue, .mini_schedule .wdt_schedule .match_final_status{font-size:12px; line-height:16px; height:auto}   
      .mini_schedule .slider .btnPrev, .mini_schedule .slider .btnNext{display:none;}
      .mini_schedule .wdt_schedule .teama .teaminfo{min-height:33px}
      .schedule_container{margin: 0 0 4%; background: #fff; width:100%}
      .wdt_schedule{border-radius: 8px; font-family: arial; padding: 5px 10px 5px; margin: 10px 0 10px 0; display: inline-block; width: 100%; vertical-align: top; min-height: 90px; box-sizing: border-box;}
      .wdt_schedule:last-child{margin-right:4%;}
      .wdt_schedule .liveScore{margin-top:5px;}
      .wdt_schedule .match_date_venue{font-size:1.0rem;line-height:1.5rem; text-align:center;white-space:normal;text-transform:uppercase;}
      .wdt_schedule .match_date_venue span{display:block; font-weight:bold;line-height:2rem;}
      .wdt_schedule .teama, .wdt_schedule .teamb{width:40%;}
      .wdt_schedule .teama .teaminfo, .wdt_schedule .teamb .teaminfo{overflow:hidden;clear:both;display:block; margin-top:2px}
      .wdt_schedule .teama img{float:left; margin:2px 5px 0 0;width:100px;}
      .wdt_schedule .teama .con_wrap{display:inline-block;vertical-align:top;}
      .wdt_schedule .teamb{text-align:right;}
      .wdt_schedule .teamb .con_wrap{display:inline-block;vertical-align:top;}
      .wdt_schedule .teamb .teaminfo img{float:right;margin:2px 0 0 5px;width:100px; border-radius: initial;}
      .wdt_schedule .teamb .country{text-align: right; display: block; width: auto;}
      .wdt_schedule .country{display:block;font-size:1.2rem;font-weight:bold;line-height:normal;text-transform:uppercase;text-align:left;width:50px;}
      .wdt_schedule .country.btm{width: 100px; clear: both; text-align: center; padding-top: 10px;}
      .wdt_schedule .teama .country.btm{float:left}
      .wdt_schedule .teamb .country.btm{float:right}
      .wdt_schedule .score{font-size:1.4rem;font-weight:bold;display:block;margin:0 0 1px;}
      .wdt_schedule .over{font-size:1.1rem; display:block;}
      .wdt_schedule .match_final_status{font-size:1rem;line-height:1.4rem; text-align:center;white-space:normal;margin-top:10px;text-transform:uppercase;}
      .wdt_schedule .match_final_status .status_live{font-size:10px;display:inline-block;padding:0 7px 0 15px;height:17px;line-height:17px;text-transform:uppercase;border-radius:3px; position: absolute; left: 0; right: 0; bottom: -1px; width: 30px; margin: auto;}
      .wdt_schedule .match_final_status .match_venue{font-size:1rem;line-height:1.5rem; overflow: hidden; display: block;}
      .wdt_schedule .match_final_status .time{margin-right:5px; color:#000;}
      .wdt_schedule .versus{width:10%; text-align: center;}
      .wdt_schedule .versus span{height: 23px; width: 23px; border-radius: 50%; display: inline-block; font-size: 12px; text-align: center; padding: 6px 0 0 0; font-weight: bold; margin-top: 5px; font-family: arial; box-sizing: border-box;}
      .wdt_schedule .calendar{display:none}
      .schedule_intro{background: #fff; width:100%}
      .matchNvenue{background: #fff; text-align: right; width:100%}
      .matchNvenue select {width: 20%; padding: 5px 5px 3px; font-size: 12px; margin-right: 2%; outline: none;}
      .matchNvenue select:last-child{margin:0;}
      .articlelist .mini_schedule{margin-bottom:15px}
      .wdt_pointsTable{width: 100%;}  
      .wdt_pointsTable .table_layout{margin:0 0 4%;}
      .wdt_pointsTable .table_layout table{width:100%;border-collapse:collapse;margin:0 auto;border-spacing:0;border:0;}
      .wdt_pointsTable .table_layout table tr td{font-size:1.0rem; font-weight:bold;text-align:center;}
      .wdt_pointsTable .table_layout table tr td:first-child{text-align:left;text-transform:uppercase;font-weight:bold;width:45%;}
      .wdt_pointsTable .table_layout table tr:first-child th:first-child{text-align:left;text-transform:uppercase;font-weight:bold;width:45%;}
      .wdt_pointsTable .table_layout table tr td:first-child img{width: 18px; height: 18px; margin-right: 5px; border-radius: 50%;}
      .wdt_pointsTable .table_layout table tr:first-child th{font-size:1.1rem;}
      .wdt_pointsTable .table_layout table td, .wdt_pointsTable .table_layout table th{padding:10px 5px;}
      .wdt_teams ul{display:flex; flex-wrap: wrap; margin-bottom:25px}
      .wdt_teams ul li{text-align:center; width:25%; margin:2% 0}
      .wdt_teams ul li a{display:block}
      .wdt_teams ul li img{width:70%}    
      .wdt_teams ul li .name{display:block; font-size:1.2rem; font-weight:bold; margin:10px 0 0 0;}
      .wdt_teams.hp ul{margin-bottom:0}
      .wdt_teams.hp ul li{width:12.5%}
      .desktop_body .wdt_venues {margin:0 0 30px}
      .desktop_body .wdt_venues ul li.news-card {padding: 0 2.5px; margin-bottom: 5px;}
      .desktop_body .wdt_venues ul li.news-card .con_wrap {padding: 7px 10px 6px; background: #282e34; display: block; box-sizing: border-box; margin:0}
      .desktop_body .wdt_venues ul li.news-card .con_wrap .text_ellipsis {color: #fff; height: 4.4rem; font-size: 1.2rem; line-height: 2rem; max-height: 4rem;}
      .desktop_body .wdt_venues ul.col12{padding:0}
      .desktop_body .wdt_venues.hp .top_section{margin:0 0 20px}
      .wdt_venues.hp .slider .btnPrev, .wdt_venues.hp .slider .btnNext{top:95px}
      .schedule.hp{margin-bottom:30px}
      .schedule.hp .wdt_schedule .teama img, .schedule.hp .wdt_schedule .teamb .teaminfo img{width:50px}
      .schedule.hp .wdt_schedule .teama, .schedule.hp .wdt_schedule .teamb{width:20%}
      .schedule.hp .wdt_schedule .country.btm{width:50px}
      .wdt_players .slider.players_slider{margin-top:30px}
      .wdt_players .slider.players_slider .slider_content li{border-right:1px solid #dfdfdf; text-align:center}
      .wdt_players .slider.players_slider ul li .img_wrap img{border-radius: 50%; overflow: hidden; width: 100px; height:100px; border:1px solid #dfdfdf; padding:5px}
      .wdt_players .slider.players_slider ul li .con_wrap .text_ellipsis{color:#000}
      .floating_cube.wdt_schedule{border-radius: 0; box-shadow: none; width: auto; margin: 5px 10px 0; padding: 0; display: block; background: transparent; min-height: 80px;}
      .floating_cube.wdt_schedule .versus span{font-size: 9px; padding-top: 5px; height: 18px; width: 18px;} 
      .floating_cube.wdt_schedule .wdt_schedule .score{font-size:1.2rem}
      .floating_cube.wdt_schedule .score, .floating_cube.wdt_schedule .over{font-size:12px}
      .floating_cube.wdt_schedule .score.active{color:#00b758}
      .floating_cube.wdt_schedule .teama img, .floating_cube.wdt_schedule .teamb img{width:20px}
      .floating_cube.wdt_schedule .match_date_venue{text-transform: initial; text-align: left; font-weight: bold; font-size: 1.1rem; background: #000; color: #fff;height: 30px; line-height: 32px; margin: -5px -10px 5px; padding: 0 10px;}
      .floating_cube.wdt_schedule .country{font-size:12px;}
      .floating_cube.wdt_schedule .match_final_status{line-height:14px; max-height: 28px; color:#828282; margin-top:3px; font-size:10px}
      .floating_cube.wdt_schedule .match_final_status .status_live{bottom:0}
      .flipbox-wrapper .flipbox-box .flipbox-side{min-height:115px}
      .flipbox-wrapper .flipbox-box .flipbox-side h4.news-txt{padding:7px 10px 0px 9px;}
      .flipbox-wrapper .flipbox-box .flipbox-side h4.news-txt a{font-size: 1.2rem; line-height: 1.8rem; height: 5.4rem; -webkit-line-clamp: 3;}
      .Cube{bottom:0;left:160px;}
      #content{height:125px;}
 `;

  const electionsCss = `
      .star-candidates {width:100%; box-sizing:border-box;}
      .star_candidates_widget {background:#e6ecee; padding:0 10px 20px; box-sizing:border-box; width:100%;}
      .star_candidates_widget .h2WithViewMore {margin: 0 0 10px 10px;}
      .star_candidates_widget h2 {font-size:22px;}
      .slider.candidates-newlist .btnPrev, .slider.candidates-newlist .btnNext {top:50%; transform: translateY(-50%);}
      .candidate_box {height:100%; box-sizing:border-box; font-weight:bold; border-radius: 5px; box-shadow: 0 0 4px 0 #c8d5d9; background-color: #ffffff; padding: 10px; text-align:center; margin: 2px;}
      .candidate_box .candidate_imgwrap {display:inline-block; position:relative; margin-bottom:10px}
      .candidate_box .candidate_imgwrap img {width:96px; height:96px; border-radius:50%; border:1px solid #e6ecee; padding:8px; box-sizing:border-box; object-fit: cover;}
      .candidate_box .candidate_name {font-size: 0.9rem; display:block; text-align: center; max-width: 100%; overflow:hidden; text-overflow:ellipsis; margin: 0 auto; white-space: nowrap;}
      .candidate_box .constituency {font-size: 0.9rem; color:#8c8c8c; display:block; marging: 5px 0 10px;}
      .candidate_box .party-name {font-size: 0.7rem; color:#8c8c8c; display:inline-block; color:#fff; border-radius:5px; padding: 5px 15px; text-transform:uppercase;}
      .candidatelist .slide {margin-bottom: 15px; margin-right: 1%; width: 15.8%; box-sizing: border-box;}
      .candidatelist .slide:nth-child(6n) {margin-right: 0;}
      .result_inner h2{font-size: 1.6rem; display:block; text-align:left; width:100%}
      .result_inner .el_resulttable {z-index: 9999; top:15px; padding:0; width: 30px; height:30px; border-radius:50%;outline:none;text-align:center;}
      .highcharts-contextbutton{display:none}
      .chart_container.exit-poll .chart-detail {font-size: 12px; position: absolute; white-space: nowrap; visibility: visible; margin-left: 0px; margin-top: 0px; left: calc(50% - 60px); top: calc(50% - 38px);}
      .chart_container.exit-poll .svg-chart-data{text-align: center; box-shadow: 1px 1px 18px #777; width: 120px; height: 120px; border-radius: 50%; background: #fff; font-size: 18px; line-height: 1; margin: 0 auto;}
      .chart_container.exit-poll .svg-chart-data h3{font-size: 1.8rem; padding-top: 36px; padding-bottom: 2px;}
      .chart_container.exit-poll .svg-chart-data h4{font-size: 1.8rem; font-weight: bold; padding: 5px 0 0 0;}
      .chart_container svg g {fill: rgba(128,128,128,.5);}
      .desktop_body .result_inner .combinedchartbox {width:100%;box-sizing:border-box;margin-bottom: 20px;padding:1px;}
      .desktop_body .result_inner .combinedchartbox .chart-horizontal .elections-slider {border: 1px solid #eee;}
      .desktop_body .result_inner .tbl_txt, .results_container .tbl_txt {background:#e6ecee; padding:7px 0; text-align:center;font-size:1.2rem; margin:0 0 15px;}
      .pie-chart{width:100%; max-width:100%; height: 330px; position:relative;}
      .chart-horizontal .pie-chart {width: 430px; height:300px;}
      .slider.elections-slider .btnPrev, .slider.elections-slider .btnNext {top:50%; transform: translateY(-50%);}
      .chartcountnew {width:100%; max-width:100%; height: 330px;}
      .chart-horizontal .chartcountnew {width:100%; max-width:100%; height: 300px;}
      .desktop_body .result_inner .highcharts-container .chart-detail {margin-top: 0;left: calc(50% - 123px);top: calc(50% - 64px);transform: translate(0, 0);}
      .desktop_body .result_inner .chart-horizontal .highcharts-container .chart-detail {left: calc(50% - 123px);}
      .desktop_body .result_inner .highcharts-container .legend {margin-bottom: 5px; height:26px; width:100px;font-size: 12px;font-weight: normal;padding: 0 7px;color: #fff;border-radius: 2px;line-height: 1;box-sizing: border-box;}
      .desktop_body .electoralmap_body .wdt_map_desktop{width:100%; position:relative}
      .desktop_body .electoralmap_body .wdt_map_desktop{position:relative; min-height:300px}
      .desktop_body .electoralmap_body .wdt_map_desktop .ctyLoader{position: absolute; z-index: -1; left: 0; right: 0; top: 0; bottom: 0; margin: auto;}
      .result-tbl {display: table; width: 100%; border-collapse: collapse; margin: 20px auto 10px; font-size: 1rem; background: #fff;}
      .result-tbl .tbl_row {display: table-row; text-align: left; color: #333;}
      .result-tbl .tbl_row:nth-child(even) {background-color: #e6ecee;}
      .result-tbl .tbl_row:last-child {background-color: transparent;}
      .result-tbl .tbl_row:last-child .tbl_col {border: none;font-weight: bold;}
      .result-tbl .tbl_row:last-child .tbl_col span {color: #7b7b7b;}
      .result-tbl .tbl_col {display: table-cell;border-bottom: 1px solid #e5e5e5;padding: 5px 2%;font-size: 13px;vertical-align: middle;line-height: 18px;text-transform: uppercase;text-align: center;}
      .result-tbl .tbl_col:first-child {border-left: 1px solid #e5e5e5;}
      .result-tbl .tbl_col:last-child {border-right: 1px solid #e5e5e5;}
      .result-tbl .tbl_col.party_logo {text-align: left;width: 45%;text-transform: uppercase;line-height: 25px;font-weight: bold;}
      .result-tbl .tbl_col.party_logo img {max-width: 32px !important; float: left; margin-right: 6px;}
      .result-tbl .tbl_col.party_logo img[src*="69376388"] {margin-top: 7px;}
      .result-tbl .tbl_heading {font-weight: normal;display: table-row;background-color: #000;text-align: left;line-height: 1.4rem;font-size: 1.3rem;color: #fff;text-transform: uppercase;}
      .result-tbl .tbl_heading .tbl_col {line-height: 24px;font-size: 12px;text-align: center;display: table-cell;vertical-align: middle;text-transform: uppercase;}
      .result-tbl .tbl_heading .tbl_col.first {text-align: left;padding-left: 4%;}
      .nodatawidget { padding: 20px; text-align: center; font-size: 18px; font-weight: bold; color: #666666; box-sizing:border-box; width:100%;}
      .nodatawidget .refresh_btn {position: static; margin-top: 30px;}
      .nodatawidget .refresh_btn a {display: inline-block; background-color: #F44336; padding: 4px 20px 4px 15px; border-radius: 5px; color: #fff;}
      .tablecount, .graphcount {width:100%;}
      .exitpoll-widget {margin: 0 0 20px;}
      .exitpoll-widget .section {padding: 0; width: 100%; box-sizing: border-box;}
      .exitpoll-widget .section .top_section { margin: 0;}
      .exitpoll-widget .section .top_section h2, .exitpoll-widget .section .top_section h1 {background: #1a1a1a; color: #fff; padding: 0px 10px; font-size: 20px; border: none; box-sizing: border-box;}
      .exitpoll-widget .section .top_section h2 a, .exitpoll-widget .section .top_section h1 a {color:#fff;}
      .exitpoll-widget .section .top_section .read_more {display:none;}
      .el_powerdby {text-align:center;}
      .el_resulttable {display:none;}
      .election_con .slider_content {white-space: nowrap; overflow: auto;}
      .table-chart {width: 100%;}
      .election_con .slider_content .table-chart {display: inline-block; margin-right: 10px; width: 350px;}
      .el_exitpoll {padding: 0 15px;}
      .results_container {position: relative;}
      .results_container .powerbycont .el_powerdby {font-size: 10px; font-weight: normal;}
      .results_container .graph-container .switcher {position: absolute; width: 100%; margin: 5px 0; box-sizing: border-box; padding: 0; z-index:10}
      .election-hpwidget .election-header-wrapper{display:flex;justify-content:space-between;align-items:center;}
      .election-hpwidget .election-header-wrapper .assembly-election-header{display:inline-block;background-color:#352f2f;border-radius:4px 0px 0px 4px;position:relative;font-size:1.2rem;line-height:2.2rem;color:#fff;padding-left:10px;}
      .election-hpwidget .election-header-wrapper .assembly-election-header span{display:inline-block;background-color:#fc363b;margin-left:7px;padding:0 7px;}
      .statestoggle .states{border:1px solid #c0c2c4;border-radius:3px;display:inline-flex;height:24px;}
      .statestoggle span{font-size:1rem; line-height:22px;color:#333;position:relative;padding:0 15px;vertical-align:top;cursor:pointer;}
      .statestoggle span b{font-weight:normal;position:relative;z-index:2;}
      .statestoggle span:last-child::after{display:none;}
      .statestoggle span.active{color:#fff;}
      .election-hpwidget .election-header-wrapper .read_more{font-size:13px;margin:0 15px 0 0;color:#000;font-weight:normal;position:relative;}
      .election-hpwidget .result_inner{margin-top:13px;padding:1px;}
      .election-hpwidget .result_inner .election-data-header{background:#ebeef1;display:flex;justify-content:space-between;align-items:center;padding:0 10px;}
      .election-hpwidget .result_inner .election-data-header .section{width:auto;}
      .election-hpwidget .result_inner .election-data-header .section .top_section{margin:0;}
      .election-hpwidget .result_inner .election-data-header .section .top_section h2{background:transparent;font-size:18px;padding:0;}
      .election-hpwidget .result_inner .election-data-header .section .top_section h2 a{color:#000;}
      .election-hpwidget .result_inner .election-data-header .electionsnav{display:flex;}
      .election-hpwidget .result_inner .election-data-header .electionsnav a{font-size:14px;color:#000;padding:0 10px;}
      .election-hpwidget .result_inner .star-candidates-wrapper{padding:0 4px 4px;}
      .election-hpwidget .result_inner .result-tbl .tbl_heading{background:transparent;color:#000; font-weight:bold;}
      .election-hpwidget .result_inner .result-tbl .tbl_heading .tbl_col{border-left:none !important;border-right:none !important;}
      .election-hpwidget .result_inner .result-tbl .tbl_heading .tbl_col:first-child {padding-left:25px;}
      .election-hpwidget .result_inner .result-tbl .tbl_col:first-child{border-right:1px solid #e5e5e5;}
      .election-hpwidget .result_inner .result-tbl .tbl_row:last-child .tbl_col{border:none;font-size:11px;}
      .election-hpwidget .result_inner .result-tbl .tbl_row:nth-child(even){background:transparent;}
      .election-hpwidget .result_inner .highcharts-container .chart-detail {left:calc(50% - 60px)}
      .result-tbl .tbl_col .partyclr {display: inline-block;border-radius: 2px;padding: 1px 7px;line-height: normal;color: #fff;font-size: 11px;margin-left: 20px;font-weight: normal;}
      .election-as-widget .statestoggle {margin-bottom:15px;}
      .election-hpwidget .row .col4.ad-count {align-items: center; align-content: center;}
  `;

  const homeCss = `
    .section-wrapper .top_headlines .news-card.lead .con_wrap .section_name {position: absolute; top: -32px; left: 0; right: 0; margin: auto; padding: 0 10px; max-width: 120px; text-align: center; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; height: 22px; line-height: 24px;}
    .section-wrapper .box-item {margin: 0 0 30px;}
    .section-wrapper .top_headlines .news-card.lead .img_wrap{height: 240px}
    .wdt-weather .wth-details .forecast {width: 55px;}
    .wdt-weather .wth-details .temp {font-size: 3.2rem; font-weight: 600;}
    .wdt-weather .wth-details .loaction {width: 35%;}
    .desktop_body .hp-rhs-ads-container {height: 310px}
  `;

  const photolistCss = ``;

  const photoshowCss = `
    .desktop_body .photomazzashow-content h1{padding:0 0 10px; font-size: 2.0rem; line-height: 3.0rem;}
    .desktop_body .photomazzashow-content .title_with_tpwidget h1{margin-bottom: 5px; width: 80%;}
    .desktop_body .photo_story li.photo_card .photo_des{padding:15px 3%; color: #9e9e9e;}
    
    .desktop_body .photo_story li.photo_card{margin-bottom: 20px; background: #282e34;}
    .desktop_body .photo_story li.photo_card .pagination{display: inline-block; vertical-align: top; width: 10%; position:static; padding: 0 2.5% 0 0; margin: 0 3% 0 0; border-right: 1px solid #505050;}
    .desktop_body .photo_story li.photo_card .pagination span b{color:#bbb}
    .desktop_body .photo_story li.photo_card .pagination span{font-size: 1.8rem;width: 60px; height: 60px; border-radius: 50%; padding: 0; text-align: center; line-height: 60px; border:1px solid #505050; font-family: arial; display: block;}
    .desktop_body .photo_story li.photo_card .photo_con{display: inline-block; vertical-align: top; width:74%;}
    .desktop_body .photo_story li.photo_card .photo_con h2{font-size: 1.4rem; margin-bottom: 5px;}
    .desktop_body .photo_story li.photo_card .caption{font-size: 1.2rem; line-height: 2.0rem; word-break: break-word;}
  `;

  const videolistCss = `
      .desktop_body .listing.videolist .section {width: calc(100% - 30px);margin: auto;}
      .desktop_body .videolist .only-heading{display: flex; align-items: center;}
      .desktop_body .videolist .only-heading .con_social {width: 40%;}
      .desktop_body .videolist .only-heading .con_social .youtube-iframe{margin-left: auto;}
      .desktop_body .only-heading .section h1 {border: 0;}
      .videolist .top_wdt_slider {background: #282e34; padding: 20px 5px 20px;margin: 0;}
      .videolist .top_wdt_slider .hidden_layer {overflow: hidden; white-space: nowrap;}
      .videolist .top_wdt_slider .col4 {min-height: 250px}
      .videolist .slider .slider_content li {display: inline-block;vertical-align: top;}
      .videolist .slider ul li .con_wrap {margin: 10px 0 0; display: block; white-space: normal;}
      .videolist .slider ul li .con_wrap .text_ellipsis {color: #fff; font-size: 1.2rem; line-height: 2rem; max-height: 6rem; -webkit-line-clamp: 3;}
      .desktop_body .listing .trending .row {margin: 0;}
      .desktop_body .listing .trending .section .read_more {display: none;}
      .desktop_body .listing ul li .con_wrap {margin: 10px 0 0; display: block;}
      .desktop_body .listing ul li .con_wrap .section_name {font-size: 1.2rem; display: block; margin-bottom: 5px;}
      .desktop_body .listing ul li .con_wrap .text_ellipsis {font-size: 1.2rem; line-height: 2rem; max-height: 4rem; -webkit-line-clamp: 2; font-weight:normal;}
`;

  const videoshowCss = `
      .desktop_body .videoshow-container .top_wdt_video .row .col8 .con_wrap {margin: 15px 0 0;}
      .desktop_body .videoshow-container.hypervideo .top_wdt_video .related-links {border-top: 2px solid #dfdfdf;}
      .desktop_body .videoshow-container .top_wdt_video .related-links {margin: 15px 0 0; padding: 15px 0 0; border-top: 1px solid #484848;}
      .desktop_body .videoshow-container .top_wdt_video .row .col8 .con_social {display: -ms-flexbox;display: flex;width: 100%;-ms-flex-pack: end;justify-content: flex-end;position: relative;}
      .desktop_body .videoshow-container .top_wdt_video .row .col8 .con_social .yContainer {margin-top: 15px;margin-right: auto;}
      .desktop_body .videoshow-container .top_wdt_video .row .col8 .con_wrap .more_items span {vertical-align: top; font-size: 1.0rem;}
      .desktop_body .videoshow-container .top_wdt_video .section {margin: 10px 0 0;}
      .vid_embbed {font-size: 1rem; text-transform: uppercase; margin: 22px auto 0 0; padding-left: 25px;}
      .desktop_body .yContainer {margin: 0;}
      .desktop_body .videoshow-container .top_wdt_video .row .col8 .con_social .wdt_timespoints {width: 90px; margin-top: 10px;}
      .desktop_body .videoshow-container .top_wdt_video .row .col8 .con_social .wdt_social_share {margin-top: 15px; display: block;}
      .desktop_body .videoshow-container .top_wdt_video .row .col8 .con_social .wdt_social_share span {width: 34px; height: 26px; margin: 0 0 0 5px;}
      .desktop_body .videoshow-container .top_wdt_video .section h1{border-bottom: 0; margin: 0; font-size: 1.6rem; height: auto;}
      .desktop_body .videoshow-container .top_wdt_video .section h1 span {height: auto; line-height: 2.6rem;}
      .desktop_body .videoshow-container .top_wdt_video .row .col8 .con_wrap .text_ellipsis {font-size: 1.2rem;}
      .desktop_body .videoshow-container .top_wdt_video .enable-read-more .first_col {width: 100%;}
      .desktop_body .videoshow-container .enable-read-more .caption {max-height: initial; -webkit-line-clamp: initial; display: block;}
      .desktop_body .videoshow-container.hypervideo .top_wdt_video .related-links .keywords h3 {font-weight: bold;}
      .desktop_body .videoshow-container .top_wdt_video .related-links .keywords h3 {margin: 0; vertical-align: top; margin-right: 10px; margin-bottom: 5px; font-size: 1.2rem; line-height: 2.0rem;}
      .desktop_body .videoshow-container .top_wdt_video .related-links .keywords a {font-size: 1.2rem; line-height: 2.0rem;}
`;

  const profieCss = `
      .section-topics h1 {font-size: 1.6rem; line-height: 2.4rem; margin:0}
      .section-topics .player-stats .profile-date {font-size: 12px; line-height:18px; color: #a0a0a0; margin: 5px 0 0; font-family: Arial, Helvetica, sans-serif;}
      .section-topics .player-stats{margin-top:20px}
      .section-topics .player-stats .recommended_news .col12{padding:0}
      .section-topics .player-stats .con_stats{width:100%}
      .section-topics .player-stats .lead-topics {margin: 0 0 10px; width: 100%;}
      .section-topics .player-stats .lead-topics .news-card.horizontal-lead {box-shadow: 0 1px 2px 1px rgba(0,0,0,0.1); padding: 10px; box-sizing: border-box; background:#fff; margin:0 0 20px}      
      .section-topics .player-stats .lead-topics .news-card.horizontal-lead .img_wrap img {width: 200px; max-height: 262px;}
      .section-topics .player-stats .lead-topics .news-card.horizontal-lead .con_wrap {width: 100%; padding: 0 0 0 20px;}
      .section-topics .player-stats .txt-wrap .only-txt, .section-topics .player-stats .profileDesc {display: block; font-size: 1.4rem; line-height: 2.6rem; width:100%; text-align: justify;}
      .section-topics .player-stats .profileInfo{border-top:1px solid #c3c3c3; margin-top:10px; padding-top:10px}
      .section-topics .player-stats .profileInfo li {display: flex; font-size: 13px; line-height: 17px; font-family: Arial, Helvetica, sans-serif; margin-bottom: 10px;}
      .section-topics .player-stats .profileInfo li:last-child{margin-bottom:0}
      .section-topics .player-stats .profileInfo li .head {width: 35%; font-weight: bold; text-transform: capitalize;}
      .section-topics .player-stats .profileInfo li .content {width: 65%;}
      .section-topics .player-stats .shortInfo li {font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: #333;}
      .section-topics .player-stats .shortInfo li span {display: inline-block; vertical-align:top; position:relative}
      .section-topics .player-stats .enable-read-more .first_col .text_ellipsis {font-size: 1.4rem; line-height: 2.2rem; max-height: 13.2rem; -webkit-line-clamp: 6; overflow: hidden;}
 `;

  const movieshowCss = `
      .movieshow .movieshowlft {width:100%;}
      .movieshow .moviewshowcard {background:#282e34; margin-bottom:15px;}
      .movieshow .moviewshowcard .mb10 {margin-bottom:10px;}
      .movieshow .moviewshowcard .movieinfo {padding:15px; color:#fff;}
      .movieshow .moviewshowcard .movietitle {display:flex;}
      .movieshow .moviewshowcard .moviemeta {font-size:1rem; line-height:1.2rem; width:70%;}
      .movieshow .moviewshowcard .ms_actions {width:30%;}
      .movieshow .moviewshowcard h1 {font-size:1.6rem; line-height:2.2rem; font-weight:bold; margin-bottom:15px;}
      .movieshow .moviewshowcard h1 b {border: 1px solid #fff;border-radius: 50%; width:24px; height:24px; font-size: 12px;vertical-align: top;color: #fff; line-height:24px; text-align:center; display:inline-block; margin-left:5px; font-weight:normal;}
      .movieshow .moviewshowcard .ms_button {border:1px solid #fff; border-radius:15px; height:30px; width:100%; margin-bottom:5px; text-align:center; font-size:1rem; background:transparent; color:#fff; cursor:pointer;}
      .movieshow .moviewshowcard svg {margin-right:5px; vertical-align:middle; width:16px; height:16px;}
      .movieshow .moviewshowcard .ms_poster {display:flex; margin-top:10px;}
      .movieshow .moviewshowcard .ms_poster .ms_posterimg {width: 25%;}
      .movieshow .moviewshowcard .ms_poster .ms_posterinfo {width:75%; padding-left:15px; box-sizing:border-box;}
      .movieshow .moviewshowcard .ms_share {margin: 10px 0; padding:10px 0 5px; border-top: solid 1px #4d4d4d; border-bottom: solid 1px #4d4d4d;max-height:45px; overflow:hidden;}
      .movieshow .moviewshowcard .ms_share .sharetxt {display:inline-block; font-size:1rem; vertical-align:text-top;}
      .movieshow .moviewshowcard .ms_share .wdt_social_share {display:inline-block; text-align:left; float:none; width:auto;}
      .movieshow .moviewshowcard .ms_synopsis h3 {font-size:1rem; font-weight:normal; margin-bottom:10px;}
      .movieshow .moviewshowcard .ms_trailers {padding-bottom: 15px;}
      .movieshow .moviewshowcard .ms_trailers .section {padding: 0 15px; box-sizing:border-box;}
      .movieshow .moviewshowcard .ms_trailers .section h2 span {font-size: 1.2rem; color:#fff; font-weight:normal;}
      .movieshow .moviewshowcard .ms_trailers .slider ul li .con_wrap .text_ellipsis {color:#fff;}
      .movieshow .moviewshowcard .ms_trailers .slider .btnPrev {left:-17px;}
      .movieshow .moviewshowcard .ms_trailers .slider .btnNext {right:-17px;}
      .movieshow .adcards {display: flex; justify-content:space-between; margin:10px 0;}
      .movieshow .adcards .box-item {width: 49%;}
      .movieshow .star-candidates {width:100%; box-sizing:border-box;}
      .movieshow .star_candidates_widget {background:#e6ecee; padding:0 10px 20px; box-sizing:border-box; width:100%;}
      .movieshow .star_candidates_widget .h2WithViewMore {margin: 0 0 10px 10px;}
      .movieshow .star_candidates_widget h2 {font-size:22px;}
      .movieshow .castprofile .slider .btnPrev, .movieshow .castprofile .slider .btnNext {top:50%; transform: translateY(-50%);}
      .movieshow .candidate_box {height:100%; box-sizing:border-box; font-weight:bold; border-radius: 5px; box-shadow: 0 0 4px 0 #c8d5d9; background-color: #ffffff; padding: 10px; text-align:center; margin: 2px;}
      .movieshow .candidate_box .candidate_imgwrap {display:inline-block; position:relative; margin-bottom:10px}
      .movieshow .candidate_box .candidate_imgwrap img {width:96px; height:96px; border-radius:50%; border:1px solid #e6ecee; padding:8px; box-sizing:border-box; object-fit: cover;}
      .movieshow .candidate_box .candidate_name {font-size: 0.9rem; display:block; text-align: center; max-width: 100%; overflow:hidden; text-overflow:ellipsis; margin: 0 auto; white-space: nowrap;}
      .movieshow .candidate_box .constituency {font-size: 0.9rem; color:#8c8c8c; display:block; marging: 5px 0 10px;}
      .movieshow .candidate_box .party-name {font-size: 0.7rem; color:#8c8c8c; display:inline-block; color:#fff; border-radius:5px; padding: 5px 15px; text-transform:uppercase;}
      .movieshow .metaitems {display:inline-block; margin-right: 10px;}
      .movieshow .metafirst {display:inline-block; border-right: solid 1px #575757; padding-right:10px; margin-right:10px;}
      .movieshow .dotlist {display:inline-block; padding-left:13px; margin-left:7px;}
      .movieshow .con_review_rate {margin:0;}
      .movieshow .rateBox {display:flex; justify-content: space-between;}
      .movieshow .rateBox h3 {margin: 0; padding: 0;font-size: 1.1rem;font-weight: normal; color:#fff;}
      .movieshow .rateBox .star-and-value {display: inline-block; vertical-align: top; white-space: nowrap;font-family: arial;}
      .movieshow .rateBox .star-and-value .rating-stars {margin: 5px 0 0 0; height: 22px; line-height: 22px; vertical-align: middle;}
      .movieshow .rateBox .rate_con {border:none; background:transparent; padding:0 10px 0; box-sizing:border-box;}
      .movieshow .rateBox .rate_con .btn-submit {display:none;}
      .movieshow .rateBox .ratespan {display:block; color:#fff;}
      .movieshow .rateBox .ratespan small {color:#fff; font-size:13px;}
      .movieshow .empty-stars {color:#d8d8d8;}
      .movieshow .rating .critic:before {color:#ff001f;}
      .movieshow .rating .ratestar:before {color:#fc0;}
      .movieshow .rating .users:before {color: #00b3ff;}
      .movieshow .short-text {font-size:1.2rem; line-height:1.8rem;}
      .movieshow .short-text .readMoreText {position:relative; color:#00b3ff; cursor:pointer; margin-left:5px; font-size:1.1rem; display:inline-block;white-space:nowrap;}
      .movieshow .moreitems {position:relative; margin-left:7px;}
      .movieshow .moreitems ul {position:absolute; top:30px; left:0; background:#fff; border-radius:6px;z-index:999;}
      .movieshow .moreitems ul li {border-bottom:solid 1px #dbdbdb; padding: 5px 15px; color:#000; font-size:0.9rem;}
      .movieshow .moreitems ul li:last-child {border:none;}
      .movieshow .rateBox .star-and-value .filled-stars:before, .movieshow .rateBox .star-and-value .empty-stars:before {font-size:16px; line-height:16px;}
 `;

  const budgetCss = `
      .wdt_budget {position:relative; border:1px solid #d4d4d4; margin:0 0 25px}
      .wdt_budget .budget_head{padding: 0 0 0 15px; display:block; overflow:hidden; background:#000; height:40px;}
      .wdt_budget .budget_head h3 {font-weight:normal; margin:0}
      .wdt_budget .budget_head h3 a{font-size: 1.6rem; line-height: 40px; color:#fff;}
      .wdt_budget .bdt_top .news-card.only-info .con_wrap .text_ellipsis{max-height: 3.6rem; -webkit-line-clamp: 2;}
      .wdt_budget .bdt_top{padding:0 10px 0 15px; margin-top:20px}
      .desktop_body .wdt_budget .wdt_lb_home h3{height: auto; line-height: initial; margin-bottom: 5px; margin-top:-5px; font-size: 1.4rem;}
      .desktop_body .section-wrapper .wdt_budget .wdt_lb_home .lb-strip{max-height:200px}
      .wdt_budget .read_more_open{margin-bottom:0}
      .wdt_budget .cheaper_dearer{margin:20px 0 15px}
      .wdt_budget .slider .slider_content li .slide img{max-height:100px}
      .wdt_budget .slider .btnPrev, .wdt_budget .slider .btnNext{top:30px}
  `;

  const newsbriefCss = `
      .briefbox { white-space:normal;}
      .slide_list li{margin:15px auto; width: 100%; overflow: hidden; border: 1px solid #d5d5d5; box-shadow: 0 2px 5px #b8b8b8;white-space:normal;}
      .slide_list li .top_content{position: relative; overflow: hidden;}
      .slide_list li .top_content .counter {padding: 0 6px;left: 5px;position: absolute;background: #000;top: 5px;box-shadow: none;font-size: 1.3rem;color: #ffffff; height:28px; line-height:28px;}
      .slide_list li  .more{font-size: 1.3rem;color: #000; display:block; text-align:right; padding:0 3%;}
      .slide_list li .img_wrap{vertical-align:top}
      .slide_list li .img_wrap img {vertical-align: bottom; width: 100%; height: auto;}
      .slide_list li .con_wrap {color:#000;  margin: auto; bottom: 0; padding: 15px 3% 10px; }
      .slide_list li .text_ellipsis {font-size: 1.3rem; line-height: 1.9rem; margin-bottom:10px; font-weight:bold;}
      .slide_list li .time-caption {font-size: 0.9rem; line-height: 1.2rem; display: inline-block; font-weight: normal;}
      .slide_list li .bottom_content{margin:3%; font-size: 1.4rem; line-height: 2.2rem; min-height:130px}
      .slide_list li .bottom_content .briefsyn {font-size: 1.2rem; line-height: 1.9rem; color:#333333;}
      .slide_list li .bottom_content ul li{font-size: 1.2rem; line-height: 1.7rem; color:#333333; border: 0;  box-shadow: none; margin: 0 0 0 15px; position: relative; overflow: initial; width: auto; display: block;}
      .slide_list li .bottom_content .wdt_download_app{display:none}
      .slide_list li .bottom_content.app_in_brief{position:relative;}
      .slide_list li .bottom_content.app_in_brief ul li, .slide_list li .bottom_content.app_in_brief ul{color:#cecece}
      .slide_list li .bottom_content.app_in_brief .wdt_download_app{display:block; position:absolute; top:0; bottom:0; left:0; right:0; margin:auto; background:#fff; color:#000; font-size:1.8rem; line-height:2.8rem; opacity:.9; text-align:center;}
      .slide_list li .wdt_download_apptooltip{display:block;text-align:center; position:absolute; bottom:40px; left:0; right:0; width:80%; margin:auto; background:#fff; color:#000; font-size:1.8rem; line-height:2.8rem; opacity:.9;}
      .slide_list li .bottom_content.app_in_brief .wdt_download_app b{display:block; margin:20px 0 10px}
      .slide_list li .bottom_content.app_in_brief .btn_app_download{background:#ff0000; font-size:1.6rem; height:30px; line-height:34px; padding:0 20px; border-radius:15px; display: inline-block; color: #fff; cursor:pointer;}
      .slide_list li .btn_app_downloadtooltip{background:#000000; font-size:1.4rem;line-height:20px; padding:8px 20px 5px; border-radius:15px; display: inline-block; color: #fff; cursor:pointer;}
      .briefbox .swipperParentDiv .article-section{ overflow:initial} 
      .landscape-mode{display:none}    
  `;
  const glossaryCss = `
      .desktop_body .section-topics .glossary-list{width:100%}
      .desktop_body .section-topics .glossary-list .tabs-circle{float:left;}
      .desktop_body .section-topics .glossary-list .tabs-circle ul{display:flex;flex-wrap:wrap;}
      .desktop_body .section-topics .glossary-list .tabs-circle ul li{margin:0 15px 15px 0;border-radius:50%;border:solid 1px #575960;background:#fff;}
      .desktop_body .section-topics .glossary-list .tabs-circle ul li, .desktop_body .section-topics .glossary-list .tabs-circle ul li a{height:42px;width:42px;align-items:center;justify-content:center;text-transform:uppercase;color:#1f81db;font-size:16px;padding:0;display:flex;line-height:normal;overflow:hidden;}
      .desktop_body .section-topics .glossary-list .tabs-circle ul li.active{background:#000;color:#fff;}
      .desktop_body .section-topics .glossary-list .tabs-circle:after{display:none;}
      .desktop_body .section-topics .glossary-list .wdt_glossary{overflow:hidden;clear:both;margin-bottom:20px;}
      .desktop_body .section-topics .glossary-list .wdt_glossary .news-card{margin-bottom:10px;border:0;}
      .desktop_body .section-topics .glossary-list .wdt_glossary .news-card .con_wrap .text_ellipsis{font-size:1.4rem;line-height:2.2rem;max-height:4.4rem;-webkit-line-clamp:2;word-break:break-word;}
  `;

  let commonStyle = commonCss + headerCss + gadgetnowCss;

  let topicsCss = glossaryCss + profieCss;
  let othersCss = budgetCss + topicsCss + sportsCss;

  const csrCritical =
    homeCss +
    articleshowCss +
    moviereviewCss +
    videolistCss +
    videoshowCss +
    photoshowCss +
    photolistCss +
    electionsCss +
    glossaryCss +
    profieCss +
    newsbriefCss +
    movieshowCss +
    budgetCss +
    sportsCss;

  if (portalName == "nbt") {
    commonStyle += `
        ${gadgetnow_nbt_css}
        ${/* base font 1rem is now 13px */ ""}
        @media (min-width: 767px) {
          html {font-size: 81.25%;}
        }
        .header-desktop .logo img{width:128px}
        .header-desktop .logo .etLogo img{width:99px;}
        .header-desktop .logo .logo-tech img{width:99px}
        .header-desktop .logo a.logo-tech{line-height:50px}
        .desktop_body .trending-bullet{line-height:1rem;}
        .desktop_body .trending-bullet a{font-size:1.0rem}
        .desktop_body .header-desktop .nav_wrap .first-level-menu{background: #000;}
        .desktop_body .articleshow_body .story-article .story-content{font-size: 1.45rem; line-height: 2.7rem;}
        .desktop_body .box-item.rhs-videolist ul.list-horizontal li .con_wrap .text_ellipsis{font-size:1.2rem; line-height:2.0rem; max-height:4.0rem}
        .desktop_body .footer .head, .desktop_body .footer .footlinks-wrapper a{font-size:1.2rem}
        .desktop_body .footer .footlinks-wrapper.footer-links a{font-size:1rem}
        .desktop_body .footer .footlinks-wrapper{margin-bottom:10px}
        .desktop_body .pollContainer .captcha-box .btnsubmit{line-height: 34px; font-size: 1.4rem;}
        .desktop_body .moviereview-sourceCard .source, .desktop_body .articleshow_body .news_card .news_card_source span.time {font-size: 1.0rem; line-height: 1.4rem;}
        .desktop_body .L1 .listing.photolist ul li .con_wrap{padding: 8px 10px 7px;}
        .desktop_body .L2 .listing.photolist ul li .con_wrap{padding:7px 10px 6px}
        .desktop_body .sel-city-content.show{height:400px}
        .desktop_body .sel-city-content.show ul{column-gap: 32px;}
        .desktop_body .sel-city-content.show ul li.head a, .desktop_body .sel-city-content.show ul li.head b{font-weight:bold}
        `;
  }
  if (portalName == "eis") {
    commonStyle += `
            @media (min-width: 767px) {
              html {font-size: 75%;}
            }
            .header-desktop .logo img{width:101px; margin-top:-10px;}
            .header-desktop .logo .logo-tech img{width:128px}
            ${/* nav releated changes */ ""}
            .desktop_body .header-desktop .nav_wrap .first-level-menu h3.home a{color:#000;}
            .desktop_body .header-desktop .nav_wrap .first-level-menu h3.home.active:after{border-color:#000}
            .desktop_body .header-desktop .nav_wrap .nav-more .nav-more-icon{color:#000;}
            .desktop_body .header-desktop .nav_wrap .first-level-menu{background: #FFA929;}
            .desktop_body .header-desktop .nav_wrap .first-level-menu .search_in_header b{color:#000;}
            .desktop_body .header-desktop .nav_wrap .first-level-menu .items ul li.special_bg a {color:#fff !important;}
            .desktop_body .header-desktop .nav_wrap .nav-more:hover .nav-more-icon{color:#fff;}
            .container .top-stories-modal h2 {color: #000;}
            .container .top-stories-modal.orange h2, .container .top-stories-modal.blue h2 {color: #fff;}
            .container .top-stories-modal .close:before, .container .top-stories-modal .close:after {background: #000;}
            .container .top-stories-modal.orange .close:before, 
            .container .top-stories-modal.orange .close:after, 
            .container .top-stories-modal.blue .close:before, 
            .container .top-stories-modal.blue .close:after {background: #fff;}
            .m-scene.tech .section h3, .m-scene.tech .section h2, .m-scene.tech .section h1{font-size:1.6rem}
            .wdt-inline-gadgets ul.scroll li label{font-size:1.1rem}
        `;
  }
  if (portalName == "mt") {
    commonStyle += `
            ${/* base font 1rem is now 13px */ ""}
            @media (min-width: 767px) {
              html {font-size: 81.25%;}
            }
            .header-desktop .logo img{width:146px}
            .header-desktop .logo .logo-tech img{width:163px}
            .header-desktop .logo a.logo-tech{line-height:50px}
            .desktop_body .news-card.lead .con_wrap .text_ellipsis{font-size: 1.6rem; line-height: 2.4rem; max-height: 7.2rem;}
            .desktop_body .trending-bullet a{font-size:1.0rem}
            .desktop_body .header-desktop .nav_wrap .first-level-menu{background: #005BAA;}
            .desktop_body .articleshow_body .story-article .story-content{font-size: 1.45rem; line-height: 2.7rem;}
            .desktop_body .box-item.rhs-videolist ul.list-horizontal li .con_wrap .text_ellipsis{font-size:1.2rem; line-height:2.0rem; max-height:4.0rem}
            .desktop_body .videoshow-container .section_videos .nbt-list li .con_wrap .text_ellipsis{font-size: 1.3rem; line-height: 2.1rem; max-height: 4.2rem;}
            .desktop_body .L1 .listing.photolist ul li .con_wrap{padding:8px 10px 7px}
            .desktop_body .L2 .listing.photolist ul li .con_wrap{padding:7px 10px 6px}
            .desktop_body .footer .footer-links a, .desktop_body .footer .copy-right{font-size:1.1rem}
            .desktop_body .section-wrapper .wdt_lb_home .lb-strip{max-height: 425px;}
        `;
  }
  if (portalName == "vk") {
    commonStyle += `
            @media (min-width: 767px) {
              html {font-size: 75%;}
            }
            .header-desktop .logo h2 a{line-height:70px}
            .header-desktop .logo img{width:137px}
            .header-desktop .logo .logo-tech img{width:160px}
            .header-desktop .logo a.logo-tech{line-height:50px}
            .desktop_body .header-desktop .nav_wrap .first-level-menu{background: #000000;}
            .desktop_body .articleshow_body .story-article .story-content{font-size: 1.45rem; line-height: 2.7rem;}
            .desktop_body .L1 .listing.photolist ul li .con_wrap{padding:10px 10px 8px}
            .desktop_body .L2 .listing.photolist ul li .con_wrap{padding:9px 10px 8px}
            .vk-mini .header-desktop .logo img, body.enable-video-show .vk-mini .header-desktop .logo img{display:none;}
            .vk-mini .header-desktop .logo h2 a{width:110px; height:53px; background: url(https://vijaykarnataka.com/photo/77152342.cms) 0 0 no-repeat; background-size: 110px; margin:0 auto;} 
            .statestoggle span {line-height: 26px;}
        `;
  }
  if (portalName == "tlg") {
    commonStyle += `
            ${/* base font 1rem is now 12px */ ""}
            @media (min-width: 767px) {
              html {font-size: 81.25%;}
            }
            .header-desktop .logo img{width:135px}
            .header-desktop .logo .logo-tech img{width:156px}
            .desktop_body .header-desktop .nav_wrap .first-level-menu{background: #e5251b;}
            .desktop_body .articleshow_body .story-article .story-content{font-size: 1.45rem; line-height: 2.7rem;}
            .desktop_body .box-item.rhs-videolist ul.list-horizontal li .con_wrap .text_ellipsis{font-size:1.2rem; line-height:2.0rem; max-height:4.0rem}
            .desktop_body .listing.photolist .slider.latest ul li .con_wrap {min-height: 102px;}
            .desktop_body .L1 .listing.photolist ul li .con_wrap{padding:8px 10px 7px}
            .desktop_body .L2 .listing.photolist ul li .con_wrap{padding:7px 10px 6px}
            .desktop_body .gadgetlist_body .gl_list_mobiles .gl_tabs{padding-left:160px}
            .m-scene.tech .section h3, .m-scene.tech .section h2, .m-scene.tech .section h1{font-size:1.6rem}
            .statestoggle span {line-height: 26px;}
        `;
  }
  if (portalName == "tml") {
    commonStyle += `
            .header-desktop .logo img{width:142px}
            .header-desktop .logo .logo-tech img{width:160px}
            .header-desktop .logo a.logo-tech{line-height:55px}
            .desktop_body .header-desktop .nav_wrap .first-level-menu{background: #1f81db;}
            .desktop_body .header-desktop .top_nav .globalnav .wdt_timespoints .tpwidget .widget-prelogin .widget-text {max-width: 105px; font-size: 11px;}
            .wdt_timespoints {font-size:95%}
            .desktop_body .gadgetlist_body .gl_list_mobiles .gl_tabs{padding-left:160px}
            .m-scene.tech .section h3, .m-scene.tech .section h2, .m-scene.tech .section h1{font-size:1.6rem}
            .wdt-inline-gadgets ul.scroll li label{font-size:1.0rem}
            .statestoggle span {line-height: 26px;}
    `;
  }
  if (portalName == "mly") {
    commonStyle += `
            ${/* base font 1rem is now 12px */ ""}
            @media (min-width: 767px) {
              html {font-size: 75%;}
            }
            .header-desktop .logo img{width:134px}
            .header-desktop .logo .logo-tech img{width:163px}
            .header-desktop .logo a.logo-tech{line-height:55px}
            .desktop_body .header-desktop .nav_wrap .first-level-menu{background: #127e6d;}
            .desktop_body .header-desktop .nav_wrap .first-level-menu .items ul li, .desktop_body .header-desktop .nav_wrap .second-level-menu ul li, .desktop_body .header-desktop .nav_wrap .con_primary li{font-family:arial; line-height:36px}
            .desktop_body .articleshow_body .story-article .story-content{font-size: 1.5rem; line-height: 2.6rem;}
            .desktop_body .body-content.section-topics .tabs-circle ul li{line-height:41px}
            .wdt_timespoints {font-size:95%}
            .desktop_body .gadgetlist_body .gl_list_mobiles .gl_tabs{padding-left:160px}
            .m-scene.tech .section h3, .m-scene.tech .section h2, .m-scene.tech .section h1{font-size:1.6rem}
            .wdt-inline-gadgets ul.scroll li label{font-size:1.0rem}
            .m-scene.tech .con_brands .brands_slider ul.tabs li{font-size: 1.1rem; margin: 0 10px 0 0;}
            .statestoggle span {line-height: 26px;}
        `;
  }
  if (portalName == "iag") {
    commonStyle += `
            ${/* base font 1rem is now 12px */ ""}
            @media (min-width: 767px) {
              html {font-size: 75%;}
            }
            .desktop_body .header-desktop .logo h1 a, .desktop_body .header-desktop .logo h2 a{line-height:65px}
            .desktop_body .header-desktop .logo a img{width:83px}
            .desktop_body .header-desktop .nav_wrap .first-level-menu{background: #1c8ab6;}
            .desktop_body .articleshow_body .story-article .story-content, .desktop_body .articleshow_body .recipe_ingriedient{font-size: 1.45rem; line-height: 2.7rem;}
            .desktop_body .box-item.rhs-videolist ul.list-horizontal li .con_wrap .text_ellipsis{font-size:1.2rem; line-height:2.0rem; max-height:4.0rem}
            .desktop_body .listing.photolist .slider.latest ul li .con_wrap {min-height: 102px;}
            .desktop_body .L1 .listing.photolist ul li .con_wrap{padding:10px}
            .desktop_body .L2 .listing.photolist ul li .con_wrap{padding:10px 10px 8px}
            .container.iag .wdt_social_share span.comments, .container.iag .comments-wrapper .bottom-comments-btns, .container.iag .photo_story li.photo_card .photo_comments, .container.iag .moviereview-summaryCard .con_wrap .btn_rate, .container.iag .moviereview-summaryCard .rateBox .rate_con, .container.iag .header-desktop .top_nav ul.extranav li.photogallery, .container.iag .section-wrapper .row.photoGallery{display:none !important}
        `;
  }

  if (pagetype == "home") {
    commonStyle += homeCss + sportsCss;
  } else if (pagetype == "articlelist") {
    commonStyle += sportsCss;
  } else if (pagetype == "videolist") {
    commonStyle += videolistCss;
  } else if (pagetype == "articleshow") {
    commonStyle += articleshowCss;
  } else if (pagetype == "videoshow") {
    commonStyle += videoshowCss;
  } else if (pagetype == "moviereview") {
    commonStyle += moviereviewCss;
  } else if (pagetype == "liveblog") {
    commonStyle += "";
  } else if (pagetype == "topics") {
    commonStyle += topicsCss;
  } else if (pagetype == "photoshow") {
    commonStyle += photoshowCss;
  } else if (pagetype == "photolist") {
    commonStyle += photolistCss;
  } else if (pagetype == "elections") {
    commonStyle += electionsCss;
  } else if (pagetype == "newsbrief") {
    commonStyle += newsbriefCss;
  } else if (pagetype == "movieshow") {
    commonStyle += movieshowCss;
  } else if (pagetype == "others") {
    commonStyle += othersCss;
  }

  commonStyle += `${/* Theme style css passed through feed will show here, if available */ ""}
                   ${themeStyle}
                  `;
  return {
    commonStyle,
    csrCritical,
  };
};
module.exports = {
  common: criticalCss,
  csrCritical: criticalCss(process.env.SITE, null, null, null).csrCritical,
};

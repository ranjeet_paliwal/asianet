import React from "react";
import PropTypes from "prop-types";
import { Provider } from "react-redux";
import routes from "../../router/master/routes";
import { Router } from "react-router";
import {
  _isCSR,
  getPageType,
  modifyAdsOnScrollView,
  isMobilePlatform,
  isIe11,
  getPageTypeUpdated,
  _getCookie,
} from "./../../utils/util";
import { publish } from "./../../utils/pubsub";

import { AnalyticsGA, Analytics_iBeat } from "./../../components/lib/analytics/index";
import Ads_module from "./../../components/lib/ads/index";

import globalconfig from "./../../globalconfig";
import { _getStaticConfig } from "../../utils/util";
import { setGRXParameter } from "../../components/lib/analytics/src/ga";
import { wapadsGenerator } from "../../components/lib/ads/lib/utils";
const siteConfig = _getStaticConfig();

const gaObj = siteConfig.ga;
const comscoreObj = siteConfig.comscore;
const adsObj = siteConfig.ads;
let isLandingPage = true;
globalconfig.isLandingPage = true;

function Root({ store, history }) {
  return (
    <Provider store={store}>
      <Router history={history} routes={routes} onUpdate={onRootUpdate} />
    </Provider>
  );
}

function onRootUpdate() {}

function scrollHandler(action, pathname) {
  // check router location , scroll only if not articleshow
  if (pathname && pathname.indexOf("articleshow") < 0) {
    // (window.setTimeout(()=>{
    //    scrollTo(document.documentElement,0);
    // },100))
    if (action === "PUSH") {
      window.scrollTo(0, 0);
    }
  }
}

function adsHandler(pathname) {
  let pagetype = "others";
  let adsec = "others";

  // To load "tech" type adcodes (tech home page dfp)
  if (
    ("nbt".indexOf(process.env.SITE) > -1 && pathname.endsWith("/tech") == true) ||
    ("vk".indexOf(process.env.SITE) > -1 && pathname.endsWith("/tech") == true)
  ) {
    pagetype = "tech";
    adsec = "tech";
    // reset ctnads for tech
    window.ctnads = adsObj.ctnads[pagetype];
  } else {
    pagetype = getPageType(pathname, true);

    if (window.location.pathname == "/") {
      pagetype = "home";
      adsec = "homepage";
      window._SCN = "home";
    }

    if (pagetype == "elections") {
      // pagetype = "others";
      adsec = "microsite";
      window._SCN = "elections";
    } else if (pagetype == "newsbrief") {
      // pagetype = "others";
      adsec = "news";
      window._SCN = "news_briefs";
    } else if (pagetype == "tech") {
      adsec = "tech";
      window._SCN = "tech";
    } else if (pagetype == "liveblog") {
      adsec = "news";
      window._SCN = "liveblog";
    }

    // reset ctnads
    window.ctnads = adsObj.ctnads;
  }
  // trigger refresh of ads
  // window.wapads = adsObj.dfpads[pagetype];

  //check if landing page or not , return if landing page
  if (isLandingPage) {
    return;
  } else {
    if (
      pagetype == "home" ||
      pagetype == "tech" ||
      pagetype == "elections" ||
      pagetype == "newsbrief" ||
      pagetype == "liveblog"
    ) {
      window.wapads = wapadsGenerator({
        secname: adsec,
        defaultWapAds: siteConfig.ads.dfpads.others,
        pagetype: pagetype,
      });
      Ads_module.resetwaitForAdsEvent("false");
    } else {
      Ads_module.resetwaitForAdsEvent(window.waitForAdsEvent);
    }

    // and destroying all ads Slots on page change
    Ads_module.destroySlots();
    Ads_module.recreateAds(
      isMobilePlatform() ? ["topatf", "atf", "fbn"] : ["topatf", "atf", "skinrhs", "innove"],
      // {
      //   wapads: pagetype != "" ? adsObj.dfpads[pagetype] : adsObj.dfpads.others,
      // },
    );

    //refresh only canvasAd for Mobile
    isMobilePlatform() ? Ads_module.refreshAds(["canvasAd"]) : "";

    // after recreation , reset fbn refresh ads
    // process.env.SITE == "tlg" ? modifyAdsOnScrollView(true) : "";
  }
}

function domUpdates(pathname) {
  if (document.getElementById("mySidenav"))
    document.getElementById("mySidenav").style.transform = "translateX(-1000px)";
  if (document.querySelector(".greyLayer")) document.querySelector(".greyLayer").style.display = "none";

  document.body.style.overflow = "scroll";
  document.querySelector("body").setAttribute("pagetype", getPageType(pathname));
  if (isIe11()) {
    document.querySelector("body").setAttribute("class", "ie-eleven");
  }
}

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default Root;

import React from "react";
import { _getStaticConfig, getLogoPath } from "../../utils/util";

const siteConfig = _getStaticConfig();

const DesktopHeader = () => {
  return (
    <header style={{ textAlign: "center", marginBottom: "25px" }}>
      <div>
        <img src={getLogoPath()} />
      </div>
      <br />
      <div>
        Asianet {process.env.SITE} {process.env.PLATFORM} Site
      </div>
    </header>
  );
};

export default DesktopHeader;

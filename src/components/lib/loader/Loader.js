import React from "react";
import styles from "./Loader.css";

//const Loader = () => {return(<div className={styles.minHeight}><div className={styles.root}></div></div>)};
const Loader = () => {
  return (
    <div
      id="ytLoader"
      className={styles.animatedloader + " " + styles.ytloader}
    ></div>
  );
};

export default Loader;

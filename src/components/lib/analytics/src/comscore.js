/* eslint-disable no-underscore-dangle */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
import loadComscore from "./../lib/comscore";
import { _getCookie } from "./../lib/utils";
import fetch from "../../../../utils/fetch/fetch";

const configure = () => {
  // if (window.landing_page) {
  //   objComScore["c9"] = window.landing_page;
  //   // objComScore["c10"] = window.landing_page;
  // }
  setCSUCFR();
  // for EU user
  // FIXME: We driving comscore by img in HTML.js , so commenting for first call
  // _comscore.push(objComScore);
};

const setCSUCFR = () => {
  // https://timesgroup.jira.com/browse/TML-2224
  // Optout check is disabled as per above JIRA
  if (window.isGDPR && _getCookie("optout") !== undefined) {
    if (_getCookie("ckns_policyV2") != undefined) {
      // if consent given
      objComScore["cs_ucfr"] = 1;
    }
    // else {
    //   objComScore["cs_ucfr"] = "";
    // }
  }
};

/**
 * fireComscore:
 * use it when route changes
 * @param {object} options - new required values
 */
export const fireComscore = (comscorepageview, options) => {
  // AS per QC team cs_ucfr param need to be set for CSR calls too.
  setCSUCFR();
  self.COMSCORE && COMSCORE.beacon(objComScore);

  // pageview-candidates call for comscore
  const pageViewCandidatAPI = `${process.env.API_BASEPOINT}/pageview_candidate.cms?t=${new Date().getTime()}`;
  if (!comscorepageview) {
    fetch(pageViewCandidatAPI).catch(e => console.log("pageview-candidates", pageViewCandidatAPI));
  }
};

/**
 * initialize:
 * intializing comscore method with a unique comscore id
 * @param {object} _objComScore - a object for comscore
 * @param {object} options - for gdpr
 */
export const initialize = (_objComScore, options) => {
  if (typeof window === "undefined" || typeof _objComScore != "object") {
    return false;
  }

  loadComscore(options);

  window._comscore = [];
  window.objComScore = _objComScore || {};
  window.isGDPR = options.isGDPR;

  configure();

  return true;
};

export default { initialize, fireComscore };

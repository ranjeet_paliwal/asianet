/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-expressions */
import loadGA from "../lib/ga";
import { _getCookie } from "../lib/utils";
import AnalyticsComscore from "./comscore";
import Analytics_fbpixel from "./fbpixel";

import { _isCSR, getPageTypeUpdated } from "../../../../utils/util";

const warn_label = "@times-analytics ";

const configureGA = (gaId, options) => {
  if (!window.ga) {
    console.warn(`${warn_label} window.ga not loaded`);
    return false;
  }
  window.ga("create", gaId, "auto", { allowLinker: true });
  window.ga("require", "displayfeatures");

  if (options && options.gaprofile && options.gaprofile != "") {
    window.ga("require", "linker");
    window.ga("linker:autoLink", options.gaprofile);
  }

  if (options && options.userId && options.userId != "" && typeof _getCookie === "function" && _getCookie("ssoid")) {
    window.ga("set", "userId", _getCookie("ssoid"));
  }

  // if (options && options.isGDPR && typeof _getCookie === "function" && _getCookie("optout") !== undefined) {
  if (options && options.isGDPR) {
    window.ga("set", "anonymizeIp", true);
  }
};

const _callGA = (...args) => {
  if (!window.ga) return console.warn(`${warn_label} window.ga not loaded , first call initialize method`);

  // TODO: Check if this is needed with new grx implementation
  // window.growthrx_web_sdk_configured && args[1].hitType == "pageview" && window.grx
  //   ? grx("track", args[1].hitType, { page: args[1].page })
  //   : null;

  return window.ga(...args);
};

/**
 * send:
 * Basic send method
 * @param {Object} gaObject , must not empty
 */
export const send = gaObject => {
  _callGA("send", gaObject);
};

/**
 * ga:
 * Returns the original GA object.
 */
export const ga = (...args) => {
  if (args.length > 0) {
    _callGA(...args);
  }

  return window.ga;
};

/**
 * pageview:
 * Basic GA pageview tracking
 * @param {String} path - the current page page e.g. '/about'
 */
export const pageview = (path, ...args) => {
  if (!path || path === "" || typeof path !== "string") {
    console.warn(`${warn_label} path is required in .pageview() | path must be string | path must not empty`);
    return;
  }
  const grxPageViewEvent = { url: path };
  try {
    path = path.indexOf(".com/") > -1 ? path.split(".com/")[1] : path;
  } catch (e) {}
  path += location.search; // adding query string with path
  path = path.trim();
  grxPageViewEvent.url = path;
  // Update screen type in case of grx, this ensures that screen type for article -> article traversal
  // or List -> List travels has updated value before firing pageview call
  const screenType = getPageTypeUpdated(path, null, true);
  if (!window.is_popup_video) {
    setGRXParameter("screen_type", screenType);
  } else {
  }

  // FIXME: As we using gtm pause all pageview
  // if (0) {
  // Required Fields
  const gaObject = {
    hitType: "pageview",
    page: path,
    // location: document.location.href,
    // ...args[0]
  };

  // set page value first in SPA as per https://developers.google.com/analytics/devguides/collection/analyticsjs/single-page-applications
  // ga("set", "page", path);
  // Set author coustom Dimension
  // commenting this code for implementing gtm
  // if (args[0] && args[0].setEditorName && args[0].setEditorName === true) {
  //   window._editorname != ""
  //     ? ga("set", process.env.SITE == "nbt" ? "dimension4" : "dimension2", window._editorname)
  //     : null;

  //   // Reset window._editorname
  //   window._editorname = "";
  // } else {
  //   // ga("set", process.env.SITE == "nbt" ? "dimension4" : "dimension2", null);
  // }

  if (args[0] && args[0].setEditorName) {
    let authorName;
    if (window && window._editorname) {
      authorName = window._editorname.toLowerCase().replace(/ /g, "-");
    } else {
      authorName = "no-author";
    }
    pushAuthorName(authorName);
    // now resetting authorname
    window._editorname = "";
  }

  // Additional param for video popup
  if (args[0] && args[0].is_popup_video) {
    grxPageViewEvent.is_popup_video = true;
  }

  // For cases when path changes Non forceful, growthrx
  if (_isCSR() && !(args[0] && args[0].forcefulGaPageview) && !(args[0] && args[0].forcefulGaPageviewWithoutEditor)) {
    fireGRXEvent("track", "page_view", grxPageViewEvent);
  }

  if (args[0] && args[0].forcefulGaPageview) {
    const data = { event: "hpautorefreshed", customPageUrl: path };
    window.dataLayer.push(data);
    // Also fire grx for forced refreshes
    fireGRXEvent("track", "page_view", grxPageViewEvent);

    // TODO - Keep scope open for forceful pageview when url is not changing
  }

  // Fired in case of next video cases
  if (args[0] && args[0].forcefulGaPageviewWithoutEditor) {
    pushAuthorName("not set");
    const dockedVideoElement = document.getElementById("dock-root-container");
    // Seems to be triggered on next video cases
    // If docked video container is currently playing add is_pip_video to grx call
    if (dockedVideoElement && !dockedVideoElement.classList.contains("hide")) {
      grxPageViewEvent.is_pip_video = true;
    }
    fireGRXEvent("track", "page_view", grxPageViewEvent);
  }
  // for pageview_candidates
  let comscorepageview = 0;
  if (args[0] && args[0].comscorepageview) {
    comscorepageview = true;
  }
  // Set default value of custom dimension 2 to 'online'
  // ga('set', siteConfig.ga.cdindex, 'online');

  // then call for pageview send call
  // disabled because of gtm
  // send(gaObject);

  /*  After a call has been successfully sent with editor name, set custom dimension as null
   This removes the parameter for further calls on CSR
  Since trigger can be from perpetual or any page , set it as null after every call. 
  Reset author dimension field 
  */
  // if (args[0] && args[0].setEditorName && args[0].setEditorName === true) {
  //   ga("set", process.env.SITE == "nbt" ? "dimension4" : "dimension2", null);
  // }

  // ga("set", process.env.SITE === "nbt" ? "dimension4" : "dimension2", "");

  // For UC Mini
  // Commented to reduce GA Event cost
  // (window.ga && (window.browser == 'uc' || window.browser == 'ucmini')) ? ga('send', 'event', 'ucvisit', 'pv', window.browser) : null;

  // temporary TODO
  // trigger pageview related adtracker event
  // window.adtrackerOn && window.ga ? ga("send", "event", "adtracker", document.location.pathname, "pv") : null;
  // }

  // typeof grx == "function" && grx("track", "page_view", { url: path }); //then call grx pageview with ga
  AnalyticsComscore.fireComscore(comscorepageview); // temporary call comscore with ga
  Analytics_fbpixel.pageView(); // temporary call fbpixel with ga

  // Trigger event on page. Will be use if required is base on pageview count
  // DMP Pixel code call with pageview and comscore commented as of now coz it is already calling in live
  // if (typeof $cr == "object" && typeof $cr.cll == "function") {
  // $cr.cll();
  // }
  if (
    args &&
    Array.isArray(args) &&
    args[0] &&
    args[0].pageCountInc !== null &&
    args[0].pageCountInc !== undefined &&
    args[0].pageCountInc === false
  ) {
  } else {
    // Initilize Universal Pageviews
    window.sessionPageView ? (window.sessionPageView += 1) : (window.sessionPageView = 1);
    const page_count = new CustomEvent("page_count", {
      detail: { pageview: window.sessionPageView },
    }); // Create custom event 'page_count'
    window.dispatchEvent(page_count);
  }
};

// GTM function
export const GTM = ({ category, action, label, event }) => {
  // Required Fields
  const gtmObject = {
    eventCategory: category,
    eventAction: action,
  };

  // Optional Fields
  if (typeof label !== "undefined") {
    gtmObject.eventLabel = label;
  }
  // Optional Fields
  if (typeof event !== "undefined") {
    gtmObject.event = event;
  }
  window && window.dataLayer
    ? window.dataLayer.push({
        event: gtmObject.event,
        eventCategory: gtmObject.eventCategory,
        eventAction: gtmObject.eventAction,
        eventLabel: gtmObject.eventLabel,
      })
    : null;
};

/**
 * event:
 * GA event tracking
 * @param args.category {String} required
 * @param args.action {String} required
 * @param args.label {String} optional
 * @param args.value {Int} optional
 */
export const event = ({ category, action, label, value, briefURL, overrideEvent }) => {
  // Simple Validation
  if (!category || !action) {
    console.warn(`${warn_label} |args.category| AND |args.action| are required in event()`);
    return;
  }

  // for gtm
  const gtmObject = {};
  let categoryForEvent = category.replace(/ /g, "_");
  categoryForEvent = categoryForEvent.toLowerCase();
  gtmObject.event = overrideEvent ? `tvc_${overrideEvent}` : `tvc_${categoryForEvent}`;
  gtmObject.eventCategory = category;
  gtmObject.eventAction = action;
  if (typeof label !== "undefined") {
    gtmObject.eventLabel = label;
  }

  if (briefURL) {
    gtmObject.briefURL = briefURL;
  }

  if (typeof value !== "undefined") {
    if (typeof value !== "number") {
      console.warn(`${warn_label} Expected |args.value| arg to be a Number.`);
    } else {
      gtmObject.eventValue = value;
    }
  }

  window && window.dataLayer ? window.dataLayer.push(gtmObject) : null;

  // Required Fields
  const gaObject = {
    hitType: "event",
    eventCategory: category,
    eventAction: action,
  };

  // Optional Fields
  if (typeof label !== "undefined") {
    gaObject.eventLabel = label;
  }

  if (typeof value !== "undefined") {
    if (typeof value !== "number") {
      console.warn(`${warn_label} Expected |args.value| arg to be a Number.`);
    } else {
      gaObject.eventValue = value;
    }
  }

  /* check if grx and ga available fire same event for both GA and GRX  */
  // if (window.growthrx_web_sdk_configured && window.ga && gaObject.eventCategory == "VIDEOCOMPLETE") {
  //   grx("track", "event", {
  //     eventCategory: "allpixel1",
  //     eventAction: "found",
  //     eventLabel: "true",
  //   });
  //   // need to check how to implement this special case in gtm
  //   // ga("send", "event", "allpixel1", "found", "true");

  //   grx("track", "event", {
  //     eventCategory: "allpixel2",
  //     eventAction: "found",
  //     eventLabel: "true",
  //   });
  //   // ga("send", "event", "allpixel2", "found", "true");
  // }

  // Send to GA
  // send(gaObject);

  // For growthrx tracking
  // const grxgaObject = { ...gaObject };
  // delete grxgaObject.hitType;
  // window.growthrx_web_sdk_configured ? grx("track", gaObject.hitType, grxgaObject) : null;
};

export const fireGRXEvent = (eventType, eventLabel, eventDetails = {}) => {
  if (typeof window !== "undefined" && !window.grx) {
    console.warn(`GRX must be initialised before firing event`);
  } else if (!eventType || !eventLabel) {
    console.warn(`GRX needs type of event and label of event`);
  } else {
    try {
      if (window.geoinfo && window.geoinfo.hasOwnProperty("isGDPRRegion") && window.geoinfo.isGDPRRegion) {
        const allowedEventsGDPR = {
          page_view: true,
          video_view: true,
          video_played: true,
          app_status: true,
        };

        // If this event is not allowed in case of gdpr, return and dont fire grx
        if (!(eventLabel in allowedEventsGDPR)) {
          return;
        }
      }

      const screenType = getPageTypeUpdated(window.location.pathname, null, true);
      if (
        screenType === "articleshow" ||
        screenType == "photoshow" ||
        screenType == "videoshow" ||
        screenType == "moviereview"
      ) {
        // Split pathname to get location and msid of article
        // Eg ["/metro/mumbai/other-news/bmc-has-started-preparati…izens-extra-bed-in-hospitals-during-corona-crisis", "81757096.cms"]
        const [pageLocation, _] = window.location.pathname.split(`/${screenType}/`);

        // Remove seolocation by splitting against last "/"
        // Eg "/metro/mumbai/other-news"
        const pageSections = pageLocation.slice(0, pageLocation.lastIndexOf("/"));

        // Split levels again and only keep truthy values (non empty strings)
        //  Eg ["", "metro", "mumbai", "other-news"] -> ["metro", "mumbai", "other-news"]
        const pageSectionLevels = pageSections.split("/").filter(level => level);

        // Set all these section levels dynamically for grx calls
        pageSectionLevels.forEach((level, index) => {
          eventDetails[`section_l${index + 1}`] = level;
        });
      }

      window.grx(eventType, eventLabel, eventDetails);
    } catch (e) {
      console.log("Error firing GRX event:", e);
    }
  }
};

/**
 * @param paramName name/label of parameter being set
 * @param paramValue value of parameter being set
 * @description Centralized function to set growthrx parameter
 * which automatically gets appended to future calls.
 */
export const setGRXParameter = (paramName, paramValue) => {
  if (typeof window !== "undefined" && !window.grx) {
    console.warn(`GRX must be initialised before setting parameter`);
  } else if (!paramName || !paramValue) {
    console.warn(`GRX needs type of event and label of event`);
  } else {
    try {
      window.grx("set", paramName, paramValue);
    } catch (e) {
      console.log("Error setting GRX parameter:", e);
    }
  }
};

/**
 * set:
 * GA tracker set method
 * @param {Object} gaObject - a field/value pair or a group of field/value pairs on the tracker
 */
export const set = gaObject => {
  if (!gaObject || (typeof gaObject !== "object" && Object.keys(gaObject).length === 0)) {
    console.warn(`${warn_label} gaObject is required | gaObject arg to be an Object | gaObject must not empty`);
    return;
  }

  _callGA("set", gaObject);
};

/**
 * initialize:
 * intializing ga method with a unique gaId
 * @param {string} gaId - a string for ga unique ID
 * @param {object} options - for gaprofile , isGDPR , userID related
 */
export const initialize = (gaId, options) => {
  if (typeof window === "undefined" || typeof gaId !== "string") {
    console.warn(`${warn_label} intialize with a valid gaId`);
    return false;
  }

  if (!window.ga) {
    loadGA(options);
  }

  // For Growthrx/coke
  // if (window && (process.env.SITE == "nbt" || process.env.SITE == "tml")) {
  if (window && !window.grx) {
    // TODO: Uncomment this to load grx
    // loadGrowthrx();
    // Central growthrx call for landing pages ( SSR )
    // if (_isCSR()) {
    //   fireGRXEvent("track", "page_view", { url: window.location.pathname });
    // }
  }

  // configureGA(gaId, options);

  AnalyticsComscore.initialize(options.comscore, options); // temporary call comscore with ga
  Analytics_fbpixel.initialize(options.fbpixel ? options.fbpixel.id : "", options); // temporary call fbpixel with ga

  return true;
};

const pushAuthorName = authorName => {
  const data = {};
  // if (window && window.sessionPageView && window.sessionPageView > 1) {
  data.event = "authorNamePushed";
  // }
  data.authorName = authorName;
  if (window && window.dataLayer) {
    window.dataLayer.push(data);
  }
};

export default { initialize, ga, set, send, pageview, GTM, event };

export default function(options, callback) {
  (function(i, s, o, g, r, a, m) {
    (a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
    a.async = 1;
    a.src = g;
    a.rel = "dns-prefetch";
    m.parentNode.insertBefore(a, m);
    a.onload = a.onreadystatechange = function() {
      if (
        !a.loaded &&
        (!this.readyState ||
          this.readyState === "loaded" ||
          this.readyState === "complete")
      ) {
        typeof callback == "function" ? callback() : null;
      }
    };
  })(
    window,
    document,
    "script",
    options && options.addressUrl
      ? options.addressUrl
      : "https://agi-static.indiatimes.com/cms-common/ibeat.min-002.js"
  ); //'https://ibeat.indiatimes.com/js/pgtrackingV9.js');
}

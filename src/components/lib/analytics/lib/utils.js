/**
 * Get value of a cookie
 *
 * @param name {String} name of the cookie for which value is required,
 *                        if name is not provided an object with all cookies is returned
 * @returns value {String | Array} value of the requested cookie / Array of all cookies
 */

export const _getCookie = name => {
  let result = name ? undefined : {};
  let cookies = document.cookie ? document.cookie.split("; ") : [];
  for (let i = 0, l = cookies.length; i < l; i++) {
    let parts = cookies[i].split("=");
    let nameK = decodeURIComponent(parts.shift());
    let cookie = parts.join("=");
    cookie = _parseCookieValue(cookie);
    if (name && name === nameK) {
      result = cookie;
      break;
    }
    if (!name && cookie !== undefined) {
      result[nameK] = cookie;
    }
  }
  return result;
};

const _parseCookieValue = s => {
  if (s.indexOf('"') === 0) {
    // This is a quoted cookie as according to RFC2068, unescape...
    s = s
      .slice(1, -1)
      .replace(/\\"/g, '"')
      .replace(/\\\\/g, "\\");
  }
  try {
    // If we can't decode the cookie, ignore it, it's unusable.
    // Replace server-side written pluses with spaces.
    return decodeURIComponent(s.replace(/\+/g, " "));
  } catch (e) {}
};

export const _formatDate = (d, isEpochTime) => {
  try {
    d = new Date(d);
    if (Object.prototype.toString.call(d) === "[object Date]") {
      if (isNaN(d.getTime())) {
        throw "IBEAT:Date format is not valid";
      }

      let monthNames = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
      ];

      let day = d.getDate();
      let monthIndex = d.getMonth();
      let year = d.getFullYear();
      let hours = d.getHours();
      let mins = d.getMinutes();
      mins = mins < 10 ? "0" + mins : mins;
      let type = hours < 12 ? " AM IST" : " PM IST";
      let time = hours + ":" + mins + type;
      if (isEpochTime) {
        return new Date(d).getTime();
      } else {
        return (
          monthNames[monthIndex] + " " + day + "," + " " + year + ", " + time
        );
      }
    } else {
      throw "IBEAT:Date format is not valid";
    }
  } catch (e) {
    console.log(e);
  }
};

export default { _getCookie, _formatDate };

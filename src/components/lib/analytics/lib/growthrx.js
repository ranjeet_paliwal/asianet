import { _getStaticConfig, isProdEnv, _isCSR, getPageTypeUpdated } from "../../../../utils/util";

const siteConfig = _getStaticConfig();

export default function(options) {
  // For GrowthRX
  (function(g, r, o, w, t, h, rx) {
    (g[t] =
      g[t] ||
      function() {
        (g[t].q = g[t].q || []).push(arguments);
      }),
      (g[t].l = 1 * new Date());
    (g[t] = g[t] || {}), (h = r.createElement(o)), (rx = r.getElementsByTagName(o)[0]);
    h.async = 1;
    h.src = w;
    rx.parentNode.insertBefore(h, rx);
  })(window, document, "script", "https://static.growthrx.in/js/v2/web-sdk.js", "grx");
  grx("init", isProdEnv() ? siteConfig.growthrx.prod.id : siteConfig.growthrx.stg.id);
  if (_isCSR()) {
    grx("track", "page_view", {
      url: window.location.pathname,
      screen_type: getPageTypeUpdated(window.location.pathname),
    });
  }

  // Central location for firing page_view in case of landing page

  // (function(g, r, o, w, t, h, rx) {
  //   (g[t] =
  //     g[t] ||
  //     function() {
  //       (g[t].q = g[t].q || []).push(arguments);
  //     }),
  //     (g[t].l = 1 * new Date());
  //   (g[t] = g[t] || {}),
  //     (h = r.createElement(o)),
  //     (rx = r.getElementsByTagName(o)[0]);
  //   h.src = w;
  //   rx.parentNode.insertBefore(h, rx);
  // })(
  //   window,
  //   document,
  //   "script",
  //   "https://static.growthrx.in/js/v2/grx-web-sdk.js",
  //   "grx"
  // );
  // var growthRxInterval = setInterval(function() {
  //   console.log("interval is still running");
  //   if (typeof grx === "function") {
  //     console.log("clearing interval");
  //     clearInterval(growthRxInterval);
  //     //grx('init', 'g41891665');   //NavBharatTimes Web
  //     //grx('init', 'g0a1c371d',{attribution_tracking:true});
  //     grx("init", siteConfig.growthrx.id, { attribution_tracking: true });
  //     window.growthrx_web_sdk_configured = true;
  //     grx("track", "page_view", { page: location.pathname });
  //   }
  // }, 0);
}

import ga from "./src/ga";
import iBeat from "./src/iBeat";
import comscore from "./src/comscore";
import fbpixel from "./src/fbpixel";

export const AnalyticsGA = ga;
export const AnalyticsComscore = comscore;
export const Analytics_iBeat = iBeat;
export const Analytics_fbpixel = fbpixel;

export default {
  AnalyticsGA,
  Analytics_iBeat,
  AnalyticsComscore,
  Analytics_fbpixel,
};

import React from "react";
const InlineLoader = () => {
  return (
    <div className="loader">
      <span className="dot"></span>
      <span className="dot"></span>
      <span className="dot"></span>
    </div>
  );
};
export default InlineLoader;

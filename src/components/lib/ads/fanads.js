import ctnAds from "./ctnAds";
import { loadJS } from "./lib/utils";
import Ads_module from "./index";

const fanjs = "https://connect.facebook.net/en_US/fbadnw60-tag.js";

export const render = (ad, el, adEl) => {
  //ad = config having id and slot size. Present on configs
  //el = fbad container
  //adEl = dfp ad container
  var slotname = el.getAttribute("data-slot");
  //console.log(slotname)
  if (typeof window === "undefined") {
    return false;
  }
  var testmode = window.location.href.indexOf("fanads") >= 0;
  //console.log(testmode, 'fan ads', ad, el);
  //Catch error if fanads file not added and throws error
  try {
    window.ADNW.v60.slots.push({
      rootElement: el,
      placementid: ad.id,
      format: ad.size,
      testmode: testmode,
      onAdLoaded: function(rootElement) {
        console.log(
          "Audience Network slot " +
            " " +
            slotname +
            " = " +
            ad.id +
            " ad loaded"
        );
        rootElement.style.display = "block";
        rootElement.removeAttribute("data-type");
        //Special condition when FB ad rendered , remove DependentCtnAd and render remaining ctns
        try {
          adEl && adEl.classList.add("hide");
        } catch (err) {
          console.log(err);
        }

        //Ads_module.render()
        //ctnAds.render({})
      },
      onAdError: function(errorCode, errorMessage, rootElement) {
        renderDfpAdsAsFallback(
          el,
          adEl,
          errorCode,
          errorMessage,
          rootElement,
          ad.id,
          slotname
        );
      }
    });
  } catch (err) {
    //error handling for any error on Above methods
    renderDfpAdsAsFallback(el, adEl, 400, err.message, ad.id, slotname);
  }
};

const renderDfpAdsAsFallback = (
  el,
  adEl,
  errorCode,
  errorMessage,
  rootElement,
  adid,
  slotname
) => {
  //ctnAds.render({});
  console.log(
    "Audience Network " +
      " " +
      slotname +
      " = " +
      adid +
      " error (" +
      errorCode +
      ") " +
      errorMessage
  );
  //if adEl not present return false
  if (typeof adEl == "undefined" || adEl == null) return false;

  //Removing Fan div on error
  try {
    el && el.classList.add("hide");
  } catch (err) {
    console.log(err);
  }

  //On FAN fallback setting attribute to render DFP ads which having attr data-adtype-temp
  document.querySelectorAll("[data-adtype-temp]").forEach(function(element) {
    element.setAttribute(
      "data-adtype",
      element.getAttribute("data-adtype-temp")
    );
  });

  Ads_module.render({}, adEl);

  //Ads_module.render()
  adEl.style.display = "block";
};

export const initialize = () => {
  loadJS(fanjs, null, "fan_js", true);

  if (typeof window === "undefined") {
    return false;
  }

  window.ADNW = window.ADNW || {};
  window.ADNW.v60 = window.ADNW.v60 || {};
  window.ADNW.v60.slots = window.ADNW.v60.slots || [];

  return true;
};

export default { initialize, render };

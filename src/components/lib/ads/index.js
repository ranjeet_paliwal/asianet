/* eslint-disable no-cond-assign */
/* eslint-disable no-nested-ternary */
/* eslint-disable import/no-cycle */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-unused-expressions */
/* eslint-disable camelcase */
/* eslint-disable func-names */
/* eslint-disable no-loop-func */
/* eslint-disable no-restricted-syntax */
import { loadJS, getFilteredKeyword, getSplatsVars, isFeatureURL, wapadsGenerator } from "./lib/utils";
import { customEventPolyfill, elementInView, _getStaticConfig } from "../../../utils/util";

const gpturl = "https://www.googletagservices.com/tag/js/gpt.js";
// const prebidurl = 'https://mobileads.indiatimes.com/prebit_testing/28may19/prebid2.16.0.js';
// const prebidurl = "https://mobileads.indiatimes.com/prebit_testing/6sept19/prebid2.31.0.js";

const _dummywapads = {};

const addParallaxAdStyling = parallaxDiv => {
  const css = document.createElement("style");
  css.type = "text/css";
  css.appendChild(
    document.createTextNode(`
  .ad1.mrec1:after{display:block;}
  .parallaxDiv {height: 300px; width: 100%; margin: 0 auto; overflow: hidden; position: relative;border: 1px #f5f5f5 solid;box-sizing: border-box;}
  .parallaxDiv .adinner {position: absolute!important;top: 0!important;left: 0!important;width: 100%!important;
    height: 100%!important;border: 0!important;margin: 0!important;padding: 0!important;clip: rect(0,auto,auto,0)!important;
    -webkit-clip-path: polygon(0 0,100% 0,100% 100%,0 100%)!important;clip-path: polygon(0 0,100% 0,100% 100%,0 100%)!important;}
  .parallaxDiv .adinner-fxbox{position: fixed!important;top: 0!important;width: 100%; left:0; right:0; height: 100%;-webkit-transform: translateZ(0)!important;display: -webkit-box;
    -webkit-box-orient: vertical;-webkit-box-direction: normal;-ms-flex-direction: column;flex-direction: column;-webkit-box-align: center;-ms-flex-align: center;align-items: center;-webkit-box-pack: center;-ms-flex-pack: center;justify-content: center;}
  `),
  );
  parallaxDiv.appendChild(css);
};

const default_config = {
  isSlotrenderEndedAttached: false,
  currentIndex: 0,
  position: 0,
  defSlots: [
    [320, 50],
    [468, 60],
    [320, 100],
  ],
  resizeAdInfo: {
    fbn: {
      resize: () => {
        const fbnHeight = document.querySelector(".ad1.fbn") ? document.querySelector(".ad1.fbn").clientHeight : 0;
        const footElem = document.querySelector('[data-node="footer"]');
        // check to fix extra space below footer when ad not visible
        if (footElem && fbnHeight > 0) document.querySelector('[data-node="footer"]').style.marginBottom = fbnHeight;
      },
    },
    btf: {
      resize: () => {
        const footElem = document.querySelector('[data-node="footer"]');
        if (footElem) footElem.style.marginTop = "10px";
      },
    },
    mtf: {
      // add border if in middle of news list
      resize: () => {
        addBorder("div.ad1.mtf");
      },
    },
    // atf: {
    //   resize: () => {
    //     const atfHeight = document.querySelector(".ad1.atf iframe")
    //       ? document.querySelector(".ad1.atf iframe").clientHeight
    //       : 0;
    //     const translateValue = Math.max(atfHeight - 90, 0);
    //     document.getElementById("childrenContainer").style.transform = `translateY(${translateValue}px)`;
    //     document.getElementById("trending-minitv").style.transform = `translateY(${translateValue}px)`;
    //     //     document.querySelector(".ad1.atf").style.minHeight = `${atfHeight}px`;
    //     //     document.querySelector(".ad1.atf").style.lineHeight = `${atfHeight}px`;
    //   },
    // },
  },
  channelCode: "",
  waitForAdsEvent: false,
  dfpAdConfig: { topatf: "elections" },
};

const mod_wapads = { adSlots: {} };
// let googletag = {cmd:[]};

const addBorder = selector => {
  const selectorObj = document.querySelector(selector);
  if (selectorObj && selectorObj.queryAll("div") && selectorObj.queryAll("div").style.display !== "none") {
    selectorObj.classList.add("news");
  }
};

/* loading google ad js from google site
 * then push defineAdSlots & createAds
 */
const loadGoogleAdJs = (item, _options) => {
  // window.setTimeout(()=>{
  if (googletag && googletag.apiReady) {
    /* push defineAdSlots to googletag cmd based on
     * status event for setsection
     * if waitForAdsEvent is enabled
     */
    if (default_config.waitForAdsEvent) {
      const listenerFun = function() {
        document.removeEventListener("status.setsection", listenerFun, true);
        googletag.cmd.push(defineAdSlots.bind(this, item, _options));
      };
      document.addEventListener("status.setsection", listenerFun, true);
    } else {
      googletag.cmd.push(defineAdSlots.bind(this, item, _options));
    }
    if (!mod_wapads.config.isSlotrenderEndedAttached) {
      mod_wapads.config.isSlotrenderEndedAttached = true;
      googletag.pubads().addEventListener("slotRenderEnded", event => {
        if (event && event.slot) {
          const slotName = event.slot.getAdUnitPath().toLowerCase();

          // fire ad ga for atf slot rendered
          if (window.adtrackerOn && slotName.indexOf("_atf") >= 0) {
            // ga("send", "event", "adtracker", document.location.pathname, event.isEmpty ? "atf_empty" : "atf_filled");
          }

          for (const i in mod_wapads.config.resizeAdInfo) {
            if (slotName.indexOf(i) != -1) {
              mod_wapads.config.resizeAdInfo[i].resize();
              break;
            }
          }

          const adUnit = document.getElementById(event.slot.getSlotElementId());
          const adUnitParent = adUnit ? adUnit.parentElement : undefined;

          if (adUnitParent) {
            if (adUnitParent.attributes.length > 0) adUnitParent.style.display = event.isEmpty ? "none" : "block";
            // Identification of Direct Campaign
            // set Attribute skip modification
            if (typeof event.slot.getHtml() === "string" && event.slot.getHtml().indexOf("!--NO_REFRESH--") > 0) {
              adUnitParent.setAttribute("ad-direct", true);
            }
          }

          // fix for parallax in new section wise ad
          if (slotName.toLowerCase().indexOf("_mrec_1") >= 0 || slotName.toLowerCase().indexOf("_mrec1") >= 0) {
            // FIXME: Throwing error on live
            const parallax_parent = adUnit.closest(".parallaxDiv");
            if (parallax_parent) {
              if (typeof event.size === "object" && event.size !== null && event.size.indexOf(600) == -1) {
                parallax_parent.classList.remove("parallaxDiv");
              } else {
                if (typeof window !== "undefined" && window.isTransforming) {
                  window.forceOffTransform();
                  window.parallaxEnabled = true;
                }
                addParallaxAdStyling(parallax_parent);
              }
            }
          }

          if (slotName.toLowerCase().indexOf("_innov1") >= 0 && !event.isEmpty) {
            window.googletag.destroySlots([window.adSlotsDFP.skinrhs]);
            // Collect all skinrhs and display none
            const skinrhsElems = document.getElementsByClassName("skinrhs");
            for (const i in skinrhsElems) {
              if (skinrhsElems[i]) skinrhsElems[i].style.display = "none";
            }
          }
        }
      });
    }

    // start creating ads div on gpt sequence , removed from here and moved to defineAds sequence
    // googletag.cmd.push(createAds.bind(this,item));
  }
  // },1000);
};

/**
 * creating ads div for the page
 *{'ATF':{id:'div-gpt-ad-1394689654028-3','name':'/7176/TOI_MWeb/TOI_MWeb_Home/TOI_MWeb_HP_PT_MTF'}}
 * @param elem : will come for the articleshow with perpetual scroll
 */

const createAds = (elem, adtype) => {
  // let elems = elem ? ((typeof elem === 'string') ? [document.getElementById(elem)] : [elem] ): document.querySelectorAll('[data-adtype]') ;
  // let elems = document.querySelectorAll('[data-adtype]');
  const el = elem;
  // for(let el of elems) {
  if (el) {
    // let ad = el.getAttribute('data-adtype') ? el.getAttribute('data-adtype').toLowerCase() : null;
    const ad = adtype && adtype != "" ? adtype.toLowerCase() : null;
    let v = mod_wapads.wapads[ad];
    // if (ad === "atf" && isMobilePlatform()) {
    //   if (typeof window !== "undefined" && window.newAtfAdSize && window.newAtfAdSize !== []) {
    //     v.size = JSON.parse(window.newAtfAdSize);
    //   }
    // }
    if (!v || v == "") {
      v = { id: el.getAttribute("data-id") };
    }

    // if(!v['id'] || v['id'] == '')continue;
    if (!v.id || v.id == "") return false;

    // For Prebid implementation. Call only if prebid js loaded
    // if (window.pbjs != undefined && window.pbjs.addAdUnits != undefined) {
    //     prebidModule.setBid(v, process.env.SITE)
    // }
    const tmpScript = document.createElement("script");
    tmpScript.type = "text/javascript";
    const divElem = document.createElement("div");
    divElem.setAttribute("id", v.id);
    divElem.appendChild(tmpScript);

    el.appendChild(divElem);
    el.setAttribute("adInViewCount", 0);
    // el.removeAttribute('data-adtype');

    if (mod_wapads.config.isGDPR) {
      googletag.pubads().setRequestNonPersonalizedAds(1);
    }

    googletag.cmd.push(() => {
      // ga for ads
      // window.adtrackerOn && typeof ga !== "undefined"
      //     ? ad == "atf"
      //         ? ga("send", "event", "adtracker", document.location.pathname, "atf")
      //         : ad == "fbn"
      //             ? ga("send", "event", "adtracker", document.location.pathname, "fbn")
      //             : ad == "topatf"
      //                 ? ga("send", "event", "adtracker", document.location.pathname, "topatf")
      //                 : null
      //     : null;
      googletag.display(v.id);
    });
  }
  // };
};

/**
 * Google ad js for defining ad slots based on the location and msid
 * {'ATF':{id:'div-gpt-ad-1394689654028-3','name':'/7176/TOI_MWeb/TOI_MWeb_Home/TOI_MWeb_HP_PT_MTF'}}
 */
const defineAdSlots = (elem, options) => {
  let _auds = [];
  window.ad_slots = {};

  if (typeof window !== "undefined" && typeof colaud !== "undefined") {
    localStorage.setItem("colaud", colaud.aud);
  }
  if (typeof colaud !== "undefined") {
    _auds = colaud.aud;
  } else if (window !== "undefined" && window.localStorage && typeof localStorage !== "undefined") {
    _auds = localStorage.getItem("colaud") ? localStorage.getItem("colaud") : null;
  }

  const splatVars = getSplatsVars();
  const sectionData = splatVars.splats;
  const _HDL = "";
  const _ARC1 = "strong";
  let _ref = splatVars._ref || default_config.channelCode;
  _ref = _ref.toLowerCase() + (process.env.PLATFORM == "desktop" ? "_web" : "_wap");

  // let _isTizen = (navigator.userAgent.indexOf('Tizen') != -1) ? true : false;
  // if (_isTizen) { ref = 'tizen'; }
  // let _Hyp1 = options && options.hyp1 && options.hyp1 != '' ? options.hyp1 : window.hyp1 ? window.hyp1 : '';
  // let _article = '';
  // let _SCN = options && options._SCN && options._SCN != '' ? options._SCN : window._SCN;
  // let _SubSCN = options && options._SubSCN && options._SubSCN != '' ? options._SubSCN : window._SubSCN;
  // let _LastSubSCN = options && options._LastSubSCN && options._LastSubSCN != '' ? options._LastSubSCN : window._LastSubSCN;
  const _SCN = sectionData[0] || "";
  const _SubSCN = sectionData[1] || "";
  const _LastSubSCN = sectionData[2] || "";
  const _hyp1 = splatVars.hyp1 || "";
  const keyword = window.tgtkeys && window.tgtkeys.keyword ? window.tgtkeys.keyword : "";
  const blacklist = window.tgtkeys && window.tgtkeys.BL ? window.tgtkeys.BL : "0";
  const pagetype = window.tgtkeys && window.tgtkeys.templatetype ? window.tgtkeys.templatetype : "";
  const puvkey = window.tgtkeys && window.tgtkeys.puvkey === "true" ? "1" : "0";

  // const newURL = new URL(window.location.href).searchParams.get("utm_source");
  const utmval = getQueryString("utm_source");
  if (utmval == null || utmval == undefined) {
    window.oem = "";
  } else {
    window.oem = utmval;
  }

  const _tval = v => {
    if (typeof v === "undefined") return "";
    if (v.length > 100) return v.substr(0, 100);
    return v;
  };

  const elems = elem
    ? typeof elem === "string"
      ? [document.getElementById(elem)]
      : [elem]
    : document.querySelectorAll("[data-adtype]");
  // let elems = document.querySelectorAll('[data-adtype]');

  _SCN !== "" ? googletag.pubads().setTargeting("SCN", _SCN) : "";
  _SubSCN !== "" ? googletag.pubads().setTargeting("SubSCN", _SubSCN) : "";
  _LastSubSCN !== "" ? googletag.pubads().setTargeting("LastSubSCN", _LastSubSCN) : "";
  _hyp1 !== "" ? googletag.pubads().setTargeting("Hyp1", _tval(_hyp1)) : "";
  window.tgtkeys.msid ? googletag.pubads().setTargeting("storyId", window.tgtkeys.msid) : "";

  googletag
    .pubads()
    .setTargeting("ARC1", _tval(_ARC1))
    .setTargeting("oem", window.oem || "")
    .setTargeting("_ref", _tval(_ref))
    .setTargeting("HDL", _tval(_HDL))
    .setTargeting("BL", blacklist)
    .setTargeting("puv", puvkey)
    .setTargeting("templatetype", pagetype)
    .setTargeting("countrycode", window.geoinfo ? window.geoinfo.CountryCode : "IN")
    .setTargeting("keyword", keyword)
    .setTargeting("sg", _auds);
  // .setTargeting("ctnkeyword", keyword);

  // googletag.pubads().enableSingleRequest();
  // googletag.pubads().collapseEmptyDivs(true); comment as per ads team recommandations
  googletag.enableServices();

  for (const el of elems) {
    const ad = el.getAttribute("data-adtype") ? el.getAttribute("data-adtype").toLowerCase() : null;
    let v = mod_wapads.wapads[ad];
    // if (ad === "atf" && isMobilePlatform()) {
    //   if (typeof window !== "undefined" && window.newAtfAdSize && window.newAtfAdSize !== []) {
    //     v.size = JSON.parse(window.newAtfAdSize);
    //   }
    // }
    // if (ad === "atf" && isMobilePlatform()) {
    //   if (typeof window !== "undefined" && window.newAtfAdSize && window.newAtfAdSize !== []) {
    //     v.size = JSON.parse(window.newAtfAdSize);
    //   }
    // }
    // Control show hide of particular adunit
    // iterate through adunit config from alaska
    // check ('*' respresents all pages) is activated in dfpAdConfig
    // check ('empty' respresents no pages) is deavtivated in dfpAdConfig
    let shouldRender = true;

    for (const adunit in mod_wapads.config.dfpAdConfig) {
      if (
        adunit !== "" &&
        ad === adunit &&
        mod_wapads.config.dfpAdConfig[adunit].indexOf("*") === -1 &&
        (mod_wapads.config.dfpAdConfig[adunit].indexOf("empty") > -1 ||
          mod_wapads.config.dfpAdConfig[adunit].indexOf(_SCN) === -1)
      ) {
        shouldRender = false;
        el.style.display = "none";
        break;
      }
    }

    if (shouldRender) {
      el.style.display = "block"; // show adsContainer before defining adslot , to handle -ve adxs,adys coordinates
      if (!v || v === "") {
        v = {
          id: el.getAttribute("data-id"),
          name: el.getAttribute("data-name"),
          mlb: el.getAttribute("data-mlb"),
          size: el.getAttribute("data-size"),
        };
      }

      if (!v.id || v.id == "") continue; // if ad has no data or data-attribute skip
      // convert size to array from string
      if (v.size && !(v.size instanceof Array)) {
        try {
          v.size = JSON.parse(v.size);
        } catch (e) {
          v.size = [[320, 50]];
        }
      }

      // Ad id for ads creating at run time.
      v.id += mod_wapads.config.position++;

      v.name = el.getAttribute("data-name") || v.name;

      if (el && el.innerHTML == "") {
        el.setAttribute("data-id", v.id); // reset id as-per record
        let gtag = {};
        if (ad == "innove") {
          gtag = googletag.defineOutOfPageSlot(v.name, v.id);
        } else {
          gtag = googletag.defineSlot(v.name, v.size && v.size != "" ? v.size : mod_wapads.config.defSlots, v.id);
        }
        // let mlb;
        // if (v.mlb && v.mlb.length > 0) {
        //   mlb = googletag.sizeMapping();
        //   for (let j = 0; j < v.mlb.length; j++) {
        //     mlb = mlb.addSize(v.mlb[j][0], v.mlb[j][1]);
        //   }
        //   if (gtag) gtag.defineSizeMapping(mlb.build());
        // }
        if (gtag) {
          if (ad == "mrec2") {
            googletag.pubads().enableLazyLoad({
              fetchMarginPercent: 100, // Fetch slots within 1 viewports.
              renderMarginPercent: 100, // Render slots within 1 viewports.
              mobileScaling: 2.0, // Double the above values on mobile.
            });
            gtag.addService(googletag.pubads());
          } else {
            gtag.addService(googletag.pubads());
          }
          if (ad == "fbn" || ad == "innove" || ad == "topatf" || ad == "canvasAd" || ad == "skinrhs") {
            window.adSlotsDFP[ad] = gtag;
          } else if (v.id) {
            window.adSlotsDFP[v.id] = gtag;
          }
          // mod_wapads.config.position++; //update position for next
        }

        el.removeAttribute("data-adtype");

        // start creating inner Ad divs
        createAds(el, ad);
      }
    }
  }
};

function getQueryString() {
  let key = false;
  const res = {};
  let itm = null;
  // get the query string without the ?
  const qs = location.search.substring(1);
  // check for the key as an argument
  if (arguments.length > 0 && arguments[0].length > 1) key = arguments[0];
  // make a regex pattern to grab key/value
  const pattern = /([^&=]+)=([^&]*)/g;
  // loop the items in the query string, either
  // find a match to the argument, or build an object
  // with key/value pairs
  while ((itm = pattern.exec(qs))) {
    if (key !== false && decodeURIComponent(itm[1]) === key) return decodeURIComponent(itm[2]);
    if (key === false) res[decodeURIComponent(itm[1])] = decodeURIComponent(itm[2]);
  }

  return key === false ? res : null;
}

export const render = (options, item, section, subsection) => {
  if (typeof window === "undefined") {
    return false;
  }

  // Switch off ads for Featured URLs.
  if (isFeatureURL(options ? options.pathname : undefined)) return false;

  default_config.dfpAdConfig =
    window.dfpAdConfig && Object.keys(window.dfpAdConfig).length >= 0 ? window.dfpAdConfig : default_config.dfpAdConfig;

  mod_wapads.wapads = options && options.wapads ? options.wapads : window.wapads;
  mod_wapads.config = default_config;
  const _options = { _SCN: section, _SubSCN: subsection };

  if (typeof mod_wapads.wapads === "object" && typeof googletag !== "undefined" && googletag.cmd) {
    googletag.cmd.push(loadGoogleAdJs.bind(this, item, _options));
  }
};

export const initialize = options => {
  if (typeof window === "undefined") {
    return false;
  }

  // Commenting gpt and prebid load for pubmatic

  // Load PreBid JS library
  // loadJS(prebidurl, prebidModule.initPreBid(), 'prebid_js', true);

  // off gpt load for now , as we loading at head : TODO
  if (!window.googletag && 0) {
    loadJS(
      gpturl,
      () => {
        googletag = window.googletag || {};
        googletag.cmd = googletag.cmd || [];
      },
      "gpt_js",
      true,
    );
  } else {
    googletag = window.googletag || {};
    googletag.cmd = googletag.cmd || [];
  }

  window.wapads = window.wapads || options.wapads || _dummywapads;
  if (typeof window.adSlotsDFP == "undefined") {
    window.adSlotsDFP = {};
  }

  default_config.isNonPersonalizeAds = options.isGDPR;
  default_config.channelCode = options.channelCode;

  // Load WESI JS to detech Ad Blocker for (ESI Ad Code)
  /* loadJS(
    "https://eisamay.indiatimes.com/wesi.cms",
    () => {
      if (window.clrcs) {
        clrcs.refresh();
      }
    },
    "wesi",
    true,
  ); */

  return true;
};

export const refreshAds = (refresh_slots, offDefaultRefresh, options) => {
  if (typeof window === "undefined" || typeof googletag === "undefined" || typeof googletag.pubads === "undefined") {
    return false;
  }

  if (options && options.wapads) window.wapads = options.wapads;

  if (typeof refresh_slots === "object" && refresh_slots.length > 0) {
    for (let i = 0; i < refresh_slots.length; i++) {
      const slot = refresh_slots[i];
      let oldAd = null;
      let oldAdInViewPort = null;

      // Multiple ads might be in viewport
      // If viewportCheck is enabled, this refreshes ad
      // in current view port
      // FIXME: Edge case of 2 same type of ads in viewport and both need to be refreshed
      if (options && options.viewportCheck) {
        const allAdSlots = document.querySelectorAll(`.${slot}`);
        if (allAdSlots && allAdSlots.length > 0) {
          for (let j = 0; j < allAdSlots.length; j++) {
            const adElem = allAdSlots[j];
            if (elementInView(adElem, true)) {
              oldAd = adElem;
              oldAdInViewPort = adElem;
              break;
            }
          }
        }
      } else {
        oldAd = document.querySelector(`.${slot}`);
      }

      if (oldAd) oldAd.style.display = "block";
      if (typeof slot === "string") {
        if (window.adSlotsDFP[slot]) {
          googletag.cmd.push(() => {
            googletag.pubads().refresh([window.adSlotsDFP[slot]]);
          });
        } else {
          recreateAds([slot], { oldAdInViewPort });
        }
      } else {
        googletag.pubads().refresh(slot);
      }
      // ga for ads
      // if (window.adtrackerOn && typeof ga !== "undefined") {
      //   if (slot == "atf") ga("send", "event", "adtracker", document.location.pathname, "atf");
      //   if (slot == "fbn") ga("send", "event", "adtracker", document.location.pathname, "fbn");
      //   if (slot == "topatf") ga("send", "event", "adtracker", document.location.pathname, "topatf");
      // }
    }
  } else if (!offDefaultRefresh) {
    googletag.pubads().refresh();
  }
};

export const recreateAds = (recreate_slots, options, adNameObj) => {
  if (typeof window === "undefined" || typeof googletag === "undefined" || typeof googletag.pubads === "undefined") {
    return false;
  }

  if (typeof recreate_slots === "object" && recreate_slots.length > 0) {
    for (let i = 0; i < recreate_slots.length; i++) {
      const slot = recreate_slots[i];

      let oldAd = null;
      // Considering one oldAdInViewPort case only
      if (options && options.oldAdInViewPort) {
        oldAd = options.oldAdInViewPort;
      } else {
        oldAd = document.querySelector(`.${slot}`);
      }

      // Replaced below logic with this function
      createNewAdFromOld(oldAd, slot, options, adNameObj);

      // if (typeof slot === "string") {
      //   if (googletag && typeof googletag.destroySlots === "function")
      //     googletag.destroySlots([window.adSlotsDFP[slot]]);

      //   const oldAd = document.querySelector(`.${slot}`);

      //   if (!oldAd || oldAd === null) continue;

      //   const newAd = document.createElement("div");
      //   newAd.setAttribute("class", `ad1 ${slot}`);
      //   newAd.setAttribute("data-adtype", slot);

      //   if (oldAd.hasAttribute("data-id")) {
      //     newAd.setAttribute("data-id", oldAd.getAttribute("data-id"));
      //   }
      //   // if (oldAd.hasAttribute("data-mlb")) {
      //   //   newAd.setAttribute("data-mlb", oldAd.getAttribute("data-mlb"));
      //   // }
      //   if (oldAd.hasAttribute("data-size")) {
      //     newAd.setAttribute("data-size", oldAd.getAttribute("data-size"));
      //   }
      //   if (oldAd.hasAttribute("class")) {
      //     newAd.setAttribute("class", oldAd.getAttribute("class"));
      //   }
      //   if (oldAd.hasAttribute("ad-recreate")) {
      //     newAd.setAttribute("ad-recreate", oldAd.getAttribute("ad-recreate"));
      //   }

      //   if (typeof adNameObj === "object" && adNameObj[slot] && adNameObj[slot] != "") {
      //     newAd.setAttribute("data-name", adNameObj[slot]);
      //   } else if (typeof adNameObj === "object" && adNameObj.default) {
      //     newAd.setAttribute("data-name", adNameObj.default);
      //   } else if (typeof adNameObj === "null" && options && options.wapads && options.wapads[slot]) {
      //     newAd.setAttribute("data-name", options.wapads[slot].name);
      //   } else if (oldAd.hasAttribute("data-name")) {
      //     newAd.setAttribute("data-name", oldAd.getAttribute("data-name"));
      //   }

      //   insertAfter(newAd, oldAd);
      //   oldAd.remove();
      //   render({}, newAd);
      // }
    }
  }
};

/** Takes old ad slot element, slot as string and recreates that ad after destroying it.
 *  @author Abhinav Mishra
 *  @description Helper function to destroy old ad and recreate the same (used to refresh same ad slot if occuring multiple times in page)
 *  @param {oldAd,slot ,options,adNameObj} oldAd html element to destroy, new slot name. Optional arguments options,adNameObj
 *  @returns {undefined}
 * */

export const createNewAdFromOld = (oldAd, slot, options, adNameObj) => {
  if (typeof window === "undefined" || typeof googletag === "undefined" || typeof googletag.pubads === "undefined") {
    return false;
  }

  if (typeof slot === "string") {
    if (
      googletag &&
      typeof googletag.destroySlots === "function" &&
      window.adSlotsDFP &&
      Object.keys(window.adSlotsDFP).length > 0
    ) {
      googletag.destroySlots([window.adSlotsDFP[slot]]);
    }

    if (!oldAd || oldAd === null) return false;

    const newAd = document.createElement("div");
    newAd.setAttribute("class", `ad1 ${slot}`);
    newAd.setAttribute("data-adtype", slot);

    if (oldAd.hasAttribute("data-id")) {
      newAd.setAttribute("data-id", oldAd.getAttribute("data-id"));
    }
    // if (oldAd.hasAttribute("data-mlb")) {
    //   newAd.setAttribute("data-mlb", oldAd.getAttribute("data-mlb"));
    // }
    if (oldAd.hasAttribute("data-size")) {
      newAd.setAttribute("data-size", oldAd.getAttribute("data-size"));
    }
    if (oldAd.hasAttribute("class")) {
      newAd.setAttribute("class", oldAd.getAttribute("class"));
    }
    if (oldAd.hasAttribute("ad-recreate")) {
      newAd.setAttribute("ad-recreate", oldAd.getAttribute("ad-recreate"));
    }

    if (typeof adNameObj === "object" && adNameObj[slot] && adNameObj[slot] != "") {
      newAd.setAttribute("data-name", adNameObj[slot]);
    } else if (typeof adNameObj === "object" && adNameObj.default) {
      newAd.setAttribute("data-name", adNameObj.default);
    } else if (typeof adNameObj === "null" && options && options.wapads && options.wapads[slot]) {
      newAd.setAttribute("data-name", options.wapads[slot].name);
    } else if (oldAd.hasAttribute("data-name")) {
      newAd.setAttribute("data-name", oldAd.getAttribute("data-name"));
    }

    insertAfter(newAd, oldAd);
    oldAd.remove();
    render({}, newAd);
  }
};

function insertAfter(newNode, referenceNode) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

export const setSectionDetail = pwa_meta => {
  if (typeof window === "undefined") {
    return false;
  }

  if (pwa_meta && pwa_meta.AdServingRules && pwa_meta.AdServingRules === "Turnoff") {
    document.removeEventListener("status.setsection", {}, true);
    return;
  }

  if (typeof pwa_meta === "undefined") {
  } else {
    window.tgtkeys = {};

    const siteConfig = _getStaticConfig();

    const keyword = typeof pwa_meta.key === "string" ? getFilteredKeyword(pwa_meta.key) : "";
    const blacklist = typeof pwa_meta.blacklist === "string" && pwa_meta.blacklist === "true" ? "1" : "0";
    const puvkey = typeof pwa_meta.issex === "string" ? pwa_meta.issex : "0";
    pwa_meta.subsectitle1 ? (window._SCN = pwa_meta.subsectitle1) : (window._SCN = "");
    pwa_meta.subsectitle2 ? (window._SubSCN = pwa_meta.subsectitle2) : (window._SubSCN = "");
    pwa_meta.hyp1 ? (window.hyp1 = pwa_meta.hyp1) : (window.hyp1 = "");
    pwa_meta.subsectitle3 ? (window._LastSubSCN = pwa_meta.subsectitle3) : (window._LastSubSCN = "");
    window.tgtkeys.keyword = keyword;
    window.tgtkeys.BL = blacklist;
    window.tgtkeys.templatetype = typeof pwa_meta.pagetype === "string" ? pwa_meta.pagetype : "";
    window.tgtkeys.ctnkeyword = keyword;
    window.tgtkeys.SCN = pwa_meta.subsectitle1 ? pwa_meta.subsectitle1 : "";
    window.tgtkeys.SubSCN = pwa_meta.subsectitle2 ? pwa_meta.subsectitle2 : "";
    window.tgtkeys.LastSubSCN = pwa_meta.subsectitle3 ? pwa_meta.subsectitle3 : "";
    window.tgtkeys.msid = pwa_meta.msid ? pwa_meta.msid : "";
    window.tgtkeys.puvkey = puvkey;

    window.wapads = wapadsGenerator({
      secname: pwa_meta.adsec || "others",
      defaultWapAds: siteConfig.ads.dfpads.others,
    });

    mod_wapads.wapads = window.wapads;
  }

  /* Publish a status Event for successful setsection
   * and enable again waitForAdsEvent if reset
   */
  if (typeof pwa_meta === "object") {
    // if(!reset){
    customEventPolyfill();
    const event = new CustomEvent("status.setsection", { detail: {} });
    // Dispatch/Trigger/Fire the event
    document.dispatchEvent(event);
    // disable waitForAdsEvent
    default_config.waitForAdsEvent = false;
  }
};

export const resetwaitForAdsEvent = status => {
  default_config.waitForAdsEvent = status === "true";
};

export const destroySlots = () => {
  for (const i in window.adSlotsDFP) {
    if (window.adSlotsDFP[i] && window.googletag && typeof window.googletag.destroySlots === "function")
      if (i.indexOf("-cube-") === -1) {
        // we are excluding cube ads from destroying their slots
        window.googletag.destroySlots([window.adSlotsDFP[i]]);
        delete window.adSlotsDFP[i];
      }
  }
  // window.adSlotsDFP = {};
};

export default {
  initialize,
  render,
  refreshAds,
  recreateAds,
  createNewAdFromOld,
  setSectionDetail,
  resetwaitForAdsEvent,
  destroySlots,
};

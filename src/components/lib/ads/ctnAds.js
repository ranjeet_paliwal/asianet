/* eslint-disable func-names */
/* eslint-disable valid-typeof */
/* eslint-disable no-nested-ternary */
import React from "react";
import ReactDOM from "react-dom";

import loadCtn from "./lib/loadCtn";
import { isFeatureURL } from "./lib/utils";
import GDPR_module from "../gdpr/index";

const config = {
  ctnads: {},
  ctnOrgTitle: " Asianet",
};

const ctnTemplate = (data, adinfo, ctnstyle, ctnheading) => {
  let template = "";
  let wrapper = "";
  Array.prototype.slice.call(data).forEach((value, idx) => {
    template += `<div class='ad-wrapper' data-col='1'><a rel='noopener' title='${value.title}' target='_blank' href='${
      value.itemType !== 4 || value.itemType !== 5 || value.itemType !== 2 ? value.clk[0] : null
    }' onclick = '${
      value.itemType === 5 || value.itemType === 2 ? value.clk[0] : null
    }'><span class='holder'><img width='120' height='90' class='dummy-img' src='${
      value.mainimage
    }' alt='columbia' /></span><span class='listing_title'>${
      value.title
    }</span></a><span class='ad-dest'><span class='ad-text'>Ad</span> ${
      adinfo === "paid" ? value.brandtext : ""
    }</span><img class='colombia-logo' src='https://static.clmbtech.com/ad/commons/images/colombia-icon.png' alt='columbia'/></div>`;
  });
  if (ctnstyle !== "inline") {
    wrapper = `<div class='ad-fromTheWeb'><div class='sectionheading'><h2><span>${ctnheading}</span></h2></div><div class='gridview'><ul class='list'>${template}</ul></div></div>`;
  } else {
    wrapper = template;
  }
  return <div dangerouslySetInnerHTML={{ __html: `${wrapper}` }} />;
};

const createWrapper = (elem, ctnid, position, ctnstyle, slot, slideCount) => {
  const randNum = Math.floor(Math.random() * 100001);

  if (!slideCount) slideCount = 0;

  slideCount = slideCount++;

  const wrapper = elem || document.createElement("div");
  const adslot = `div-clmb-ctn-${ctnid}-${randNum}`;

  wrapper.setAttribute("id", adslot);
  wrapper.setAttribute("data-slot", ctnid);
  wrapper.setAttribute("data-position", randNum);
  wrapper.setAttribute("data-pos", position);
  wrapper.setAttribute("data-section", "0");
  // (ctnstyle != 'inline') ? wrapper.setAttribute("data-cb","adwidget") : null;
  wrapper.setAttribute("class", "colombia");
  wrapper.setAttribute("ctn-style", ctnstyle);
  wrapper.setAttribute("data-col", "1");
  // For refresh ad on scroll
  wrapper.setAttribute("adInViewCount", 0);

  return wrapper;
};

// fill CTN ads
const initdrawCTN = (data, identifier) => {
  const temp_identifier = identifier;
  const _container = document.querySelector(`#${identifier}`);
  // wrapper added for ctnshow widget
  if (_container.getAttribute("ctn-style") == "ctnshow") {
    if (data.paidAds != null) {
      const ad_paid = document.createElement("div");
      identifier = `${temp_identifier}_ad_paid`;
      ad_paid.setAttribute("id", identifier);
      _container.appendChild(ad_paid);
    }
    if (data.organicAds != null) {
      const ad_organic = document.createElement("div");
      identifier = `${temp_identifier}_ad_organic`;
      ad_organic.setAttribute("id", identifier);
      _container.appendChild(ad_organic);
    }
  }

  if (data === null) {
    _container.style.display = "none";
    return true;
  }
  // _container.innerHTML = '';
  // To Handle Crousal/Paid Ads
  if (_container.getAttribute("ctn-style") === "inlinetop") {
    _container.setAttribute("ctn-style", "inline");
  }

  if (data.paidAds != null) {
    if (_container.getAttribute("ctn-style") == "ctnshow") identifier = `${temp_identifier}_ad_paid`;
    drawCTN("paid", _container.getAttribute("ctn-style"), data.paidAds, identifier);
  }
  if (data.organicAds != null) {
    if (_container.getAttribute("ctn-style") == "ctnshow") identifier = `${temp_identifier}_ad_organic`;
    drawCTN("organic", _container.getAttribute("ctn-style"), data.organicAds, identifier);
  }
};

// create CTN ad Wrapper
const drawCTN = (adtype, ctnstyle, data, identifier) => {
  const _container = document.querySelector(`#${identifier}`);

  ctnstyle = ctnstyle === "ctninlineshow" || ctnstyle === "ctn-slider" ? "inline" : ctnstyle;
  const ctnheading = adtype === "paid" ? "From the Web" : config.ctnOrgTitle; // heading for ctn widget
  const recontext = ctnstyle !== "inline" ? "Recommended By Colombia " : ""; // bottom credit in widget
  const inlinecredit =
    '<img width="16" height="16" src="https://static.clmbtech.com/ad/commons/images/colombia-icon.png" style="margin-right:0; margin: 0;height: 16px;width: 16px;float: right;">';

  // wrapper added for widget
  if (ctnstyle !== "inline") {
    const conText = ctnTemplate(data, adtype, ctnstyle, ctnheading);
    ReactDOM.render(conText, _container);
  }

  if (data[0].itemType !== 4) {
    _container.classList.remove("fillcolombia");
    _container.style.display = "block";
  }

  // ad remove ad option
  _container.addEventListener("click", function(event) {
    // Polyfill for IE
    if (!Element.prototype.matches) {
      Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
    }
    if (event.target.matches('[data-plugin="closectn"]')) {
      event.stopPropagation();
      this.remove();
    }
  });

  // bottom credit for showpage
  if (ctnstyle !== "inline" && adtype == "organic") {
    const showcredit = `<p>${recontext}${inlinecredit}`;
    const tempPara = document.createElement("p");
    tempPara.setAttribute("class", "clickMore");
    tempPara.innerHTML = showcredit;
    if (_container.querySelector(".box p.clickMore")) _container.querySelector(".box p.clickMore").remove();
    _container.querySelector(".gridview").after(tempPara);
  }

  // enable video ads
  for (let i = 0; i < data.length; i++) {
    const link = data[i].clk[0];
    if (adtype === "paid" && data[i].itemType === 4) {
      renderVideoAds(_container, data[i], link);
    }
  }
};

const removeCTN = ctnobj => {
  document.querySelector(ctnobj).style.display = "none";
  document.querySelector(ctnobj).remove();
};

const renderVideoAds = (containerElem, vidAdData, adLinkUrl) => {
  const vidAd = {
    placeholderId: `${containerElem.getAttribute("id")}-video-iframe`,
    container: containerElem.querySelector(`#${containerElem.getAttribute("id")}-video`)
      ? containerElem.querySelector(`#${containerElem.getAttribute("id")}-video`)
      : containerElem,
    placeholder: document.getElementById(`${containerElem.getAttribute("id")}-video-iframe`),
  };

  if (!vidAd.placeholder) {
    vidAd.placeholder = document.createElement("iframe");

    vidAd.placeholder.setAttribute("id", vidAd.placeholderId);
    vidAd.placeholder.setAttribute("class", "videoplayer-iframe");
    vidAd.placeholder.setAttribute("frameBorder", "0");
    vidAd.placeholder.setAttribute("scrolling", "no");
    vidAd.placeholder.setAttribute("src", config.ctnads.ctnVideoURL);
    vidAd.placeholder.setAttribute(
      "style",
      "background-color:#000;width: 100%;height: 250px;border: 0px; overflow: hidden; display:none;",
    );

    vidAd.placeholder.addEventListener("onload", () => {
      vidAdData.autoplay = containerElem.querySelector("[data-autoplay]") ? 1 : 0;
      vidAdData.audio = containerElem.querySelector("[data-audio]") ? 1 : 0;
      if (this.contentWindow.colombiaVideoAd) {
        this.contentWindow.colombiaVideoAd(vidAdData, containerElem.getAttribute("id"));
        this.contentWindow.document.querySelector("#abort-vid-ad").style.display = "none";
      }
    });
    vidAd.container.appendChild(vidAd.placeholder);
  }
};

const colombiaVidAdStart = identifier => {
  const vidFrame = document.getElementById(identifier);
  const vidFrameContainer = document.getElementById(identifier.replace("-iframe", "")).closest('[data-plugin="ctn"]'); // getting wrapper
  vidFrameContainer.classList
    .remove("fillcolombia")
    .querySelector(".dummy-img")
    .remove();
};

const colombiaVidRemove = identifier => {
  document
    .getElementById(identifier)
    .closest('[data-plugin="ctn"]')
    .remove();
};

/* CTN Ad js */
const configure = options => {
  // For EU if optOut cookie set by user then only load dmp script
  // const forEU = window.geoinfo && !window.geoinfo.notEU && GDPR_module && GDPR_module.getOptOutCookie() == "0";
  // const forNonEU = window.geoinfo && window.geoinfo.notEU;
  const forEU =
    window && window.geoinfo && window.geoinfo.isGDPRRegion && GDPR_module && GDPR_module.getOptOutCookie() == "0";
  const forNonEU = window && window.geoinfo && !window.geoinfo.isGDPRRegion;
  // No need to block aa.js for CCPA as per dicussion with CTN team (Rohit & Mitin)
  const forCCPA = window && window.isCCPARegion;
  if (options && options.dmpUrl) {
    if (forNonEU || forEU || forCCPA) {
      options.ctnAddress = typeof encodeURIComponent === "function" ? `${options.dmpUrl}` : options.dmpUrl;
      loadCtn(options);
    }
  }
  // ?_u=${encodeURIComponent(location.href)}
  if (typeof colombia === "undefined") {
    const colombia = window.colombia || {};
    colombia.fns = colombia.fns || [];
    window.adwidget = initdrawCTN;
    window.colombiaVidAdStart = colombiaVidAdStart;
    window.colombiaVidRemove = colombiaVidRemove;
    // loadCtn(options);
    // load DMP Header Script tag same on all channel
    // options.ctnAddress = "https://ade.clmbtech.com/cde/ae/2658/var=_ccaud";
    // loadCtn(options);
  } else {
    window.colombia.update();
  }
};

export const render = (options, elem) => {
  if (typeof window === undefined) return false;

  let ctnid = "";

  // Switch off ads for Featured URLs.
  if (isFeatureURL(options ? options.pathname : undefined)) return false;

  const ctnElems = elem
    ? typeof elem === "string"
      ? [Array.prototype.slice.call(document.getElementById(elem))]
      : [elem]
    : Array.prototype.slice.call(document.querySelectorAll('[data-plugin="ctn"]'));

  if (typeof window.ctnads === "object") Object.assign(config.ctnads, window.ctnads);

  // excute only if ctn object exist
  if (typeof config.ctnads === "object" && ctnElems.length > 0) {
    ctnElems.forEach((item, i) => {
      if (!item) return true;
      if (item.innerHTML != "" || item.hasAttribute("data-position")) {
        return true;
      }
      const position = ++i;

      /* to check custom ctn id */
      if (
        typeof item.getAttribute("data-slot") !== "undefined" &&
        item.getAttribute("data-slot") !== null &&
        item.getAttribute("data-slot") != ""
      ) {
        const ctnObj = config.ctnads[item.getAttribute("data-slot")];
        if (typeof ctnObj === "object") {
          const { dfpSlot, dfpSize } = ctnObj;
          item.setAttribute("data-dfpslot", dfpSlot);
          item.setAttribute("data-dfpsize", JSON.stringify(dfpSize));
          item.setAttribute("data-type", "dfp");
          ctnid = ctnObj.ctnId;
        } else {
          ctnid = ctnObj;
        }

        item.setAttribute("data-adwidth", "300");
        item.setAttribute("data-adheight", "250");
      } else if (
        document.querySelector('[data-page="home"]') &&
        document.querySelector('[data-page="home"]').length > 0
      ) {
        /* <!-- for static code  --> */
        ctnid = config.ctnads.ctnHome;
      } else {
        const ctnObj = config.ctnads[item.getAttribute("ctn-style")];
        ctnid = typeof ctnObj === "object" ? ctnObj.ctnId : ctnObj;
      }

      if (item.getAttribute("data-slot") === "ctnmidart") item.setAttribute("data-adheight", "320");

      createWrapper(item, ctnid, position, item.getAttribute("ctn-style"), item.getAttribute("data-slot"));
    });
  }
};

export const initialize = options => {
  if (typeof window === "undefined") return false;

  config.ctnads = Object.assign({}, options.ctnads) || config.ctnads;

  const platform = process.env.PLATFORM ? process.env.PLATFORM : "mobile";

  // reset ctnads
  // if(platform == 'desktop' && location.pathname.indexOf('/tech/') > -1){
  //   window.ctnads = Object.assign(config.ctnads , config.ctnads['desktop']['tech'])
  // }else if(platform == 'desktop'){
  //   window.ctnads = Object.assign(config.ctnads , config.ctnads['desktop'])
  // }else

  if ("nbt".indexOf(process.env.SITE) > -1 && location.pathname.indexOf("/tech") > -1) {
    window.ctnads = config.ctnads.tech;
    // } else if ("vk".indexOf(process.env.SITE) > -1 && pathname.endsWith("/tech") == true) {
  } else if ("vk".indexOf(process.env.SITE) > -1 && location.pathname.endsWith("/tech") == true) {
    window.ctnads = config.ctnads.tech;
  }

  render(options);

  configure(options);
};

export default { initialize, render };

/* eslint-disable func-names */
/* eslint-disable no-multi-assign */
/* eslint-disable import/no-cycle */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-var */
/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-nested-ternary */
/* eslint-disable vars-on-top */

import { getPageType, _isCSR } from "../../../../utils/util";
import globalconfig from "../../../../globalconfig";

/**
 * Note- Do not use ES6 for below functions as it is used for HTML.js for making CSR.
 */

/**
 * Load javascript file on a page
 * @function js
 * @param {String} url URL of the javascript file
 * @param {Function} [callback] function to be called when js is loaded
 * @param {String} [id] id to be given to the javascript tag
 * @param {Boolean} [async] true by default, set false to load synchronously
 * @returns {HTMLElement} generated script tag element
 *
 */
export function loadJS(url, callback, id, async) {
  const head = document.getElementsByTagName("head") ? document.getElementsByTagName("head")[0] : null;
  if (head) {
    const script = document.createElement("script");
    const done = false; // Handle Script loading
    if (id) {
      script.id = id;
    }
    if (async) {
      script.async = async;
    }
    if (!url) {
      throw new Error("Param 'url' not defined.");
    }
    script.src = url;
    script.onload = script.onreadystatechange = function() {
      // Attach handlers for all browsers
      if (!script.loaded && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
        script.loaded = true;
        const endTime = new Date().getTime();
        const timeSpent = endTime - script.startTime;
        if (callback) {
          try {
            callback();
          } catch (e) {
            // to handle
            throw new Error("logger.error", e.stack);
          }
        }
        script.onload = script.onreadystatechange = null; // Handle memory leak in IE
      }
    };
    script.startTime = new Date().getTime();
    head.appendChild(script);
    return script;
  }
  throw new Error(`Head Element not found. JS '${url}' not loaded. `);
  return null;
}

export default { loadJS };

/**
 * To check is Feature URL or not
 * @param {*} pathname
 */
export const isFeatureURL = pathname => {
  let bool = false;
  let isCsr = true;

  if (typeof pathname !== "undefined") {
    isCsr = _isCSR();
  }

  pathname = pathname || (typeof window !== "undefined" && window.location.pathname);
  // if((pathname.indexOf('/articleshow/') > -1)){
  if (pathname && pathname.indexOf("-fea-ture/") > -1) {
    // ads block removed in case of feature URL.
    if (isCsr) {
      // document.querySelectorAll(".prerender").forEach(item => {
      //   item.style.display = "none";
      // });
      document
        .querySelectorAll(
          '.featured-article .parallaxDiv, .featured-article .ad1 , .featured-article [data-plugin="ctn"]',
        )
        .forEach(item => {
          item.style.display = "none";
        });
    }

    bool = true;
  }
  return bool;
};

/**
 * It is for getting Sectional Values which we get from parsing the URL or pathname
 * @param pathname : location pathname (String)
 */
export const getSplatsVars = pathname => {
  let splats = [];
  let hyp1 = "";
  let pagetype = "";
  let isCsr = true;

  if (typeof pathname !== "undefined") {
    isCsr = _isCSR();
  }

  if (isCsr) {
    pathname = pathname || window.location.pathname;
    pagetype = window.current_pagetype;
    if (window.meta && window.meta.longurl && window.meta.longurl != "") {
      pathname = window.meta.longurl;
    }
  }

  const getHyp1Value = function(splats) {
    const tempSplats = splats.slice();
    let hyp1val;
    for (const val of tempSplats) {
      if (val.indexOf("_hyp1_") > -1) {
        hyp1val = val.split("_hyp1_")[1];
      }
    }
    return hyp1val;
  };

  if (typeof pagetype === "undefined" || pagetype === "") {
    pagetype = getPageType(pathname);
  }
  // Remove '-' or ' ' with underscore
  pathname = pathname.replace(/-/g, "_");

  // special handling for home
  // if(pathname == '' || pathname == '/'){
  const wfhArr = [
    "virtual",
    "virtual_office",
    "virtual_position",
    "remote_office",
    "home_based",
    "telecommute",
    "online_learning",
    "video_conference",
    "freelance_writing",
    "freelance",
    "work_at_home",
    "customer_service",
    "remote_work",
    "telework",
    "flexible_workplace_learn_from_home",
    "work_from_home",
    "work_together",
  ];
  const covidArr = ["corona", "covid"];
  const wfhArrCheck = wfhArr.filter(item => pathname.indexOf(item) != -1);
  const covidArrCheck = covidArr.filter(item => pathname.indexOf(item) != -1);

  if (pagetype === "home") {
    if (pathname.indexOf("/us") > -1 || pathname.indexOf("/bangladesh") > -1 || pathname.indexOf("/gulf") > -1) {
      splats.push("home" + "_" + pathname.split("/").filter(item => item != ""));
    } else {
      splats.push("home");
    }
  } else if (pagetype === "newsbrief") {
    splats.push("newsbrief");
  }
  // special handling for coronavirus
  else if (covidArrCheck.length > 0 && wfhArrCheck.length > 0) {
    splats.push("coronavirus,workfromhome");
  } else if (covidArrCheck.length > 0) {
    splats.push("coronavirus");
  } else if (wfhArrCheck.length > 0) {
    splats.push("workfromhome");
  } else {
    splats = pathname.split("/").filter(item => item != "");
    // TODO Might be moved above.
    hyp1 = pathname.indexOf("_hyp1_") > -1 ? getHyp1Value(splats) : "";
    // FIXME: Special handling of short url of ipl landing
    if (pathname.indexOf("/iplt20.cms") > -1) {
      splats = splats.splice(0, 2); // To get first 2 , for Short Urls
    } else if (pathname.indexOf(".cms") > -1) {
      if (pagetype.indexOf("show") > -1 || pagetype.indexOf("moviereview") > -1) {
        splats.splice(-3, 3); // To remove last 3 values
      } else if (pagetype.indexOf("list") > -1) {
        splats.splice(-2, 2); // To remove last 2 values
      }
    } else {
      splats = splats.splice(0, 3); // To get first 3 , for Short Urls
    }
  }

  const ref = "";
  if (isCsr) {
    document.referrer.indexOf("facebook") != -1
      ? "facebook"
      : document.referrer.indexOf("google") != -1
      ? "google"
      : document.referrer.indexOf("twitter") != -1
      ? "twitter"
      : document.referrer.indexOf("whatsapp") != -1
      ? "whatsapp"
      : document.location.href.indexOf("utm_source=") != -1
      ? new URL(window.location.href).searchParams.get("utm_source")
      : document.referrer && document.referrer.length > 0
      ? "others"
      : undefined;
  }

  return { splats, _ref: ref, hyp1 };
};

// FIXME: need to optmize in single replace call.
export const getFilteredKeyword = key => {
  if (typeof key === "string") {
    const keyArr = key.split(",");
    const arr = keyArr.filter(key => {
      if (/[^a-zA-Z0-9, -]+/g.test(key)) {
        return false;
      }
      return true;
    });
    key = arr.join(",");
    key = key.replace(/, /gi, ",");
    key = key.trim();
    key = key.replace(/-/gi, "_");
    key = key.replace(/ /gi, "_");
    return key.toLowerCase();
  }
  return null;
};

/**
 * To renderDfpAds
 * @param {*} preRenderElemArray
 * @param {*} elem
 * @param {*} channelCode
 * @param {*} i
 */
export const renderDfpAds = (preRenderElemArray, elem, channelCode, i, keyword, blacklist, pagetype, puvkey) => {
  elem.style.display = "block";
  let _auds;

  if (typeof window !== "undefined" && typeof colaud !== "undefined") {
    localStorage.setItem("colaud", colaud.aud);
  }
  if (typeof colaud !== "undefined") {
    _auds = colaud.aud;
  } else if (window !== "undefined" && window.localStorage && typeof localStorage !== "undefined") {
    _auds = localStorage.getItem("colaud") ? localStorage.getItem("colaud") : null;
  }

  try {
    const sectionData = splatsVars.splats;
    var _SCN = sectionData[0] || "";
    var _SubSCN = sectionData[1] || "";
    var _LastSubSCN = sectionData[2] || "";
    var _oem = new URL(window.location.href).searchParams.get("utm_source");
    var _ref = splatsVars._ref || channelCode;
    _ref = _ref.toLowerCase() + (process.env.PLATFORM == "desktop" ? "_web" : "_wap");
    var _hyp1 = splatsVars.hyp1 || "";
  } catch (e) {}

  googletag.cmd.push(() => {
    var adtype = elem.getAttribute("data-adtype");
    var adObj = {
      name: adtype && window.wapads[adtype] ? window.wapads[adtype].name : elem.getAttribute("data-path"),
      size: adtype && window.wapads[adtype] ? window.wapads[adtype].size : JSON.parse(elem.getAttribute("data-size")),
      id: elem.getAttribute("id"),
    };
    var gtag = googletag.defineSlot(adObj.name, adObj.size, adObj.id).addService(googletag.pubads());

    elem.setAttribute("data-path", adObj.name);
    // elem.setAttribute("data-size", adObj.size);

    if (window.adSlotsDFP) {
      window.adSlotsDFP[elem.getAttribute("id")] = gtag;
    } else {
      window.adSlotsDFP = {};
      window.adSlotsDFP[elem.getAttribute("id")] = gtag;
    }

    // googletag.pubads().enableSingleRequest();
    if (getCookie("optout") === "1") {
      googletag.pubads().setRequestNonPersonalizedAds(1);
    }
    googletag.pubads().addEventListener("slotRenderEnded", event => {
      if (event && event.slot) {
        const slotName = event.slot.getAdUnitPath().toLowerCase();
        if (
          slotName.indexOf("_atf") > -1 &&
          typeof event.slot.getHtml() === "string" &&
          event.slot.getHtml().indexOf("Expandable") > 0 &&
          document.querySelector(".ad1.atf")
        ) {
          document.querySelector(".ad1.atf").classList.add("expando");
        } else if (slotName.indexOf("_atf") > -1 && document.querySelector(".ad1.atf")) {
          document.querySelector(".ad1.atf").classList.remove("expando");
        }
      }
    });

    _SCN !== "" ? googletag.pubads().setTargeting("SCN", _SCN) : "";
    _SubSCN !== "" ? googletag.pubads().setTargeting("SubSCN", _SubSCN) : "";
    _LastSubSCN !== "" ? googletag.pubads().setTargeting("LastSubSCN", _LastSubSCN) : "";
    _hyp1 !== "" ? googletag.pubads().setTargeting("Hyp1", _hyp1) : "";
    window.tgtkeys.msid ? googletag.pubads().setTargeting("storyId", window.tgtkeys.msid) : "";

    googletag
      .pubads()
      .setTargeting("ARC1", "strong")
      .setTargeting("_ref", _ref)
      .setTargeting("oem", _oem || "")
      .setTargeting("BL", blacklist || "0")
      .setTargeting("puv", puvkey || "0")
      .setTargeting("templatetype", pagetype || "")
      .setTargeting("countrycode", window.geoinfo ? window.geoinfo.CountryCode : "IN")
      .setTargeting("keyword", keyword || "")
      .setTargeting("sg", _auds);
    // .setTargeting("ctnkeyword", keyword || "");

    googletag.enableServices();
  });

  const tmpScript = document.createElement("script");
  tmpScript.type = "text/javascript";
  tmpScript.innerHTML = `var _elem_${i} = preRenderElemArray[${i}]; googletag.cmd.push(function() { googletag.display(_elem_${i}.getAttribute('id')); });`;
  elem.append(tmpScript);
};

/**
 * SectionWise ads implementation with wapadsGenerator function by returning obj for window.wapads based on params
 * @param {string} secname (it will carry the defined secname provided from denmark via feed)
 * @param {string} pagetype optional field mainly used for some specific pages like home, elections, tech
 * @param {object} defaultWapAds it carries the others sectional ads
 * @returns {object} wapads object to be set in window.wapads
 */
export const wapadsGenerator = ({ secname = "others", defaultWapAds = {}, pagetype = null }) => {
  let wapads = defaultWapAds; // siteConfig.ads.dfpads["others"];
  try {
    if (pagetype) {
      if (pagetype === "home") {
        secname = "homepage"; // window._SCN = "home";
      } else if (pagetype === "elections") {
        secname = "microsite"; // window._SCN = "elections";
      } else if (pagetype === "newsbrief") {
        secname = "news";
        window._SCN = "newsbrief";
      } else if (pagetype === "tech") {
        secname = "tech"; // window._SCN = "tech";
      } else if (pagetype === "liveblog") {
        secname = "news";
        window._SCN = "liveblog";
      }
    }

    if (typeof window !== "undefined") {
      if (window.location.pathname.indexOf("show/") > -1) {
        wapads.mrec1.size.push([300, 600]);
      }
      // special handling for gadgetnow
      if (window.location.href.indexOf(".gadgetsnow.com") > -1) {
        secname = "tech";
      }
    }

    wapads = JSON.stringify(wapads);
    wapads = wapads.toLowerCase().replace(/_others/g, `_${secname.toLowerCase()}`);
    wapads = JSON.parse(wapads);
  } catch (err) {
    console.error(err.msg);
  }
  return wapads;
};

import React from "react";
// import { Link } from "react-router";
import "./../css/desktop/FakeCard.scss";
const FakeAds300Card = props => {
  let { showImages } = props;
  return (
    <div className="table fake-ad300">
      <div className="table_row">
        <span className="table_col img_wrap">
          {showImages == "no" ? null : <div className="dummy-img"></div>} {props.children}
        </span>
      </div>
    </div>
  );
};

export default FakeAds300Card;

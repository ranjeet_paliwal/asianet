/* eslint-disable import/no-useless-path-segments */
import React from "react";
import FakeNewsListCard from "./FakeNewsListCard";
import FakeVideoListCard from "./FakeVideoListCard";
import FakeVideoShowCard from "./FakeVideoShowCard";
import FakeDesktopDefault from "./FakeDesktopDefault";
import "./../css/desktop/FakeCard.scss";

const FakeListing = props => {
  const { showImages, pagetype } = props;
  if (pagetype == "videolist") {
    return (
      <React.Fragment>
        <FakeVideoListCard key={11} showImages={showImages} />
        <FakeVideoListCard key={12} showImages={showImages} />
        <FakeVideoListCard key={13} showImages={showImages} />
        <FakeVideoListCard key={14} showImages={showImages} />
        <FakeVideoListCard key={15} showImages={showImages} />
      </React.Fragment>
    );
  }
  if (pagetype == "videoshow") {
    return (
      <React.Fragment>
        <FakeVideoShowCard key={21} showImages={showImages} />
        <FakeDesktopDefault key={22} showImages={showImages} />
      </React.Fragment>
    );
  }
  return (
    <React.Fragment>
      <FakeNewsListCard key={1} showImages={showImages} />
      <FakeNewsListCard key={2} showImages={showImages} />
      <FakeNewsListCard key={3} showImages={showImages} />
      <FakeNewsListCard key={4} showImages={showImages} />
      <FakeNewsListCard key={5} showImages={showImages} />
    </React.Fragment>
  );
};

export default FakeListing;

import React from "react";
import { _isCSR, isMobilePlatform } from "../../../utils/util";
import FakeLeadListCard from "./FakeLeadListCard";

const FakeCompareShowCard = props => {
  // let { showImages } = props;
  return _isCSR() ? (
    <div className="fake-compare-show-card">
      <div className="row">
        <div className="col12">
          <div className="fakelisting fakeadcard"  style={{height: "50px"}}></div>
        </div>
      </div>
      <div className="row">
        <div className="col12">
          <br />
        </div>
      </div>
      <div className="row">
        <div className={isMobilePlatform() ? "col6" : "col3"}>
          <FakeLeadListCard />
        </div>
        <div  className={isMobilePlatform() ? "col6" : "col3"}>
          <FakeLeadListCard />
        </div>
        {!isMobilePlatform() && (
          <React.Fragment>
            <div className="col3">
              <FakeLeadListCard />
            </div>
            <div className="col3">
              <FakeLeadListCard />
            </div> 
          </React.Fragment>   
        )}
      </div>
      <div className="row">
        <div className="col12">
          <br />
        </div>
      </div>
      <div className="row">
        <div className="col12">
          <div className="fakelisting fakearticle"></div>
        </div>
      </div>
    </div>
  ) : null;
};

export default FakeCompareShowCard;

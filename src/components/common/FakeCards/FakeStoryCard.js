import React from "react";
import { _isCSR } from "../../../utils/util";

const FakeStoryCard = props => {
  let { showImages } = props;
  return _isCSR() ? (
    <div className="article-section articleshow">
      <main className="box">
        <h1 className="heading1 blur-text">
          <div className="blur-div"></div>
          <div className="blur-div img"></div>
        </h1>
        <div className="news-source">
          <div className="blur-div"></div>
          <div className="blur-div"></div>
          <div className="blur-div small"></div>
        </div>

        <article>
          <div>
            <div className="blur-div big"></div>
            <div className="blur-div big"></div>
            <div className="blur-div big"></div>
            <div className="blur-div big"></div>
            <div className="blur-div big" style={{ width: "70%" }}></div>
            <br></br>
            <div className="blur-div big"></div>
            <div className="blur-div big"></div>
            <div className="blur-div big"></div>
            <div className="blur-div big"></div>
            <div className="blur-div big" style={{ width: "70%" }}></div>
          </div>
        </article>
      </main>
    </div>
  ) : null;
};

export default FakeStoryCard;

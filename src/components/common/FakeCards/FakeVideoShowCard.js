import React, { Component } from "react";
import FakeLeadListCard from "./FakeLeadListCard";
import FakeTrendingListCard from "./FakeTrendingListCard";
import FakeAds300Card from "./FakeAds300Card";
import FakeListDesktop from "./FakeListDesktop"
class FakeVideoShowCard extends Component {
  render() {
    let { showImages } = this.props;
    return (
      <div className="videoshow-container">
        <div className="top_wdt_video">
          <div className="row">
            <div className="col8">
              <FakeLeadListCard />
            </div>
            <div className="col4">
              <FakeAds300Card>
                <FakeTrendingListCard />
              </FakeAds300Card>
            </div>
          </div>
        </div>
        <FakeListDesktop listtype="vertical" />
      </div>
    );
  }
}

export default FakeVideoShowCard;

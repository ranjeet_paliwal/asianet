import React, { Component } from "react";
// import { Link } from "react-router";
import styles from "./../css/NewsListCard.scss";
class FakeNewsListCard extends Component {
  render() {
    let { showImages } = this.props;
    return (
      <div data-plugin="list" className="table fake-listview">
        <div className="table_row">
          <span className="table_col img_wrap">
            {showImages == "no" ? null : <div className="dummy-img"></div>}
          </span>
          <span className="table_col con_wrap">
            <span className="blur-div"></span>
            <span className="blur-div" style={{ width: "70%" }}></span>
            <span className="blur-div small" style={{ width: "40%" }}></span>
          </span>
        </div>
      </div>
    );
  }
}

export default FakeNewsListCard;

import React from "react";
import "./../css/desktop/FakeCard.scss";
const FakeListDesktop = props => {
    const { listtype, pagetype } = props;
    return (
      <div className="row">
            <div className="col4">
                {listtype == "vertical" ? (
                    <div className="fakelisting fakevertical" style={{height: "660px"}}></div>
                ) : (
                    <div className="fakelisting fakemidimg" style={{height: "420px"}}></div>
                )}
            </div>
            <div className="col4">
                {listtype == "vertical" ? (
                    <div className="fakelisting fakevertical" style={{height: "660px"}}></div>
                ) : (
                    <div className="fakelisting fakemidimg" style={{height: "420px"}}></div>
                )}
            </div>
            <div className="col4">
                {listtype == "vertical" ? (
                    <div className="fakelisting fakevertical" style={{height: "660px"}}></div>
                ) : (
                    <div className="fakelisting fakemidimg" style={{height: "420px"}}></div>
                )}
            </div>
      </div>
    );
}

export default FakeListDesktop;

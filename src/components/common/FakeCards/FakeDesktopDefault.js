/* eslint-disable import/no-useless-path-segments */
/* eslint-disable react/prefer-stateless-function */
import React from "react";
// import { Link } from "react-router";
import FakeVideoListCard from "./FakeVideoListCard";
import FakeListDesktop from "./FakeListDesktop";
import { _isCSR } from "../../../utils/util";
import "./../css/desktop/FakeCard.scss";

const FakeDesktopDefault = props => {
  const { showImages, pagetype } = props;
  const locationpath = _isCSR && typeof window !== "undefined" ? window.location.pathname : "";
  if (locationpath.indexOf("videolist") > -1) {
    return (
      <React.Fragment>
        <FakeVideoListCard key={11} showImages={showImages} />
        <FakeListDesktop listtype="vertical" />
      </React.Fragment>
    );
  }
  return (
    <React.Fragment>
      {pagetype != "home" ? (
        <div>
          <span className="blur-div" style={{ width: "40%", height: "19px", marginBottom: "20px" }}></span>
          <span className="blur-div" style={{ height: "32px", marginBottom: "10px" }}></span>
        </div>
      ) : null}
      <div className="fake-top-headlines" data-exclude="amp">
        <div className="row">
          <div className="col8">
            <div style={{ width: "100%" }}>
              <div className="row">
                <div className="col6">
                  {/* lead post */}
                  <div className="fakelisting fakevertical"></div>
                </div>
                <div className="col6">
                  {pagetype == "home" ? (
                    <div className="fakelisting" style={{ height: "264px" }}></div>
                  ) : (
                    <div className="fakelisting fakevertical"></div>
                  )}
                </div>
              </div>
              <div className="row" style={{ marginTop: "20px" }}>
                {pagetype == "home" ? (
                  <React.Fragment>
                    <div className="col4">
                      <div className="fakelisting fakenoimage" style={{ height: "264px" }}></div>
                    </div>
                    <div className="col4">
                      <div className="fakelisting fakenoimage" style={{ height: "264px" }}></div>
                    </div>
                    <div className="col4">
                      <div className="fakelisting fakenoimage" style={{ height: "264px" }}></div>
                    </div>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <div className="col6">
                      <div className="fakelisting fakemidimg" style={{ height: "264px" }}></div>
                    </div>
                    <div className="col6">
                      <div className="fakelisting fakemidimg" style={{ height: "264px" }}></div>
                    </div>
                  </React.Fragment>
                )}
              </div>
            </div>
          </div>
          <div className="col4">
            {/* ads 300*250 */}
            <div className="fakelisting fakeadcard"></div>
            <div className="fakelisting fakeadcard" style={{ height: "100px", marginBottom: "20px" }}></div>
            <div className="fakelisting" style={{ height: "264px" }}></div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default FakeDesktopDefault;

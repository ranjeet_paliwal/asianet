import React from "react";

const FakePhotoMazzaCard = props => {
  const { showImages, view } = props;
  const listItems = 3;
  return !view || view != "horizontal" ? (
    <div className="fake-photoview">
      {[...Array(listItems)].map(() => (
        <FakeListView showImages={showImages} view={view} />
      ))}
    </div>
  ) : (
    <div className="fake-photoview">
      <div className="scroll_content">
        {[...Array(listItems)].map(() => (
          <FakeListView showImages={showImages} view={view} />
        ))}
      </div>
    </div>
  );
};

const FakeListView = props => {
  const { showImages, view } = props;
  const isVerticalView = !view || view != "horizontal";
  return (
    <div data-plugin="list" className="table fake-listview" style={{ width: isVerticalView ? "100%" : "" }}>
      <div className="table_row">
        <span className="table_col img_wrap">
          {showImages == "no" ? null : (
            <div className="dummy-img" style={{ height: isVerticalView ? "270px" : "" }}></div>
          )}
        </span>
        <span className="table_col con_wrap">
          <span className="blur-div"></span>
          <span className="blur-div"></span>
          <span className="blur-div"></span>
        </span>
      </div>
    </div>
  );
};

export default FakePhotoMazzaCard;

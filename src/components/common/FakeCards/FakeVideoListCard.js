import React, { Component } from "react";
import FakeLeadListCard from "./FakeLeadListCard";
import FakeTrendingListCard from "./FakeTrendingListCard";
import FakeAds300Card from "./FakeAds300Card";
class FakeVideoListCard extends Component {
  render() {
    let { showImages } = this.props;
    return (
        <div className="top_wdt_slider">
          <div className="row">
            <div className="col4">
              <FakeLeadListCard />
            </div>
            <div className="col4">
              <FakeLeadListCard />
            </div>
            <div className="col4">
              <FakeAds300Card>
                <FakeTrendingListCard />
              </FakeAds300Card>
            </div>
          </div>
        </div>
    );
  }
}

export default FakeVideoListCard;

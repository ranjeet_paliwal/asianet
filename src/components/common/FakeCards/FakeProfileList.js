import React from "react";
import "./../css/desktop/FakeCard.scss";

const FakeProfileList = props => {
  const { elemSize, cardheight } = props;
  let elemSizeArr = new Array(elemSize ? parseInt(elemSize) : 3).fill(1);
  return (
    <div style={{display: "flex", flexWrap: "nowrap"}} data-exclude="amp">
      {elemSizeArr.map((item, index) => {
        return (
          <div className={`fakelisting profilelist col`} style={cardheight ? {height: cardheight+"px"} : {}}></div>
        );
      })}
    </div>
  )
};

export default FakeProfileList;

import React from "react";
import { _isCSR, isMobilePlatform } from "../../../utils/util";

const FakeStoryCardDesktop = props => {
  // let { showImages } = props;
  return _isCSR() ? (
    <div className="row">
      <div className="col8">
        <div className="fakelisting fakestory"></div>
        <div className="fakelisting fakearticle"></div>
      </div>
      <div className="col4">
        <div className="fakelisting fakeadcard"></div>
        <div className="fakelisting" style={{height: "390px"}}></div>
      </div>
    </div>
  ) : null;
};

export default FakeStoryCardDesktop;

/* eslint-disable radix */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from "react";
import styles from "./../css/NewsListCard.scss";
class FakeHorizontalListCard extends Component {
  render() {
    let { showImages, elemSize } = this.props;
    let elemSizeArr = new Array(elemSize ? parseInt(elemSize) : 3).fill(1);
    return (
      <div className="fake-horizontalView">
        <div className="scroll_content">
          <ul>
            {elemSizeArr.map((item, index) => {
              return (
                <li data-plugin="list" className="table fake-listview" key={index.toString()}>
                  <div className="table_row">
                    <span className="table_col img_wrap">
                      {showImages == "no" ? null : <div className="dummy-img"></div>}
                    </span>
                    <span className="table_col con_wrap">
                      <span className="blur-div"></span>
                      <span className="blur-div" style={{ width: "70%" }}></span>
                    </span>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
}

export default FakeHorizontalListCard;

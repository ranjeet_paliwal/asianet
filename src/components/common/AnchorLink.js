import React from "react";
import { Link } from "react-router";

const AnchorLink = props => {
  const { href } = props;
  return <Link to={href}>{props.children}</Link>;
};

export default React.memo(AnchorLink);

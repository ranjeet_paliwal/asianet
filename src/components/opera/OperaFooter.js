import React, { Component } from "react";
import { Link } from "react-router";
import PropTypes from "prop-types";
import { scrollTo, throttle, gaEvent } from "./../../utils/util";
class OperaFooter extends Component {
  render() {
    return (
      <div id="c_wdt_footer_1">
        <div className="ad1 FBN"></div>
        <footer data-node="footer" className="clearfix" id="footer">
          <h5>
            <a
              data-gtm-event="backtotop_click"
              id="backtotop"
              className="backtotop"
              href="#c_wdt_OperaHeader_2"
            >
              <span className="btpText">BACK TO TOP</span>
              <span className="mobile-sprite circle"></span>
            </a>
            <ul className="footer-links clearfix">
              <li className=" ">
                <Link
                  to="/aboutus.cms?lang=english"
                  className="track"
                  data-track="npfooter_About Us"
                  data-gtm-event="npfooter_About Us"
                >
                  About Us
                </Link>
                <span className="seperator">&nbsp;|&nbsp;</span>
              </li>

              <li className=" ">
                <Link
                  to="/termsofpolicy.cms?lang=english"
                  className="track"
                  data-track="npfooter_Terms of Use"
                  data-gtm-event="npfooter_Terms of Use"
                >
                  Terms of Use
                </Link>
                <span className="seperator">&nbsp;|&nbsp;</span>
              </li>

              <li className=" ">
                <a
                  className="track"
                  href="mailto:npappfeedback@timesinternet.in?subject=Feedback on NewsPoint - Across Publications"
                  data-track="npfooter_Feedback"
                  data-gtm-event="npfooter_Feedback"
                  src="http://recuperator5.indiatimes.com/recuperator/imgserver/serve?dimension=240&amp;source=Feedback"
                >
                  Feedback
                </a>
                <span className="seperator">&nbsp;|&nbsp;</span>
              </li>

              <li className=" ">
                <Link
                  className="track"
                  to="/privacypolicy.cms?lang=english"
                  data-track="npfooter_Privacy Policy"
                  data-gtm-event="npfooter_Privacy Policy"
                >
                  Privacy Policy
                </Link>
              </li>
            </ul>
            <span className="copyright">
              Copyright © 2016-2017 Bennett, Coleman &amp; Co. Ltd. All rights
              reserved.
            </span>
            <span className="reprint">
              For reprint rights:{" "}
              <a
                data-gtm-event="tss_click"
                className="track"
                href="https://timescontent.com/"
              >
                Times Syndication Service
              </a>
            </span>
          </h5>
        </footer>
        <div className="overlay-mask"></div>
        <div className="push-overlay">
          <div className="header-text">
            Push Details
            <a className="close-icon"></a>
          </div>
          <div className="inner-wrapper">
            <div className="text-heading">Title of the push</div>
            <div className="first-row clearfix">
              <input type="text" id="storyTitle" />
              <div
                className="copy"
                id="copyTitle"
                data-clipboard-target="#storyTitle"
              >
                Copy
              </div>
            </div>
            <div className="text-heading">Deeplink of the push</div>
            <div className="second-row clearfix">
              <input type="text" id="deeplinkArticle" />
              <div
                className="copy"
                id="copyDeeplink"
                data-clipboard-target="#deeplinkArticle"
              >
                Copy
              </div>
            </div>
            <div className="text-heading">Share url</div>
            <div className="third-row clearfix">
              <input type="text" id="shareUrlArticle" />
              <div
                className="copy"
                id="copySU"
                data-clipboard-target="#shareUrlArticle"
              >
                Copy
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

OperaFooter.propTypes = {};

export default OperaFooter;

import React, { Component } from "react";
import { Link, browserHistory } from "react-router";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { throttle, elementInViewport, makeUrl } from "./../../utils/util";
import Cookies from "react-cookie";
import { removeClass, getLanguageUrlSuffix, gaEvent, isUcBrowser } from "./../../utils/util";
import InlineLoader from "./../lib/inlineloader/InlineLoader";
import { fetchPublications, fetchTopPublicationsData } from "./../../actions/publications/publications";
import { changeShowImages, updateMenu } from "./../../actions/config/config";
import config from "./../../config";

class OperaHeader extends Component {
  constructor(props) {
    super(props);
    let userFont = Cookies.load("userFont");
    if (typeof userFont == "undefined") {
      userFont = "62.5";
    }
    this.state = { userFont: userFont, showPubImg: false, showLangImg: false };
    this.fetchSubSections = this.handleFetchSubSections.bind(this);
  }

  componentDidMount() {
    //console.log("Inside OperaHeader componentDidMount");
    const { dispatch, params, query } = this.props;
    OperaHeader.fetchData({ dispatch, query, params });
    //OperaHeader.fetchTopPublications({ dispatch, query, params});
    let _this = this;
    if (typeof window != "undefined") {
      window.addEventListener("resize", () => {
        //console.log("INSIDE WINDOW RESIZE", document.getElementById("siteSharePopup").style.display);
        if (document.getElementById("siteSharePopup").style.display == "block") {
          _this.shareSitePopup();
        }
      });
      if (isUcBrowser()) {
        document.querySelector("body").classList.add("ucbrowser");
      }

      let navitems = document.querySelectorAll(".np-nav-item");
      let width = 0;
      if (navitems.length < 10) {
        width = navitems.length * 75;
      } else {
        for (let nav of navitems) {
          width += nav.clientWidth;
        }
        let shouldWidth = 940;
        if (document.querySelector("html").classList.contains("large-font")) {
          shouldWidth = 1068;
        }
        if (document.querySelector("html").classList.contains("small-font")) {
          shouldWidth = 792;
        }

        if (width > shouldWidth) {
          width = shouldWidth;
        }
      }
      // if(typeof(window)!='undefined'){
      //   alert("Hello:"+width+" : "+navitems.length);
      // }

      document.querySelector(".np-nav-list").style.minWidth = width + "px";
    }
  }

  // swipeRight(){
  //   if(this.props.showSwiper==true || document.getElementById("swiperContainerDiv")!=null)
  //     return;
  //   let currentSelectedNode = document.querySelector(".np-nav-link.selected");
  //   if(typeof(currentSelectedNode)=='undefined'){
  //     return;
  //   }
  //   let currentNode = currentSelectedNode.parentNode;
  //   let prevSibling = currentNode.previousSibling;
  //   if(typeof(prevSibling)!='undefined'){
  //     let childNodes = prevSibling.childNodes;
  //     childNodes[0].click();
  //   }
  // }

  // swipeLeft(){
  //   if(this.props.showSwiper || document.getElementById("swiperContainerDiv")!=null)
  //     return false;
  //   let currentSelectedNode = document.querySelector(".np-nav-link.selected");
  //   if(typeof(currentSelectedNode)=='undefined'){
  //     return;
  //   }
  //   let currentNode = currentSelectedNode.parentNode;
  //   let nextSibling = currentNode.nextSibling;
  //   if(typeof(nextSibling)!='undefined'){
  //     let childNodes = nextSibling.childNodes;
  //     childNodes[0].click();
  //   }
  // }

  changeShowImage(value) {
    Cookies.save("userShowImages", value, {
      path: "/",
      maxAge: 3600 * 24 * 30,
    });
  }

  changeShowImageYes() {
    document.getElementById("changeShowImagesYesDiv").classList.add("checked");
    document.getElementById("changeShowImagesNoDiv").classList.remove("checked");
    let { dispatch } = this.props;
    dispatch(changeShowImages("yes"));
    this.changeShowImage("yes");
    this.closeNavigation();
    gaEvent("Settings", "Tap", "Show Images");
  }

  changeShowImageNo() {
    document.getElementById("changeShowImagesYesDiv").classList.remove("checked");
    document.getElementById("changeShowImagesNoDiv").classList.add("checked");
    this.changeShowImage("no");
    let { dispatch } = this.props;
    dispatch(changeShowImages("no"));
    this.closeNavigation();
    gaEvent("Settings", "Tap", "Hide Images");
  }

  changeFontSize(event) {
    let elems = document.querySelectorAll(".font-size-change");
    removeClass(elems, "checked");

    let htmlElem = document.querySelector("html");
    let fontSize = event.currentTarget.getAttribute("data-size");
    htmlElem.className = "";
    switch (fontSize) {
      case "50":
        htmlElem.classList.add("small-font");
        break;
      case "62.5":
        htmlElem.classList.add("normal-font");
        break;
      case "75":
        htmlElem.classList.add("large-font");
        break;
      default:
        htmlElem.classList.add("normal-font");
    }
    htmlElem.style.fontSize = fontSize + "%";
    event.currentTarget.classList.add("checked");
    Cookies.save("userFont", fontSize, { path: "/", maxAge: 3600 * 24 * 30 });
    document.querySelector(".settings-wrapper").classList.remove("show");
    document.querySelector("body").classList.remove("noscroll");
    gaEvent("Settings", "Tap", "Font Change");
  }

  getInlineLoading() {
    return <InlineLoader />;
  }

  getFavoritePublication(event) {
    this.state.showPubImg = true;
    let _this = this;
    let currentLang = event.currentTarget.getAttribute("data-lang");
    const { dispatch, params, query } = this.props;
    OperaHeader.fetchPublicationsData(dispatch, currentLang);
    //document.getElementById("favoritePublication").innerHTML = this.getInlineLoading();
    //React.render('<InlineLoader/>',document.getElementById("favoritePublicationContainer"));
    document.getElementById("favoritePublicationContainer").classList.remove("hide");
    gaEvent("Pubsbylang", currentLang);
  }

  getPubName() {
    if (this.props.hasOwnProperty("languages")) {
      if (this.props.languages.length) {
        let languages = this.props.languages;
        for (let i in languages) {
          if (languages[i].hasOwnProperty("appList")) {
            let applist = languages[i].appList.filter(el => el.channelkey == this.props.location.query.nav);
            if (
              typeof applist != "undefined" &&
              typeof applist[0] != "undefined" &&
              applist[0].hasOwnProperty("appname")
            ) {
              return applist[0].appname;
            }
          }
        }
      }
    }
  }
  rightNavigation() {
    document.querySelector(".settings-wrapper").classList.toggle("show");
    if (Array.from(document.querySelector(".more-cat-wrapper").classList).includes("movefromright")) {
      document.querySelector(".more-cat-wrapper").classList.remove("movefromright");
      document.querySelector("body").classList.remove("noscroll");
    }
    document.querySelector("body").classList.toggle("noscroll");
    if (isUcBrowser()) {
      if (document.querySelector(".settings-wrapper").classList.contains("show")) {
        document.querySelector(".overflowHidden").style.height = "680px";
      } else {
        document.querySelector(".overflowHidden").style.height = "";
      }
    }
  }

  closeNavigation(event) {
    document.getElementById("menuLayer").style.display = "none";
    document.querySelector(".np-menu-container").classList.remove("show");
    document.querySelector("body").classList.remove("noscroll");
    document.querySelector(".change-lang-wrapper").classList.add("hide");
    document.getElementById("favoritePublicationContainer").classList.add("hide");
    document.querySelector(".more-cat-wrapper").classList.remove("movefromright");
    document.querySelector("#viewall-cat-wrapper").classList.add("hide");
    document.querySelector(".settings-wrapper").classList.remove("show");
    try {
      let eventName = event.currentTarget.getAttribute("data-gtm-event");
      if (typeof eventName != "undefined" && eventName != "") {
        gaEvent(eventName, "Tap");
      }
    } catch (ex) {}
    if (isUcBrowser()) {
      document.querySelector(".overflowHidden").style.height = "";
    }
  }

  handleFetchSubSections(event) {
    let that = this;
    let index = event.target.getAttribute("data-index");
    let currentCategory = this.props.navdata.morecategory[index];
    let nav = currentCategory.nav;
    let cat = currentCategory.cat;
    let sd = currentCategory.sd;
    let msid = currentCategory.msid;
    let tag = currentCategory.tag;
    let uid = currentCategory.uid;
    let sectionurl = currentCategory.sectionurl;
    let url = "";
    if (sectionurl) {
      let subnavindex = sectionurl.indexOf("subnav=");
      let catindex = sectionurl.indexOf("category=");
      let subnav = subnavindex != -1 ? sectionurl.substring(subnavindex + 7).split("&")[0] : "";
      let category = catindex != -1 ? sectionurl.substring(catindex + 9).split("&")[0] : "";
      if (sectionurl.indexOf("nav") != -1 && subnav) {
        url = `${config.API_URL}/api_pubcatnav.cms?feedtype=sjson&nav=${nav}&channel=${nav}&subnav=${subnav}`;
      } else if (catindex != -1) {
        url = `${config.API_URL}/api_pubcatnav.cms?feedtype=sjson&nav=${nav}&channel=${nav}&category=${category}`;
      }
      fetch(url)
        .then(response => response.json())
        .then(function(data) {
          //console.log('handleFetchSubSections',data);
          that.props.navdata.morecategory = data.morecategory;
          that.forceUpdate();
        })
        .catch(err => {
          //console.log("INSIDE handleFetchSubSections catch==============",err);
        });
    }
  }

  leftNavigation() {
    gaEvent("Action Bar", "HamburgerMenu");
    document.querySelector(".np-menu-container").classList.toggle("show");
    if (document.getElementById("menuLayer").style.display == "block") {
      document.getElementById("menuLayer").style.display = "none";
    } else {
      document.getElementById("menuLayer").style.display = "block";
    }
    if (Array.from(document.querySelector(".more-cat-wrapper").classList).includes("movefromright")) {
      document.querySelector(".more-cat-wrapper").classList.remove("movefromright");
      document.querySelector("body").classList.remove("noscroll");
    }
    document.querySelector("body").classList.toggle("noscroll");

    //show logo image
    let publicationLogos = document.querySelectorAll(".publicationLogo");
    for (let elem of publicationLogos) {
      elem.setAttribute("src", elem.getAttribute("data-src"));
    }

    if (isUcBrowser()) {
      if (document.querySelector("body").classList.contains("noscroll")) {
        document.querySelector(".overflowHidden").style.height =
          document.querySelector(".np-menu-inner").scrollHeight + 50 + "px";
      } else {
        document.querySelector(".overflowHidden").style.height = "";
      }
    }
  }

  viewAllCategoryWrapper() {
    gaEvent("Browsebycategory", "ViewAll");
    document.querySelector("#viewall-cat-wrapper").classList.remove("hide");
    if (isUcBrowser()) {
      document.querySelector(".overflowHidden").style.height =
        document.querySelector(".viewmore-wrapper").scrollHeight + 50 + "px";
    }
  }

  changeLanguagePopup() {
    let userLanguages = Cookies.load("userLanguages");

    if (userLanguages && userLanguages.length > 0) userLanguages = userLanguages.split(",");
    else userLanguages = [];

    let elements = document.querySelectorAll("li.languageSelectionBox");
    removeClass(elements, "selected");

    let langList = [];
    let elementSelected = false;
    for (let elem of elements) {
      let currentLang = elem.getAttribute("data-lang").trim();
      if (userLanguages.indexOf(currentLang) >= 0) {
        elem.classList.add("selected");
        elementSelected = true;
      }
    }

    elements = document.querySelectorAll("li.languageSelectionBox img");
    for (let elem of elements) {
      let src = elem.getAttribute("data-src");
      elem.setAttribute("src", src);
    }

    document.querySelector(".change-lang-wrapper").classList.toggle("hide");
    document.querySelector("body").classList.toggle("noscroll");
    if (document.querySelector(".change-lang-wrapper").classList.contains("hide") == false) {
      document.querySelector("body").classList.add("noscroll");
    }
    if (!elementSelected) {
      document.querySelector(".bottom-row").style.bottom = "-90px";
    }
    if (isUcBrowser()) {
      document.querySelector(".overflowHidden").style.height = "650px";
    }
    gaEvent("Action Bar", "Language", "Listing");
  }

  checkBoxClick(event) {
    event.currentTarget.classList.toggle("selected");
    if (document.querySelector(".languageSelectionBox.selected")) {
      document.querySelector(".bottom-row").style.bottom = "0px";
    } else {
      document.querySelector(".bottom-row").style.bottom = "-90px";
    }

    gaEvent("EditLang", "Article Listing");
  }

  subNavigation() {
    let categoryOperaHeader;
    categoryOperaHeader = document.getElementById("categoryOperaHeader");
    let offsetTop = categoryOperaHeader.offsetTop + categoryOperaHeader.offsetHeight;
    document.querySelector(".more-cat-wrapper").style.top = offsetTop + "px";

    if (categoryOperaHeader.classList.contains("fixed-OperaHeader")) {
      document.querySelector(".more-cat-wrapper").style.height = "100vh";
    } else {
      let height = document.querySelector("#c_wdt_OperaHeader_2").clientHeight + categoryOperaHeader.clientHeight;
      let footerheight = document.querySelector(".ad1.FBN") ? document.querySelector(".ad1.FBN").clientHeight : 0;
      let windowHeight = document.body.clientHeight;

      document.querySelector(".more-cat-wrapper").style.height = windowHeight - height - footerheight + 5 + "px";
    }

    document.querySelector(".more-cat-wrapper").classList.toggle("movefromright");
    document.querySelector("body").classList.toggle("noscroll");
    // if(document.querySelector("body").classList.contains("noscroll")){
    //   this.stopBodyScrolling(true);
    // } else {
    //   this.stopBodyScrolling(false);
    // }
    gaEvent("al-more_click", "tap");
  }

  favoritePublicationClose() {
    document.getElementById("favoritePublicationContainer").classList.add("hide");
  }

  selectLanguages() {
    const { dispatch, query, params } = this.props;
    dispatch(updateMenu("top-news"));
    let elements = document.querySelectorAll("li.languageSelectionBox.selected");
    let langList = [];
    for (let elem of elements) {
      langList.push(elem.getAttribute("data-lang").trim());
    }
    langList = langList.join(",");

    if (langList != "") {
      let date = new Date();
      date.setTime(date.getTime() + 30 * 24 * 60 * 60 * 1000);
      let expires = date.toGMTString();
      //cookie.save('userInfo', JSON.stringify(userInfo), { path: '/', maxAge: new Date(Date.now() + (3600000 * 24 * 365)) });
      Cookies.save("userLanguages", langList, {
        path: "/",
        maxAge: 3600 * 24 * 30,
      });
      browserHistory.push("/?lang=" + langList);
    }
    this.props.updateFavoritePublication();
    this.closeNavigation();

    OperaHeader.fetchTopPublications({ dispatch, query, params });

    gaEvent("LanguageSelection", "Article Listing", langList);
  }

  closeCatNavWrapper() {
    document.getElementById("viewall-cat-wrapper").classList.add("hide");
    if (isUcBrowser()) {
      documet.querySelector(".overflowHidden").style.height =
        document.querySelector(".np-menu-inner").scrollHeight + 50 + "px";
    }
    gaEvent("Browsebycategory", "Close");
  }

  navigationCategoryClicked(event) {
    //slideInLeft
    /*try{
      let currentNode = document.querySelector(".np-nav-link.selected").parentNode;
      let nextNode = event.currentTarget;
      if(currentNode==nextNode)
        return;
      let currentNodeIndex = currentNode.getAttribute('data-index');
      let nextNodeIndex = nextNode.getAttribute('data-index');
      let multiply = 1;
      let diff;
      if(currentNodeIndex>nextNodeIndex){
        document.getElementById("parentContainer").classList.remove("slideInRight");
        document.getElementById("parentContainer").classList.add("slideInLeft");
        multiply = -1;
        diff = -70;
      } else {
        document.getElementById("parentContainer").classList.remove("slideInLeft");
        document.getElementById("parentContainer").classList.add("slideInRight");
        diff = 70;
      }

      //set node to center of navigation
      let containerNode = nextNode.parentNode;
      if(nextNode.getBoundingClientRect().left<150 && diff>0){
        diff=0;
      }
 
      if(nextNode.getBoundingClientRect().right>(containerNode.scrollWidth-150) && diff>0){
        diff=0;
      }

      containerNode.scrollLeft = containerNode.scrollLeft+diff;
    } catch(ex){
      //console.log("ERROR",ex);
      document.getElementById("parentContainer").classList.remove("slideInRight");
      document.getElementById("parentContainer").classList.add("slideInLeft");
    }
    window.setTimeout(()=>{
      document.getElementById("parentContainer").classList.remove("slideInLeft");
      document.getElementById("parentContainer").classList.remove("slideInRight");
    },1000);*/
    let section = event.currentTarget.querySelector(".np-nav-link").getAttribute("data-cat");
    gaEvent("HNav-Across Pub", "Tap", section);
  }

  shareSitePopup() {
    try {
      let windowHeight = document.documentElement.clientHeight;
      let windowWidth = document.documentElement.clientWidth;
      let popup = document.querySelector("#siteSharePopup");
      let popHeight = parseInt(getComputedStyle(popup).height);
      let popWidth = parseInt(getComputedStyle(popup).width);
      let calcWidth = windowWidth / 2 - popWidth / 2;
      let calcHeight = windowHeight / 2 - popHeight / 2;
      if (calcWidth < 0) {
        calcWidth = 0;
      }
      if (calcHeight < 0) {
        calcHeight = 0;
      }
      popup.style.left = calcWidth + "px";
      popup.style.top = calcHeight + document.documentElement.scrollTop + "px";
      popup.style.display = "block";
      document.querySelector("#siteShareLayer").style.display = "block";
    } catch (e) {}
  }

  handleGAEvent(gaEventOptions) {
    gaEvent(gaEventOptions.category, gaEventOptions.action, gaEventOptions.label);
  }

  removeTouchEvent(event) {
    event.preventDefault();
    return false;
  }

  render() {
    let { userFont } = this.state;
    let langSuffix = getLanguageUrlSuffix();
    let {
      data,
      navdata,
      publications,
      isFetching,
      languages,
      isFetchingTopPub,
      topPubs,
      selectedMenu,
      hideSubOperaHeader,
      showPubNav,
      showImages,
      networkStatus,
    } = this.props;
    // console.log("INSIDE OperaHeader RENDER", selectedMenu);
    if (this.props.location.query.feedSection && this.props.location.query.feedSection != "top-news") {
      selectedMenu = this.props.location.query.feedSection;
    } else {
      selectedMenu = "Top Stories";
    }

    // console.log("Header ", this.props.location.query.feedSection);
    return (
      <div className="">
        <div
          className="menuLayer"
          id="menuLayer"
          onClick={this.closeNavigation}
          onTouchMove={this.removeTouchEvent}
          onScroll={this.removeTouchEvent}
        ></div>
        <div id="c_wdt_OperaHeader_2">
          <header>
            <a
              href="/menu"
              data-gtm-event="menu_click"
              className={showPubNav == false ? "np-hamburger navTrigger" : "np-hamburger navTrigger  hide"}
              onClick={this.leftNavigation.bind(this)}
            >
              <i></i>
              <i></i>
              <i></i>
            </a>
            <div
              className={showPubNav == false ? "np-logo" : "np-logo  hide"}
              onClick={() => {
                this.handleGAEvent({ category: "Homeicon", action: "Tap" });
              }}
            >
              <Link to={"/"}>
                <img src="/android-icon-36x36.png" alt={config.SITE_TITLE} />
              </Link>
              <Link to={"/"} className="app-title">
                NewsPoint
              </Link>
            </div>

            <div className={showPubNav ? "np-logo" : "np-logo hide"} onClick={this.closeNavigation}>
              <Link
                to={"/"}
                className="np-home-icon mobile-sprite"
                onClick={() => {
                  this.handleGAEvent({ category: "Homeicon", action: "Tap" });
                }}
              ></Link>
            </div>

            <div className="np-lang-wrap">
              <a
                href="/languages"
                data-gtm-event="changelangbtn_click"
                className="np-lang-icon mobile-sprite"
                onClick={this.changeLanguagePopup.bind(this)}
              ></a>
            </div>
          </header>
        </div>

        {!showPubNav ? (
          <div id="categoryOperaHeader" className={hideSubOperaHeader ? "hide" : ""}>
            <nav className="np-nav">
              <div className="np-nav-inner" id="npNavList">
                <ul className="np-nav-list">
                  {/*
              typeof(data)!='undefined' && data.othercat?
              data.othercat.map((cat, i)=>{
                  if(!cat.text)
                    return null;
                  let compareText = cat.text.toLowerCase();
                  // if(compareText=='top stories'){
                  //   compareText = 'top news';
                  // }
                  if(i>=2 || cat.text=='watch')
                    return null;

                  return (
                    <li className="np-nav-item" key={i} data-index={i} onClick={this.navigationCategoryClicked}>
                      <Link className={typeof(selectedMenu)!='undefined' && selectedMenu!="" && (selectedMenu.toLowerCase()==compareText || makeUrl(selectedMenu)==makeUrl(compareText))?"np-nav-link viral-vid selected":"np-nav-link viral-vid"} to={cat.href} data-cat={cat.text} data-gtm-event="al-category_click"  onClick={this.closeNavigation}>
                        <span className="video-text" style={{marginLeft:0}}>{cat.text}</span>
                      </Link>
                    </li>
                  )
              }):null
              */}
                  <li className="np-nav-item">
                    <Link
                      className="np-nav-link viral-vid selected bold"
                      to={"/menu"}
                      data-gtm-event="al-category_click"
                    >
                      <span className="video-text" style={{ marginLeft: 0 }}>
                        {selectedMenu}
                      </span>
                    </Link>
                  </li>
                  <li className="np-nav-item">
                    <Link className="np-nav-link viral-vid" to={"/menu"} data-gtm-event="al-category_click">
                      <span className="video-text" style={{ marginLeft: 0 }}>
                        More
                      </span>
                    </Link>
                  </li>
                </ul>
                {/*<div data-gtm-event="al-more_click" className="np-nav-toggle" onClick={this.subNavigation.bind(this)}><label className="np-nav-link">More</label></div>*/}
              </div>
            </nav>
          </div>
        ) : (
          <div
            id="categoryOperaHeader"
            className={showPubNav && navdata.maincategory && navdata.morecategory ? "single-pub" : "hide"}
          >
            <nav className="np-nav">
              <div className="np-nav-inner">
                {navdata.maincategory ? (
                  <div className="np-nav-logo ">
                    <Link
                      to={navdata.maincategory.href}
                      className={
                        selectedMenu == "" ||
                        selectedMenu.toLowerCase() == navdata.maincategory.text.toLowerCase() ||
                        selectedMenu == "top news"
                          ? "np-nav-link selected"
                          : "np-nav-link selected"
                      }
                    >
                      {navdata.maincategory.text}
                    </Link>
                  </div>
                ) : null}
                <div data-gtm-event="al-more_click" className="np-nav-toggle" onClick={this.subNavigation.bind(this)}>
                  <label className="np-nav-link np-nav-publisher">More</label>
                </div>
              </div>
            </nav>
            <div className="more-cat-wrapper" style={{ top: "94px" }}>
              <div className="whiteLayer"></div>
              <ul>
                {navdata.morecategory && typeof navdata.morecategory.length != "undefined"
                  ? navdata.morecategory.map((cat, i) => {
                      return (
                        <li key={i}>
                          <Link
                            data-index={i}
                            data-sec={cat.sectionurl}
                            to={cat.href}
                            onClick={cat.sectionurl ? this.fetchSubSections : this.closeNavigation}
                          >
                            {cat.text}
                          </Link>
                        </li>
                      );
                    })
                  : null}
                {navdata.morecategory &&
                typeof navdata.morecategory == "object" &&
                typeof navdata.morecategory.length == "undefined" ? (
                  <li key={navdata.morecategory.cat.text}>
                    <Link
                      data-index="0"
                      data-sec={navdata.morecategory.cat.sectionurl}
                      to={navdata.morecategory.cat.href}
                      onClick={navdata.morecategory.cat.sectionurl ? this.fetchSubSections : this.closeNavigation}
                    >
                      {navdata.morecategory.cat.text}
                    </Link>
                  </li>
                ) : null}
              </ul>
            </div>
          </div>
        )}
        <div
          id="siteShareLayer"
          style={{
            display: "none",
            position: "fixed",
            left: "0px",
            top: "0px",
            width: "100%",
            height: "100%",
            backgroundColor: "rgb(0, 0, 0)",
            opacity: "0.5",
            zIndex: "99999",
          }}
        ></div>
      </div>
    );
  }
}

OperaHeader.fetchData = function({ dispatch, query, params }) {
  return dispatch(fetchPublications("english", query));
};

OperaHeader.fetchPublicationsData = function(dispatch, lang) {
  return dispatch(fetchPublications(lang));
};

OperaHeader.fetchTopPublications = function({ dispatch, query, params }) {
  return dispatch(fetchTopPublicationsData());
};

OperaHeader.propTypes = {
  data: PropTypes.object.isRequired,
  updateFavoritePublication: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    ...state.publications,
    ...state.config,
  };
}

export default connect(mapStateToProps)(OperaHeader);

"use strict";

const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;
const WEBPACK_DEV_SERVER = process.env.WEBPACK_DEV_SERVER;

module.exports = ({ buildEnv }) => {
  return buildEnv && buildEnv == "production" && WEBPACK_DEV_SERVER
    ? [
        new BundleAnalyzerPlugin({
          analyzerMode: "static",
          reportFilename: "report.html"
        })
      ]
    : [];
};

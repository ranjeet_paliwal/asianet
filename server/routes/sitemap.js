"use strict";

const express = require("express");
const router = express.Router();
const request = require("request");
const fs = require("fs");
const path = require("path");
const dateFormat = require("dateformat");

function compareStrings(a, b) {
  // Assuming you want case-insensitive comparison
  a = a.toLowerCase();
  b = b.toLowerCase();

  return a < b ? -1 : a > b ? 1 : 0;
}

function parseUrl(str) {
  return str
    .toLowerCase()
    .split(" ")
    .join("-");
}

router.get("/", (req, res) => {
  let d = new Date();
  let time =
    dateFormat(d, "yyyy-mm-dd") + "T" + dateFormat(d, "hh:MM:ss") + "+05:30";
  let str = `<?xml version="1.0" encoding="UTF-8"?>
      <sitemapindex xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">
          <sitemap>
            <loc><![CDATA[${process.env.API_ENDPOINT}/sitemap/home.xml]]></loc>
            <lastmod><![CDATA[${time}]]></lastmod>
          </sitemap>
          <sitemap>
            <loc><![CDATA[${process.env.API_ENDPOINT}/sitemap/states.xml]]></loc>
            <lastmod><![CDATA[${time}]]></lastmod>
          </sitemap>
          <sitemap>
            <loc><![CDATA[${process.env.API_ENDPOINT}/sitemap/cities.xml]]></loc>
            <lastmod><![CDATA[${time}]]></lastmod>
          </sitemap>
      </sitemapindex>
      `;
  res.setHeader("content-type", "text/xml");
  res.end(str);
});

router.get("/home.xml", (req, res) => {
  let d = new Date();
  let time =
    dateFormat(d, "yyyy-mm-dd") + "T" + dateFormat(d, "hh:MM:ss") + "+05:30";
  let siteurl = process.env.API_ENDPOINT;
  let str = `<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="https://www.w3.org/1999/xhtml">
        <url>
          <loc><![CDATA[${siteurl}]]></loc>
          <lastmod><![CDATA[${time}]]></lastmod>
          <changefreq><![CDATA[daily]]></changefreq>
          <priority><![CDATA[0.5]]></priority>
        </url>
        </urlset>
      `;
  res.setHeader("content-type", "text/xml");
  res.end(str);
});

router.get("/states.xml", (req, res) => {
  let fileData = fs
    .readFileSync(path.join(__dirname, "/../data/state.json"))
    .toString();
  let jsonObj = JSON.parse(fileData);
  jsonObj.sort(function(a, b) {
    return compareStrings(a.sname, b.sname);
  });

  let d = new Date();
  let time =
    dateFormat(d, "yyyy-mm-dd") + "T" + dateFormat(d, "hh:MM:ss") + "+05:30";
  let siteurl = process.env.API_ENDPOINT;
  let str = `<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="https://www.w3.org/1999/xhtml">`;

  jsonObj.map(function(obj) {
    let tempurl = siteurl + "/state/" + parseUrl(obj.slug);
    let tempstr = `<url>
        <loc><![CDATA[${tempurl}]]></loc>
        <lastmod><![CDATA[${time}]]></lastmod>
        <changefreq><![CDATA[daily]]></changefreq>
        <priority><![CDATA[0.5]]></priority>
      </url>`;
    str = str + tempstr;
  });

  str = str + "</urlset>";
  res.setHeader("content-type", "text/xml");
  res.end(str);
});

router.get("/cities.xml", (req, res) => {
  let fileData = fs
    .readFileSync(path.join(__dirname, "/../data/allcities.json"))
    .toString();
  let jsonObj = JSON.parse(fileData);
  jsonObj.sort(function(a, b) {
    return compareStrings(a.city, b.city);
  });

  let d = new Date();
  let time =
    dateFormat(d, "yyyy-mm-dd") + "T" + dateFormat(d, "hh:MM:ss") + "+05:30";
  let siteurl = process.env.API_ENDPOINT;
  let str = `<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="https://www.w3.org/1999/xhtml">`;

  jsonObj.map(function(obj) {
    let tempurl =
      siteurl + "/" + parseUrl(obj.stateSlug) + "/city/" + parseUrl(obj.city);
    let tempstr = `<url>
        <loc><![CDATA[${tempurl}]]></loc>
        <lastmod><![CDATA[${time}]]></lastmod>
        <changefreq><![CDATA[daily]]></changefreq>
        <priority><![CDATA[0.5]]></priority>
      </url>`;
    str = str + tempstr;
  });

  str = str + "</urlset>";
  res.setHeader("content-type", "text/xml");
  res.end(str);
});

module.exports = router;

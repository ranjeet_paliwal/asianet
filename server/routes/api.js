"use strict";

const express = require("express");
const router = express.Router();
const request = require("request");
const fs = require("fs");
const path = require("path");
var url = require("url");
var parseString = require("xml2js").parseString;
// const siteConfig = require('./../../src/common/' + process.env.SITE);
// const SSO = siteConfig.sso;
const SSO = "";

function get(key, cb) {
  cb(true, null);
  return false;
  /*client.get(globalkey+key, function(err, reply){
    if(reply==null){
      cb(true, null);
    } else {
      cb(false, JSON.parse(reply));
    }
  });*/
}

function set(key, value) {
  //value = JSON.stringify(value);
  //client.set(globalkey+key, value,'EX', 60 * 60 * 0.1);
}

function parseUrl(str) {
  return str
    .toLowerCase()
    .split(" ")
    .join("-");
}

/*router.get('/topnews', (req, res) => {
      request.get('https://navbharattimes.indiatimes.com/feeds/pwa_home/'+siteConfig.siteid+'.cms?type=headline&count=20&feedtype=sjson', function(err, response, body) {    
  if (!err && response.statusCode == 200) {
          try{
            var locals = JSON.parse(body);
            res.json(locals);
          } catch(ex){
            res.json([]);
          }
      } else {
        res.status(404).send('Not found getlist');
      }
  });
});*/

router.get("/checkliveblog", (req, res) => {
  request.get(
    "https://langnetstorage.indiatimes.com/configspace/host-nbt,format-json.htm?feedtype=sjson",
    function(err, response, body) {
      if (!err && response.statusCode == 200) {
        try {
          var locals = JSON.parse(body);
          res.json(locals);
        } catch (ex) {
          res.json([]);
        }
      } else {
        res.status(404).send("Not found getlist");
      }
    }
  );
});

/*router.get('/api_bnews', (req, res) => {
  request.get('https://langnetstorage.indiatimes.com/LANGBNews/nbtbnews.htm', function(err, response, body) {
      if (!err && response.statusCode == 200) {
          try{
            
            //var locals = JSON.parse('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><a target="_blank" href="https://navbharattimes.indiatimes.com/breaking-news-in-hindi/liveblog/58166012.cms">जम्मू-कश्मीरः हंद्वारा के बाटपोरा में सुरक्षा बलों और आतंकियों के बीच मुठभेड़ चल रही है। अपडेट्स के लिए जुड़े रहें...</a>');
              //res.json({bnews : '<a target="_blank" href="https://navbharattimes.indiatimes.com/breaking-news-in-hindi/liveblog/58166012.cms">जम्मू-कश्मीरः हंद्वारा के बाटपोरा में सुरक्षा बलों और आतंकियों के बीच मुठभेड़ चल रही है। अपडेट्स के लिए जुड़े रहें...</a> '});
           res.json({bnews: body});
          } catch(ex){
            res.json({});
          }
      } else {
        res.status(404).send('Not found getlist');
      }
  });
});*/

/*
router.get('/api_miniscorecard', (req, res) => {
  request.get('https://nbtstorage.indiatimes.com/regionalscorecard/scorecard/scorecardnw_widget.json', function(err, response, body) {
      if (!err && response.statusCode == 200) {
          try{
            var locals = JSON.parse(body);
            res.json(locals);
          } catch(ex){
            res.json({});
          }
      } else {
        res.status(404).send('Not found MiniTV');
      }
  });
});

*/

/*
router.get('/getliveblogmeta', (req, res) => {
  var url_parts = url.parse(req.url, true);
  var msid = url_parts.query.msid;
  request.get('https://navbharattimes.indiatimes.com/pwafeeds/pwa_lbmeta.cms?version=v9&msid=' + msid + '&feedtype=sjson', function(err, response, body) {
    if (!err && response.statusCode == 200) {
      try{
        var locals = JSON.parse(body);
      } catch(e){
        locals = {};
      }
        res.json(locals);
    } else {
      console.log(err,response);
      res.status(404).send('Not found getlist');
    }
  });
});*/

/*
router.get('/getindialiveblog_hp', (req, res) => {
  request.get('https://langnetstorage.indiatimes.com/Score/pwa_indiaLiveblog_hp.htm', function(err, response, body) {
    if (!err && response.statusCode == 200) {
      try{
        var locals = JSON.parse(body);
      } catch(e){
        locals = {};
      }
        res.json(locals);
    } else {
      console.log(err,response);
      res.status(404).send('Not found indialiveblog');
    }
  });
});
*/

router.get("/getliveblog", (req, res) => {
  var url_parts = url.parse(req.url, true);
  var msid = url_parts.query.msid;
  request.get(
    "https://langnetstorage.indiatimes.com/configspace/clubedinfo-msid-" +
      msid +
      ",format-json,host-nbt.htm",
    function(err, response, body) {
      if (!err && response.statusCode == 200) {
        try {
          var locals = JSON.parse(body);
        } catch (e) {
          locals = {};
        }
        res.json(locals);
      } else {
        console.log(err, response);
        res.status(404).send("Not found getlist");
      }
    }
  );
});

router.get("/getliveblogcount", (req, res) => {
  var url_parts = url.parse(req.url, true);
  var msid = url_parts.query.msid;
  request.get(
    "https://langnetstorage.indiatimes.com/configspace/msid-" +
      msid +
      ",callback-livejsoncnt,host-nbt.htm",
    function(err, response, body) {
      if (!err && response.statusCode == 200) {
        try {
          var startPos = body.indexOf("({");
          var endPos = body.indexOf("})");
          var jsonString = body.substring(startPos + 1, endPos + 1);
          var locals = JSON.parse(jsonString);
        } catch (e) {
          locals = {};
        }
        res.json(locals);
      } else {
        console.log(err, response);
        res.status(404).send("Not found getlist");
      }
    }
  );
});
/*
router.get('/api_article_show.cms', (req, res) => {
  var url_parts = url.parse(req.url, true);
  var queryArr = [];
  var query = url_parts.query;
  for(var prop in query){
    queryArr.push(prop+"="+query[prop]);
  }
  var queryString = queryArr.join("&");

  request.get(`https://navbharattimes.indiatimes.com/pwafeeds/pwa_articleshow.cms?${queryString}`, function(err, response, body) {
      if (!err && response.statusCode == 200) {
          try{
            var data = JSON.parse(body);
            res.json(data);
          } catch(ex){
            res.json({});
          }
      } else {
        res.status(404).send('Not found');
      }
  });
});

router.get('/getlisting', (req, res) => {
  var url_parts = url.parse(req.url, true);
  var queryArr = [];
  var query = url_parts.query;
  for(var prop in query){
    queryArr.push(prop+"="+query[prop]);
  }
  var queryString = queryArr.join("&");

  request.get(`https://navbharattimes.indiatimes.com/feeds/pwa_homelist.cms?${queryString}`, function(err, response, body) {
  // request.get(`http://navbharattimes.indiatimes.com/feeds/appnavigationshowv3next.cms?${queryString}`, function(err, response, body) {
      if (!err && response.statusCode == 200) {
          try{
            var data = JSON.parse(body);
            res.json(data);
          } catch(ex){
            res.json({});
          }
      } else {
        res.status(404).send('Not found');
      }
  });
});*/

router.get("/validateticket", (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  let tempurl =
    "https://socialappsintegrator.indiatimes.com/socialsite/v1validateTicket?ticketId=" +
    query.ticketId +
    "&channel=" +
    SSO.SSO_CHANNEL;
  request.get(tempurl, function(err, response, body) {
    if (!err && response.statusCode == 200) {
      try {
        var locals = JSON.parse(body);
        res.json(locals);
      } catch (ex) {
        res.json([]);
      }
    } else {
      console.log(err, response);
      res.status(404).send("Not validated");
    }
  });
});

router.get("/profileinfo", (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  let tempurl =
    "https://myt.indiatimes.com/mytimes/profile/info/v1/?ssoid=" + query.ssoid;
  request.get(tempurl, function(err, response, body) {
    console.log("Here i am", response);
    if (!err && response.statusCode == 200) {
      try {
        var locals = JSON.parse(body);
        res.json(body);
      } catch (ex) {
        res.json([]);
      }
    } else {
      console.log(err, response);
      res.status(404).send("Not validated");
    }
  });
});

router.get("/api_slike", (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  var eid = query["id"];
  request.get(
    "https://slike.indiatimes.com/feed/stream/x2/pb/" +
      eid +
      "/" +
      eid +
      ".json",
    function(err, response, body) {
      if (!err && response.statusCode == 200) {
        try {
          let videourl = "";
          var locals = JSON.parse(body);
          if (locals.flavors) {
            for (var i in locals.flavors) {
              if (locals.flavors[i].type == "mp4") {
                videourl = locals.flavors[i].url;
                break;
              }
            }
          }
          res.json({ videourl: videourl });
        } catch (ex) {
          res.json([]);
        }
      } else {
        console.log(err, response);
        res.status(404).send("Not found");
      }
    }
  );
});

/*
router.get('/api_articlelist', (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  var msid = query.msid;
  if(isNaN(msid)){
    msid = siteConfig.pages[msid];
  }
  if(typeof(msid)=='undefined'){
    res.status(404).send('Data not available');
    return;
  }
  //let tempurl = 'http://navbharattimes.indiatimes.com/feeds/appnavigationlistv6/'+msid+'.cms?tag=alrtdf&feedtype=sjson&version=v4&isnative=true&curpg='+(typeof(query.curpg)!='undefined'?query.curpg:1);
  let tempurl = 'https://navbharattimes.indiatimes.com/pwafeeds/pwa_articlelist/'+msid+'.cms?feedtype=sjson'+ (query.curpg && query.curpg > 1 ? ('&curpg=' + query.curpg) : '');
  request.get(tempurl, function(err, response, body) {
      if (!err && response.statusCode == 200) {
          try{
            var locals = JSON.parse(body);
            res.json(locals);
          } catch(ex){
            res.json([]);
          }
      } else {
        //console.log(err,response);
        res.status(404).send('Data not available');
      }
  });
});


router.get('/api_videolist', (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  var msid = query.msid;

  if(typeof(msid)=='undefined'){
    //res.status(404)
    //res.end();
    res.status(404).send('Data not available');
    return;
  }

  let tempurl = 'http://navbharattimes.indiatimes.com/pwafeeds/pwa_videolist/'+msid+'.cms?feedtype=sjson&curpg='+(typeof(query.curpg)!='undefined'?query.curpg:1);
  
  request.get(tempurl, function(err, response, body) {
      if (!err && response.statusCode == 200) {
          try{
            var locals = JSON.parse(body);
            res.json(locals);
          } catch(ex){
            res.json([]);
          }
      } else {
        //console.log(err,response);
        res.status(404).send('Data not available');
      }
  });
});


router.get('/api_photomazza', (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  var msid = query.msid;
  if(isNaN(msid)){
    msid = siteConfig.pages[msid];
  }
  if(typeof(msid)=='undefined'){
    res.status(404).send('Data not available');
    return;
  }

  let tempurl = 'https://navbharattimes.indiatimes.com/pwafeeds/pwa_photolist/'+msid+'.cms?feedtype=sjson'+ (query.curpg && query.curpg > 1 ? ('&curpg=' + query.curpg) : '');

  request.get(tempurl, function(err, response, body) {
      if (!err && response.statusCode == 200) {
          try{
            var locals = JSON.parse(body);
            res.json(locals);
          } catch(ex){
            res.json([]);
          }
      } else {
        //console.log(err,response);
        res.status(404).send('Data not available');
      }
  });
});

router.get('/api_photomazzashow', (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  var msid = query.msid;

  let tempurl = 'https://navbharattimes.indiatimes.com/feeds/pwa_photoshow.cms?feedtype=sjson&msid='+msid;

  request.get(tempurl, function(err, response, body) {
      if (!err && response.statusCode == 200) {
          try{
            var locals = JSON.parse(body);
            res.json(locals);
          } catch(ex){
            res.json([]);
          }
      } else {
        //console.log(err,response);
        res.status(404).send('Data not available');
      }
  });
});

router.get('/api_videoshow', (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  var msid = query.msid;
  if(typeof(msid)=='undefined'){
    res.status(404).send('Data not available');
    return;
  }
  let tempurl = 'http://navbharattimes.indiatimes.com/pwafeeds/pwa_videoshow.cms?feedtype=sjson&msid='+msid;

  request.get(tempurl, function(err, response, body) {
      if (!err && response.statusCode == 200) {
          try{
            var locals = JSON.parse(body);
            res.json(locals);
          } catch(ex){
            res.json([]);
          }
      } else {
        //console.log(err,response);
        res.status(404).send('Data not available');
      }
  });
});
*/

router.get("/NPRSS/search/trending-keywords", (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  var queryArr = [];
  for (var prop in query) {
    queryArr.push(prop + "=" + query[prop]);
  }
  var queryString = queryArr.join("&");
  request.get(
    "http://envoy.indiatimes.com/NPRSS/search/trending-keywords?" + queryString,
    function(err, response, body) {
      if (!err && response.statusCode == 200) {
        try {
          var locals = JSON.parse(body);
          res.json(locals);
        } catch (ex) {
          res.json([]);
        }
      } else {
        console.log(err, response);
        res.status(404).send("Not found getlist");
      }
    }
  );
});

module.exports = router;

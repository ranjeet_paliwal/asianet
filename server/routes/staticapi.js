"use strict";

const express = require("express");
const router = express.Router();
const fs = require("fs");
const path = require("path");

router.get("/file", (req, res) => {
  let deferred = new Promise(function(resolve, reject) {
    let filename = req.query.filename;
    let fileData = String("");

    try {
      fileData = fs
        .readFileSync(path.join(__dirname, "../../static/", filename))
        .toString();
    } catch (err) {
      // Here you get the error when the file was not found,
      if (err.code === "ENOENT") {
        console.log("File not found!", filename);
      } else {
        reject(err);
      }
    }

    if (fileData) {
      // res.status(200).json({htmlstr:fileData});
      resolve(res.json({ htmlstr: fileData }));
    } else {
      // res.status(500).send('File Not found');
      reject(res.send("File Not found"));
    }
  });
});

module.exports = router;

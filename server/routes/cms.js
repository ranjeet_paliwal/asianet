"use strict";
const express = require("express");
const router = express.Router();
const request = require("request");
const fs = require("fs");
const path = require("path");
var url = require("url");
// var redis = require("redis");
const configfile = require("./../../common/" + process.env.SITE);
//var client = redis.createClient({host:configfile.redis.host,port:configfile.redis.port,'db':configfile.redis.db});

router.get("/", (req, res) => {
  res.json({ pages: "Page api's working" });
});

router
  .route("/pages")
  // create a new page with url and page name (accessed at POST http://localhost:8080/cms/pages)
  //url: absolute url of page
  //page: page html file name
  .post(function(req, res) {
    var body = req.body;
    var error = false;
    if (typeof body.url == "undefined" || body.url == "") {
      error = "please add url";
    } else if (typeof body.page == "undefined" || body.page == "") {
      error = "please add page html";
    }

    if (error) {
      res.json({ error: error });
      return;
    }

    client.get(body.url, function(err, reply) {
      if (reply == null) {
        client.set(body.url, body.page, function(err, reply) {
          if (err) {
            console.log("page not created!");
          }
          res.json({ message: "Page created!" });
        });
      } else {
        res.json({ message: "Page already exists!" });
        return;
      }
    });
  })

  // get available pages from page directory (accessed at GET http://localhost:8080/cms/pages)
  .get(function(req, res) {
    var pages = [];
    fs.readdirSync(configfile.staticPagesDir).forEach(file => {
      pages.push(file);
    });
    res.json(pages);
  });

//get all urls (accessed at GET http://localhost:8080/cms/pages/urls)
router.route("/pages/urls").get(function(req, res) {
  client.keys("*", function(err, replies) {
    let url = [];
    for (var r of replies) {
      if (r != "undefined" && typeof r != "undefined") {
        url.push(r);
      }
    }
    res.json(url);
  });
});

// get page with url (accessed at GET http://localhost:8080/cms/pages?url=<url>)
router.route("/pages/url").get(function(req, res) {
  var url_parts = url.parse(req.url, true);
  var urlString = url_parts.query.url;
  client.get(urlString, function(err, reply) {
    if (reply == null) {
      res.json({ message: "Page does not exists!" });
    } else {
      res.json({ url: urlString, page: reply });
      return;
    }
  });
});

// get page with url (accessed at POST http://localhost:8080/cms/pages/url)
router.route("/pages/url").post(function(req, res) {
  var body = req.body;
  var urlString = body.url;
  //console.log(client.del);
  client.del([urlString]);
  res.json({ message: "Page deleted!" });
});

module.exports = router;

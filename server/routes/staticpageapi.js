"use strict";

const express = require("express");
const path = require("path");
const fs = require("fs");
const router = express.Router();

router.get("/file", (req, res) => {
  // let deferred = new Promise(function(resolve, reject) {
  let filename = req.query.filename;
  let fileData = String("");
  try {
    fileData = fs
      .readFileSync(path.join(__dirname, "../../staticfiles/", filename))
      .toString();
  } catch (err) {
    // Here you get the error when the file was not found,
    return res.status(500).send("File Not found!");
  }

  if (fileData && fileData !== "") {
    return res.status(200).json(fileData);
    // resolve(res.json({htmlstr:fileData}))
  } else {
    return res.status(500).send("File Not found");
    // reject(res.send('File Not found'))
  }
  // })
});

module.exports = router;

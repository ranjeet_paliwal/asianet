const request = require("request");
const express = require("express");

const router = express.Router();

router.get('/', (req, res) => {
    request.get(`${process.env.API_BASEPOINT}/monitoring.cms?feedtype=sjson`, (err, resp, body) => {
        if (!err && resp.statusCode == 200) {
            res.send(
                `<!DOCTYPE html >
                    <html>
                        <head>
                            <meta content="NOINDEX, NOFOLLOW" name="ROBOTS">
                        </head>
                        <body>
                            ${JSON.parse(resp.body).response}
                        </body>
                    </html>`,
            );
        } else {
            res.status(404).send('Health Check failed from feed end');
        }
    });
});

module.exports = router;
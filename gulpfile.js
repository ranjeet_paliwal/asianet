var exec = require("child_process").exec;
var gulp = require("gulp"),
  $ = require("gulp-load-plugins")({ camelize: true }),
  runSequence = require("run-sequence");

var cleanCSS = require("gulp-clean-css");
var concatCss = require("gulp-concat-css");
var strip = require("gulp-strip-comments");

gulp.task("merge", function() {
  return gulp
    .src("src/public/css/main.css")
    .pipe(concatCss("bundle.css"))
    .pipe(gulp.dest("./dist/css"));
});

gulp.task("clean", function() {
  return gulp
    .src("./dist/css/bundle.css")
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(strip.text())
    .pipe(gulp.dest("./dist/css"));
});

// gulp.task('clean1', function() {
//   return gulp.src('./dist/css/main.css')
//               .pipe(cleanCSS({compatibility: 'ie8'}))
//               .pipe(strip.text())
//               .pipe(gulp.dest('./dist/css'))
// })
// gulp.task('clean2', function() {
//   return gulp.src('./dist/css/home.css')
//               .pipe(cleanCSS({compatibility: 'ie8'}))
//               .pipe(strip.text())
//               .pipe(gulp.dest('./dist/css'))
// })
// gulp.task('clean2', function() {
//   return gulp.src('./dist/css/keyframes.css')
//               .pipe(cleanCSS({compatibility: 'ie8'}))
//               .pipe(strip.text())
//               .pipe(gulp.dest('./dist/css'))
// })

gulp.task("list", function() {
  exec("node ./list.js", function(err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
  });
});

// gulp.task('css:watch', ['css'], function() {
//   gulp.watch('app/styles/**/*.scss', ['css'])
// })

// gulp.task('moveAssets', function() {
//   return gulp.src('./app/assets/**/*')
//              .pipe(gulp.dest('./dist/assets'))
// })

// gulp.task('build:revAssets', ['css', 'moveAssets'], function() {
//   var rev = new $.revAll()
//   return gulp.src('./dist/**/*')
//              .pipe(rev.revision())
//              .pipe(gulp.dest('./dist/public'))
//              .pipe(rev.manifestFile())
//              .pipe(gulp.dest('./dist'))
// })

// gulp.task('build:cpServer', function() {
//   return gulp.src('./app/**/*.{js,ejs}')
//              .pipe(gulp.dest('./dist/server-build'))
// })
// gulp.task('build:revServer', ['build:cpServer'], function() {
//   var manifest = gulp.src('./dist/rev-manifest.json')
//   return gulp.src('./dist/server-build/{components,containers}/**/*')
//              .pipe($.revReplace({ manifest: manifest }))
//              .pipe(gulp.dest('./dist/server-build'))
// })

gulp.task("build", function() {
  runSequence("merge", "clean", "list");
});
